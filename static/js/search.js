var fuseOptions = {
  shouldSort: true,
  includeMatches: true,
  includeScore: true,
  threshold: 0.3,
  tokenize: true,
  ignoreLocation: true,
  //distance: 1000,
  //distance: 100,
  //maxPatternLength: 32,
  minMatchCharLength: 5,
  keys: [
    {name:"author",weight:0.7},
    {name:"author1",weight:0.7},
    {name:"author2",weight:0.7},
    {name:"author3",weight:0.7},
    {name:"title",weight:0.7},
    {name:"contents",weight:1},
  ]
};

const maxScore = 0.7;
const summaryInclude=250;

var searchQuery = param("s");
if(searchQuery){
  $("#search-query").val(searchQuery);
  executeSearch(searchQuery);
}else {
  $('#search-results').append("<p>Please enter a word or phrase above</p>");
}


function executeSearch(searchQuery){
  $.getJSON("/en/index.json", function(data) {
    var pages = data;
    var fuse = new Fuse(pages, fuseOptions);
    var result = fuse.search(searchQuery);
    //console.log({"matches": result});
    if(result.length > 0){ 
      populateResults(result);
    }else{
      $('#search-results').append("<p>No matches found</p>");
    }
  });
}

function populateResults(result){
  window._s = [];
  $.each(result, function(key,value){
    if (value.score >= maxScore) {
        return;
    }
    if (window._s.includes(value.item.permalink)) {
        return;
    } else {
        window._s.push(value.item.permalink);
    }
    var content = value.item.contents;
    var author = "";
    if (value.item.author != null) {
        author += value.item.author
    }
    if (value.item.author1 != null) {
        if (author != "") author += ", "
        author += value.item.author1
    }
    if (value.item.author2 != null) {
        if (author != "") author += ", "
        author += value.item.author2
    }
    if (value.item.author3 != null) {
        if (author != "") author += ", "
        author += value.item.author3
    }
    var snippet = "";
    var snippetHighlights=[];
    var tags =[];

    var section = "";
    if (value.item.section != null) {
        section = value.item.section
    }
    console.debug("section: " + section)

    var issummary = false;
    if (value.item.issummary != null) {
        issummary = value.item.issummary
    }

    var islicense = false;
    if (value.item.islicense != null) {
        islicense= value.item.islicense
    }

    var isresources = false;
    if (value.item.isresources != null) {
        isresources = value.item.isresources
    }

    if(fuseOptions.tokenize){
      snippetHighlights.push(searchQuery);
    } 

    if(snippet.length < 1){
      snippet += content.substring(0,summaryInclude*2);
    }
    snippet = snippet.trim();

    //pull template from hugo template definition
    var templateDefinition = $('#search-result-template').html();

    let lastWordIndex = snippet.lastIndexOf(" ");
    snippet = snippet.substring(0, lastWordIndex);
    if (snippet.startsWith("Introduction")) {
        snippet = snippet.slice("Introduction".length, snippet.length);
    }
    if (snippet.startsWith("Why this report? Who are we?")) {
        snippet = snippet.slice("Why this report? Who are we?".length, snippet.lenght);
    }
    snippet = snippet + "…"
    if (snippet.startsWith(".youtube")) {
        snippet = "[embedded video]";
    }

    //replace values
    var output = render(templateDefinition,{
        key:key,author:author,
        tags:value.item.tags,
        summary:snippet,
        title:value.item.title,
        link:value.item.permalink,
        tags:value.item.tags,
        categories:value.item.categories,
        snippet:snippet,
        isnumbers: section == "numbers",
        isstories: section == "stories",
        isengage: section == "engage",
        isteam: section == "about",
        issummary: issummary,
        islicense: islicense,
        isresources: isresources
    });

    $('#search-results').append(output);

    $.each(snippetHighlights,function(snipkey,snipvalue){
      $("#summary-"+key).mark(snipvalue);
    });

  });
}

function param(name) {
    return decodeURIComponent((location.search.split(name + '=')[1] || '').split('&')[0]).replace(/\+/g, ' ');
}

function render(templateString, data) {
  var conditionalMatches,conditionalPattern,copy;
  conditionalPattern = /\$\{\s*isset ([a-zA-Z]*) \s*\}(.*)\$\{\s*end\s*}/g;
  //since loop below depends on re.lastIndex, we use a copy to capture any manipulations whilst inside the loop
  copy = templateString;
  while ((conditionalMatches = conditionalPattern.exec(templateString)) !== null) {
    if(data[conditionalMatches[1]]){
      //valid key, remove conditionals, leave contents.
      copy = copy.replace(conditionalMatches[0],conditionalMatches[2]);
    }else{
      //not valid, remove entire section
      copy = copy.replace(conditionalMatches[0],'');
    }
  }
  templateString = copy;
  //now any conditionals removed we can do simple substitution
  var key, find, re;
  for (key in data) {
    find = '\\$\\{\\s*' + key + '\\s*\\}';
    re = new RegExp(find, 'g');
    templateString = templateString.replace(re, data[key]);
  }
  return templateString;
}
