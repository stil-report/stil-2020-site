The content for this site is licensed under different licenses.

Please visit [Licenses and Citations](https://internetlanguages.org/en/license/) for an up-to-date review of the different licenses under which each material is published.


License and Citations
=====================

We balance openness of content, data, and design with the dignity and security of marginalized communities. We want the State of the Internet’s Languages report to be shared easily and freely, because we believe this work is foundational to a multilingual decolonized internet and digital technologies. At the same time, because this report includes work from and with marginalized communities who have historically seen their knowledge exploited by others, we are respectful of all that they generously share with us and the world.

We are committed to licensing the content, including art and design on this site with the open license: CC NC-SA 4.0. This means that unless otherwise specified, information and content on this site can be freely shared, changed or reused (but you cannot use it for profit or any commercial reason). You must acknowledge Whose Knowledge?, Oxford Internet Institute, Centre for Internet and Society, and the other creators of this content as you do so, whether communities, organizations, or people. You also must distribute your new work with the same license.

We are licensing the data on this site with the ODbL open license. This means that you can use, create, and adapt from the database that has been created for this report. You must acknowledge Martin Dittus, Mark Graham and Oxford Internet Institute, as well as the State of the Internet’s Languages report, as you do so, and you must also distribute your new work with the same license.

We are also committed to licensing the design code for this website with an MIT license, which means that you can re-use, change and modify the code for any purpose, so long as you include the original copy of the MIT license when you distribute your new work.

We suggest this format to cite us:

Whose Knowledge?, et al. “The State of the Internet’s Languages Report”, a collaboratively researched and documented initiative, Date, Time, internetlanguages.org.

We are grateful to Sara Ahmed, P. Gabrielle Foreman, Jessica Marie Johnson and Kelly Foster for their leadership in just and radical citation practices. Following them, we ask you to enjoy sharing this report in excerpts or in its entirety, but not to forget to cite it. This applies to the report as well as to any social media mentions about it. Please tag us or let us know if you share it, so we can track its usefulness and impact (@whoseknowledge). If you have questions about the licenses or citations, please do reach out to us.

