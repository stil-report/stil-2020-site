AboutTitle: ""
About: "# Welcome to the first ever State of the Internet’s Languages report!\n

The internet and its different digital spaces provide one of the most critical infrastructures of knowledge, communication, and action today. Yet with over 7000 languages in the world (including spoken and signed languages), **how many of these languages can we fully experience online?**\n

We are a collaborative of three organizations — Whose Knowledge?, the Oxford Internet Institute, and the Centre for Internet and Society (India) — as well as a host of individuals and communities across the world, trying to answer this question and offer different insights, experiences, and analyses on the state of languages on the internet.\n

This report maps some of the ways in which languages are represented online, raises awareness about the need to make the internet more multilingual, and advances an agenda for action. We welcome multiple generations and communities of people to join us in our work, and hope that this initiative will be a starting point for further research and action."

SummaryTitle: "Summary Report"
Summary: "Our report is what we’re calling “digital first”: that is, the best way to read, listen, and learn from it is through this website, because the report has many layers and levels. We are bringing together Numbers, which provide a statistical perspective and give us an overview of the status of languages online, and Stories, through which we learn more deeply about how easy or difficult it is for people to use the internet in their own languages.\n

In this summary, we explore the geographical and digital knowledge inequities, as well as matters of language support and content online. We outline what we have learned about the multilingual internet and how we can do better, by looking closely at the power dynamics in place and the ways to move forward."

StoriesTitle: "Stories"
Stories: "This section gives us a deeper understanding of how different people and communities around the world experience the internet in their own languages. How meaningful and equitable is our access to the web? Are we able to create and produce public online knowledge to the same extent we consume it?\n

We invited these stories in written and spoken forms, which include textual essays, audio and video interviews. In this section, you will find contributions about Indigenous languages like Chindali, Cree, Ojibway, Mapuzugun, Zapotec, and Arrernte from Africa, the Americas, and Australia. You’ll read contributions about minority languages like Breton, Basque, Sardinian, and Karelian in Europe, as well as regionally and globally dominant languages like Bengali, Indonesian (Bahasa Indonesia) and Sinhala in Asia, and different forms of Arabic across North Africa."

NumbersTitle: "Numbers"
Numbers: "Our Numbers analyze critical language issues in widely-popular digital platforms, apps and devices that we use in our everyday lives. Here, you will find data visualizations and analyzes based on the data from open and publicly available datasets and materials. We analyzed the language support offered by 11 websites, 12 Android apps, and 16 iOS apps. In this analysis, platforms were divided into four main categories: knowledge access, language learning, social media, and private and group messaging. On top of a broader platform survey, we explore two cases in more detail: Google Maps and Wikipedia.\n

The results, as well as methodological constraints and opportunities for further research, are also explored in detail in this section."
