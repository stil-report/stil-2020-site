AboutTitle: ""
About: "# Te damos la bienvenida al primer informe sobre el estado de las lenguas en Internet!\n

Internet y sus diferentes espacios digitales constituyen una de las infraestructuras de conocimiento, comunicación y acción más importantes de la actualidad. Sin embargo, con más de 7000 lenguas en el mundo (en señas y habladas), **¿cuántas de ellas podemos disfrutar plenamente en línea?**\n

Somos una asociación formada por tres organizaciones — Whose Knowledge?, Oxford Internet Institute y Centre for Internet and Society (India) —, así como un lugar de encuentro para personas y comunidades de todo el mundo, que intentan responder a esta pregunta y ofrecer diferentes perspectivas, experiencias y análisis sobre el estado de las lenguas en Internet.\n

Este informe mapea algunas de las formas en que las lenguas están representadas en línea, sensibiliza sobre la necesidad de hacer que Internet sea más multilingüe y propone un agenda para la acción.. Damos la bienvenida a diversas generaciones y comunidades de personas para que se unan a nuestro trabajo, y esperamos que esta iniciativa sea un punto de partida para nuevas investigaciones y acciones."

SummaryTitle: "Resumen del Informe"
Summary: "Nuestro informe es lo que llamamos “digital first”: es decir, la mejor manera de leerlo, escucharlo y aprender de él es a través de este sitio web, porque el informe tiene muchas capas y niveles. Reunimos cifras, que proporcionan una perspectiva estadística y nos dan una visión general de la situación de las lenguas en línea, e historias, a través de las cuales aprendemos en profundidad lo fácil o difícil que es para la gente utilizar Internet en sus propias lenguas.\n

En este resumen, exploramos las desigualdades geográficas y de conocimiento digital, así como las cuestiones relativas al apoyo lingüístico y los contenidos en línea. Esbozamos lo que hemos aprendido sobre la Internet multilingüe y cómo podemos mejorar, examinando de cerca las dinámicas de poder existentes y las estrategias de acción."

StoriesTitle: "Historias" 
Stories: "Esta sección nos permite conocer más a fondo la forma en que diferentes personas y comunidades de todo el mundo experimentan Internet en sus propias lenguas. ¿En qué medida es significativo y equitativo nuestro acceso a la red? ¿Somos capaces de crear y producir conocimiento público en línea en la misma medida en que lo consumimos?\n

Hicimos un llamado a enviar historias en formas escritas y habladas, que incluyen ensayos textuales y entrevistas de audio y video. En esta sección, encontrarás artículos sobre lenguas indígenas como el chindali, el cree, el ojibway, el mapuzugun, el zapoteco y el arrernte de África, América y Australia. También contribuciones sobre lenguas minoritarias como el bretón, el vasco, el sardo y el careliano en Europa, así como sobre lenguas dominantes a nivel regional y mundial como el bengalí, el bahasa y el cingalés en Asia, y diferentes formas de árabe en el norte de África."

NumbersTitle: "Cifras"
Numbers: "Nuestra sección de cifras analiza cuestiones lingüísticas críticas en las más extendidas plataformas digitales y aplicaciones, y en los dispositivos que utilizamos en nuestra vida cotidiana. Aquí encontrarás visualizaciones y análisis basados en conjuntos de datos y materiales abiertos y públicos. Hemos analizado el soporte lingüístico que ofrecen 11 sitios web, 12 aplicaciones de Android y 16 aplicaciones de iOS. En este análisis, las plataformas se dividieron en cuatro categorías principales: acceso al conocimiento, aprendizaje de idiomas, redes sociales y mensajería privada y grupal. Además del análisis general de las plataformas, exploramos dos casos con más detalle: Google Maps y Wikipedia.\n

Los resultados, así como las limitaciones metodológicas y las oportunidades para seguir investigando, también se exploran en detalle en esta sección."
