#!/usr/bin/zsh
lang=$1
newlang=$2
function dolink() {
    for file in *.${lang}.md;
    do
        newn=$(echo $file | cut -d'.' -f1);
        newf="$newn.$newlang.md";
        if [[ -f $newf ]]; then
        else
            echo "creating $newf link";
            ln $file $newf;
        fi;
    done;
}
dolink
