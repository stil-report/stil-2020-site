---
author: Whose Knowledge
title: Licencias y Citas
date: 2019-05-13
toc: false
---

No queremos que el libre acceso al contenido, los datos y el diseño de este informe menoscabe la dignidad y la seguridad de las comunidades desfavorecidas. Queremos que el Informe sobre el Estado de las Lenguas en Internet pueda ser compartido libre y fácilmente, ya que creemos que se trata de una obra fundacional para la articulación de una red y unas tecnologías digitales multilingües y descolonizadas. Al mismo tiempo, debido a que este informe contiene una labor hecha en y desde comunidades desfavorecidas cuyo trabajo ha sido históricamente explotado, queremos respetar todo lo que tan generosamente han compartido con nosotres y con el mundo.  

Por estas razones hemos decidido publicar el **contenido de esta página, incluyendo el arte y el diseño** bajo una **licencia abierta**: [CC NC-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.es). Esto significa que, salvo que se especifique lo contrario, la información y el contenido de esta página puede compartirse, modificarse o reutilizarse libremente (pero no usarse con fines comerciales ni con ánimo de lucro). En todo caso debes reconocer y citar a Whose Knowledge?, Oxford Internet Institute, Centre for Internet and Society y a los demás creadores de este contenido, ya sean comunidades, organizaciones o personas individuales. También debes distribuir cualquier obra creada a partir de este material mediante esta misma licencia.  

Los **datos** usados en esta página están publicados bajo la licencia libre [ODbL](https://opendatacommons.org/licenses/odbl/summary/). Esto quiere decir que puedes usar, adaptar o crear a partir de la base de datos que ha sido creada para este informe. Si lo haces, debes reconocer y citar a Martin Dittus, Mark Graham, el Oxford Internet Institute y el Informe sobre el Estado de las Lenguas en Internet y distribuir tu trabajo resultante bajo esta misma licencia. 

Nos comprometemos también a hacer público el **código del diseño** de esta página web con una licencia [MIT](https://choosealicense.com/licenses/mit/), que implica que puedes reutilizar, cambiar y modificar el código para cualquier propósito, siempre y que incluyas la copia original de la licencia MIT cuando distribuyas tu nuevo trabajo. 

Este es el formato que sugerimos para que nos cites:

_“Whose Knowledge?, et al. “Informe sobre el Estado de las Lenguas en Internet”, una iniciativa de investigación y documentación colaborativa, fecha, hora, internetlanguages.org._

Agradecemos a [Sara Ahmed](https://feministkilljoys.com/2013/09/11/making-feminist-points/), [P. Gabrielle Foreman](https://coloredconventions.org/about/team/), [Jessica Marie Johnson](https://twitter.com/jmjafrx) y [Kelly Foster](https://kellyfoster.co.uk/) por su liderazgo en cuanto a prácticas justas y radicales de citación y referencia. Siguiendo su ejemplo, les animamos a que compartan este informe, ya sean fragmentos o en su totalidad, y a que no se olviden de citarlo. Esto se refiere tanto al informe como a cualquier mención en redes sociales. Por favor, etiquétanos y/o avísanos si lo compartes para poder así seguir su impacto y su utilidad (@whoseknowledge). Si tienes alguna duda en cuanto a licencias o citas no dudes [en contactarnos](mailto:languages@whoseknowledges.org).