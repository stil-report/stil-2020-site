---
author: Whose Knowledge
title: Licences et citations
date: 2019-05-13
toc: false
---

Nous concilions l’ouverture des contenus, des données et de la création avec la dignité et la sécurité des communautés marginalisées. Nous souhaitons que le présent État des lieux des langues d’Internet puisse être partagé facilement et librement, car nous le considérons comme un travail fondamental pour créer un Internet et des technologies numériques multilingues et décolonisés. D’un autre côté, ce rapport comprend des productions par et pour des communautés marginalisées dont les savoirs ont été historiquement exploités par d’autres. **Nous respectons tout ce qu’elles partagent généreusement avec nous et avec le monde entier**.

Nous nous engageons à mettre le **contenu, y compris l’art et le design** de ce site sous licence **Creative Commons** : [CC NC-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/). Cela veut dire que, sauf indication contraire, les informations et le contenu de ce site peuvent être librement partagés, modifiés ou réutilisés (mais vous ne pouvez pas l’utiliser dans un but monétaire ou pour quelque raison commerciale que ce soit). Vous devez mentionner Whose Knowledge?, l’Oxford Internet Institute, le Centre for Internet and Society et les autres créateur·ices de ce contenu (communautés, organisations, personnes). Vous devez également diffuser vos nouvelles productions sous la même licence.

Nous mettons les **données** de ce site sous la licence ouverte [ODbL](https://opendatacommons.org/licenses/odbl/summary/). Cela veut dire que vous pouvez utiliser la base de données créée pour ce rapport, l’adapter et vous en servir pour créer. Dans ce cas, vous devez mentionner Martin Dittus, Mark Graham et l’Oxford Internet Institute, ainsi que l’État des lieux des langues d’Internet. Vous devez également diffuser votre travail sous la même licence. 

Par ailleurs, nous avons choisi de mettre le **code source du design** de ce site Web sous la licence [MIT](https://choosealicense.com/licenses/mit/), ce qui veut dire que vous pouvez le réutiliser, le changer et le modifier dans n’importe quel but, du moment que vous incluez la copie originale de la licence MIT quand vous diffusez vos nouvelles créations. 

Nous suggérons ce format de citation :

 _« Whose Knowledge?, et al. “État des lieux des langues d’Internet”, un projet collaboratif de recherche et de documentation, Date, Heure, internetlanguages.org. »_

Notre reconnaissance va à [Sara Ahmed](https://feministkilljoys.com/2013/09/11/making-feminist-points/), [P. Gabrielle Foreman](https://coloredconventions.org/about/team/), [Jessica Marie Johnson](https://twitter.com/jmjafrx) et [Kelly Foster](https://kellyfoster.co.uk/) qui sont des modèles en termes de pratiques de citations justes et radicales. Nous suivons leur exemple : n’hésitez pas à partager des extraits de ce rapport, voire le rapport entier, mais n’oubliez pas d’en citer la source. Cette demande s’applique au rapport ainsi qu’à toute mention du rapport sur les réseaux sociaux. Nous vous demandons de nous identifier ou de nous faire savoir si vous le partagez, pour que nous puissions évaluer son utilité et son impact (@whoseknowledge). Si vous avez des questions sur les licences ou les citations, n’hésitez pas à [nous contacter](mailto:languages@whoseknowledges.org).