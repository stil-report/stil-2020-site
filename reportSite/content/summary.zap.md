---
Author1: Anasuya Sengupta
Author2: Abbey Ripstra
Author3: Adele Vrana
Title: 'Ga dzogto chop chone dill'
IsSummary: true
Date: 2020-01-15
hasboxes: true
translationKey: 
Imageauthor1: Anasuya_Sengupta.webp
Imageauthor2: Abbey_Ripstra.webp
Imageauthor3: Adele_Vrana.webp
haspdf: true
PDFsummary: /media/pdf-summary/ZAP-STIL-SummaryReport.pdf
hasaudio: true
Audio1: https://archive.org/download/STIL-Summary-ZAP/01-Zapotec.mp3
Audio2: https://archive.org/download/STIL-Summary-ZAP/02-Zapotec.mp3
Audio3: https://archive.org/download/STIL-Summary-ZAP/03-Zapotec.mp3
Audio4: https://archive.org/download/STIL-Summary-ZAP/04-Zapotec.mp3
Audio5: https://archive.org/download/STIL-Summary-ZAP/05-Zapotec.mp3
Audio6: https://archive.org/download/STIL-Summary-ZAP/06-Zapotec.mp3
Audio7: https://archive.org/download/STIL-Summary-ZAP/07-Zapotec.mp3
Audio8: https://archive.org/download/STIL-Summary-ZAP/08-Zapotec.mp3

---

## ¿Nhu xa nhak nheto? ¿Bixchen dzogto ka da ki?

Dan nhe'ake “diccionario” llwen dill, dill ke llo ka llak´lhelhen llio lldello dill llenlello nezlhe benhe yublhe. Lhaoze da bkwan xuxtaollo xtill llo ka, nha zag sden benhe ka nao llío. Bayate llunlello llin xtishllo'ka, ka llayunllo xbab, kat lluello dill, kat bi dzenayllo... kelhe tuz llío llun lhenllo llin xtishllo´ka, ke yoyte benhe nechgllo. Xtishllo ka shlhue´aken gaklhe nak nha zu kuez benhe nache. Leskakse shlhab´aken dill da ba guke nha llgo´aken ltipllo gun biello lueshgllo. Bixha lue, ¿Bi dill nheo? ¿Bi dill ll´nhelho yelh?¿Nlla dillen llunho xbab kat dao lhao xchino nha kat su'u yo'ko? ¿Wakuell ka dzenayo llagnielho bin llulh´ake?

Yoyze dill´ka de lhao yell liun nakte ké gakle gunhenllo llin enchanha wak gumbiello nha guchalg nhenllo benhe yublhe. Guxken xtill´llo kan nezello gaklhe zukuez benhe lla lhao yell liun, le gan´aken nha ntil chay llio nha dzetg yoyte da ka llunllo. **Baya kuen dill de, (de dill da nhello nha da llchuk yaj lhaollo) da dzogllo nha da llunllo chikia nhen ya wakuell cha bill lhe da yublhe**, cha ket [dxaollo o cha dzillo bllag](https://pages.ucsd.edu/~rose/Whistled%20and%20drum%20languages.pdf)! **dill ka nhak aken da blhao** lhegan´akenha llaklhenhe enchanha wak gulhelhao da ka llunllo xbab, llaglello o cha bi da llenlello nezlhello.

Bayun xbab bí dill ka nheo, llunho xbab, llnelho yelhe, nha dzogo. ¿Bale dill´kí llak llunhenho llin lue dan numbiello internet? ¿Bi ba blhelho nha bi ba guk ko kat llunhenho llin dill´kí ? ¿Llak zdogo lue nha? ¿Yallg wachao gaklhe zdogo encha wak gulhepo bi dan llenlho lue dan nhe internet? ¿Kat bi llayilhjo lue nha, niogen dillen llunhenho llin? ¿Yasg guzedo dill yublhe enchana wak gunenho llin dan nhe internet? Cha lhue ballío “ka” yoyte da kí bnabyull´ake lhue, llayal nezlho tu chopze benhe lla lhao yell luin llallele da ka llayilhg´ake lhue internet, nha niogten xtill´ake, yulhuls benhka llallel daki neake chajnieaklhe dill Inglés.

Ba yate da llenlhello nezello yoten lhue dan nhe Internet. Ka du guyon chí gayoa dill shaglla duxente yell líu, (dill da nhea´ke nha dill da llchab nha´ake o llunhenhen llín lhao´ake nha xín´ake) **¿Balhe ke dill kí wak gunhenllo llín lhue dan nhe Interntet? ¿Gaklhe gunzen nha chenhak bayate dill de yell liu yo lhue nha?**

Nallen dzogto danhí, enchanha wallito ka da ka bnabyullto lhue da nell. Ganhí [llaklhen]({{<trurl>}}/about{{</trurl>}}) nheto chon yoo wazede da nzi lha ¿Nhu ke nhak yel nezen? Xlatge Internet gan nhe Oxford, nha xlatge Internet nha yoy yell Internet (India). Btil luellgto nhen benhe llaksedlhe xtishllo´ka enchanha bayunto xbab gaklhe llunnhen´ake llin dill´ka lue dan nhe Internet, llbexto kwekto to Internet ga zukuez yoyte dill de yell liun.

Ganhi llenlheto wayag dill chone kuen da zed dé ke xtill´llo ka.

* **Da nhi shluelhen gakle llitj xtishllo´ka lue dan nhe internet**: Llalhal dzedto bi dill llunhen´ake llin lhue dan nhe internet nha gaklhe llunen´aken llin. Enchnha wak walhabto dill ka dhe nha balen lllunhen´llo llín (da xen llaklhelhe dan nello internet enchanha wallelh daki) leskakse shnab yullto (benhe llunhen llín xtill´aken lhue dan nhe Internet).
* **De gunllo yex benhe nechg guyllo enchanha gunhen´ake llín xtill´aken lue internet**: Bnhí nhak llín nhí, lhaose leskakse llaglla benhe gaklhen enchanha zaá llín nhi.
* **Llayal yog bi llín ka gak**: Nha gaklhe guda niá-nha' benheka neze ke da kí, enchanha lhue da ne internet gunenllo llín yoyte dill ka sagde lhao yell líun.

### ¿Bi xan llayag dill ganhí?

¡Bi na wayull llín nhí,nha nhaoton, nha llalhal dzogton!

Benhe tu lhaz, yoo wased´ake nha yell´ka llalhal llun´ake llín nhen xtill´llo ka. Llwiato nha nhaoto xchín´aken, le´aken llgo ltipto enchana dzogto ka da ki, lhaoze bi llenlheto gunton bnhi enchanha nhuteze wak gulh len. Lakse bi numbieto yoyte benhe ka nhun xchine dill ka yo lu internet llwal´aktue gan ll´lhabto [benhe llaklhelhe]({{<trurl>}}/resources{{</trurl>}}) nha [llueto yel lluken](#toc_6_H2) ke´ake.

Nha yashgll wayilgto nha gunabyullto da gak gunhento llín gan [llalhabto]({{<trurl>}}/numbers{{</trurl>}}) dill ka. Cha lhue dzedo xtillo´ka, lee waweto gaklhenho nheto enchanha kollo ltipe nha wadshiwllo llín nhí.

Llda bayto dzogto danhí enchanha nuteze wak gul len. Bi llenleto llagtilhe nhu dill da nak bnhi kat gul lhen, leskase llenleto nuteze wak gunhen nheto llín. Kakse llenlheto wayogen biteze dill dé (cha lue llazogo dill´ake: [bayilg nheto]({{<trurl>}}/engage{{</trurl>}})!). Kanake ba byoj llín nhí biteze dill, bayilhg netho enchnha wak gulon xtillon nha ka bi llagtilho nha chagnielhon.

Llbexto dan dzogto nhí gaklhelhen bill lhe llín yublhe ga wayag dill ke xtillo´ka, kanhake llatup llachayen llín da ba dekse.


### ¿Nuxa nhak nheto nha bixchen btil niá-nhato enchana bzogto llín nhí?

Chon ton btil niá nhato: dan nzi nha Xlatge Intenet nha yoy yell, nha dan lhe xlatge Internet gan nhe Oxford, ¿Nhu ke nhak yel nezen? Yoyzto llakzedlhe gaklhe llitg dan nhe interntet lhao llín ke benhe ka dzed gaklhe zukuez benhe nache, nha bi da zed de lhao yell liu.

Ba guk chop chone yiz nauto lla yunto xbab nha llanizlheto kelhe yoy benhe de internet´ke: ¿Nhu llgo xneze yoy da ka shlhelhello lue Internet? Ba gukbelheto bi bí shlhelteklhello lhue internet da niog xtishllo. Nallen llnabyullto ¿Ba nían dill ke llo ka yo lue Internet? Lhaoze bi bÍ ballelklheto, llaklhallto da kí dzogto ganhí gaklhelhen enchanha wadup yoyte dill´ka de lue dan nhe Internet.

_Gaklhe bio xneze llín nhí kan bzulhao yillwen lhe Covid-19_: Bsulhaoto llunto llín nhi yiz galga yua tu galghe, ka za zulhao yillwen numbiello ka Covid-19, lhaoze kan bnabyullto benhe nha bzogton, llalhele yillwé nhí da ba bchá gaklhe zu kuezllo. Yoyz benhe guklhelhe biog llín nhi blhon l´chixe bi da llun´ake, nhallén kane bayullen. Nhén yill wé nhi banhizlheto kan ndil´llo shallo lhao yell líu nhí nha da blhaon guchalglhenllo benhe yublhe gaklhe llunllo xbab, nallen yallg latg lue interneten ga gunhenllo llín xtishllo´ká.

## Gaklhe wak gul llo llín nhí

Dan nioge ganhí nhaken da gun llín lhue dan numbiello Internet, ganha wak ghul´llon nha guzenayllon lhaolhas gunhenzo llín nhu yiche. Llín nhi [llalhaben]({{<trurl>}}/numbers{{</trurl>}}) nha [lluen dill]({{<trurl>}}/stories/{{</trurl>}}). Gaklhe llitg dill ka lue Internet, konhen lenha llnezeto bí da zed llat kat llunhen ben ka llín xtillake lhue nhí. Kat llchalg nhen benhe´ka netho gaklhe llunhena´ke llín xtill´aken da xen dzagnielhen nheto, kan llak llakbeto zu ket bnhi llakaklhe gunhenhake llín xtill gaken lhue internet, nha zu ket bi nhaken ka. Llalhabto dill ka enchanha llaklhelhen wanizlheto bi da zed de, nha bi llin gak enchnha wak chaá bayate dill lhue Internet.

Kí ngoto xneze llín nhí:

* Zatk basogto yoyte da ka nza llín nhi nha gaklhe guken (¡Dan llulho nazteke!)
* Natelle [llalhabto]({{<trurl>}}/numbers{{</trurl>}}) bi da zed de ke dill´ka lue internet. Benhe ka numbieto lla gan nhé Xlatge Internet gan nhe Oxford blho´ake xneze llín nhi, nha leskakse da zan da llagdilho ganhí. Wakbekzelho ke to da balleleton llanhizlheto ganí. Leskake bnhi nhaken guzedto yoyte dill dé, kanhake bi nezeto balhe benhe llunhen´ake llín dill kí kanhake kuen kuen ze llcha´aken.
* [Dill shlhabe ka ba guke]({{<trurl>}}/stories/{{</trurl>}}) Dill ki llaklhelhen llumbiello gaklhe llunhen llín benhe su gayoblhe xtill´ake lue Internet, kanhake cha ket bi llayilgake bi lla llel´akle ka dillen nhe´aken. Nheto [bento yex benki](https://whoseknowledge.org/initiatives/callforcontributions/) enchnha wak gulho nha guzenayo bi da ba guk ke ake chak llunhenake llin xtillake. Bi ka numbieto gan nhe dill xtilhe Centro Para Internet y la Sociedad guklhelh´akbe bayun´akbe tuze dill ka gaklhen llak ke dill ka llunhenllo llín lhue internet duxente yell liu. Ben zan benhe ne dill wlhall´ake guklhel nheto, benhe lla duxente yell líu.

¡Da blhao gulhen nezello llín nhi, niogen da zan dill wlhall´ake, nha llbexto gul'lhe cha guzenaylhe bayate dill dé!

​​Leskakse bchixto bxen ake da benh ben´ka llunhen nheto llín. Bxen ki zanizlhen gaklhe zukuez xtishllo´ka.

## Llá llaa da zan dill lhue dan nhe Internet?

Dan nhe Internet bi nha gaken xtlaje (niezchlhaz, nha bi gaksen zete) ga wa dup da zan dill. Nheto llenlheto nezeto bix chen nha [llalhabto]({{<trurl>}}/numbers{{</trurl>}}) [llatupto xtill benh´ake]({{<trurl>}}/stories/{{</trurl>}}) galhe llunhen´ake llín dill ke´ake ka lhue internet duxente yell líu. Ganhi waxuxgto xchin benhe ka blho ltipto encha bzogto danhí, nha llunto lhue yex enchanha gulo youlhulte dillki.

Danesh bayilgto xlatge ga llunhen benhe llín xtill´ake lhue Internet duxente yell llíu. Guyiato bí da ka dzelake nha sh´lep´ake, gate dzel´aken nha bi dill llunhen´ake llín. Nhatelle guyiato ga sh´lepake bi ke´ake, ga llchalg nhen´ake benhe yublhe nha bi dill´ake lluen latg gunhen´ake llín. Leskase guyiato gaklhe llitg da ka nzí lha dill xtilhe Google Maps nha Wikipedia kanhake nhak´aken xlatge ga lla dup da zan dill.

Leskakse shlhueto xchín ben´ka llunhenhe llín xtill´ake lhue Internet kat bi ll´lep´ake. Dan gadilheto, benhe ka guklhen nheto enchanha biog da ní nhe´ake baya dill wlhall, lhaoze cha llen´aklhe gulhep´ake biteze nha guzog´ake de gunhen´ake llín dill yuble ka nhake dill xtilhe.

### ​​Kelhe lebze wak gunhenllo llín Internet cha nello dill

{{< summary-quote
    text="Bi de latg ke xtillo´ka lhue da nhe Internet."
    author="Ana Alonso"
    link="/zap/stories/dill-wlhall-on-the-web/"
>}}

{{< summary-quote
    text="Kanhake bi de latg ke xtillo´ka llayakbello llun´ake kaze xtillo´ka nha llue´aklle ltipe dill xtilh´ka. Kan llak ke dillen lhe' Mapuzugun."
    author="Kimeltuwe project"
    link="/zap/stories/use-of-our-ancestral-language/"
>}}

Nheto llalhabto balhe benhe llunhnen llín Internet duxente yell líu, [llde wyun lhao tugayua benhe lla yell líu](https://www.internetworldstats.com/stats.htm) llak yitgnhen len lhue da ka numbiello celular. Benhe ka llunhenll len llín, lla´ake gan numbiello Zu Cawí ke yell líu: Asia, África, América Latina, Caribe, nhen Islas del Pacifico, lhaose ke lhe lhebse llak llunhnen´aken llín, nha bi llak gulepllo ka da ka nezesho, shlhello, dzenayllo xtishllo. Llak llwiallo nha shletgllo dill xtilhe lue dan nhe internet. Ga nhí llnaba lhe, ¿Llak bi ll´lheplhe lue nha, na niog xtill´lhe nha da shlhuen lhe gakle zukuezlhe?

Benhe ka le Martin and Mark balhab´ake benhe ka llun nhen llín Internet, gadí´aklhe zag llall latg ga bí dllín intenet nha ga bí llunhen´ak len llín. Kanhake llío llalo gan nhe Zu Cawí ke yell líu bi bi ll´lheptkllo ka da llak lhao lhallo, llak llwiallo nha dzenayllo bi da llak gayublhe lhaoze Internet bi llunt ken latg gunhenllo llín xtillo ka. Yoyze benhe llunhen llín dan zi lha Wikipedia, nha dan lhe Github (ga llachaw´ake xchinlaze da ka nhe´ake dill xtile aplicaciones) nha dan le TOR (ga llagilg´ake biteze lhue internet), lla´aken gan nhe Europa nha Stad.

{{< summary-side-fig-fancybox
    src_small="/media/summary/STIL-Internet-regions-500px.webp"
    src_full="/media/summary/STIL-Internet-regions-1000px.webp"
    alt="Da llaxollg gaklhe llunen´ake llín Internet benhe lla yell líu. (Da bletg´ake gan nhe: World Bank galhg ga yua tu galgh, Wikimedia Foundation galhg ga yua tu galgh, Wikipedia galhg ga yua chínchone, Github galhg ga yua galg, Tor galhg ga yua tu galgh)"
    caption="Da llaxollg gaklhe llunen´ake llín Internet benhe lla yell líu. (Da bletg´ake gan nhe: World Bank galhg ga yua tu galgh, Wikimedia Foundation galhg ga yua tu galgh, Wikipedia galhg ga yua chínchone, Github galhg ga yua galg, Tor galhg ga yua tu galgh)"
    PDFlink="/media/pdf/STIL-Internet-regions.pdf"
>}}

¿Bixa llenlhe nhen kelhe lhebze llunhenllo llín dan lhe Internet lhao yell líunhi? Youlhull´llo wak gunhenhllon llín xtishllo? ¿Wak bi gulhepllo da niog xtillo?

Kan [shlhuelhe](https://www.internetworldstats.com/stats7.htm) llío, guyon chínollo lhao tugayoa benhe lla yell líu llitgnhen´ake dan nhe internet, lhaoze zelhaoze chí kue ze dill ka de llhunhenhe ake llín, tu ze dill xtill´ka bchuch lan dill wlhall ke llo ka ll´lhue aklhe ganha. Dill ka numbiello ka (Inglés, Francés, Alemán, Portugués, Español) Leskase llag lla dill ll´nebiá lhao dill nha bi nhe´akte, dill kí nhak´aken (Chino, Arábico nha Ruso…). Yiz galhg ga yua chínchone, za´aklhe gan nhe China ga nha llunhen´akse llín Internet, bi de galall´llo dillen lhe Chino kelhe tuz dillen, [dill da lla tup da zan kuen dill](https://www.oxfordhandbooks.com/view/10.1093/oxfordhb/9780199856336.001.0001/oxfordhb-9780199856336-e-1).

Ka ze yoy dill ka zukuez yell lío gulgh gaken gan nhe Europa, lhaoze llagllash zan dill yell yublhe. Kanhake ka du guyon chí gayoa dill lla yell´liu [Ka du chua gayoa gulg´aken gan nhe Asia nha Africa](https://www.ethnologue.com/guides/how-many-languages#population) (bdetelhe galga yua galg to to yell kí), nha gan nhe Islas del Pacífico nha Americas bdetelhe chí gayua dill dé. Da ka nhe dill xtilh Papua Nueva Ginea nha Indonesia [nakaken xtlatje ga dell dill](https://www.ethnologue.com/guides/countries-most-languages).

{{< summary-side-fig-fancybox
    src_small="/media/summary/living_languages_vs_population__pies_500px.webp"
    src_full="/media/summary/living_languages_vs_population__pies_1000px.webp"
    alt="Balhe dill nha balhe benhen nhé lea´ken duxente yell´líu."
    caption="Balhe dill nha balhe benhen nhé lea´ken duxente yell´líu."
    source="Ethnologue"
    link="https://www.ethnologue.com/guides/continents-most-indigenous-languages"
    PDFlink="/media/pdf/living_languages_vs_population__pies.pdf"
>}}

Da zan dill da nhe´ake yell ka lla gan nhe Zu Cawí Asía (Hindi, Bengali nha Urdu) llelhao´aken [ze´aken xlatje chí lhao dill da de yell líu](https://www.ethnologue.com/guides/ethnologue200) Ben zan nhe le´aken kanhaken nha ken xtill´ake nesh lhaoze bi llak llunhen aken llín lhue Internet. Benhen le [Ishan]({{<trurl>}}/stories/marginality-within-marginality{{</trurl>}}), nhe dill wlhall ke dan le Bengali, nheé ke lhakse wak gunhenllo llín xtishllon lhue internet cha bí llénlhello wayilgo bí nhake bie cha wallelelon, kanhake bea ke benhe nhak nllalhe nha bea da niog ka zú kuez nhole nha benhe bío, ke da kín nhun xchin benhen le Ishan. Leskakse llak gan nhe Sudeste Asíatico, benhen le [Paska]({{<trurl>}}/stories/flickering-hope{{</trurl>}}) ballel´lhe lhebze da zed ka da ká gadilhe benhen le Ishan lhalle.

Kanhake guyonchí gayua dill dhe yell líu, kelhe yoyen [zelhaoze chua gayuan](https://www.ethnologue.com/enterprise-faq/how-many-languages-world-are-unwritten-0) llyoj. Lhaoze kelhe benhe lhao yellkan bayilg´ake gaklhe yog xtill´aken, benhe xtilh´kan bzed´ake guzog´aken enchanha wak kwiá chaa´ake benhe lhao yell ka. Kelhe dan llak yiogen nha llenlen nhen yoyte benhe llagnielhe nha llunhenlen llín, yoyse dill wlhall ka llde´aken kuen kuense bidao ka za nao kat nelhelh´ake ben o cha kat llue´ake dill nha chab nhiá nha´ake. Zagdekze dill da llyoj lhaoze nhao´akse dzogake dill ka llnebía. Yiz galga yua chop dan nhe Google blhaben [tuga yua chilloa millón yich bzog´ake](https://www.pcworld.com/article/508405/google_129_million_different_books_have_been_published.html), ka du tapayua tap lhalge yich bzog´aken biteze dill´ake. Yoyze yich da ba byog [da llaa yell líu](https://www.theatlantic.com/science/archive/2015/08/english-universal-language-science-research/400919/) [nha gaklhe zu kuez benhe](https://www.tandfonline.com/doi/abs/10.1080/01639269.2014.904693) niogen dill Inglés. Yich da nyoj lhay nhaa ganhen [baba zog akshe ] dazan dill ([https://en.wikipedia.org/wiki/Bible_translations](https://en.wikipedia.org/wiki/Bible_translations)) de yell líu (ka du chíllua gayua dill), leskakse [da zan lhaz baba yog](https://www.ohchr.org/EN/UDHR/Pages/Introduction.aspx) yichen gan llía [bea ke benhe nache](https://en.wikipedia.org/wiki/Universal_Declaration_of_Human_Rights) (ka du chiyon gayua dill de yell líu).

¿Bix chen llakzed lheto danhí? Ganhi nheto bix chén. Kat bín shlhep´ake lhue internet´en, ba nhezlhen bi dill´ka llnebyía yell líun nha dill kín walha kethen bin wayilg´llo, nha bi lluén latje ke dill ka yelha. Bixá dill ka bi niog, dill ka gan llchab tak´ake o cha ga llunhen´ake llín bi te ze chí´ake, bi bí latge dé ga guzog´aken lue Internet´en.

Ka tsagnielhe [Ana]({{<trurl>}}/stories/dill-wlhall-on-the-web{{</trurl>}}) lliu “bi nhak interneten xlatg ga cha dill wlhall´ka, da ka na né benhe lhao yell nha da llde´ake lo lluaa´ake”. Bi llallelhello dill kí gan llchabe tak´ake o cha ga dxawake. Ka benhento dill [Joel nha Caddie]({{<trurl>}}/stories/signs-across-generations{{</trurl>}}) gull´ake nheto gaklhe llak gan nhe Australia ga bén´ake bxen dao da guzel´llo kat nhu dzogzo, bayilg lalla ke ben´ake lhao bxen dao kí kan nhak benhe lla ganha, nha bzog´aktd bi llenlhen nhe. Ki nhe [Emna]({{<trurl>}}/stories/write-in-another-language{{</trurl>}}) ke dill dan lhe Tunisia, nha yoyze dill ka nhe benhe lla yell líu, “kat llenhello waxue´aken kelhe tuz dill ka llak llyogen de welhaullo, nhente dill ka bi de gaklhe guzogllon de gakzedlhello”.

Yoyze xchin laze Interneten waklhelhen enchanha wlhello bale dill ka de yell líun nha gaklhe zukuez´aken. Leskase waklhelhen waxue dill´ka ba llníte. [bdetelhe chua lhao tugayua dill de yell líu](https://www.endangeredlanguages.com/about/). Yoy beo [Chop dill wlhall](https://news.un.org/en/story/2019/12/1053711) nha gaklhe zu kuez benhe nhé len, llalhan.

¿Bix chen da blahon cho dill kí lhue Internet?

Xchín [Claudia]({{<trurl>}}/stories/decolonizing-minority-language{{</trurl>}}) llaxoxgen gaklhe ndil dill ka nha Internet´en, gaklhe gunhenllo llín xtill llo ka luenha nha gaklhe llitg´en. Benhen lhe Claudia gadilhe “dill da llnebya” (llag dilhellon gan nhe Europa) dill kin bchuchg lhan xtillo wlhall´ka. Legan´aken nha llalha lue interneten nha nhap´aken dallan xchín laze enchanha gunhen benhe len llín. Bi llak ke dill ka yela ka´, kanhake lue computadora nha celular ka´ bnhi llak wzogllo xtishllo´ka, bi nzan dill nha bxen enchna gunhenllo llín xtillo´ka, nha leskakse bi llayumbielhen cha nello dill wlhall. Yoyze benhe nhun xchinhe Internet lluelallaklle llunake llín dill ka llnebia kanhake legann´ake nha llkuan mesh, nha bi llakzed´aktklhe gaklhe gak guxe´ake ga zukuell dill wlhall lla yell líu.

Dan llakzedlhello nhán llgó ltipllo enchanha wallelhello gaklhe gunllo x´latge ga zukuell dill wlhall lla yell líu, za zá yeto kuen ga wanizlhello da zeden [zazá](#toc_4_H2).

### Dill ka llun llín ga llak wzogllo, dzelsho bxen nha nhello benhe yublhe.

{{< summary-quote
    text="Kat dzogo “padiux” xtillon, za wayull telhe guzon ba ll´chate Interneten len. Bi llunen latg wzogo xtishllon, nha llachan len dill xtilhe cha dill da llnebiá. Kat dzoga padiux dill dan nhe Chindali “mwalamusha”), de guzoga duxente dillen xexwé nha Interneten dzeygen lhen da xna, bi lla yumbielhen xtillan."
    author="Donald Flywell Malanga"
    link="/zap/stories/challenges-of-expressing-chindali/"
>}}

{{< summary-quote
    text="Gan dzogllo dan dzí lha teclado dill xtilhe, bi llunhen llín cha guzogllo dill dan nhe Cingalés. Xagulg to´ka bachuy´ake dill´ka nha bda´aken gan dzog´aken kuite dillen llnebía dan lé Inglés. De tu da llak llunhenllo llin dzogllo dill nhí, nha len UNICODE."
    author="Uda Deshapriya"
    link="/zap/stories/amidst-virtual-impunity/"
>}}

{{< summary-quote
    text="Cha bi welhall benhe´ka llun llín ke Interneten wayilg lhall´ake gaklhe wak guzogllo xtishllo´ka, bidaoka se zaa naw llío bi gakzedaklhebe xtill´akben, kan llak ke dillen le Breton, ball nebyá dill Fránces lhawe."
    author="Claudia Soria"
    link="/zap/stories/decolonizing-minority-language/"
>}}

De wayunllo xbab bixchén bi lla yoy dill lhue internet´en, ganhan´ganhe llallelello biteze llayilgllo —xchín laze llík nhenhe llunhenllo llín Internet — enchanha wak cha yote dill ka luenha.

{{< summary-gallery-fancybox
    caption_all="Xchín laze llak nhenhe llunhenllo llín da zan dill lue Internet."

    src_small1="/media/summary/Bengali-Wikipedia-500px.webp"
    src_full1="/media/summary/Bengali-Wikipedia-1000px.webp"
    caption1="Wikipedia nha xchín laze da llak nhenhe llunhenllo llín dill Bengali lue Internet."

    src_small2="/media/summary/Breton-Wikipedia-500px.webp"
    src_full2="/media/summary/Breton-Wikipedia-1000px.webp"
    caption2="Wikipedia nha xchín laze da llak nhenhe llunhenllo llín dill Bretón lue Internet."

    src_small3="/media/summary/English-Wikipedia-500px.webp"
    src_full3="/media/summary/English-Wikipedia-1000px.webp"
    caption3="Wikipedia nha lxchín laze da llak nhenhe llunhenllo llín dill Inglés lue Internet."

    src_small4="/media/summary/Spanish-Wikipedia-500px.webp"
    src_full4="/media/summary/Spanish-Wikipedia-1000px.webp"
    caption4="Wikipedia nha xchín laze da llak nhenhe llunhenllo llín dill xtilh lue Internet."

    src_small5="/media/summary/Zapotec-Wikipedia-500px.webp"
    src_full5="/media/summary/Zapotec-Wikipedia-1000px.webp"
    caption5="Wikipedia nha xchin laze da da llak nhenhe llunhenllo llín dill wlhallen lue Internet."
>}}

[Martin nha Mark]({{<trurl>}}/numbers/a-platform-survey{{</trurl>}}) bayilhga´ake cha wak gulhep´ake bíteze dill lue chnheg xlatg internet, chínn xlatge Android, nha chínto xlatge dan nhe iOS. Benhenh´ake llín xtlatge kí kanhake duxente yell líu numbié nha llitgnhen´aken. Leskakse benhen ake llín da ba yokse lue Internet nha bazog´aken lhaolhaz ne´aken o cha guzenhay´aken.

Da ki nhe´ake dill xtilhe plataformas nhaken tap kuen xlatge lue Internet.

* **Da llaklhen dzedllo** (xlatge ga guzedzo nha wayilg´llo bi da llenhello nellezo): Dill xtilhe le´aken Google Maps, Google Search, Wikipedia, YouTube.
* **Da llaklhelh llío guzedllo bill lhe dill** (llgón ltipllo enchanha guzedllo dill yublhe): Ki le da kí dill xtilhe, DuoLingo, Coursera nha Udemy.
* **Ga lltill chayllo benhe lla duxen yell líu** (xlatge ga llumbiello benhe): Kí le da kí dill xtilhe, Facebook, Instagram, Snapchat, TikTok, Twitter.
* **Ga lluelhenllo dill benhe´ake** (Ga nhí llak bi dzel llo ke benhe ka lluelhenllo dill lue Internet) Da kín wak gunhenllo llín, imo, KakaoTalk, LINE, LINE Lite, Messenger, QQ, Signal, Skype, Telegram, Viber, WeChat, WhatsApp, Zoom.

{{< summary-side-fig-fancybox
    src_small="/media/data-survey/PS_Figure_1-platform_summary-500px.webp"
    src_full="/media/data-survey/PS_Figure_1-platform_summary-1000px.webp"
    alt="Balhe dill wak gunhenllo llín gan llá xchín laze dan llak nhenhe llunhenllo llín Internet."
    caption="Balhe dill wak gunhenllo llín gan llá xchín laze dan llak nhenhe llunhenllo llín Internet."
    PDFlink="/media/pdf/PS_Figure_1-platform_summary.pdf"
>}}

Leskakse gadilheto kelhe tu lluaze llag lla dill´ka lhue internet, tus da ka lhe dill xtilhe Wikipedia, Google nha Facebook shlhue´aken da zan kuen dill. Dan nhe Wikipedia (bí nha ke ben tulhaze nha nhuteze wak guzog) ganhí llalhas da zan dill da llazog´ ake. Leska len llatup ka du tap gayua dill lakse nhaken xlatge internet da lbete. Leskakse llagdilhello ka du tugayua yich da niog ka du chon gayua dill. Natelle nhau dan nhe Google Ga Wayilgllo, ganhí llag lla tu gayua yogach dill, nha dan nhe Facebook llunhenhen llín ka tu wyonchí nha tu gayoa dill. Eskelhe dan nhe Signal llelhaon lhao yoyte da ka nhe aplicaciones da llunhenllo llin dzogllo benhe yublhe, ganhi llunhenhen llín guyonchí dill lue dan nhe Android nha chiyonhen lue iOS. Kan ba bnhekllo, kelhe yoye da ka nhak xlatge gunllo llín lue internet llunhen´aken llín dill zan, bal aken llun´aken kase dill ka bi llnebía. Dan lhe messenguer app QQ tus dill Chino llunhenhen llín.

Dill ka llak llunhen´ake llín lue internet nhak áken dill Inglés, dill xtilhe, Portugués, Francés nha chop chone dill ke Asia kanhake Chino Mandarín, Idionesio, Japonés nha Koreano. Dill ka le Árabe nha Malayo bi detk lhatge lue internet, leskakze llak ke dill yublhe.

¿Bí llenlhen nen, kelhe yoy dill de xlatge lue internet dullente yell líu? Yiz galga ga yua tu llua ka du [gall billon benhe nhache](https://www.worldometers.info/) yell líu nhi, benhe zan benhe llagza gan nhe Adda, (ka du tap 4-7 billion), (nha África (ka du to billon) bi llak llunhen´ake llín xtill´aken lue internet.

* _Benhe nhe dill wlhall ke África_: Interneten bi zin kaz yoyze dill ka nhe´ake Africa, ka du taplhalg chí lhao tu gayoa benhe ka lla ganhí yallg´lhake yeto dill enchanha wak gunhen´ake Internet llín, nha dill nhi nhake dill xtilhe da za Europa.
* _Benhe nhe dill wlhall ke Sur de Asia_: Interneten bi zin yoye dill ka nhe´ake ganhí, lakse nhaken dill da ne ben zan lla yell kí, kanhake dan ne Hindi nha Bengali bi de xlatge lhue internet.
* _Benhe nhe dill wlhall ke Zul Zilhe yell Asia_: Leskakse Interneten bi zín kaz yoyze dill ka ne´ake ganhí, tuz ka dill ka ne Indionesio, Vietnamita nha Tailandés llunhen´aken llín lue internet.

Benhe´ka le Martín nha Marks llayak be´aklhe da zán nha yallg duxente yell kí, ka daka de gan llnebía dill xtilh´ka. [Donald]({{<trurl>}}/stories/challenges-of-expressing-chindali{{</trurl>}}) benhe Malawi bnhablhe benhe ka nhe dill Chindali, dill da nxull walhan, gullake lhe bnhi nak guzog´ake xtill´ake lue Celular´ka, kanhake zlhe dill xtilh ká Inglés, Fráncés nha Árabe llunhen´aken llín. Nall bazinha lzáke gulhe zak tu celular, nha xchínlaze ka enchanha gunhen llín. Nhallen nhe benhe kí, ¿Bixa gunh´za zía tu celular da bi llía xtilla, nha enchanha wak chitnhenhan de gunha llín dill yublhe da bi llag nielha?

Blhueten bi de latg ke dill lue Internet, kanhake yiz galhg ga yua chínchone ka dan nhe [Twitter bayumbielhen dillen le Swahili](https://www.theafricancourier.de/culture/swahili-makes-history-as-first-african-language-recognized-by-twitter/), xtill benhe ka lla Zuzilhe yell África (xtill´ake nell o da gullope). Kenha bzulhao llunhen llín Twitter dill kí, lhaoze bayumbié´ake yoyte dill kí ka dill Indioneso. Benhe zan btaagunhe enchana wayumbie´aklhe dill Swahili, nhallen guklhen Twitter enchanha bzá llin nhí.

Leskakse gan nhe dill xtilhe América Latina bi de latge ke dill wlhall ka lue internet. Kan bayag dill ke llín nha le [Kimeltuwe](({{<trurl>}}/stories/use-of-our-ancestral-language{{</trurl>}})) dan nhao benhe´ka llag llá gan nhe Mapuzungun yell Chile nha Argentina bnhe´ake “wawe´aklhe kuis chenhá´ake wak gulhep´ake xtill´aken lue da ka numbiallo ka YouTube nha Facebook. Kat bi llenlho gulepo lue Facebook o cha YouTube dill Mapuzugun, bit llhalhan dillen gan nhak xlatg wéllo dill ka llenlello walhanllo dan ll´lepllo. Llen´aklhe lakze gulepaken dill xtilhen, we Internet latg gulep´ake gaklhe né´aken xtill´ake”.

Benhe ka lé Martín nha Mark bi bzed´ake bill lhe xchín la ze Internet, tuze celular´ka, lhaoze llakbeksello bi bi nha gun´akte enchanha gucha´ake gan dzogllo kan ne teclado, enchanha wak gunhenllo llín xtillon. De tu da lhe Gboard, xtlage ga guzog´llo ke celular´ka, [lluen latg gunhenllo llín gaga yua kuen dill](https://arxiv.org/pdf/1912.01218.pdf) xullken benhe ka llakzed lhe dill nha benhe zded lé´aken ba bza llín nhí, lhaose ke lhe yoy celular nzá len, tuz da ka zak lzake.

Leskakse llak ke [Uda benhen dzed dillen lé Sinhala]({{<trurl>}}/stories/amidst-virtual-impunity{{</trurl>}}) — dill nhí né galg millón benhe lla gan nhe Sri Lanka, balhale bsedaken kato gulhgake nha yalhatake se'e bsedaken— kan llakse ke lloy dill wlhall´ka, bi de latgke lue internet, nha benhe ka llun llín ganhí bi llag nieaklhen kanhake bi niogen lhebse ka dill ka llnebiá Europa. Kí nhe benhe bzed da nhí “dan lhe Unicode Sinhala lenha llapa dill ka katen dzogllo´aken lue internet, nha bayolhaon dill xtilhen, kanhake nllalhe dzog´llo txillo´ka”.

[Dan nhe Unicode](https://home.unicode.org/) nhaken xtlage ga lla dazan dill´ake da llaklhen llío dzogzo. De ton da le Version 13 ganha lla [tuga yua milhe yote xon gayua tu gal gayun dille](https://www.unicode.org/faq/basic_q.html) nha dill ki llak lla dup aken yeto kuen. Tu kuenhen llunh llín enchanha guzogzo ka tu chí llua dill (nha de escritura Latina llunhen´aken llín kas yoye dill lla Europa Occidental, nhatese dé gan llaklhelhen enchanha guzogllo da zan dill lla Japón china, nha Corean. Dill Devanagiri llunhen llín ke dill ka lla Sur de Asia. Unicode nhí lluen latge guzog llo dill Inglés da gulhe. Dan le Consorcio Unicode (da bi nhak ga zelhall´ake bi da zed´ka dé) llunhen yoy [bxen dao llunhenllo llín lhaolhas [guzogllo](https://home.unicode.org/emoji/about-emoji/) — lue internet.

[Dan bnabyull Martin nha Mark’s]({{<trurl>}}/numbers/a-platform-survey{{</trurl>}}) gaklhe llak ke dill ka lue internet nha bí dill [ llunhen lén benhe´ka llin]({{<trurl>}}/stories{{</trurl>}}) shlhab´ake duxente yell líu, shlhuelhen kelhe tuz ka tllín interneten yell ka, kelhe yoy benhe llak llunhen´nhe llín kanhake bi nzan dill ka nhe´aken, nha ba zín lzake zak bi xchinlaze ka yallg. ¡De gulho dan bzog benhe kí!

### Bi dill lla Internet, cha llak llunhenllo yoyen llín, nha nhu dzog le´aken.

{{< summary-quote
    text="Bi de gaklhe nello xtillon yoy da zed ka zuyib nholhe ka lla yell líu. De xlatge ga lláyag xtille yoy da zed ke nholka llun llín lixe, yiz tu gal ga yua taplhalg chone nawa´ake llun´ake llín llak zed´aklhe bea ke nolhe´ka. Lhaoze yiz galhg ga yua tu galghe bzulhao shlep´ake lue Internet da zed ka de ke nhol´ka lue nhí, nha benhen´ake llín xtill´ake Cingalés."
    author="Uda Deshapriya"
    link="/zap/stories/amidst-virtual-impunity/"
>}}

{{< summary-quote
    text="Lhause bnhi ba guke –nha nhaksen– wayilgllo da niog dill Bahasa Indionesio, kat wayilgllo “LGBT” o cha “Benhe Nuinlhalle” lue Google ­­— gan llallelello yoyte — llagdilhello dill ki “penyimpangan” (klhelhe), “dosa” (dol) nha “penyakit” (yillue)."
    author="Paska Darmawan"
    link="/zap/stories/flickering-hope/"
>}}

{{< summary-quote
    text="Da xen llgo lchixe gaklhe llállelello ka dill da niog Bengali kí lue internet (leska zu kat bi bí yokze) nha yezisklhe llak´lhelhen encha weáklhe benhe nuinlalle."
    author="Ishan Chakraborty"
    link="/zap/stories/marginality-within-marginality/"
>}}

Llenleto gumbieto gaklhe llitg Internet, bí llallelello luenha. Kanhake [ka du guyon lhao tugayuaa da ka lla luenna](https://w3techs.com/technologies/overview/content_language) llalhan dill Inglés, dill nhín llnebiá.

Ka yoyz benhe ka llunhento llin nha llakzedlh´ake gaklhe gak gunhen llín Internet yoyte benhe lla yell líu, lhakse né´ake biteze dill. Nha llagdilhe le´aken bí nunen lath gunhenllo llin xtillon nha de gudab yibllo gunhenllo llín xtillon. Bi de ak´xenhello gunhenllo llín dill ka balla lue nha. Llíon de guchizllo xtishllon lue internet, de gulepllo nhu bxen, nhu dill da llue dill ka guk, nhu da xtil benhe, konhe len zulhao yan xtishllo ka lue nha.

​​Da nhi gaklhe´lh benhe ka bi llue latg Interneten gunhen llín xtill´ake nha llunhen le´ake káse dan bi numbie´aklhe dill ka llnebiá.

Benhen le [Joel]({{<trurl>}}/stories/signs-across-generations{{</trurl>}}) llue dill gaklhe guk bzu lhao dan lé Indigemoji, ka zagdezé gukllé nha bzulhao bzoge xtille dan le Arrernte enchanha llawe xía ke bxen dao´ka llunhenllo llín lhaolhaz dzogllo. Ka za bzulhao´ake benhen´ake llín bxen dao kí, lhe gukdilhel Yell ka Nhil Nia Nha bzuyib´ake enchanha wak gunhen´ake llín bxen dao kí gan lla dill ka bi llak dzog´aken ka dan le Arrernte. Ka ban bnekllo beche gan zu Consorcio Unicode llayag dille cha wak gunhen´ake llin bxen dao´kí nha leska cha wak gunh´ake lhall bneb ke yell lhe Australia, lhaoze benhe ka llun llín ganhí [bi be´ake latge](https://unicode.org/emoji/emoji-requests.html). Xchín Joel nhí, Caddie nha benhe yublhe nhu benhe llaban, bi dao, nha benhe gulhe, dzuyiben enchanha wallelh xlatge dill ke´ake ka kanhake lé ganh´aken nha llgó ltipe yell ke llo ka.

{{< summary-side-fig
    src="/media/summary/Indigemoji-tweet.webp"
    alt="Tu da le Tweet ga blep´ake bxendao´ake dill Arrente."
    caption="Tu da le Tweet ga blep´ake bxendao´ake dill Arrente."
    source="Indigemoji."
    link="www.indigemoji.com.au"
>}}

Da blhao´n nezello yeto chopze dill wlhall ka lla yell líun, kanhake ka bed benhe xtilh ka da zan gulhen bchuchg lan´ake, kelhe tus dill ka bdao ke, nhente benhe ka nhe le´aken kanhake bllun´ake gune´aken llín. Okse benhen xue dill wlhall ka nha ben latge gunewía dill xtilha, da ka né´ aklle yell líu.

[Ishan]({{<trurl>}}/stories/marginality-within-marginality{{</trurl>}}) shlhelhe nha llak dile gunhe´nhe llín lue internet. Nha leska llap yínhe enchanha wallele lue internet dill Bengalí nha gaklhe zu kuez benhe niulhalle. Kanhake bi bí yo ka da kí lue Internet, “llakbelhe bi de latg ke benheka nhak ka lé, bi shlen´ake le ake lue internet nha llun´ake leake kase”.

Lakse benhe zan nhe dill wlhall ka, kanhake Bengalí—nhe chon gayua millón benhe lla lhao yell líu — bi bí llalha lue Internet.

Benhe ka lhe Martín nha Mark bzed´ake kwllellú,dill ka llunhen llín da ka le dill xtilhe Google Maps nha Wikipedia.

#### Google Maps

¿Wak gunhenllo llín yoyte dill de lue dan nhe Google Maps? Llaa gaklhe shlhelhello da nech guy lhue ka llunhenho llin dill yuble?

Benhe ka le Martin nha Mark enchanha guk ballí´ake da ki llnab yull´ake gaklhe llitg [Google Maps]({{<trurl>}}/numbers/google-maps-language-geography{{</trurl>}})’ nha dill ka llunhenhen llín, ka du chí dillake llunhenen llin: Inglés, Chino Mandarin, Hidi, Dill Xtilhe, Francés, Árabe, Bengali, Ruso, Portugues nha Indonesio (Bahasa Indonesia). Ba yilg´ake gaklhe llunhen llín dill kí, benhe lla Yell líu (Ga lla benhe kí).

Dill inglés na ganhen llunhen´ake llín kat bi llayilg´ake lue Google Maps, (dill nhí ganh lla llelh bíteze) Europa, América del Norte, Sureste de Asía nha America Latina llunhen ake llín dillen, eskelhe Africa ga bi llunhen´akten llín.

{{< summary-side-fig-fancybox
    src_small="/media/data-survey/GM_Figure_1-English-500px.webp"
    src_full="/media/data-survey/GM_Figure_1-English-1000px.webp"
    alt="Da lla llel lue Google Mapas dill Inglés. Gan nhak gasgze shlhuelhen ga nianlle da llallel."
    caption="Da lla llel lue Google Mapas dill Inglés. Gan nhak gasgze shlhuelhen ga nianlle da llallel."
    PDFlink="/media/pdf/GM_Figure_1-English.pdf"
>}}

​​Cha wiallo lúa yell líu duxenten lladub dill Inglés, dill Bengali (nhaken da nell xtill) [Ishan]({{<trurl>}}/stories/the-unseen-story{{</trurl>}}) tuz gan nhe Sur de Asia nhe´aken nha leska bi bi xchinlaze de enchanha wak gunhenllo llín lue Google Maps. Nha cha llenlhe benhe ki llag lla ganhí wía´ake bill lhe dayuble de gunhen ake llín dill inglés. Leskase llak ke dill Hindi (da guyone dill da nhe´aklle yell líu, nha naon koll Inglés nha Chino Mandarín).

{{< summary-side-fig-fancybox
    src_small="/media/data-survey/GM_Figure_2-Bengali-500px.webp"
    src_full="/media/data-survey/GM_Figure_2-Bengali-1000px.webp"
    alt="Da lla llelh lue Google Mapas ke benhe llne Bengali. Gan nhak gasgze shlhuelhen bi xlatg´ka niánlle."
    caption="Da lla llelh lue Google Mapas ke benhe llne Bengali. Gan nhak gasgze shlhuelhen bi xlatg´ka niánlle."
    PDFlink="/media/pdf/GM_Figure_2-Bengali.pdf"
>}}

Ganhí wak gulho yelhat ke Google Maps biteze dill [Da bzog Martín nha Mark’s]({{<trurl>}}/numbers/google-maps-language-geography{{</trurl>}}).


#### Wikipedia

Kan wlhelhe Martín nha Marks llío [gan bnabyull´ake]({{<trurl>}}/numbers/a-platform-survey{{</trurl>}}) lue Wikipedia llak llunhenllo da zan dill, llall dill lue ka da ka le Google nha Facebook.

Yoyse da ka llá lue Wikipedia —da llaklhelhe llío dzedzo— niogen chone gayua kuen dill. Lhaoze kelhe yoy benhe né dill kí llak lshlhe´aken, nallen llnablluyto cha ¿dell dill da llue´ake latg gunhen llín benhe ka? [Xchín Martín nha Mark]({{<trurl>}}/numbers/wikipedia-language-geography{{</trurl>}}). Guklhelhe netho enchanha guk ballito da kí.

Yiz galhg ga yua chínchone benhento llín da ka nhe geoetiquetas (da shlhuellío xlatge ka´dzog´ake yoy da lla lue Wikipedia) leskakse bzedto balhe llín da nioge lla lue´nha, nha cha ba bianhen. Da xen guknhelhe dill wlhall´ka da ka nhak dill blhao lue dan le [Unicode CLDR](https://cldr.unicode.org/) (xlatge ga lla dill zan lue Internet) nha dill ka bdelelhe chíllua lhao tugayua benhe nhe len yell líu.

Natelle bayilgto dill´ka, i.e. nell benhe zan tu tu yell lla yell´liu. Balleleto wyon yo chíne da llunhen´akllé llín. Gagl yo chdá yell ne´ake dill Inglés, natelle nau dill Arabia nha dill xtilhe (chín chone yell), dill Fránces (chine yell), Portugués (gall yell), Alemán (tape yell) nha Holandés (chon´yell). Nha chop yell né´aklle dill Chino, Italiano, Malayo, Rumano, Griego nha Ruso. Tu ze yell lladup guyon dill ka bi blhab´llo ganhí .

Enchanha wiato ka sagnhit dill ka dzog´ake lue Wikipedia nha da ka shlep benhe ka llunhen len llín, zakt bayilgto bi dill llunhen´akse llín lue nha. Dill Inglés llelhao, tap lhalg chí yell llunhen´len llín, na tell naho Francés (ga yell), Alemán (xon yell), Dill xtilhe (gall yell), Catalán nha Ruso (tap yell), Italiano nha Serbio (chon yell), dill Holandés, Griego, Árabe, Serbocroata, Sueco nha Rumano (chop yell). Tu ze yell llunhenhe llín tu llua dill ka bi blhab´llo ganhí, kat bi shlep´ake Wikipedia.

[Yich niog dazan dill aké nha lla´aken lue Wikipedia](https://en.wikipedia.org/wiki/List_of_Wikipedias) nha zág llian´aken. Kelhe lhebze nhak yoye yich nioge, balhen nianll dill llian, biteze lluen dille nha dé da dzog tus benhe nha da dzgog txong´ake. Lue Wikipedia llá xope millón yich da nioge, nha chua´millón benhe dzog. Benhe ka dzogll lue nha llunhen´aklle llín Dill Xtilhe, Alemán nha Francés, llalhab´ake du tap-xop millón benhe dzog chop millón yich kí. Bi llunhen´aktke dill ka yela llín, du tu gayua milh yich llá da llunhenhe llín guyonchí dill yublhe da ka bi blhabllo ganhí. Lhatze yich ka lla lue Wikipedia llunhen´aken llín dill Inglés.

{{< summary-side-fig-fancybox
    src_small="/media/data-survey/WP_Figure_2-English-500px.webp"
    src_full="/media/data-survey/WP_Figure_2-English-1000px.webp"
    alt="Yoy da balhabto lue Wikipedia yiz galhg ga yua chínchone”. Gan nhak gasgze shlhuelhe ga dzog´ake ich ka lla lue nha"
    caption="Yoy da balhabto lue Wikipedia yiz galhg ga yua chínchone”. Gan nhak gasgze shlhuelhe ga dzog´ake ich ka lla lue nha"
    PDFlink="/media/pdf/WP_Figure_2-English.pdf"
>}}

{{< summary-side-fig-fancybox
    src_small="/media/data-survey/WP_Figure_3-ar,_bn,_hi,_es-500px.webp"
    src_full="/media/data-survey/WP_Figure_3-ar,_bn,_hi,_es-1500px.webp"
    alt="Da ballel dill Árabe, Bengalí, Hindi, nha Dill xtilhe lue Wikipedia yiz galhg ga yua chin chone. Gan nhak gasgze shlhuelhe ga dzog´ake ich ka lla lue nha."
    caption="Da ballel dill Árabe, Bengalí, Hindi, nha Dill xtilhe lue Wikipedia yiz galhg ga yua chin chone. Gan nhak gasgze shlhuelhe ga dzog´ake ich ka lla lue nha."
    PDFlink="/media/pdf/WP_Figure_3-ar,_bn,_hi,_es.pdf"
>}}

Nhaken da wabanlello kanhake dill ka llunhen´ake llín Wikipedia leskake llunhen´ake llín lue dan nhe Google Maps.

Cha wiallo bale yich nioge (dill nell nha da gullope) ke benhen´ka dzog len. Lladilheto lebze yich ka lla lue Wikipedia da ka niog dill Inglés, Fránces, Dill xtilhe, Ruso nha Portugués ka benhe´ka nhe len. Nllalhe llak ke dill Chino Mandarín, Hindi, Árabe, Bengali nha Idionesio (Bahasa Indonesia), lakse benhe zan gulhe nhe len bi llatke yich da niog dill kí lue Wikipedia. Llall yich da niog Francés, Dill xtilhe nha Portugués ka yich da nioge Mandarín, Hindi, nha Árabe, lakse lladup´aken [xlatge gay dill](https://en.wikipedia.org/wiki/List_of_languages_by_total_number_of_speakers) da né´aklle yell líu, nha llelhaon lhao dill Francés nha Portugués.

{{< summary-side-fig-fancybox
    src_small="/media/data-survey/WP_Figure_1-language_ranking-500px.webp"
    src_full="/media/data-survey/WP_Figure_1-language_ranking-1000px.webp"
    alt="Da balleleto lue Wikipedia nha bale benhe llunhenen llín da nell chí dill ka nhe´aklle duxente yell líu. (ke tu benhe lla: Ethnologue galhg ga yua tu galgh. Da llalhab ba le benhe ne xtill´ake nell nha da gullope.)"
    caption="Da balleleto lue Wikipedia nha bale benhe llunhenen llín da nell chí dill ka nhe´aklle duxente yell líu. (ke tu benhe lla: Ethnologue galhg ga yua tu galgh. Da llalhab ba le benhe ne xtill´ake nell nha da gullope.)"
    PDFlink="/media/pdf/WP_Figure_1-language_ranking.pdf"
>}}

[Dan bzog Martín nha Marks]({{<trurl>}}/numbers/wikipedia-language-geography{{</trurl>}}) llatupen yoy da ka llá lhue Wikipedia nha bixa dill niogen, llayoxhen ka dillen ba be' benhe ka ba bualto ganhi.

Nhente lue internet llun´ake kase dill wlhall´ka bi llnebya, lhakse dill da ne benhe zan lhao yell líu. Cha llenlhello guzog´llo xtishllo lue Wikipedia de gunhenllo llín da ba bzog benhe yuble nha da nzalhall llínke´llo (ka ban wlhal´llon beche) bi bí llín dekte da niog dazan dill. [Emna]({{<trurl>}}/stories/write-in-another-language{{</trurl>}}) naowe dzede Wikipedia, llue dill gaklhe llak kat llayilge llín da niog dill ke yell África. “gukdilelha bayilga llín da niog xtilla, nha yelha dill da ne´ake África, nha guk belha bi bí llalleltk´lhello da niog biteze dill llunhen´llo llín”.

Gan nhe Europa benhe ka nhe dill ka bi llnebía les ka llagti´ake kat llen´aklhe gunhen´ake llín Wikipedia. Benhen le [Claudia]({{<trurl>}}/stories/decolonizing-minority-language{{</trurl>}}) bchalg ne nhe benhe né dill Bretón nha gadilhe benhe zan benhe ki bí numbie´aklhe wak gunhen´ake llín Wikipedia xtill´ake, lakse tugalg lhao tu gayua benhe bzog´ake nha guklelh´ake biog (xon lhao tu gayua) da ka llá lue Wikipedia. Nholhe ni llak´lhe benhe zanll gunhen llín dill´ka llnebía, kanhake bi nhaken bnhí chitg nhen´aken.

Xlatge Wikipedia [nialgen] [https://en.wikipedia.org/wiki/Open_source)](https://en.wikipedia.org/wiki/Open_source)) (enchanha nhuteze benhe wak gunhenhen llín). Yoyze llín ka llakzedlhe dill wlhallka lue internet llgón ltiplló enchanha zu lhao gat latg ke yoy dill dé. Benhe kí nhau llín´kí nhez´aklhe da zán kuen gaklhe guzollgo dill Inglés, Árabe, o cha Chino dé, nha leska bnhí nhaken wayunllo tusee yoyte dill´ka. Dan llagdilhello [lue Wikipedia kelhe lhebze](https://wikipedia20.pubpub.org/pub/myb725ma/release/2) lllunhen´ake llín dill´ka.

¿Bixá ka gun llío benhe tulhaz, yell nha benhe ntil niá nhaa ecnhanha gat latg ke yoyte dill ke llo ka lue Internet? Ganhi wasogto yoyte da ka [ba blhabto]({{<trurl>}}/numbers{{</trurl>}}) nha [dill llue benhe]({{<trurl>}}/stories/{{</trurl>}}) nhenchanha wayumbello ka llínka de gunllo enchanha cha yoyte dill de yell líu lue Internet.

## Bí banezello ke dill ka llha lue Internet?

Llakte dzoogto llín nhí da zan da bzedtoo ke Intenet´en, nha dill ka lla luenha. Ganhí wasog´to da blhao bzedto.

**Da llzedllo** Kelhe dan llak wchalg´llo nazen nhak da blhao lhao xtishllo ka, nhaken blhao enchanha zu kuello yell líu. Nhallen llunto zed chá' da zan dill lue Internet, kanhake shllhuelhen yel chao ke benhe ka lla yell líu.

​​​​**Gaklhe yo xneze dill´ka** Yoyze benhe llunhen llin xtille yell liunhi llunhenake llin gall milh dill. Kanhake dill ( da ne´ake nha da llchab niá nha´ake) cha da dzog´ake.

Lhaoze da guyon chí tu gayua dill ka dé, tu chop´aksen de latgke lue Internet, zelhao gallayua zen shlhab´ake. Dill ka nhe´aklle yell líu bi yo´aken lue Internet, de latg´ake ga lla zan dill de yell líu, le´akenha numbiello ka (Google Maps nha Wikipedia) tus dill Inglés llunhenhen llín.

**Wayunllo xbab:** **Bi de latg ke dill ka lue internet kan llaklhello na kán llenhello gaken.**

​​**Wanizlelllo:** Benzan llunhen llín dill ka llnebía, da ka zá gan nhe Europa (Dill xtilhe, Inglés, Portugués nha Fránces…) o cha (Chino, Árabe…) enchanha cho´ake lo Internet. Kan llak ke yell kello´ka kanhaken bi llnebia´aken llun´aken kase nha kaksen llak ke xtishllo´ka lue Internet.

## ¿Bixa ka wak gunllo da gaklhen? Da gak gunllo enchnha gat xlatge dill kello´ka lue Internet

{{< summary-quote
    text="Kat gunhnenllo llín dill´ka bi llnheb yía llenkazlhen gudabayllo nha zubiyllo, kanhake bnhi nhaken nha bi llabumbie´aktlhen."
    author="Claudia Soria"
    link="/zap/stories/decolonizing-minority-language/"
>}}

{{< summary-quote
    text="Cha llenlhello tu Internet ga chaz bíteze dill dé, de guyallo zatke gaklhe llak nban dill´ki. Bi´ak tuke nesllo gunllo xtlatge lue Internet, de gumbiello xtill yell´ka né le´aken nha gaklhe llak sagsdenn lhao bidao nha bi llaban."
    author="Jeffrey Ansloos and Ashley Caranto Morford"
    link="/zap/stories/learning-and-reclaiming-indigenous-languages/"
>}}

{{< summary-quote
    text="Bi dao nha bí llaban yell Mapuche blliwlhenhakbe Internet, ganhan llak llwia´akbe biteze da nza dill Mapuzugun. De yog xtill yell´kellon, wak llogen nha wak guzenhayllo dill Mapuzugun. Dillen llue ka da ba guk ke yell keton kelhe llwalhen benhe ka bazlha nha bcheylhaw´ake stad nha yelha yell xtilhe. Dill keton ll´lhaben gaklhe guk bzuyíbto benhe ka guklhall wuchglan nheto, gaklhe guk bllog nholhen zage llgene llín gayuble, nha gaklhe guk bayed benhe ka bza batnhate nha bill balheb´ake “lofs” lhall´ake.Dill kí shlhab benhe yell Mapuche nhak´aken xtill benhe llaglla lhaw yell nhí"
    author="Kimeltuwe project"
    link="/zap/stories/use-of-our-ancestral-language/"
>}}

Xlatge llchinto dan le Gaklhe llunlelhllo llín xtishllo´ka lue dan nhe Internet, llatupto yoye xbab ke benhe´ka guklhelhe nheto biogen, nha benhe ka bento yex yiz galhg ga yua tu galg [Llunto xlatge dill´ka lue Internet](https://whoseknowledge.org/resource/dtil-report/), enchanha baniz´aklhe nheto da wak gunllo enchnha wayak Interneten xlatge dill dé yell líu. De gunabyull kuinllo enchanha nezello bí nez nawllo encha cháa llínnhi llenlello.

* **¿Nu llnebyá nha llgo xneze bi da gunllo?**
* **¿Nhu nhe bí´ka gunllo, nha bí´ak guzedllo nha gulhello?**
* **¿Nhu nbanlhe duxen Intenet nha xchín laze?**
* **Nhu llun xbab nha llgo xneze da ka lla lue Internet?**

### ¿Nu llnebyá nha llgo xneze bi da gunllo?

#### Gaklhe ngó ake xneze

Dan ba bzedto, nha dill ka shlhab´ake, shlhuelhen llío kan llunh´ake káze xtishlloka kat llunhenllo´aken llín gateze zagllo nha lue Internet, nha kelhe nello dan bi lla benhen zan nhe len.

Shlhab´ake [guyon tugayua benhe lla lhau yell´ka](https://www.cwis.org/2015/05/how-many-indigenous-people/) benhe né' dill wlhall´ka lla yell líu. Benhe kí blla da nell, ka za lha benhe xtilhka bchuchj lanake yellka nha dill´ka. Millon lhe benhe né nha llunhen´ake dill ka llnebía yell´ka le Asia nha África lhaoze leskakse bi de xlatge lue Internet. Cha wayunllo xbab bale yell lla yell líu nha balhe kuen dill nhe benhen´ka zu lhao yell kí. Dill Árabe,Chino, Hindi, Bengali, Punjabi, Tamil, Urdu, Idonesio (Bahasa Indonesia), Malayo, Swahili, Hausa, bi de latg ke´aken lue Intenet lhakse benhe zan né le´aken.

Kanhate gulhen llen´aklhe walan´ ake gaklhe zukuezllo yell´ka, nha bi llakzedlhe benhe gunebia´ka gaklhe waxué xtishllo´ka, benhe gunhe bía, nha yoo wasede´ka llaksed´akshlhe dill lla (Europa nha América del Norte). Nallen bnhí llakaklhe ko´ake ltipe dill ka né benhe lhao yell wlhall nha yell gazge.

Yell líun llagzullon llun´ake kaze xtishllo´ka, lltitg´aklhe llío ket nellon, llbia cha´ake nholhe ka lla lhao yell, zechill´ake benhe zu kuell nll´alhe. Nallen llag´lhaollé dill ka lla Europan lhakse bi llat benhe zan nhe len. Keten bi llyoj dill nllalhe lhue interneten bnhí nhaken wayilgllon nha leskakse bi detk latg ke biteze da niog dill yuble. Ka llak ke dill Cingálesen, Bi bí da llue dill bixchén zu yib nhol´ka yo lue Internet, leska bibí yo ke benhe zu kuell nlla´lhe, bi niogen dill ka lhe Bengalí nha Indionesio (Bahasa Indonesia).

Xtishllo´ka nakaken yichg lhalldao´llo, kelhe lixagen bi llak gunhenllo llín bateze llenhello. Kanhake bi de latg ke xtishllo yezisklhé llbekw yillen le´aken. Kan nhe benhen le [Uda]({{<trurl>}}/stories/amidst-virtual-impunity{{</trurl>}}), “dan bi de xlatge lue Interneten ga wayag dill bea ke benhe nache nha bix chen zu yib nholka duxente yellíu, yeziklhe llgon lchixe Internet kanhake llux llgadi´ake benhe né dill yublhe da bi llnebiá, nha nolhe nha benhe bío zu kuez nllalhe. Nha yallg xlatge ga wak gulepllo bihe dayuble, kelhe ye da lez da gulegtllo da llun´llío kase kanhake nello nhu dill nha nhakllo benhe lao yell. Nha dekse dill yel yía nha da zekill´akse nolhe´ka lo Internet”. Nha kelhe tuze yell ka llnebya shllhello ka da ka, nhenkse lhao yell ke llo ká den, yell ka [lliacha´ake](https://www.equalitylabs.org/facebookindiareport) benhe ka llag lhe bedao yublhe, nha benhe ka bi nchao.

Nallen llunhen´ake llín Internet, enchanha zuyíb´ake gapchí´ake, welhao´ake, guzed´aklhe nha bi gunhake kase xtill´akeka. Benhe ka lé [Jeffrey nha Ashley]({{<trurl>}}/stories/learning-and-reclaiming-indigenous-languages{{</trurl>}}) bzed´ake chínchona llua gayua da ka né tweets da benhen llín chíno llua da ka nhe hastags da niog dill gulhall, nha chua yo chínchope dill blao ke guyon chí kuen dill da ba bayumbie Canadá. Gadi´aklhe konhen da ka nhe tweets ltill chay benhe ka lla duxen Candánan nha llaklhel´ake enchanha ben zan gumbié xtill´aken lue Internet.

Leskan zelhazgulhe “llenlhe benhe gunebia´ka guchuch lan´ake dill ka de Canádan, [enchanha wayun´ake tuze benhe ka](https://indigenousfoundations.arts.ubc.ca/the_residential_school_system/), nállen zak yoyte da ka lshlepaken lue dan nhe Twitter, kanhake ganha wak gumbié ben yuble xtill´ake ka nha gat latg waxue dill kí enchanha guzed´aklhen bidao za nao”.

{{< summary-side-fig
    src="/media/stories-content/Learning_and_Reclaiming_Indigenous_Languages_image1.webp"
    alt="In Dene, the word for Caribou also means Stars. I love that (emoji with heart-shaped eyes) #denecosmology #advancedlanguagesclass)"
    caption="Benhen le Dene Meliza benhe latg enchanha walto tweeten ganhi, nha leska llenle walto lha benhe bzed lhe le tu dill, nholen le Dene nha meaxkuelhen le Eileen Beaver."
>}}

#### Da gak gunllo

* Wayumbiello nhu benhe´ka nha xlatge ká llía cha llío
* Gunllo xníd mesh da gaklhelhe waxue nha gulhello dill´ka.
* Wayixgo bi gaklhelhe zuyibllo enchanha bi gunh´ake kaze yell kello´ka, kanhake nhente xtishllo´ka llbeí ake.

### ¿Nhu né bi gak gunllo, nha bi gak guzedllo nha gulhello?

#### Gaklhe ngo ake xneze

Dill dan llaxuxg gaklhe bzulhao Internet shlhuelhen llío ga yell nha nhu benhe´ka bné kí gunllo. Nhakbiekse blho niá nha benhe llích ka len, bi gukzed´aklhe gunabyull´ake yell ka yela gaklhe zu kuez ake, nha ben´ake Internet kan zukue´ake. [Nán ba llnen benhe gaska](https://www.theguardian.com/technology/2014/apr/11/powerful-indians-silicon-valley), lhaoze kelhe nuteze, benhe dekslhe mell nha llnebiá nhen. Internet nhaken — xlatge ga guchallo da nhechg llío, nha gusedllo gaklhe sukuez benhe lla yell líu, lhaoze llak zedshlhen benhe tu lhaz ka benhe ntil niá nha —.

Da xen bneb´ia dan nzí´ake yiz bienhí, kana bzulhao blo´ake tlipe yel gunllo xbab nha waniz yoyte. Lhaoze bi de galhashllo ga benhen´ake llín da xen yel gulhabe, yel guzede yebá, nha yel gun wanhá, yell ka lla gan nhe Zu Cawí yell líu, za chollo yiz bienhí. Dan bembie´aklhe Mesopotamia kenha, ná nhaken Irán ná Irák, ganha gulg kan shlabllo nha kan dzogllo. Leskakse kan bzulhao llak yel chao ke benhe ka lla Zu Chule yell líu, bzulhao bchuchg lhan´ake da dé lhao yell ka né Zu Cawí yell líu, nha bén´ake kase, nha bun´aklhe llín benhe ka lla ganhí. Ganhí bllé yel llach ke yell ka bí llnebía nha yell gunía ke yellka llnebía.

Bchuchg lang´ake gaklhe zu kuell yell ka, gaklhe llagnié´aklhe nha shlheaklhe yoy da nhechg´ake, yoze da ka guk zed benhe ka lla yell lhale. Dill ka nhaknllalhe ka da ka llnebiá llaklhelen zanislhello gaklhe zu kuesllo, de te dill da walsho yoyte da de lhao yell líu, lhaoze kan bchuchg lan´ake dill ka, dazan bníte nha balhán. Nhallen yedaleze llagdilhello da llía dill xtilhe nha bill lhe dill llnebiá lue internet.

Kan llue [Ana]({{<trurl>}}/stories/dill-wlhall-on-the-web{{</trurl>}}) dill, “Laskse kelhe da zan dillka de lhao yell líun bi niog, zag sdekzen kuen kuense lhao bidoa nha bi llaban, lhaose tus dill ka niog llnebía lue Internet. Ke zag dillen tus dill ka niog bi walhán guxe willg”.

Kan sag llnít xtishllo´ka, sag llníllto gaklhe zu kuezllo will gonhí. Llag llnitllo gaklhe llchalgllo nha ´ll´lhello yoy da nechg llío. [Lebze kan llnít xtillo, llbeyí ya yeo ka](http://www.unesco.org/new/en/culture/themes/endangered-languages/biodiversity-and-linguistic-diversity/) nha yoyte da nbán llag lla yell líu nhí.

Da xen gaklhelhe Interneten enchanha waxue xtillo´ka kanhake wak gulepllo dil da niog, da llunhlelhe llín yeto kuen ka guzog´aken, nha yelá dill dé. Lhaoze dé wen latg ke yoyte dill ka de yell líu. De guzed´ ake gaklhe llun yell ka enchanha nha nbán xtill´ake ka, bi gak gun´ake konganze lhe dé yell ga llapchí yel wakuell, xtishlhee, wayá, xalhee, yelwao, nha bi´ake gun´ake konganze gulhe´aklhe nuteze dakí.

Yellen le [Papa Reo](https://papareo.nz/) ben´ake tu da llayumbía chi´aken kat nhe xtillaken nzi (Maorí) ke Aoteaora/ Nueva Zelanda. Llín nhí naken ke yell Māori, leake nez´aklhe [bi xchinla ze yallg nha gaklhe chitgnhen´aken](https://www.wired.co.uk/article/maori-language-tech) nha bi gak zí nha gunhenhe llín benhe tu lhaz len. Enchanha guk llinhí benhe ka le Papa Reo benhen´ake llín tu da le dill xtilhe código abierto, lhaoze bi blep´ake yoyte da nezlhake lue danhi kanhake bi llue´ake mesh yell Maorí enchanha za llín ke´aken, kan llue´ake mesh yela yell llunhenhe llín código abierto. Dan nhe [Mukurtu](https://mukurtu.org/) nhaken xlatg dan nhello Código abierto, len llunhenhe llín yell´ka enchanha shlep´ake biteze nez´aklhe ke dillke´ake ka´.

#### Bi gak gunllo

* Gutil niá nhallo enchanha wak kollo xneze llin´ake lhao yell ke llo´ka, enchanha gat xlatge xtishllo ka lue Internet, nha da wé dill gaklhe gap chíllo yell líu nha da zukuezlhelhsho. De wello lhatg nhuteze benhe gunhenhen llín ka daki gulepllon.
* Bi zéllo lliteze, de naollo zuyibsho enchanha bi gunebía to chopze dill xlatge, gan llen´aklhe gaw´ake nha walan´ake yoyte da nechg le´ake.
* The wayumbello nha kollo ltipe yoyte llín ka nhak nllalhe, nha zuyibllo enchanha gat lhatge ke xtillo´ ka lue Internet. Leskakse de wayumbiello yell ka, xull ken´ake nao´ake zuyib´ake nha llunh´ake latg llumbiallo gaklhe zukuez´ake.

### ¿Nhu nbanlhe duxen Interneten nha nhu nhe gaklhe gunhen llin?

#### Gaklhe ngó ake xneze

Benhe´ka llun llín lue Internet, bi llak zed´aklhe gat latg ke yoyte dill´ka luení. Nha leskakse yell ka lla gan nhe Zu Chule yell líu bi bi llun´ake enchanha lebze ka cha yoyte dill dé lue Internet.

Benka nbanlhe Internet –benhe ka llgo xneze yoyte da ka lla lue nha – bi llakzed´aklhe gaklhe gunh´ake encnhana zukuell yote dill ka lue Internet kanhake bi nez´aklhe nha bi llen´aklhe gakbé´aklhe guyonchí tu gayoa dill ká dé nhak´aken da blhao lue Interneten.

Katen llen´aklhe bi got´ake kenhallen llayechjake llnabake nú ne dill´ka, kanhake da ka llá Europa nha daka nheake “dill kub”. Kan llak ke Zu Cawí nha sureste Asíatico, ba bzulhao lluen´ake [xlatge dill ka](https://blog.google/inside-google/googlers/shachi-dave-natural-language-processing/) nhe´ake ganha, kanhake ganha´ganhen lla ben lla´o ke´ake.

Lekse benhe ka nhak xan gan dllog meshen llun llín yell líu, nlan´ake xlatge dill ka bí llnebía, kanhake le´aken delheake mesh enchanha za llín ki. Zan nllalhe yo xnelle ke dan le Wikipedia kanhake nuteze wak gunhenhen llin dan nhaken Código Abierto. Benhe ka ngo nía nha llín kí nha zelhall´ake, bi llak nhen´ake enchanha gatg latg ke dill ka lue Internet. Leskakse, yoyze da´ka llaklhelhe Internet enchanha gaken [xlatg yoy dill dé](https://dl.acm.org/doi/pdf/10.1145/3442188.3445922), yallg nezlhen gaklhen nak tu tu dill dé, balhe benhe né len, gaklhe dzog´aken nha néaken. Zu ket llallhelh´ake da kí –nha bí nhaken dill lí o cha llad yí benhe yublhe-, lakse ka, llunhen´aktezen llín.

Benhe´ka le [Jeffrey nha Ashley]({{<trurl>}}/stories/learning-and-reclaiming-indigenous-languages{{</trurl>}}) gadi´aklhe “xlaatgekagan [llagtielhe] xtishllo´ka enchanha nha nban´aken, zíte llad yí´ake benhe. Zuk ket dzel´ake dill o cha bxen da llad yí benhe ka llunhenhen llín xlatg kí. Leskake zu kat llunhen´ake llín da ne bots lue internet nha lle´ake bots kí bí gúnhake, nha zú kat benhe kze llad yí benhe yuble. Kat nhak´aken bots ba nhak te gaklhe chigt´aken, dzel´aken da bi nhak lí nha llun chix´aklhen benhe.”

Cha bi llakzedlhe benhe´ka nbanle Internet dill ka dé lhao yell´ka, nxull gun´ake konganze. Gan nhe Myanmar gan llunhen´ake llín Facebook (o cha Meta), benhe llakzede dill ka bdixgue´aklhe benhe ka nlan len de ke ben zan lladí´ake luenha ka za kue xtlag dill Birmano. Yiz galga yua chinno, lue Facebook yo [tap benhe nhe dill Birmano](https://www.reuters.com/investigates/special-report/myanmar-facebook-hate/) nha llwia´ake bi llún gall millón benhe lue Facebook gan nhe Myanmar. ¿Bixá guk kanhake bi guk zed´aklhe bi dill nhe benhe kí nha gakle zukuez´ake? Gan lladup yoye yell lla yellíu nha llayag dill bi da zed de, bnhe´ake benheka nbán Facebooknhan nhap dole bazat´ake benhe Musulmán, nha benhe Rohingya lla yell Myanmar. Na lelhake benhe ke Facebook nhao llnabake yel korchiz lhao [benhe ka llun yel korchiz duxente yell líu](https://thediplomat.com/2020/08/how-facebook-is-complicit-in-myanmars-attacks-on-minorities/).

Leskakse [benhe ka lla India god yí´ake](https://www.ndtv.com/india-news/facebook-officials-played-down-hate-in-india-reveals-internal-report-2607365) benhe musulman, Dalits nha yelá yell´ake, nha bitbí llun Facebook laskse cha Indianha llá benzan llunhen len llín, nha leska nha de dill zan llunhenll benhe llín duxente yell líu. Facebook dzelen [tap lhalg lhao tugayua llchín laze gan lladub dill dé](https://www.washingtonpost.com/technology/2021/10/24/india-facebook-misinformation-hate-speech/) enchanha bi chizlaz benhe ka lla Stade dill da bi nhake lixag lue internet lakse to chopz benhe lla llunhen´ Facebook llín. Da chínto lhao tu gayuan llagán llagen ke yell ka yela.

Benhen´ka llun llí ke Internet de wayumbi´ake llín xen zí gunh´ake xlatge Internet ga wak chá yoyte dill de yell líu nha benhe nhe le´aken.

​​Benhe´ka llunhento llín llwal´ake daka bí llue latg cha yoy dill lue Internet. Da nell bi dé xchin laze internet (kanhake bi llinhén yoyte yell llá yell liu nha leska bí de celular nha computadora) da wak chiktlhelh´llo xtishllo, yezizklhe bní nha lleé´llaklhello gunhenllo llín xtillo ka lue Internet. Ganhi llaxogto da ka bi llué latg.

**Bi nak xlatg´ke dill´ka bi llnebía, lue dan numbiello Tecnología.**

Benhen lé [Donald]({{<trurl>}}/stories/challenges-of-expressing-chindali{{</trurl>}}) llue dill gaklhe llak lhalle yellen nzi Malawi ga bí de Internet nha celular enchanha gak guchalg benhe´ka. Bnabyushlhe galg benhe ne dill Chindali, chí benhe dzed, nha ye chí benhe gulhe´ake. Benhe galg´ka zelhaoze gay´ake nhape celular, ye gall´ake bitbi nhap´ake. Zelhaoze tap benh galg´ka —yoy´akse benhe dzede— nhap dan nhe´ake computadora. Nha leskakse kelhe yoy´ake nhap internet, tus benhe ka dzede llak llao´aken o cha llunhen´aken llín lo yoo wased´ka.

Lakse nhap´ake internet, zu ket bi nhak computadora nha celular´ka ga wak gullog xtishllo. Benhe zan llunhen llín dhan nhe “teclado” xlatge gan guzog´aken da llía dill yuble, kanhake nhu dill xtilh´ka llnebía. Mí nhaken gunhe´ake llín teclado dan nza computadora ka, yezizklhe mí nhak ke celularen kanhake nhake da daoze. Zan bí dekse xltag dill´ka bi niog.

Kan llwekse dill [Ana]({{<trurl>}}/stories/dill-wlhall-on-the-web{{</trurl>}}) ke xtillben, “xlatge gan dzogllo bí nzan dill da wak guzogllo dill wlhall. Llín xén ba bxí dan llayilh´ake tuze ka bayog dill wlhallen, kan dzog´ake dill xtilhen. Leska da bkuan benhe xtil´ka kan dzogllo dill wlhallen, nha ba bénllo ka kello”. Dillen le Arrente leska ll´llcheylhaon kanhake bi niogen, chí´ake, nha yel llchab nha´ake nha llunhen´ake llín [Joel nha Caddie]({{<trurl>}}/stories/signs-across-generations{{</trurl>}}) be´ake dill ke llín ke aken le Indigemoji.

Cha lhue nako benhe lhao yell ga lléblhe kan llun´aklhe lhe, nha yetuchí bi nhak Interneten xlatge ga wak gunhenollín xtillo, nhaksen zí bi chulhasho chou lue Interneten enchanha gulepo gaklhe zu kuez yell kon enchanha lelhe bene nhechjo o benhe lla gayublhe. Benhen le [Paska]({{<trurl>}}/stories/flickering-hope{{</trurl>}}) lhao llchínhen dill Bahasa Indionesio ke benhe ka né dill xtilhe LGBTQIA+. nhé “benzan benhe nhen LGBTQIA+. Indonesia bi numbie´aklhe Internet, nha bi nhez´aklhe gakle wayilg´ake bi llag´aklhe lue nha”.

Leska bi de xlatge lue internet ke yell ka lla gan nhe Zu Chule yell líu, bi de dá gaklhelh le´ake zu kuez´ake nha gap chí yel nbán ke´aken. Benhe ka le [Jeffrey nha Ashley]({{<trurl>}}/stories/learning-and-reclaiming-indigenous-languages{{</trurl>}}) nhe´ake “bi dé gaklhe gude´ake dill ka lé Hul'qumi'num, Sḵwx̱wú7mesh (Squamish), Lewkungen, and Neheyawewin (Cree) dill yublhe, nha leska llakchix´aklhe nhen dill Alemán, Estonio, Filandés, Vietnamita, nha Fránces kat bi ll´lep´ake lue dan nhe Twitter”.

Benhe lé [Uda]({{<trurl>}}/stories/amidst-virtual-impunity{{</trurl>}}) nhe “bi bí ll´chín lhazllo dé da gunhenllo llín enchanha gulep xtishllo ka lue Internet”. De nezello gaklhe llítg yote da ka lla luena, guntekso xchínllo len. Ganhín llagtillo kat llenhezo gunllo Interneten xltage dill wlhall ke llo´ka.

**Benhe yub telhen né gaklhe gak ke dill ke llo ka, benhe ka zák yayublhe nha llén´aklhe guzed´aklhe llío gaklhe chítgnenllo yoy da de lhao lhall´llon.** Benhen le [Claudia]({{<trurl>}}/stories/decolonizing-minority-language{{</trurl>}}) né benhe´ka nbánhe Interneten nhan llun´ak kon ke nhén, “[lagaze] benhe gunía´ka llunhen guklhe, nha bi llue´ake latge né yell´ka gaklhe llen´aklhe gak lue Internet. Leskakse nhí ke nez´akse cha dan ll´lpea´ken llunhe guklhe benhe ka llwí nha llao lhen. Van Esch et al. (galhg ga yua tu galg) llun´ake zed kanhake benhe ka nbanhe Internet de gunhen´ake llín benhe lao yell ka, enchanha nez´aklhe bi da ka gaklhelh le´ake”.

​​Bi nez´lhe benhe kí gaklhe zu kuez yell´ka nha gaklhe gutil niá nha´ake nen benhe´ka enchanha gat latg gunhen ´ake llín yoye dill dé. Kan bchalgelhe benhen le [Emma]({{<trurl>}}/stories/write-in-another-language{{</trurl>}}) nhen benhen le Gamil benhe Sudán né dill Árabe Sudanés, gulle lhe “Yell Sudán da zán yell llá nha da zan dill dé. Yell ka lla llalhe nheake Árabico (dan niá nhada), da bkwan benhe xtilh ka guklhall guchochg lan nheto. Gan ll´lha will nha llenhe benhe ka llá ganha nhe´ake yeto kuen dill, tus le´ake llagnielhe nha né len. Benhe ka lla lhale bi nhe´aken, lete cha ba gazu´aké ganha nha bzed´aken. Xtillto nhell bembieton ká Cushíta, da gulhte dillen. Kan balhabg Presa Alta bnit´to dan llayumbiá nheto ka benhe cushíta. Na llá bi llalleleto gaklhe wanizlheto dillen nha gaklhe wadeton dill yuble. Eskelhe dill Nubio, llak dzog´aken nha llade´aken dill yuble. Celular´ká numbié´aklhe ka Huawei nza´akten dill nhó. Dill ka nhe´ake gan ll´lha nha llen wish´en bi niog´ aken (ka llaklha, bi nhez lha kozo, de gúnaba). Zan lhale nheto dill Árábe. Benhe yell África nheto, nha nheto dill árabe, kelhe benhe Árabe nheto”.

Xchín Emma nhí guklhelhen lé bayumbialhe “Internet yallgen benhe zan, benhe zdog nha benhe bi dzog. Kelhe tuz benhe ka llunhen llín Internet de gulhaba, nhete benhe ka llnebía nha nban´aklhe Internet´en. Yoyze benhe llá yell líu llakzed´alhe gaklhe gaklhe gakllo tulle, nha yelá dan llunk´ake kase benhe ka llnebyía´ake. Lhaoze cha da líkze llenh´aklhe gucha´ake nha guchuchg lhanllo dakí, lue Internet nha de guzolhao´ake, benhe ka llelhao, nha de wía´ake gaklhe gun´ake ecnhanha gak internet ke yoyte benhe lla Yell líu”.

De wanizlhello gaklhen llitg internet ka, nha bi nhake lhebe ke yoytello, dill xen llunhen´ake — llín enchanha gulep´ake yoyte da ka lla luela, da ne código dill xtilhe — dill Inglés. [To chop lle dill](https://www.wired.com/story/coding-is-for-everyoneas-long-as-you-speak-english/) da bi nhak código dill Inglés dé. Benhe´ka nhu llchíne da nhi, de gumbie´ake kullellú dill nhí. [Llunen´ake llín diill Qalb enchanha llgo´ake xneze Internet](https://www.afikra.com/talks/conversations/ramseynasser), dill nhí nzalhallen ka dill Árabe, nha ke lén chop dill de da nhak código. Yoyte da kín de nezle benhe ka ngo nía nha Internet, gaklhe gak gunhen´ake llín yoyte dill dé.

Kí nhe benhe ka lé [Jeffrey nha Ashley]({{<trurl>}}/stories/learning-and-reclaiming-indigenous-languages{{</trurl>}}) “xchinto ll´lhuen de ke dé wayunllo xbab gaklen yo xneze dan nhe “tecnología” nha gaklhe llnebían kat llunhen´aken llín.

#### Da gak gunllo

* De nezlhello bea ke benhe nhache dan gak gunh´ake Interneten xlatge ga cha yoyte dill de yell líu, de we latg benhe ka llnebyá nha ngo niá nha Internet, nha kakse llayal gaklhelhe benhe ka ngo nía nha bi da zed de yell líu nha llun yel korchiz kanhake [UNESCO](https://en.unesco.org/news/tell-us-about-your-language-play-part-building-unescos-world-atlas-languages).
* Gat latg ke yell ka ko´ake xneze llín da wé xlatg xtill´ake lue Internet. Wak gutil niá, nha´ake nhen benhe ka llnebía nha nbanlhe Internet, nha llayal chagle´ake nha gap´ake balhan yell´ka.
* Yell ka de né cha´ llen´aklhe le dazan llín ka nhao llun´ake nhen xtill´ake ka. Bi de guche´aklhee le´ake, nha de gak Internet xlatge ga nhu nhu bi gunle benhe ka llnebía´ake, llad yí´ake nha llun´ake kase.

### Nhu llun xbab nha llgo xneze da ka lla lue Internet?

Yoyte da ka ba blhabllo ganhí, llakbello da gakzen cha ka llenlhello, cha llenlhello gat xlatge xtishllo´ka lue Internet, lhaoze bi gak gunllon llío ze, yallglhello benhe zan. Bi walhan dill ka llun´ake kase, cha kuedillo llín nhí nha gulenllo benhe ka lliá cha´a nha llnebi´a.

Benhe´ka llunhento llín llayun´ake xbab bí ka gak gunllo enchanha gat latge xtishllo´ka lue Internet, chop kuenle nhake, de gakuelhe benhe´ka llun llín lue internet, benhe ka llgo xneze nha leska de guda niá nha benhe lhao yell´ka. Bi cha llinha cha bi guzenhay´ake xbabke yoy benhe llaksedlhe danhí nha gunh´ake tuz dan né benhe´ka llgo llneze Internet. Kan nhe [Claudia]({{<trurl>}}/stories/decolonizing-minority-language{{</trurl>}}) “Benhe ka né dill ka bi llnebía bit yallg áklhe yed benhe yuble yé le´ake bi gunh´ake, de guzenayllo bí da zed de ke´ake nha gaklhe gaklhenhake nha koake llneze. Nllá nllá nhak dill de lhao yell nha kakse nllá nllá gaklhe llgolhe xneze tu tu yell.

Benhe ka llunhento llín llue´ake dill gaklhe llunhen llín yell ka xchín laze Interneten enchanha llun chí´ake xtill´ake. Benhen le [Ishan]({{<trurl>}}/stories/marginality-within-marginality{{</trurl>}}) kin né “ka nheto nhakto benhe zukuez nshalhe nha bi nchauto, llayal gunhento llín yoyte da nechgto, enchanha yénle yoyte benhe bín llnbato. Nheto llunhento llín Interneten llayal guchato, enchanha gaken xlatge yoyte benhe yallglhe len.”

​​Benhen le [Ana]({{<trurl>}}/stories/dill-wlhall-on-the-web{{</trurl>}}) llue dill gaklhe benhe lhao yell´ka llunhenll´ake llín xlatge Internet da nzá bxen nha da gak guzenhay´ake. “Kan llak ke dill wlhallen lue Internet, llayilg´aklle da nzá bxen nha da nza da gak guzenay´ake, ka da gulake. Ka da ka numbiello YouTube, Facebook, WhatsApp nha Instagram da xen llaklhel´aken dill ka bi niog. Daki llue´aken latge gulhep´ake nhu bxen laolhaz guzog´ake, nha leskakse wiá´benhe yublhe len lhaolhaz gulen, nhallen llunhe´akse llín latg kí lhao yell´ka. Kanhake yell whall ka yoyte lshlep´ake lue Facebook, ka lní ka, leska kat llen´aklhe bi chixguelh´ake, nha bill lhe yela dé lhao yell´ka ka wayá nha wakuell. Ka za zulhaotelhe Covid-19 benhe zan benhe gulhall ka lla síte ka nhu lá, zite nha stad llunhen´ake llín Facebook, kat nu llat, ganhan yixgue´aklhe nha llep´ake kat llná´ake yelh nha llkuachake benhe waten. Leze Facebook llunhen llín bal yell´ka gan llí dan nhe dill xtilhe “radio” enchanha wak guzenha´ake ga bi llín bex nhe nhí.”

Da ka nhe dill Inglés “Hastags” leska llpelhen benhe ka llunhen llín dill da nioge enchanha gunhenhe llín xtillen lue Internet. Kan llue dill benhe ka le [Jeffrey nha Ashley]({{<trurl>}}/stories/learning-and-reclaiming-indigenous-languages{{</trurl>}}) “Yell Canadá dazan dill ka balan benhe ka llnebía ka guklhall´ ake gun´ake tuze yoyte benhe lla ganhá nha bi bdap´ake balhan gaklge zukuell benhe kí. Nhallen da xen llaklhelhe dan nhe “Hastags” kanhake nhaken xlatge ga dzog´ake xtill´ake nha benhe yublhe wak gumbié lhe.

Zagze ll´tix luell “Hastags”, nhaken ka dú, benhe ka dzed Gwichin, “Hastags”,blho´ake ltipe yeto kwen benhe dzed dill Ojibway enchanha benake “Hastags”. Kakse guk ke txong dill lé (Cree) ka ben´ake “Hastags”, ke´aken blhó´ake ltipe benhe wasede dill Hul'qumi'num enchanha gulhep´ake yoy lla tu dill da niog xtill´ake”

Da zan yell da ka llía cha benhe´ka llnebyía ba llunhen´ake llin xlatge da kí ba blhallo, nhallen de welash benhe´ka llgo llneze Interneten guyá´ake gaklhe gunhen´ake benhe kí llín, nha gutil nia´nha´ake lhao lhaz gunh´ake lea´ke kase.

Benhen le [Claudia]({{<trurl>}}/stories/decolonizing-minority-language{{</trurl>}}) né de ke benhe ka zakzedlhe dill wlhall´ka de nhao tez nhao´ake guzed´ake enchanha za llín ke´ake. “Lakse da chao´n llen´aklhe ke yell ke´ake ka, [benhe zú yib lhao yell´ka] zu ket bi lloáklhe xneze llín nha bi ll´lhuen bín llun´aken, nha yilxgen da zed kanhake lhao yell ka bí de yagglhao mesh: nha ndil niá nha benhe zan. Zu ket bi nez´aklhe bí llín ba dé o bí yallj encnhana zá llin ke´aken. Nallen cha llenlello gak Internet xlatge ga chá yoy dill dé, de nezello bi dill ka ba llunhén´akse llín lue Internet, cha lluneh´ake llín welse nha bi ké. Yeto da bi dé galhallo, cha benhe ka né dill da bí llnebía llagti´ake kat llunhen´aken llín lue Internet: ¿Llagti´aklhe kat shunhen´ake Internet´en? ¿Lleb´ake gunhen´aken llín? ¿Kat dzog xtill´aken, ll´lhue aklhen gaklhe zukue´ake? ¿Bi llunhen xtill´aken llín dan lléb´ake nhu gutitk lhe le´ake nha gad yí le´ake? Leskakse, bi nezello gaklhe llenlhe benhe´ka ne xtill´aken, lue Internet´en. ¿Bí ka llen´aklhe cho', nha bí ka llen´aklhe wayilg´ake?”

Llín nhi llaklhenhen enchanha chagniello gaklhe llchá ken yolhaollo gunllo llín xlatge xtishllo´ka... cha gunllo yedaleze bi zá xchinllon. Kelhe lebze ka gun llín tu “aplicación” dill Inglés ka tu aplicación dill Indonesio (Bahasa Indonesio). Cha llenhello gak interneten nllalhe, zatk de wallá kan zu kuez benhe ka llnebía benhe lla yell yublhe, bi bíde gúnllo cha llaklhello de xchínla ze internet da bi llun llín nha de wayun´ake o cha gullá´ake da kub.

Tu Internet da nhake xlatge yoy dill de ndzan chop te da kí bá bual´llo. De gukuelhaollo bi da zed ka de ke dill wlhall´ka nha yell´ka ne le´aken, —kelhe de gukuelhaollo dazed ke Internet´en— cha naollo nezen shllhue´aklhe llío nhí,wakse gunllo tu Internet da likse yallge nha da gaklhen benhe. Tu chopz benhe lla yell líu llen aklhe chó xnheze xtillo´ka nha xlatge gan llunhenllo llín. Dan le “[Indigemoji](https://www.indigemoji.com.au/ayeye-akerte-about) nlla lhe nzalhallen ka Interneten llunhen´ake llín gan nhe Australia Central. Nha llunhen yex né´ake gaklhe llen´aklhe gunhen´ake llín Internet. ¿Kelhe leska llén´aklhe gunebiá´ake? ¿Gaklla ka gak gunllo enchanha gulenllo xtillo nha gaklhe zukuezllo luenha, enchanha gúnllon kello?

#### Bi gak gunllo

* Yoyze xchín laze dan nhe tecnología de wían bi da zed nha bi llayglhe xtillo´ka ecnhanha wak gumbiá yoyte benhe llá yell líu le´aken, lhaolhaz gaklhello tu ze dill wák gunhenllo llín yoytello.
* De wayilg lhallo gaklhe gunhenllo llín Internet enchanha zillo banez yoyte da llagdillo lue nha, dé guchalg´llo nhen (nhallo, lhaollo, nha nhe bxen´ake). Da nhí gak´lhén lhe benzan len nha chagnielhake bi dan llenlello nhez´aklhe.
* Cha llenlello bí gúnllo lue internet, de wiallo nha gusedllo gaklhe zukuez yell´ka, nha leska de gapllo balhan xbab ke ´ake, le tuzen nhak´ake. Llakte llunho danhí de wayunllo xbab gakle llenlello gak gúxe willg. [De wabillo enchanha wiallo bi za zá](https://www.rnz.co.nz/national/programmes/morningreport/audio/2018662501/ka-mua-ka-muri-walking-backwards-into-the-future).

## Ba llayulll, bi lla gak gunllo?

Cha nhu benhe bi ne koslho dill Inglés, kelhe nello bi llagnielhé. Lhaolhaz Inglés´en, né dill yublhe, tu dill lhao guyon chí tugayua dill llá Yell líu .

Yoyte llío nzedlhello, nha nezlhello bíteze, de gutil niá nhallo enchanha zákello gunllo tu Internet ga cha yoyte xtishllo´ka. Leska de wiállo kanhake dan nezlhello nha llenlello gumbía benhe yublhe bi gulle´yín le'ake nha llayalte wiallo dalikse gaklhelhen le´ake. Yallglhello dan né´ake “gutil nhaniallo ke llín”

### Cha lhue llunho llin lhue dan ne tecnología:

* Bwiachk chá gán layo xlatge xchínon, llaklhelhen enchanha chá yoy dill dé lue internet nha leskakse chá llaklhelhen gumbié benhe yuble llín ka gan til nía nhaa´ake.
* De welhao yoyte dill ka bi llnebía, nha bi gaklho dá bi zaken nha yell yach kazen né len. Kelhe lhuenhan shag zlhao benhe ka nhe dill kí, de gunhenho´ake txen enchanha za llín nha.
* Be latg guzedlhe benhen ka ba numbié ka dakí, de wíao cha zaá llín nha enchanha bi gulleín benhe nache.
* Bi de gunho kongance kat gunho llín nhen xtill yell´ka, de gaplo balhan gaklhe zukuell´ake, nha bi guzanlallo´aken, de gunhenho´ake txen lhao llín nha.
* Kat llgenho llín cha ba sago to yell gap´akue balhan, cha bi gunho llellue nxull guzeyí´o ka zu kuez´ake.
* Cha napo llchín laze da gaklhelhe benhe ka ba blhue lhue lltix´aken, de gunhon nall nha´ake.

### Cha lue llgoo xneze gaklhe gunhen´ake llín dan nhe tecnología:

* Zatk de wayumbielho gaklhe ndil dill´ka nha kan zukuez benhe.
* Gunho txen lhao llín yell ka llneb yía´ake, enchanha wak gunho llínkon kozlho kanhake le´aken nezlhe.
* Gunho yex benhe llá yell yuble da lliá cha´ake, de weo latg enchanha zulhao nhén´ake bi llín ka dé gak.

### Cha nhako benhe gunebía:

* Wiao cha dillen nhe´ake yell kon nhake da gunhen llín yoy benhe, kelhe tuze benhe llak né, dzgoge nha llul len.
* Kou ltipe nha gaklhelho gaz lhaz yoy llín da niog dill ka llia cha´ake lhao yell ko.
* Kou ltipe nha gaklhelo gunchi´ake nha wayog yoy dill de lhao lhallon, kelhe tu ze da ka llnebiá.

### Cha llunho llín ke bene tulhaz lue Internet enchanha yote benhe wak wia bin ll´lepon:

* De nezlho de Internet dan wak gunhen nuteze llin, da bi llaxj enchanha nuteze wak gunhen len llín lhauze llagti´aken kat lla dayublhe da llnebía, lhaske nhaken da gaklhelhe yoyte benhe.
* De gapo balhane gaklhe llenhle yell´ka gulhe´aklhe lhue nha gue´ake dill kan zukuez´ake kanhake zelhaz gulhen ba bzak zí´ake leake nha bchuch lan´ake yoy da nez´aklhe.
* Gunhabyullo bin llín´nha llenlhe yell ka gak lue Internet, lhaolhaz gunho ko da nhenko o cha da llaklho´lhue nhaken da blhao.

### Cha lue nheho dan nhe GLAM (ga llot´ake bxen, yich wasede, ga lla da ben llín nha ga shlhab ake da guk ka nhí):

* De wayumbielho guxken dill kan nxue kan zukuelll benhe nache.
* Yell ka ba bzak zí´ake de né gaklhe llen´aklhe gunh ´ake llí nhen yoy da nez´aklhe. Le´akén de ne cha llen´aklhe gazlaz da nez´aklhe nha gaklhe zu kuez´ake. Da zan da ka nhe GLAM (ga llot´ake bxen, yich wasede, ga lla da ben llín nha ga shlhab ake da guk ka nhí) da lla gan Zu Kawí yell líu ba bzak zí yell´ka.
* De wiao cha yoy llín ka gulepo wak wayilg nha gunhen llín yoyte benhe lhao yell kí ba bzak zí´ake, nenchanha gakllo txen kat gunho xlatge ga chá yoy dill lue Internet.

### Cha lhue Shlhuelho nha dzedo:

* Kat dzedllo, bi bí detde da nioge nhu dill yublhe.
* De wayilg lhazo gaklhe gulhe´lho enchanha gulenho bi dill yublhe.
* Béele, bzenay nha wual llín ka da llazog´ake dill yublhe, enchanha gupelho benhe yublhe gúnhen.

### Cha lhuellbago yich nioge enchanha benhe zan gumbié le´aken:

* De nezello da zanll yich wasede llbag´ake duxente yell líu, da niog dill ka dé Europa.
* De yán dill da llunhenho llín kat llbagllo yich níoge.
* De kuago yich wasede nha bíteze llín, da niog biteze dill.
* De wayilg lhallo gaklhe llenlo gunho xchínhon, enchanha weo latg ke dill ka niog, da ka llunhen llín tuz chí´ake nha da ka nza bxen.
* Weo yel lluxkén nha wayumbelho xchín benhe ka llazog dill yublhe.

### Cha lhue llaklhelo yell nha benhe:

* Biteze llín llaklhelho, de nezlho dill nhá ganhen shllhuelhe llío yoy da nezlhe benhe nha ka ba bzu´ake lhao yell líu nhí.
* Kat ga lladup llallaylhe cha bi lní gunlhe de guzu benhe gúde dill enchnha ka youlhul benhe chajnie.
* Llayal gaklhelho waxue nha gun´ake xlatge lue Internet dill´ka nhe´ake yell ka llaklhelho. Leskakse yoyte llchín lazo ka de yog´aken yoyte dill ka ne´aken yell ka gan llunho llín.

### Cha zúu yell ga llun´ake kaze dill ka:

* Su kat llaklhello tuze lliun nlayllo ka llinen, llenlheto nezlho nllayksllon.
* De nezlho yell kan llayal né bi llen´aklhe nha ga´klhe gulhe´aklhe nha guzed´aklhe benhe yublhe yoy da nez´aklhe nhaa da zu kueznhen´ake.
* De gutilh nhao benhe gulhe, bidao dzede, bi llaban, nha benhe numbielho yell yublhe enchanha gulhelhe, guchalglh nha guzenaylhe da nezlhe yoylhe.
* Cha llenlo gumbielho benhe llun ka llín nha llunho, wakse neo o cha guzogo nheto.

### Cha lhue llawelho dill ka nha llenlo nezlho wi´ak gunho:

* De nezlho yoyse dill ka nhak´aken yich laxdaw benhe nha yell ka nhe le´aken. —¡Leskakse xtillon!
* Belhelhe dill ben bichj luellgo nha yelá benhe nechjo enchanha wayakbelho bixchén llag llá dill llnebiá ka nhake dill Inglés, nha gaklhe gutilh chayllo enchanha guchallo kan llak ke xtill llo ka.
* Bayilg, bele, bzenay nha bdiz laz bi da llag dilho da ba bzog yell ka llun´ake kase. (¡Ka llín nhí!).
* Cha llenlo chixjueto lhue gaklhe zag xchínton. ¡Bnhau nheto xlatgen nhunto enchanha wak welhelto lhue dill!

## Yel xuxken

Youlhul nheto Nlleto, llapto balhan nha llaklhelto yell ka lliá cha´ake (yell gulhall nha yela) kanhake nhen xtill´ake kan llayumbi´ake le´ake. Yoy da ka llunh´ake enchanha nxue nha gumbia´benhe yublhe xtill´aken, llgón ltipto kanhake llenleto gak internet xlatg yoy dill ga wayumbie´aklhe llío kan nhakllo. Leska llueto yel xuken ke benhe´ka llak zedlhe dill ka ká nheto nha lluelhall´ake llun´ake llín enchanha gat xlatg xtillo ka lue Internet kan de latg ke gan llagllallo.

Yoyte benhe´ka [guklhelhe, bayog]({{<trurl>}}/about{{</trurl>}}), nha yell lla yell líu (benhe ka bnhen nheto kan bayag dill [gaklhe lello nllalhe dill´ka lla lue internet yiz galga yua tu galghe)](https://whoseknowledge.org/resource/dtil-report/): Lluxken´le nhaolhe ka llín kí duxente yell líu, nha leska kanhake ba bzelhenlhe nheto chop yiz. Lluxken benhe´ ka ben bxen ká lla lhao llín nhi.

Gyxken yoy benhe numbieto, nha benhe ka [bné](/about) gaklhe blhe´aklhe llín nhí da niog da zan dill. Zukse ga bxingto, lluxkenlhn guklhenlhe nheto enchanha bya llín nhi. Leska llueto yel xuken ke bich luellgto nha benhe llaklheto ka bich luellgto, bi ga bdesto yiz kí ba bdé (kanhake galhg ga yua tu galgh- tullua) chenhak bi benle nheto txen lhakse blheto lhe lue Internet. Tus dill llayakllo kat nlle luellgllo nha llgó ltipllo.

## Ganhi shaxoxgt tu chop dill da llagdilho lhao llín nhi

Da zan kuen de gaklhe wayag dill´ka da bualto ganhí, kelhe lebze benlento´ake llín. Ganí wak gulho bi llenlen nhen dill kí.

* **Dill llnebía**: Dill da llné ben zan lhao yell, leska dill llunhen llin benhe gunebía. Kan llak ke dill Hindi, llnebían lhao dill ka yela llá gan nhe Zu Cawí Asia. Leskakse dan nhe Chino Mandarín benhe gunhebía blho´ake ltipe enchanha gunebían lhao dill ka yelá né benhe llá yell China. Nhen dill kín llayumbie´aklhe yell ka, nha da dé gunhen´ake llín biteze gunh´ake.
* **Dill da zá gan nhe Europa**: Dill dé gan ne Zull Llé Europa da guzlall yell África, Asia, Ámerica, Caribe nha Islas del Pacífico, kan benhe zít ka bchug lhan´ake yell kí ké nhite. Dill kí nhaken Inglés, dill xtilhe, Fránces, Portugués, Holandés nha Alemán. Kelhe tu ze gan né América Latina llnebía dill kí, leska gan Zu Chule América.
* **Zu Cawí nha Zu Chule ke yell líu**: Gan nhe Zu Cawi Yell líu, nbanhen yell África, Asia, América Latina, Caribe nha Islas Pacíficas, yell kin bchuchg lan Zu Llé Europa. Kelhe llénlello wayumbello ga llgán latg kí lhao yell líu, llenlello gakbelhello ke lhe lebze nhak´ake ka yell ka llnebía, da ka numbiello ka Europa, nha “Zu Chule América”. Benhe ka dzed ka da zed dé nha gaklhe zu kuez benhe, lé´aken bzulhao llunhenhe llín Zu Cawí enchanha bi gunhe´ake llín dill´en llun káze yell kí. Kan bede benhe zít ka, benhe zan lhao yell ka lla gan Zu Chule yell líu, bazát, nha benhe zan ben ka llaglla gan zu cawí yell líu guklhelh´ake enchanha bllía cha benhe llich´ka yell kí. Nhallen tuage nheto zu tu Cawí gan zú chule ke yell líu, nha leskakse Zu Tu chule lue gan Zu Cawí ke yell líu. Kan yo xneze yell líun leska llunhen llue xtillo´ka (wía ga zá dillen nhe “da zan nha llnebía tu chopzen”).
* **Dill wlhall´ake**: Dill da né yell wlhall´ake. Benhe ka llag lla lhao yell wlhall kí, benhe “blha ká nhíten” nha bzakzí benhe llích´ka. Da wayak´chí dill ka nhe´ake duxente yell líu, yell wlhallka ganhen nhe le´aken.
* **Dill wlhall´ake nha naá dill blhao**: Gakteze guchalgllo, cha guchab niá nhallo, gul´llo, guzogllo, zakzen le leskakse yeto kuen dill´aken. Bal benhe zed dill kí, llaxog´ake dan nhello naá dill blhao ka da zan kuen gaklhe nhen tu dill, zu ket bi llagniellon laske nzalhall´aken —nha ne benzan len. \
Lhao llín nhi, bi benhentkto llín naá dill blhao kanhake llunhen kase xtishllo ka, nllalhe dill´en nha nllalhe naá dill blhao. Llunhentoo llín “dill nllay”, kanhake lakse bi nhak´aken lebze nzalhall´ake ka ba guk ke´ake ka dill Árabe, Chino o cha Hindi.
* **Dill de lhao yell**: Lhao llín nhí bchizto dill nhí kat llenhento walto dill ka nhe benhe zan lhao to yell llwalto ka llayaj dill.
* **Dill bi llnebía/ dill lliá chá´ake**: Llín nhi ll´llhuelhen dill ka bi llnebía lue Internet, bi bí llagdillo da niog ka dill kí. Kelhe nello bi llnebía´aken dan bi né nha bi llap chí benhe lhao yell ka le´aken, kan zukuez duxente yell líun, benhe zít´ka nha benhe llnhebía´kan bhuchlan´aken nha bazín nha bi llakzed´aklhen. Da zan dill kí bi llnebía ba llalhan´ake, kanhake (dill wlhall´ka). Nha balen nanxue kanhake nha né benhe zan len lhalle o gayublhe lhao yell líu, lhaoze bi bi xlatge de lue Internet (ka dill ka Punjabi nha Kabil, leska Hausa nha Zulú, nha yelá dill llnebiá Asia nha África).
* **Dill da né nha bi ne´aklle**: Cha walhabllo balé dill né´ake yell líu, dell dill bi nhe´akte lhau yell´ka nha dé dill da ne´aklle.
* **Dill da nell tu chop benhe lhao yell líu**: Kánhake kan yo xneze yell líun, benhe´ka llnebía nha llía cha´ake yell wlhall nha yell yach, lluen latg gun´ake kaze nha zé chíll´ake xtishllo´ka. Kelhe tuze dill´ka llunh´ake xué, leska gaklhe zu kuez benhe. Níanll yell kí lue internet nha gan llagllallo, lhaoze bi llnebía´aken nallen llak´aklhe tu chopz benhe nhe lén, lakse nhak´aken “dill da nell tu chop benhe lhao yell líu”.

[Cha llenlo nezlho gakle gunhenho llín dan niog ganhí.]({{<trurl>}}/license{{</trurl>}})

[Cha llenlo nezlho ga zá dan dzogto ganhí nha nhu blhó ltipto.]({{<trurl>}}/resources{{</trurl>}})

[Wakse guletgo llín nhí (PDF 1.7 MB).](/media/pdf-summary/ZAP-STIL-SummaryReport.pdf)