---
Author1: Anasuya Sengupta
Author2: Abbey Ripstra
Author3: Adele Vrana
Title: 'Muhtasari wa Ripoti'
IsSummary: true
Date: 2020-01-15
hasboxes: true
translationKey: 
Imageauthor1: Anasuya_Sengupta.webp
Imageauthor2: Abbey_Ripstra.webp
Imageauthor3: Adele_Vrana.webp
haspdf: true
PDFsummary: /media/pdf-summary/SW-STIL-SummaryReport.pdf
hasaudio: true
Audio1: https://archive.org/download/STIL-Summary-SW/01%20Swahili.mp3
Audio2: https://archive.org/download/STIL-Summary-SW/02%20Swahili.mp3
Audio3: https://archive.org/download/STIL-Summary-SW/03%20Swahili.mp3
Audio4: https://archive.org/download/STIL-Summary-SW/04%20Swahili.mp3
Audio5: https://archive.org/download/STIL-Summary-SW/05%20Swahili.mp3
Audio6: https://archive.org/download/STIL-Summary-SW/06%20Swahili.mp3
Audio7: https://archive.org/download/STIL-Summary-SW/07%20Swahili.mp3
Audio8: https://archive.org/download/STIL-Summary-SW/08%20Swahili.mp3

---

## Kwa nini ripoti hii? Sisi ni nani?

Kamusi na sarufi hutuambia kuwa lugha ni njia iliyopangwa ya kueleza habari zinazowasiliana, haswa kati ya wanadamu.. Lakini lugha ni zaidi ya hiyo: ni urithi wa msingi tunaopeana sana sana tunaopokea kutoka mababu zetu, na ikiwa tuna bahati, urithi unaopitishwa kwa wale wanaokuja baada yetu. Tunapotafakari, tunapozungumza, tunaposikia, tunapowazia … tunatumia lugha sisi wenyewe na kila mmoja. Ni katika msingi wa uwepo wetu, na jinsi tulivyo katika ulimwengu. Ni lugha hutusaidia kusimulia hadithi kutuhusu tunavyojijua na tunavyojua kila mmoja. Unaongea lugha gani? Je, unaota kwa lugha hiyo? Je, unafikiri kwa lugha tofauti na ile unayozungumza ukiwa kazini? Je, muziki unaoupenda upo unaweza kuwa katika lugha ambayo huelewi?

Kila lugha ni mfumo wa uwepo, utendaji, na kuwasiliana duniani. Na muhimu zaidi, kujua na kuwaza. Kila moja ya lugha zetu ni mfumo wa maarifa: lugha yetu ni njia ya kimsingi ambayo tunaelewa maana ya ulimwengu wetu na kuueleza. Lugha zetu zaweza kuwa za mdomo (kutamkwa na kusainiwa), kuandikwa, au kupitishwa kupitia sauti zinazotolewa kwa [filimbi au ngoma](https://pages.ucsd.edu/~rose/Whistled%20and%20drum%20languages.pdf)! Katika fomu hizi zote, **lugha ni wakala wa maarifa**. Twaweza sema kuwa lugha ndiyo njia mwafaka ya kueleza mawazo, uaminifu, na ujuzi wetu.

Sasa zingatia lugha unazo zungumza, kufikiria, kuota au kuandika. Kati ya lugha hizo, ni ngapi unaweza kushiriki kikamilifu na kuwasiliana katika nafasi za kidijitali? Je, una uzoefu gani wa kutumia lugha au lugha nyingi mtandaoni? Je, maunzi unayotumia yana herufi za lugha yako? Je, unahitaji kurekebisha kibodi ili kuitumia pamoja na lugha yako? Unapotafuta maelezo kwa kutumia injini ya utafutaji, je, matokeo yarudi katika lugha unayotaka? Je, umelazimika kujifunza lugha tofauti na lugha yako ili kutumia na kuchangia kwenye mtandao? Ikiwa jibu lako kwa swali lolote au mengi kati ya haya ni “La“, basi wewe ni miongoni mwa watu wachache waliobahatika duniani ambao wanaweza kutumia intaneti katika lugha yako kwa urahisi. Na lugha yako labda ni… Kiingereza.

Mtandao na nafasi zake tofauti za kidijitali hutoa mojawapo ya miundo msingi muhimu zaidi ya maarifa, mawasiliano na hatua leo. Bado kukiwa na zaidi ya lugha 7000 duniani (zikiwemo lugha za mazungumzo na ishara), **ni lugha ngapi kati ya hizi tunaweza kushiriki kikamilifu mtandaoni? Je, mtandao wa jumuia ya lugha ungekuwa vipi, tunapouangalia, tunapouhisi na tunapousikia?**

Ripoti hii ni njia mojawapo ambayo tunajaribu kujibu swali hili. Sisi ni [muungano]({{<trurl>}}/about{{</trurl>}}) wa mashirika matatu: Ujuzi wa Nani? (Whose Knowledge?), Taasisi ya Mtandao ya Oxford (Oxford Internet Institute), na Kituo cha Mtandao na Jamii (The Centre for Internet and Society, India). Tulijumuika kutoa maona, uzoefu, na uchambuzi tofauti kuhusu hali ya lugha kwenye mtandao, na kwa ushirikiano na wengine wanaojali masuala haya, tunatumai kuunda mtandao, teknolojia za kidijitali na mazoea zilizo na ujumuia wa lugha.

Ripoti hii inakusudia kufanya mambo matatu:

* **Ramani ya hali ya lugha kwenye mtandao**: tunajaribu kuelewa ni lugha zipi zinazowakilishwa kwa sasa kwenye mtandao na jinsi gani. Tunafanya hivi kupitia data ya kiasi (kuangalia nambari kwenye mifumo tofauti ya dijiti, zana na nafasi), na pia data ya ubora (kujifunza kutoka kwa hadithi za watu wenyewe na uzoefu wa lugha mtandaoni).
* **Kuongeza ufahamu wa changamoto na fursa katika kufanya mtandao kuwa ujumuia wa lugha**: Kuunda na kudhibiti teknolojia, maudhui na jamii za lugha za ulimwengu kuna changamoto kubwa, na pia kuna uwezekano na fursa za kusisimua. Ripoti hii itaweka wazi baadhi ya changamoto na uwezekano huu.
* **Kuendeleza ajenda ya kuchukua hatua:** Kwa maarifa na ufahamu huu, tunatoa baadhi ya njia ambazo sisi — na wengine wengi wanaoshughulikia masuala haya duniani kote — tungependa kupanga na kuchukua hatua ili kuhakikisha mtandao unaotumia lugha nyingi zaidi.

### Ripoti hii ni nini na sio nini?

Ripoti hii ni kazi endelevu au kazi katika mchakato!

Watu wengi tofauti, jamii, na taasisi zimekuwa zikifanya kazi kwenye vipengele tofauti vya lugha kwa muda mrefu sana, na vipengele tofauti vya lugha mtandaoni, hivi majuzi. Tumehamasishwa moyo nao, lakini ripoti hii haikusudiwi kuwa uchunguzi kamili, wa kina wa kila mmoja wao na kazi yao. Pia hatujui kila mtu anayeshughulikia lugha na mtandao, ingawa tumejaribu kujumuisha wengi wa wale tunaowajua na kuhamasishwa nao kwa njia fulani au nyingine, kwa kuwajumuisha katika sehemu za [Rasilimali]({{<trurl>}}/resources{{</trurl>}}) na [Gratitude]((#toc_6_H2)).

Tumezuiliwa na takwimu tunayoweza kukusanya, na tumejadili baadhi ya vikwazo hivyo katika sehemu za [Nambari]({{<trurl>}}/numbers{{</trurl>}}). Tunakaribisha maoni na mapendekezo ya kuboresha na kusasisha maelezo ambayo tumetoa hapa, na tungependa kusikia kutoka kwa wale ambao tayari wanashughulikia masuala haya na tungependa kujumuishwa katika sasisho zijazo za ripoti hii.

Tumejitahidi tuwezavyo kuandika ripoti hii kwa mtindo unaofikika iwezekanavyo. Tunataka vizazi na jamii nyingi za watu zijiunge nasi katika kazi yetu, na hatutaki lugha ya istilahi au “kielimu” ziwe vizuizi cha kutafakari au kutenda. Tunataka pia itafsiriwe katika lugha nyingi iwezekanavyo (wafasiri: [wasiliana nasi]({{<trurl>}}/engage{{</trurl>}})!), na ingawa tuliandika baadhi ya ripoti hii kwanza kwa Kiingereza, hatutaki Kiingereza kiwe kizuizi cha kutafakari au kuchukua hatua.

Tunatumai ripoti hii itatumika kama “msingi” kwa utafiti zaidi, majadiliano na hatua juu ya maswala haya, huku tukizingatia juhudi nyingi za zamani.

### Je, sisi ni nani, na kwa nini tulikusanyika kufanya ripoti hii?

Mashirika matatu yalikuja pamoja kufanya utafiti wa ripoti hii: Kituo cha Mtandao na Jamii(The Centre for Internet and Society), Taasisi ya Mtandao ya Oxford (Oxford Internet Institute), na Ujuzi wa Nani? (Whose Knowledge?). Sote tunavutiwa na athari za mtandao na teknolojia ya dijitali kutoka kwa mitazamo tofauti ya utafiti, sera na utetezi.

Katika miaka kadha, tumekuwa tukifanya kazi kwa njia zetu wenyewe kuelewa ukosefu wa usawa wa maarifa na ukosefu wa haki kwenye mtandao: nani anachangia maudhui mtandaoni na kwa jinsi gani? Tuligundua kuwa kulikuwa na takwimu kidogo sana juu ya maarifa katika lugha tofauti kwenye mtandao. Kisha tulitaka kujua zaidi: ni kwa kiwango gani lugha za ulimwengu zimewakilishwa kwenye mtandao hivi sasa? Je, mtandao una ujumuia wa lugha? Ugunduzi wetu ulifanyika katika mipaka ya habari za umma zilizo wazi, tunatumai itakuwa mchango mwingine kwa sote ambao tunajitahidi kupata mtandao wa ujumuia wa lugha.

_Ujumbe mfupi kuhusu Uviko-19 na ripoti hii_: Tulianza kufanyia kazi ripoti hii mwaka wa 2019 kabla ya Uviko-19, lakini kazi nyingi za uchambuzi, mahojiano na uandishi ilifanyika wakati wa janga la kimataifa ambalo limebadilisha maisha yetu binafsi na kwa pamoja. Kila mtu aliyechangia ripoti hii ameathirika kwa namna fulani, na ilichukua muda mrefu zaidi ya tulivyotarajia kuishiriki na ulimwengu. Lakini Uviko-19 pia ilitusaidia kukumbuka jinsi tulivyounganishwa, jinsi ilivyo muhimu kwetu kuweza kuwasilisha mawazo changamano katika lugha tofauti, na jinsi ilivyo muhimu kuwa na miundombinu (ya kijidijatali) thabiti yenye ufikiaji iliyo kwa kweli na ujumuia wa lugha.

## Jinsi ya kusoma ripoti hii?

Ripoti hii tunaiita “dijitali kwanza“, yaani njia mwafaka ya kusoma, kusikiliza, na kujifunza ni kupitia tovuti hii, kwa sababu ripoti ina tabaka na viwango tofauti tofauti. Ripoti yetu inaleta pamoja [Nambari]({{<trurl>}}/numbers{{</trurl>}}) na [Hadithi]({{<trurl>}}/stories{{</trurl>}}). Kujifunza kuhusu hali ya lugha mtandaoni kutoka kwa mtazamo wa takwimu hutupatia masuala juujuu na hutusaidia kuelewa miktadha tofauti ambayo watu wanapitia. Lakini uzoefu wa lugha mtandaoni kwa watu kote ulimwenguni, katika mazingira tofauti, hutusaidia kujifunza kwa undani zaidi jinsi ilivyo rahisi au vigumu kwa watu kutumia mtandao katika lugha zao. Kwa hadithi na takwimu, tunaweza kuanza kushughulikia baadhi ya miktadha ya msingi, changamoto na fursa.

Hii ndiyo sababu kuna tabaka tatu kuu za ripoti hii:

* Muhtasari wa kile kilicho katika Ripoti ya Hali ya Lugha na jinsi tulivyoiunda (unayoisoma sasa hivi!)
* [Takwimu]({{<trurl>}}/numbers{{</trurl>}}) zinazochanganua masuala nyeti ya lugha katika baadhi ya mifumo ya kidijitali, programu na vifaa tunavyotumia kila siku. Marafiki wetu katika Taasisi ya Mtandao ya Oxford (Oxford Internet Institute) waliongoza kazi hii, na utapata taswira za kuvutia za takwimu na uchanganuzi hapa. Tafadhali kumbuka kuwa uchanganuzi huu ni wa nambari tuliyoweza kufikia kutoka kwa seti na nyenzo wazi na za umma. Vikwazo vingine vya mbinu vinajadiliwa kwa undani zaidi katika insha ziliyomo, lakini muhimu zaidi: ni vigumu kupata njia moja na thabiti ya kutambua lugha, na vile vile, ni vigumu kukadiria idadi ya watu wanaotumia lugha mahususi, hasa kwa sababu lugha na matumizi yake yanabadilika na nyakati.
* [Hadithi]({{<trurl>}}/stories{{</trurl>}}) zinazotupa ufahamu wa kina wa jinsi watu na jamii mbalimbali duniani kote hushiriki mtandaoni katika lugha zao wenyewe, na ugumu wa kupata taarifa wanazozihitaji katika lugha yao wenyewe. [Tulizialika](https://whoseknowledge.org/initiatives/callforcontributions/) hadithi hizi kwa maandishi na mazungumzo, kwa hivyo utapata insha za maandishi, pamoja na mahojiano ya sauti na video. Marafiki wetu wa Kituo cha Mtandao na Jamii (The Centre for Internet and Society) waliongoza kazi hii, wakiunganisha pamoja ususi wa utajiri wa uzoefu wa lugha kutoka kote ulimwenguni. Tuna michango kuhusu lugha za kiasili kama Chindali, Cree, Ojibway, Mapuzugun, Zapotec, na Arrernte kutoka Afrika, Amerika, na Australia; lugha za wachache kama Breton, Basque, Sardinian, na Karelian huko Ulaya; pamoja na lugha zinazotawala kikanda na kimataifa kama Bengali, Kiindonesia (Bahasa Indonesia) na Sinhala huko Asia, na aina tofauti za Kiarabu kote Afrika Kaskazini.

Muhimu zaidi, wachangiaji wetu wameandika au kuzungumza kwa lugha zao wenyewe na pia kwa Kiingereza, na muhtasari wetu pia umeandikwa na kuzungumzwa katika lugha tofauti, kwa hivyo tunatumai utafurahiya kusoma au kusikiliza katika lugha zaidi ya moja tu!

Pia tumejitahidi kadri tuwezavyo kuipa uhai michango hii katika umbo la kuona, kwa vielelezo dhahania na uhuishaji unaoleta pamoja vipengele tofauti vya kijamii na kiufundi vya lugha. Kama ilivyo kwa kila kitu katika ripoti yetu, hizi pia ziliandaliwa kwa ushirikiano na mchoraji wetu kwa makubaliano na wachangiaji wetu.

## Je, mtandao una ujumuia wa lugha?

Mtandao bado (na cha kusikitisha, haujakaribia) kuwa na ujumuia wa lugha kama tulionao katika maisha halisia. Tunajaribu kuelewa kina kwa kuangalia [takwimu]({{<trurl>}}/numbers{{</trurl>}}) na [uzoefu]({{<trurl>}}/stories{{</trurl>}}) wa watu duniani kote. Hapa tunakupa tu muhtasari mfupi na uchambuzi wa utajiri na kina cha kazi iliyofanywa na wachangiaji wetu kutoka kote ulimwenguni, kwa hivyo tafadhali angalia insha zao kupata undani na uhamasisho.

Kwanza tunaangalia miktadha ambayo watu wanatumia mtandao kutoka kote ulimwenguni, katika lugha tofauti. Tunaangalia njia ambazo habari na maarifa yanasambazwa, au la, katika lugha na jiografia tofauti. Kisha tunaangalia kwa kina zaidi mifumo mikuu na programu tunazotumia kuunda maudhui, kuwasiliana, na kushiriki maelezo mtandaoni, na ni lugha ngapi ambazo kila moja inaweza kutumia. Tunaangalia kwa kina Ramani za Google na Wikipedia kama hifadhi mbili za maudhui yaliyo na ujumuia wa lugha na sisi sote tunazotumia kila siku, na jinsi zinavyofanya kazi katika lugha tofauti.

Pia tunashiriki hadithi za uzoefu kutoka kwa watu wanavyoshiriki na kuchangia maarifa kwenye mtandao katika lugha zao wenyewe. Kama tulivyo jifunza, wachangiaji wengi hujipata wakihitaji kubadili lugha yao ya kwanza kwa lugha nyingine ili kushiriki na kuchangia masuala wanayothamini.

### Muktadha wa lugha: ukosefu wa usawa wa maarifa ya kijiografia na kidijitali

{{< summary-quote
    text="Lugha zenye mapokeo simulizi hazina nafasi kwenye wavuti tulionao leo."
    author="Ana Alonso"
    link="/sw/stories/dill-wlhall-on-the-web/"
>}}

{{< summary-quote
    text="Inaonekana kwetu kwamba majukwaa haya, kwa ujumla, yanaendeleza ukoloni mamboleo kwamba kuna lugha ​​zilizo na thamana zaidi na uwezo wa kuwasiliana, ambao ni mtazamo wa kugandamiza kwa lugha za wachache kama vile Mapuzugun."
    author="Mradi wa Kimeltuwe"
    link="/sw/stories/use-of-our-ancestral-language/"
>}}

Tunajua kwamba [zaidi ya 60% ya dunia](https://www.internetworldstats.com/stats.htm) sasa inakadiriwa kuunganishwa kidijitali, wengi wetu kupitia simu zetu na vifaa vya mkononi. Kati ya wale wote walio mtandaoni, robo tatu yetu wanatoka Kusini mwa Ulimwengu: kutoka Asia, Afrika, Amerika ya Kusini, Karibiani na Visiwa vya Pasifiki. Lakini ufikiaji wetu una maana na usawa kiasi gani? Je, tunaweza kuunda na kuchangia maarifa ya umma mtandaoni kwa kiwango sawa na tunavyotumia?

Katika uchunguzi wa Martin na Mark kuhusu idadi ya watu duniani ikilinganishwa na idadi ya watumiaji wa mtandao, tunaona kwamba watu fulani wana ufikiaji mwafaka kuliko wengine, zikiwemo nafasi za dijitali zinazojulikana. Kwa mfano, wengi wetu mtandaoni tunatoka Kusini mwa Ulimwengu, hatuwezi kushiriki mtandaoni kama waundaji na watayarishaji wa maarifa, kama watumiaji tu. Uhariri mwingi wa Wikipedia, akaunti nyingi kwenye Github (hazina ya msimbo), na idadi kubwa zaidi ya watumiaji wa Tor (kivinjari salama), wanatoka Ulaya na Amerika Kaskazini.

{{< summary-side-fig-fancybox
    src_small="/media/summary/STIL-Internet-regions-500px.webp"
    src_full="/media/summary/STIL-Internet-regions-1000px.webp"
    alt="Vipimo vya ushiriki wa kidijitali kulingana na maeneo ya ulimwengu. (Data: Benki ya Dunia 2019, Wikimedia Foundation 2019, Wikipedia 2018, Github 2020, Tor 2019)"
    caption="Vipimo vya ushiriki wa kidijitali kulingana na maeneo ya ulimwengu. (Data: Benki ya Dunia 2019, Wikimedia Foundation 2019, Wikipedia 2018, Github 2020, Tor 2019)"
    PDFlink="/media/pdf/STIL-Internet-regions.pdf"
>}}

Ufikiaji huu usio na usawa una matokeo gani kwa lugha? Je, sote tunaweza kufikia mtandao katika lugha zetu? Je, tunaweza kuunda maudhui na taarifa katika lugha zetu?

Kama [makadirio mengine](https://www.internetworldstats.com/stats7.htm) yanavyotuonyesha, zaidi ya 75% ya wale wanaotumia intaneti hufanya hivyo katika lugha 10 pekee — nyingi ya lugha hizo zina historia ya ukoloni wa Ulaya (Kiingereza, Kifaransa, Kijerumani, Kireno, Kihispania …) au zinatanda katika maeneo maalum ambapo lugha nyingine zinatatizika kubaki kuwa muhimu (Kichina, Kiarabu, Kirusi …). Mnamo 2020, ilikadiriwa kuwa [asilimia 25.9% ya watumiaji wote wa mtandao walikuja mtandaoni kwa Kiingereza](https://www.statista.com/statistics/262946/share-of-the-most-common-languages-on-the-internet/), wakati asilimia 19.4% walifikia intaneti kwa Kichina. Uchina ndiyo nchi yenye watumiaji wengi wa mtandao duniani kote, na unafaa kukumbuka kuwa kile tunachokiita “Kichina” si lugha moja, bali ni [familia ya lugha nyingi tofauti](https://www.oxfordhandbooks.com/view/10.1093/oxfordhb/9780199856336.001.0001/oxfordhb-9780199856336-e-1).

Cha kutia maanani ni kwamba, lugha za mtandaoni kwa sasa huenda zikawa na asili ya Ulaya, lakini Ulaya ina lugha chache zaidi duniani, ikilinganishwa na mabara mengine yote. Kati ya lugha 7000+ duniani, [zaidi ya 4000 zinatoka Asia na Afrika](https://www.ethnologue.com/guides/how-many-languages#population) (zaidi ya lugha 2000 kila moja), na Visiwa vya Pasifiki na Amerika vina zaidi ya lugha 1000 kila moja. Papua New Guinea na Indonesia ndizo [nchi zenye lugha nyingi zaidi](https://www.ethnologue.com/guides/countries-most-languages), zenye zaidi ya 800 katika PNG na zaidi ya 700 nchini Indonesia.

{{< summary-side-fig-fancybox
    src_small="/media/summary/living_languages_vs_population__pies_500px.webp"
    src_full="/media/summary/living_languages_vs_population__pies_1000px.webp"
    alt="Idadi ya lugha na jumla ya idadi ya wasemaji kulingana na eneo, kote ulimwenguni. Chanzo:"
    caption="Idadi ya lugha na jumla ya idadi ya wasemaji kulingana na eneo, kote ulimwenguni. Chanzo:"
    source="Ethnologue"
    link="https://www.ethnologue.com/guides/continents-most-indigenous-languages"
    PDFlink="/media/pdf/living_languages_vs_population__pies.pdf"
>}}

Lugha nyingi kutoka Asia ya Kusini (Kihindi, Kibengali, Kiurdu…) ziko katika [lugha 10 bora zaidi za ulimwengu](https://www.ethnologue.com/guides/ethnologue200) kwa idadi ya watu wanaozizungumza kama lugha ya kwanza (au asili), lakini hizi sio lugha ambazo watu kutoka Asia ya Kusini wanaweza kupata mtandao. Na bila shaka, tunajifunza kwa hadithi ya [Ishan]({{<trurl>}}/stories/marginality-within-marginality{{</trurl>}}), ambaye lugha yake ya kwanza ni Kibengali, hata kama unaweza kupata maudhui ya kidijitali katika lugha uliyochagua, aina ya taarifa unayotafuta inaweza kuwa haipo katika lugha hiyo; katika kesi ya Ishan, maudhui kuhusu ulemavu na haki za ujinsia. Hali katika Asia ya Kusini Mashariki — ambayo ina matumizi ya juu zaidi ya mtandao duniani na baadhi ya anuwai kubwa ya lugha — ni sawa. [Paska]({{<trurl>}}/stories/flickering-hope{{</trurl>}}) hupata masuala sawa na maudhui kuhusu haki za ngono katika Kiindonesia (Bahasa Indonesia) kama Ishan inavyopata katika Kibengali.

Pia tunajua kwamba kati ya zaidi ya lugha 7000 duniani, [takriban 4000] (https://www.ethnologue.com/enterprise-faq/how-many-languages-world-are-unwritten-0) kati ya hizo ndizo zenye mifumo ya maandishi au hati.Hata hivyo, maandishi mengi haya hayakutengenezwa na wazungumzaji wa lugha wenyewe, bali kama sehemu ya michakato mingi ya ukoloni duniani kote. Kuwa na hati haimaanishi kuwa inaeleweka au inatumiwa sana. Lugha nyingi ulimwenguni hupitishwa kwa njia ya mazungumzo au sahihi, na sio kwa maandishi . Hata katika lugha ambazo zina maandishi, uchapishaji umeelekezwa kwa lugha za kikoloni za Ulaya, na kwa kiasi kidogo, kuelekea lugha kuu za kikanda. Mnamo 2010, Google ilikadiria kuwa kulikuwa na [vitabu milioni 130 vilivyowahi kuchapishwa](https://www.pcworld.com/article/508405/google_129_million_different_books_have_been_published.html), na idadi kubwa ya vitabu hivyo vilikuwa katika lugha 480 hivi. Jarida nyingi za kitaaluma zilizoimarishwa vyema katika [sayansi](https://www.theatlantic.com/science/archive/2015/08/english-universal-language-science-research/400919/) au [sayansi ya jamii](https://www.tandfonline.com/doi/abs/10.1080/01639269.2014.904693) ziko katika Kiingereza. [Kitabu kilichotafsiriwa zaidi](https://en.wikipedia.org/wiki/Bible_translations) ulimwenguni ni Biblia (katika lugha zaidi ya 3000), na [hati iliyotafsiriwa zaidi](https://www.ohchr.org/EN/UDHR/Pages/Introduction.aspx) ulimwenguni ni Umoja wa Mataifa (United Nations) [Tamko la Kimataifa la Haki za Kibinadamu](https://en.wikipedia.org/wiki/Universal_Declaration_of_Human_Rights) (katika lugha zaidi ya 500).

Kwa nini jambo hili ni muhimu? Kwa sababu teknolojia za lugha dijitali hutegemea uchakataji wa kiotomatiki wa nyenzo zilizochapishwa katika lugha tofauti ili kuboresha usaidizi wa lugha na maudhui yao. Kwa hivyo uchapishaji wa maandishi ulimwenguni kote unapoegemea upande wa lugha fulani — na kukosa kujumuisha lugha zozote zisizoandikwa — inazidisha kutokuwepo kwa usawa wa lugha tunao. Na bila shaka, lugha zisizotegemea maandishi — zile zinazotegemea madokezo, sauti, ishara, kuigizwa — hazipo kabisa kwenye tasnia ya uchapishaji, na kwa hivyo mara nyingi pia kwa teknolojia ya lugha ya dijiti.

Kwa mfano, kama [Ana]({{<trurl>}}/stories/dill-wlhall-on-the-web{{</trurl>}}) anavyotueleza “wavuti haikuundwa kwa maslaha ya watumiaji wa lugha ​simulizi”. Katika mfumo huu wa lugha zilizoandikwa kwenye mtandao, ni vigumu kupata maudhui kutoka kwa mapokeo ya lugha simulizi na lugha ashiria. Kwa mfano, hatuwezi kutafuta kwa urahisi ishara, madokezo na filimbi. Katika mahojiano yetu na [Joel na Caddie]({{<trurl>}}/stories/signs-across-generations{{</trurl>}}), wanatuambia kuhusu seti ya kwanza ya emoji za Wenyeji za Australia zilizotengenezwa kwenye ardhi ya Arrernte huko Mparntwe/Alice Springs, na jinsi ishara ya mwili mara nyingi huunganishwa na neno linalozungumzwa ili kuleta maana katika Arrernte. [Emna]({{<trurl>}}/stories/write-in-another-language{{</trurl>}}) anasema vivyo hivyo kuhusu Tunisia, na lugha tofauti zinazozungumzwa na watu wake: “Inapokuja suala la kuhifadhi lugha, hatupaswi kuzingatia maandishi tu, tunapaswa kuifanya kwa njia za mdomo, ishara, madokezo, miluzi na kadhalika, na hiyo haiwezi kunaswa kikamilifu kwa maandishi.”

Teknolojia za kidijitali hutupatia uwezekano kama huu wa kuwakilisha wingi wa maumbo ya lugha ambayo yanatokana na maandishi, sauti, ishara na zinginezo. Pia zinaweza kutusaidia kuhifadhi na kufufua lugha ambazo ziko katika hatari ya kufa: [zaidi ya 40% ya lugha zote](https://www.endangeredlanguages.com/about/). Kila mwezi, [lugha mbili za kiasili](https://news.un.org/en/story/2019/12/1053711) na maarifa yaliyomo katika lugha hizi zinakufa na kutopotelea.

Kwa nini miktadha hii tofauti ya lugha haijawakilishwa vyema mtandaoni?

Katika insha yake, [Claudia]({{<trurl>}}/stories/decolonizing-minority-language{{</trurl>}}) anatupa nguzo tatu za kuelewa uhusiano kati ya lugha na teknolojia: upatikanaji, utumikaji, na ukusaji wa teknolojia. Kama tunavyopata katika ripoti hii, kile ambacho Claudia anakiita “lugha za ubabe” (na tunapata kuwa lugha nyingi za wakoloni wa Ulaya au lugha zinazotawala eneo) zina anuwai ya vyombo vya habari, huduma, violesura na programu zinazopatikana, ilhali lugha zingine zina upatikanaji mdogo sana, ikijumuisha katika masuala ya miundombinu kama vile kibodi, tafsiri ya mashine au utambuzi wa matamshi. Makampuni ya teknolojia pia hutumia wakati na rasilimali nyingi zaidi katika matumizi lugha za ubabe, kwasababu zinawapa faida ya kifedha kubwa zaidi. Hatimaye, anagundua kwamba teknolojia nyingi za lugha huendelezwa kupitia michakato ya juu chini kwa ushirikiano haba na jamii za lugha, au juhudi chache za kushirikisha jamii hupangwa na kuratibiwa vibaya.

Wasiwasi na changamoto hizi za muktadha pia hutupatia njia za kuunda mtandao wa lugha nyingi zaidi, na tutarejea kwenye uwezekano huu [baadaye](#toc_4_H2).

### Usaidizi wa lugha: majukwaa na programu za kutuma ujumbe

{{< summary-quote
    text="Unapoandika neno “habari ya asubuhi” kabla ya kumaliza kuandika, simu au kompyuta itakuwa tayari imependekeza neno hilo. Ninapoandika neno hilohilo katika lugha ya Chindali (“mwalamusha”), sina budi kuandika neno zima na hii inachukua muda mwingi; na itapigiwa mstari kwa sababu kompyuta au simu haiwezi kutambua neno hilo."
    author="Donald Flywell Malanga"
    link="/sw/stories/challenges-of-expressing-chindali/"
>}}

{{< summary-quote
    text="Kibodi zilizo na herufi za Kisinhala na Kitamil ni nadra. Wazazi wetu walichapisha herufi ndogo za Kisinhala, wakazikata, na kuzibandika kwenye herufi za kibodi kando ya herufi asilia za Kiingereza. Ingawa fonti nyingi za Kisinhala zimetengenezwa, hakuna zinazofanya kazi kama vile fonti za Unicode."
    author="Uda Deshapriya"
    link="/sw/stories/amidst-virtual-impunity/"
>}}

{{< summary-quote
    text="Ikiwa programu maarufu na violesura muhimu vya programu hazitatolewa hivi karibuni kwa Kibretoni, na haziwezi kushindana na programu za Kifaransa, lugha hiyo itaonekana kuwa ya kutovutia kwa vizazi vichanga."
    author="Claudia Soria"
    link="/sw/stories/decolonizing-minority-language/"
>}}

Tulichunguza kwa kina kuelewa jinsi mtandao bado hauna ujumuia wa lugha kama ulimwengu tunaoishi. Tuliangalia ni aina gani ya usaidizi wa lugha — yaani violesura vya watumiaji katika lugha tofauti— mifumo mikuu ya kidijitali na programu hutupatia kwa kuwasiliana, kuunda na kushiriki maudhui katika lugha zetu.

{{< summary-gallery-fancybox
    caption_all="Kiolesura cha Wikipedia katika lugha nyingi."

    src_small1="/media/summary/Bengali-Wikipedia-500px.webp"
    src_full1="/media/summary/Bengali-Wikipedia-1000px.webp"
    caption1="Kiolesura cha Wikipedia katika Kibengali."

    src_small2="/media/summary/Breton-Wikipedia-500px.webp"
    src_full2="/media/summary/Breton-Wikipedia-1000px.webp"
    caption2="Kiolesura cha Wikipedia katika Kibretoni."

    src_small3="/media/summary/English-Wikipedia-500px.webp"
    src_full3="/media/summary/English-Wikipedia-1000px.webp"
    caption3="Kiolesura cha Wikipedia katika Kingereza."

    src_small4="/media/summary/Spanish-Wikipedia-500px.webp"
    src_full4="/media/summary/Spanish-Wikipedia-1000px.webp"
    caption4="Kiolesura cha Wikipedia katika Kihispania."

    src_small5="/media/summary/Zapotec-Wikipedia-500px.webp"
    src_full5="/media/summary/Zapotec-Wikipedia-1000px.webp"
    caption5="Kiolesura cha Wikipedia katika Zapoteco."
>}}

[Martin na Mark]({{<trurl>}}/numbers/a-platform-survey{{</trurl>}}) walichanganua usaidizi wa lugha kwa tovuti 11, programu 12 za Androidi na programu 16 za iOS. Walichagua majukwaa yanayotumika sana ambayo yana utaalam katika kukusanya na kushiriki maarifa, haswa yale ambayo yanatafuta kuwa na uwepo na hadhira ulimwenguni kote. Pia iliwabidi kutegemea takwimu za umma za programu hizi zilizoegemea kwa maandishi, violesura na yaliyomo zaidi ya usimulizi na sauti.

Majukwaa yaliwekwa katika makundi manne makubwa (kwa kutambua kwamba yanaingiliana):

* **Ufikiaji wa maarifa** (hifadhi za maarifa na habari, ikijumuisha injini za utafutaji): Ramani za Google, Tafuta na Google, Wikipedia, YouTube.
* **Kujifunza lugha** (jukwaa za kujifunza lugha kwa mwongozo binafsi): DuoLingo, na hifadhi za elimu Coursera, Udacity, Udemy.
* **Mitandao ya kijamii** (mitandao ya kijamii inayoelekea umma): Facebook, Instagram, Snapchat, TikTok, Twitter.
* **Kutuma ujumbe** (ujumbe wa kibinafsi na wa kikundi): imo, KakaoTalk, LINE, LINE Lite, Messenger, QQ, Signal, Skype, Telegram, Viber, WeChat, WhatsApp, Zoom.

{{< summary-side-fig-fancybox
    src_small="/media/data-survey/PS_Figure_1-platform_summary-500px.webp"
    src_full="/media/data-survey/PS_Figure_1-platform_summary-1000px.webp"
    alt="Idadi ya lugha za kiolesura zinazotumika kwa kila jukwaa, kulingana na kategoria ya jukwaa."
    caption="Idadi ya lugha za kiolesura zinazotumika kwa kila jukwaa, kulingana na kategoria ya jukwaa."
    PDFlink="/media/pdf/PS_Figure_1-platform_summary.pdf"
>}}

Tuligundua kuwa usaidizi wa lugha inayotegemea maandishi unasambazwa kwa usawa sana katika mifumo tofauti ya kidijitali. Mifumo mikuu ya wavuti kama Wikipedia, Tafuta na Google, na Facebook hutoa usaidizi wa lugha zaidi kwa sasa. Cha kufurahisha, Wikipedia (ambayo ni shirika lisilo la faida, na kuhaririwa na wafanyakazi wa kujitolea kutoka duniani kote) ndilo jukwaa lililotafsiriwa kwa mapana zaidi kufikia sasa. Wikipedia inaauni zaidi ya lugha 400 na kiolesura cha msingi cha mtumiaji, huku Wikipedia katika lugha zipatazo 300 ikiwa na angalau makala 100. Utafutaji wa Google unaweza kutumia lugha 150, wakati Facebook inaauni lugha 70-100 zinazotumika. Signal huongoza programu za kutuma ujumbe zenye takriban lugha 70 zinazotumika kwenye Androidi na 50 kwenye iOS. Kwa upande mwingine, majukwaa mengi yanalenga tu usaidizi wao wa lugha kwenye idadi ndogo ya lugha zinazozungumzwa na watu wengi, hivyo basi lugha nyingi hazitumiki. Programu ya mjumbe QQ, kwa mfano, inasaidia Kichina pekee.

Idadi ndogo ya lugha zinazotumiwa na mifumo mingi iliyochunguzwa ni pamoja na lugha za Ulaya kama vile Kiingereza, Kihispania, Kireno na Kifaransa, na baadhi ya lugha za Asia kama vile Mandarin Kichina, Kiindonesia, Kijapani na Kikorea. Lugha kuu kama vile Kiarabu na Kimalei hazitumiki ipasavyo, na lugha nyingine zinazozungumzwa na mamia ya mamilioni hazijawakilishwa vyema katika suala la usaidizi wa kiolesura.

Je, ukosefu huu wa usaidizi wa lugha unamaanisha nini kwa watu wengi kote ulimwenguni? Mnamo 2021, tunakadiria takriban [binadamu bilioni 7.9](https://www.worldometers.info/) kwenye sayari yetu, ambao wengi wao wanaishi Asia (karibu bilioni 4.7) na Afrika (karibu bilioni 1.4). Hata hivyo sehemu kubwa ya dunia haitumikiwi na lugha za mtandao:

* _Watu wanaozungumza lugha za Kiafrika_: idadi kubwa ya lugha za Kiafrika hazitumiki kama lugha ya kiolesura na jukwaa lolote lililofanyiwa utafiti, na kwa sababu hiyo zaidi ya 90% ya Waafrika wanahitaji kubadili lugha ya pili ili kutumia jukwaa – kwa wengi hii inaweza kumaanisha lugha ya ukoloni au lugha inayotawala zaidi katika eneo lao.
* _Watu wanaozungumza Lugha za Asia Kusini_: Huko Asia Kusini, karibu nusu ya majukwaa yaliyofanyiwa utafiti hayatoi usaidizi wa kiolesura kwa lugha yoyote ya kieneo, na hata lugha kuu za Asia Kusini kama vile Kihindi na Kibengali, zinazozungumzwa na mamia ya mamilioni ya watu, hazitumiki sana kuliko lugha nyinginezo.
* _Watu wanaozungumza lugha za Asia ya Kusini-Mashariki_: Usaidizi wa lugha za Kusini-Mashariki mwa Asia umechanganywa vile vile. Ingawa Kiindonesia, Kivietinamu na Kithai huelekea kuungwa mkono vyema na mifumo tuliyochunguza, lugha nyingine nyingi za Kusini-Mashariki mwa Asia hazitumiwi na mifumo mingi tuliyochunguza.

Matokeo ya Martin na Mark yanaimarishwa na hali halisia ya kila siku ya wale wanaoishi katika maeneo haya ya dunia. [Donald]({{<trurl>}}/stories/challenges-of-expressing-chindali{{</trurl>}}) kutoka Malawi aligundua, kwa mfano, kwamba alipowauliza wazungumzaji wa Chindali, lugha ya Kibantu iliyokuwa hatarini kutoweka, jinsi wanavyowasiliana kupitia simu zao, wote walieleza jinsi lugha ya Chindali inavyotumia muda mwingi, kwani simu zao nyingi zilipachikwa na usaidizi wa lugha katika Kiingereza, Kifaransa, au Kiarabu na hazikumtambua Chindali. Changamoto hizi za kiteknolojia, bila shaka, ni pamoja na vikwazo vya kiuchumi na kijamii vinavyozuia uwezo wa wazungumzaji wa Chindali kununua simu mahiri au mipango ya data. Hata kwa wale wanaotumia lugha ya taifa ya Malawi, Chichewa, ukosefu wa usaidizi wa lugha hufanya iwe vigumu: “Kwa nini ninunue simu ya bei ghali au nipoteze muda wangu kwenda kupata mtandao wakati lugha ni Kiingereza ambayo sielewi?”

Kwa kweli, ukosefu wa usaidizi wa lugha kwa lugha nyingi za Kiafrika ulibainika sana mnamo 2018, wakati [Twitter ilitambua Kiswahili kwa mara ya kwanza](https://www.theafricancourier.de/culture/swahili-makes-history-as-first-african-language-recognized-by-twitter/), lugha inayozungumzwa na zaidi ya watu milioni 50-150 kote Afrika Mashariki na kwingineko (kama lugha ya kwanza au ya pili). Hadi wakati huo, Kiswahili na lugha nyingine nyingi za Kiafrika zilirejelewa kuwa Kiindonesia kwenye jukwaa. Utambuzi wa maneno ya Kiswahili na usaidizi wa tafsiri haukuanzishwa na kampuni hio ya teknolojia; kwa hakika ilikuwa ni matokeo ya kampeni ya watumiaji wanaozungumza Kiswahili wa Twitter.

Hali si bora zaidi katika Amerika ya Kusini kwa lugha za kiasil. Katika mahojiano na mradi wa Kimeltuwe unaoshughulikia Mapuzugun, inayozungumzwa na watu wa Mapuche kote Chile na Argentina ya sasa, walisema kwamba, “Itakuwa vyema kuweza kuchapisha katika Mapuzugun kwenye majukwaa kama YouTube au Facebook. Sio hata kwa suala la kiolesura kinachotafsiriwa, lakini kwa suala la kuweza kuweka alama tu, kwenye menyu zinazopatikana, lugha kama Mapuzugun. Kwa mfano, unapopakia video kwenye YouTube au Facebook, huwezi kuongeza manukuu katika Mapuzugun kwa kuwa haionekani katika orodha ya lugha zilizoamuliwa mapema. Kwa hivyo, ikiwa unataka kupakia nakala katika Mapuzugun, lazima useme kwamba iko katika Kihispania au Kiingereza.”

Martin na Mark hawakuchanganua usaidizi wa lugha kwenye vifaa maalum, kama vile simu za rununu, lakini tunajua kuwa kibodi za kidijitali ni moja wapo ya nafasi chache muhimu ambazo wanaisimu na wanatekinolojia wamepiga hatua zaidi. Gboard, kibodi ya Google ya simu mahiri ya mfumo wa uendeshaji wa Android, kwa mfano, inaweza kutumia [zaidi ya aina 900 za lugha](https://arxiv.org/pdf/1912.01218.pdf) na inategemea kazi muhimu na jumuiya na wasomi wa lugha mbalimbali. Bado tunaweza kufikia kibodi ya simu mahiri na uwezo huu, tu kwa kumudu simu mahiri ya hali ya juu.

Wakati huo huo, [uzoefu wa Uda na Kisinhala]({{<trurl>}}/stories/flickering-hope{{</trurl>}}) — lugha inayozungumzwa na zaidi ya watu milioni 20 nchini Sri Lanka kama lugha ya kwanza au ya pili — inaonyesha kuwa bado ni vigumu kuunda maudhui katika lugha ambayo maandishi yake hayaeleweki kwa urahisi na baadhi ya wanateknolojia wanaofanya kazi ya usaidizi wa lugha, hasa ikiwa fomu ni tofauti sana na hati ya Kilatini ya lugha za Magharibi mwa Ulaya. Anasema, “Suala kuu la Unicode Sinhala linahusiana na mpangilio ambao herufi tofauti zinahitaji kuingizwa ili kuunda herufi. Agizo hili linahitaji ufuate konsonanti zilizo na viambajengo. Mawazo haya yanafuata sheria katika Lugha za Ulaya kulingana na maandishi ya Kilatini. Walakini, katika Kisinhali, wakati mwingine diacritics hutangulia konsonanti.”

[Unicode](https://home.unicode.org/) ni kiwango cha teknolojia cha kusimba maandishi ambacho kinaonyeshwa katika mfumo wa uandishi wa lugha au hati. Toleo la 13 lina [herufi 143,859](https://www.unicode.org/faq/basic_q.html) kwa zaidi ya mifumo 30 ya uandishi inayotumika leo, kwa kuwa mfumo huo wa uandishi unaweza kutumika kwa zaidi ya lugha moja (kwa mfano, hati ya Kilatini kwa lugha nyingi za Ulaya Magharibi, hati ya Han ya lugha za Kijapani, Kichina na Kikorea, na hati ya Devanagiri kwa lugha tofauti za Asia Kusini). Pia ina herufi za hati za kihistoria za lugha ambazo hazitumiki kwa sasa. Unicode Consortium (shirika lisilo la faida lililo California) pia huamua kuhusu [emojis](https://home.unicode.org/emoji/about-emoji/) — ishara tunazotumia kila siku kupitia violesura tofauti.

[Utafiti wa Martin na Mark]({{<trurl>}}/numbers/a-platform-survey{{</trurl>}}) kuhusu usaidizi wa lugha na uzoefu wa [wachangiaji]({{<trurl>}}/stories{{</trurl>}}) wengine kutoka duniani kote, yote yanaongeza maelezo zaidi kwa maelezo haya mafupi ya usaidizi mdogo na usio na usawa wa kiufundi kwa lugha nyingi kwenye mifumo na programu kwa sasa. Yasome!

### Maudhui ya lugha: upatikanaji na uzalishaji

{{< summary-quote
    text="Maudhui ya ufeministi hayapatikani haswa katika lugha za kienyeji. Wakfu wa Maendeleo ya Wanawake (Women’s Development Foundation) ni kikundi cha wanawake wa vijijini ambao wamekuwa wakifanya kazi katika masuala ya haki za wanawake tangu 1983. Lakini ilikuwa mwaka wa 2019 pekee ambapo tulianza kushiriki maudhui ya wanawake wa lugha ya Kisinhala kuhusu masuala ya kijamii na kisiasa na kiuchumi mtandaoni."
    author="Uda Deshapriya"
    link="/sw/stories/amidst-virtual-impunity/"
>}}

{{< summary-quote
    text="Kwa bahati mbaya, ilikuwa--na bado ni--vigumu sana kupata maudhui ya kielimu na chanya katika Bahasa Indonesia kwenye mtandao... Tukitafuta “LGBT” au “homoseksualitas”  kwenye Google--injini ya utafutaji kubwa na maarufu--tutapata matokeo mengi sana yenye neno hili “penyimpangan” (kupotoka), “dosa” (dhambi), na “penyakit” (ugonjwa)."
    author="Paska Darmawan"
    link="/sw/stories/flickering-hope/"
>}}

{{< summary-quote
    text="Taarifa juu ya makutano ya ukuchu na ulemavu (au hata kutokuwepo kwake) ambayo inapatikana katika Kibengali kwenye mtandao kwa kiasi kikubwa imeundwa na kwa upande wake muhimu katika kuunda chuki ya ushoga na ubabe wa uwezo."
    author="Ishan Chakraborty"
    link="/sw/stories/marginality-within-marginality/"
>}}

Tulitaka kuelewa maudhui kwenye mtandao kwa kuchanganua ni toleo gani la dunia na maarifa tunayopata, pindi tunapokuwa mtandaoni maanake [zaidi ya 63% ya tovuti zote](https://w3techs.com/technologies/overview/content_language) hutumia Kiingereza kama lugha yao ya msingi ya maudhui.

Katika insha na mahojiano yao, wachangiaji wetu wanajadili makundi mbalimbali ya changamoto za kihistoria, kijamii na kisiasa, kiuchumi na kiteknolojia katika kufikia mtandao ipasavyo katika lugha zao. Zaidi ya hayo wote hushughulika na changamoto katika kutafuta maudhui muhimu kwenye mtandao katika lugha zao, na katika kuunda maudhui ambayo yana maana kwao katika lugha hizi. Hii ni kusema kuwa haitoshi kwetu kuweza kupata taarifa na maarifa yaliyoundwa kwa ajili yetu katika lugha nyingine na wale ambao huenda hawaelewi mazingira na uzoefu wetu, na mbaya zaidi, kuwachukia. Tunahitaji kuwa na uwezo wa kuzalisha maarifa ya maana kwa ajili yetu wenyewe na jamii zetu, au, angalau, kuwa na uwezo wa kusaidia uzalishaji na upanuzi wa maudhui haya katika lugha zetu zote tofauti tofauti.

Hii ni kweli hasa kwa wale ambao wana matatizo ya ufikivu, na kwa wale wanaokumbwa na aina tofauti za utengaji na kutengwa.

Kama [Joel]({{<trurl>}}/stories/signs-across-generations{{</trurl>}}) anavyotueleza kwenye mahojiano kuhusu mradi wa Indigemoji, ilianza na kufhadhaishwa na Twiti iliyochanganyikiwa yeye kutoka kwa gari lake na kuliegemeza kando ya barabara, na kuanza kuweka maneno ya Arrernte dhidi ya emojis kuelezea maana yake. Kwa miongo kadhaa baada ya emojis kutambulishwa kwa mara ya kwanza kwenye mtandao, Mataifa ya Kwanza au watu wa kiasili walikuwa wameomba bila mafanikio kutumiwa kueleza lugha zao za simulizi na za kuona, kama vile Arrernte. Kama tulivyosema awali, Muungano wa Unicode hujadili maombi ya umma ya emoji mpya, na maombi kama vile emoji ya Bendera ya Wenyeji wa Australia [yalikataliwa](https://unicode.org/emoji/emoji-requests.html). Kwa Joel, Caddie na wengine, mradi wa Indigemoji ukawa juhudi za vizazi vingi kusukuma nyuma aina hizi nyingi za kutengwa kimwili na kiuhalisia, na kuunda maudhui yao wenyewe kwa njia ambazo zilikuwa na maana kwa utambulisho na lugha zao za Asilia.

{{< summary-side-fig
    src="/media/summary/Indigemoji-tweet.webp"
    alt="Tweet iliyo na orodha ya emoji na maneno ya Arrernte karibu nayo. Chanzo:"
    caption="Tweet iliyo na orodha ya emoji na maneno ya Arrernte karibu nayo. Chanzo:"
    source="Indigemoji."
    link="www.indigemoji.com.au"
>}}

Ni muhimu kukumbuka kuwa lugha za kiasili ni lugha za “wachache” katika ulimwengu wetu leo, kwa sababu ya historia ya mauaji ya halaiki kupitia ukoloni ambapo Mataifa ya Asili yaliharibiwa au kuwa idadi ya watu wachache baada ya kuwa wakaaji wakuu wa eneo au ardhi fulani. Na taratibu hizi za ukoloni pia huathiri lugha tawala, zinazozungumzwa na mamilioni duniani kote.

[Ishan]({{<trurl>}}/stories/marginality-within-marginality{{</trurl>}}) ni mwanachuoni kuchu, mlemavu wa macho ambaye kuwa mtandaoni yenyewe ni juhudi dhidi ya uwezekano wowote. Kisha anajitahidi kupata taarifa muhimu katika Kibengali kuhusu ulemavu, kuhusu uwepo kuchu, na hata zaidi, kwa makutano ya masuala haya. Hii inasababisha kile anachokiita 'upungufu ndani ya ukingo': “kwa upande mmoja, mitazamo ya chuki ya ushoga na ubabe wa uwezo wa jamii, na kwa upande mwingine, chuki ya nafsi ya watu na/au ubabe wa uwezo wa watu binafsi (wa uwepo kuchu na/au walemavu) — kwa pamoja, hali hizi za nyongeza zinaendeleza utaratibu wa kutengwa. Eneo la kijamii la mtu mlemavu na wa jamii ya upinde linaweza kuelezewa kama 'utengaji ndani ya utengaji.’”

Kwa zaidi, michakato muhimu ya ufikiaji na habari hata katika lugha kuu kama Kibengali — inayozungumzwa na karibu milioni 300 ulimwenguni kote — haipo kwenye mtandao.

Martin na Mark waliamua kuzidi kuchanganua kwa kina kiwango na aina ya yaliyomo katika lugha tofauti, kwenye aina mbili tofauti za majukwaa ya habari na maarifa: Ramani za Google na Wikipedia.

#### Ramani za Google

Je, tunaweza kufikia Ramani za Google katika lugha zetu nyingi tofauti? Je, lugha tunayotumia inabadilisha toleo la ulimwengu tunaloona kupitia Ramani za Google?

Ili kujibu maswali haya, Martin na Mark walikusanya data kuhusu [Ramani za Google]({{<trurl>}}/numbers/google-maps-language-geography{{</trurl>}})’ maudhui ya kimataifa katika lugha 10 zinazozungumzwa na watu wengi zaidi: Kiingereza, Mandarin Kichina, Kihindi, Kihispania. , Kifaransa, Kiarabu, Kibengali, Kirusi, Kireno, na Kiindonesia (Bahasa Indonesia). Walikusanya makumi ya mamilioni ya matokeo ya utafutaji ya kibinafsi katika lugha hizi, na kati ya hizi walitambua na kuweka ramani karibu maeneo milioni tatu ya kipekee (mahali na maeneo mengine).

Haishangazi kuwa ramani huwa na maudhui mengi tunapofikia Ramani za Google kwa Kiingereza. Ramani ya Google ya lugha ya Kiingereza inashughulikia ulimwengu, ingawa ni mnene zaidi (yaani, ina habari zaidi) katika Ulimwengu wa Kaskazini, ikilenga Ulaya na Amerika Kaskazini. Pia inashughulikia Asia ya Kusini na sehemu za Kusini-Mashariki mwa Asia, pamoja na sehemu kubwa za Amerika ya Kusini vizuri kiasi. Ingawa, kwa kulinganisha, sehemu nyingi za Afrika ni chache katika maudhui.

{{< summary-side-fig-fancybox
    src_small="/media/data-survey/GM_Figure_1-English-500px.webp"
    src_full="/media/data-survey/GM_Figure_1-English-1000px.webp"
    alt="Msongamano wa taarifa wa Ramani za Google kwa wazungumzaji wa Kiingereza. Utiaji meusi zaidi huonyesha ambapo matokeo ya utafutaji yanajumuisha idadi kubwa ya maeneo."
    caption="Msongamano wa taarifa wa Ramani za Google kwa wazungumzaji wa Kiingereza. Utiaji meusi zaidi huonyesha ambapo matokeo ya utafutaji yanajumuisha idadi kubwa ya maeneo."
    PDFlink="/media/pdf/GM_Figure_1-English.pdf"
>}}

Ikilinganishwa na ramani ya Kiingereza ambayo imeshugulikiwa vyema, tunapata kwamba Kibengali (ambayo ni lugha ya kwanza ya) [Ishan]({{<trurl>}}/stories/the-unseen-story{{</trurl>}}) iko katika hali nyingine iliokithiri — huduma yake inatumika zaidi Asia Kusini, hasa India na Bangladesh, na Ramani za Google hazina maudhui yoyote kwa wazungumzaji wa Kibengali katika sehemu nyingi za dunia. Ili kugundua maudhui ya ziada, na kusafiri katika maeneo nje ya India na Bangladesh, wasemaji wa Kibengali wanahitaji kubadili hadi lugha ya pili kama vile Kiingereza. Hii pia ni kweli kwa Ramani za Google katika Kihindi (lugha ya tatu inayozungumzwa zaidi duniani, baada ya Kiingereza na Mandarini Kichina).

{{< summary-side-fig-fancybox
    src_small="/media/data-survey/GM_Figure_2-Bengali-500px.webp"
    src_full="/media/data-survey/GM_Figure_2-Bengali-1000px.webp"
    alt="Msongamano wa taarifa wa Ramani za Google kwa wazungumzaji wa Kibengali. Utiaji meusi zaidi huonyesha ambapo matokeo ya utafutaji yanajumuisha idadi kubwa ya maeneo."
    caption="Msongamano wa taarifa wa Ramani za Google kwa wazungumzaji wa Kibengali. Utiaji meusi zaidi huonyesha ambapo matokeo ya utafutaji yanajumuisha idadi kubwa ya maeneo."
    PDFlink="/media/pdf/GM_Figure_2-Bengali.pdf"
>}}

Mengi zaidi kwenye Ramani za Google katika lugha tofauti katika [insha ya kina ya Martin na Mark]({{<trurl>}}/numbers/google-maps-language-geography{{</trurl>}}).

#### Wikipedia

Kama vile Martin na Mark [utafiti wa jukwaa]({{<trurl>}}/numbers/a-platform-survey{{</trurl>}}) inavyotuonyesha, Wikipedia iko mstari wa mbele katika usaidizi wa lugha kwenye mtandao, huku kiolesura chake kikiwa kimetafsiriwa katika lugha nyingi zaidi kuliko majukwaa yoyote ya kibiashara. tuliangalia, ikiwa ni pamoja na Google na Facebook.

Kwa upande wa maudhui halisi — habari na maarifa katika makala za Wikipedia — Wikipedia ina zaidi ya matoleo 300 ya lugha. Bado wazungumzaji wa lugha hizi hawapati ufikiaji wa maudhui sawa, au kiasi sawa cha habari. Tulitaka kuingia ndani zaidi katika kuuliza na kujibu: je, utangazaji wa maudhui wa Wikipedia katika matoleo yake ya lugha ni mzuri kiasi gani? Je, baadhi ya lugha zina uwakilishi mzuri zaidi kuliko nyingine? Je, jumuiya fulani za lugha zinaweza kufikia maudhui zaidi kuliko nyingine? Tulijibu baadhi ya maswali haya kwa kina katika [uchambuzi wa Wikipedia wa Martin na Mark]({{<trurl>}}/numbers/wikipedia-language-geography{{</trurl>}}).

Tulitumia takwimu za 2018 na tagi za kijiografia (njia ya kupachika marejeleo ya kijiografia kama vile viwianishi ndani ya makala ya Wikipedia), na tukachanganua idadi ya makala na ukuaji wa maudhui katika lugha tofauti. Pia tuliegemeza uchanganuzi wetu kwenye lugha za “kienyeji“, tukifafanua hizi kuwa lugha ambazo ama zimeainishwa kama lugha rasmi katika [Unicode CLDR](https://cldr.unicode.org/) (msimbo unaofadhili huduma ya lugha kwenye mtandao), au zinazotumiwa na angalau 30% ya watu katika nchi yoyote.

Kisha tukatambua lugha ya kienyeji iliyoenea zaidi, yaani, inayozungumzwa na idadi kubwa zaidi ya watu katika kila nchi. Tulipata lugha 73 kama hizo, ambazo zimeenea zaidi katika angalau nchi moja. Kiingereza kinazungumzwa na watu wengi zaidi, na ndiyo lugha inayoenea zaidi katika nchi 34. Inafuatwa na Kiarabu na Kihispania (nchi 18), Kifaransa (nchi 13), Kireno (nchi saba), Kijerumani (nchi nne), na Uholanzi (nchi tatu). Kichina, Kiitaliano, Kimalei, Kiromania, Kigiriki, na Kirusi ndizo lugha zinazoenea zaidi katika nchi mbili, huku lugha 60 zilizosalia zikiwa zimeenea zaidi katika nchi moja.

Ili kulinganisha usambazaji huu wa lugha ya ndani na maudhui ya Wikipedia katika kila nchi, tulitambua toleo la lugha ya Wikipedia na idadi kubwa zaidi ya makala kuhusu nchi hiyo. Tulipata upendeleo kwa maudhui ya lugha ya Kiingereza. Kiingereza ndiyo lugha kuu ya Wikipedia katika nchi 98, ikifuatiwa na Kifaransa (nchi tisa), Kijerumani (nchi nane), Kihispania (nchi saba), Kikatalani na Kirusi (nchi nne), Kiitaliano na Kiserbia (nchi tatu), na Kiholanzi, Kigiriki. , Kiarabu, Kiserbo-kroatia, Kiswidi, na Kiromania (nchi mbili). Lugha 21 zilizobaki za Wikipedia zimeenea zaidi katika nchi moja.

Ingawa [idadi ya makala katika kila lugha Wikipedia](https://en.wikipedia.org/wiki/List_of_Wikipedias) inabadilika na inaendelea kukua, ni wazi kwamba matoleo ya lugha ya Wikipedia yanatofautiana sana kwa ukubwa na ukuzaji — kwa idadi ya makala, na kwa ukubwa wa jumuiya zao za wahariri. Wikipedia ya Kiingereza ndiyo kubwa zaidi kufikia sasa, ikiwa na makala zaidi ya milioni sita na karibu wachangiaji milioni 40 waliosajiliwa. Jumuiya za wachangiaji zinazofuata ni matoleo ya Wikipedia ya Kihispania, Kijerumani na Kifaransa, kila moja ikiwa na wachangiaji kati ya milioni nne na sita, na takriban nakala milioni mbili. Matoleo ya lugha yaliyosalia ni madogo kwa kulinganisha: ni takriban matoleo 20 tu ya lugha ambayo yana makala zaidi ya milioni moja, na 70 pekee ndiyo yana zaidi ya makala 100,000. Matoleo mengi ya lugha ya Wikipedia yana sehemu ndogo tu ya yaliyomo katika Wikipedia ya Kiingereza.

{{< summary-side-fig-fancybox
    src_small="/media/data-survey/WP_Figure_2-English-500px.webp"
    src_full="/media/data-survey/WP_Figure_2-English-1000px.webp"
    alt="Msongamano wa taarifa wa Wikipedia ya Kiingereza mwanzoni mwa 2018. Kivuli cheusi kinaonyesha idadi kubwa ya makala zilizowekwa alama za kijiografia."
    caption="Msongamano wa taarifa wa Wikipedia ya Kiingereza mwanzoni mwa 2018. Kivuli cheusi kinaonyesha idadi kubwa ya makala zilizowekwa alama za kijiografia."
    PDFlink="/media/pdf/WP_Figure_2-English.pdf"
>}}

{{< summary-side-fig-fancybox
    src_small="/media/data-survey/WP_Figure_3-ar,_bn,_hi,_es-500px.webp"
    src_full="/media/data-survey/WP_Figure_3-ar,_bn,_hi,_es-1500px.webp"
    alt="Msongamano wa taarifa za Wikipedia za Kiarabu, Kibengali, Kihindi na Kihispania mapema mwaka wa 2018. Kivuli cheusi kinaonyesha idadi kubwa ya makala zilizowekwa alama za kijiografi."
    caption="Msongamano wa taarifa za Wikipedia za Kiarabu, Kibengali, Kihindi na Kihispania mapema mwaka wa 2018. Kivuli cheusi kinaonyesha idadi kubwa ya makala zilizowekwa alama za kijiografi."
    PDFlink="/media/pdf/WP_Figure_3-ar,_bn,_hi,_es.pdf"
>}}

Kinachosisimua ni jinsi yaliyomo katika lugha zote za Wikipedia yanaangazia usambazaji sawa katika Ramani za Google tulizoona hapo awali.

Hii pia ni kweli tunapoangalia idadi ya makala katika Wikipedia za lugha tofauti kwa kulinganisha na idadi ya wazungumzaji wa lugha hizo (kama lugha ya kwanza na ya pili). Tunaona kwamba katika lugha za Ulaya kama vile Kiingereza, Kifaransa, Kihispania, Kirusi na Kireno, idadi ya makala katika Wikipedia hizo inalingana na idadi ya wazungumzaji. Lakini hii si kweli kwa lugha nyingine zinazozungumzwa na watu wengi: Kichina cha Mandarin, Kihindi, Kiarabu, Kibengali, na Kiindonesia (Bahasa Indonesia) kila moja inazungumzwa na mamia ya mamilioni ya watu, lakini matoleo yao ya Wikipedia ni madogo zaidi, na idadi ndogo ya makala ikilinganishwa. kwa matoleo katika lugha za Ulaya. Kuna makala nyingi zaidi katika Wikipedia za Kifaransa, Kihispania au Kireno kuliko matoleo ya Mandarin, Kihindi au Kiarabu, ingawa haya yako katika [lugha tano zinazozungumzwa](https://en.wikipedia.org/wiki/List_of_languages_by_total_number_of_speakers) ulimwenguni, na wazungumzaji wengi kuliko Kifaransa na Kireno.

{{< summary-side-fig-fancybox
    src_small="/media/data-survey/WP_Figure_1-language_ranking-500px.webp"
    src_full="/media/data-survey/WP_Figure_1-language_ranking-1000px.webp"
    alt="Maudhui ya Wikipedia na idadi ya wazungumzaji kwa lugha 10 zinazozungumzwa na watu wengi zaidi duniani. (Kadirio la idadi ya watu: Ethnologue 2019, ambayo inajumuisha wasemaji wa lugha ya pili.)"
    caption="Maudhui ya Wikipedia na idadi ya wazungumzaji kwa lugha 10 zinazozungumzwa na watu wengi zaidi duniani. (Kadirio la idadi ya watu: Ethnologue 2019, ambayo inajumuisha wasemaji wa lugha ya pili.)"
    PDFlink="/media/pdf/WP_Figure_1-language_ranking.pdf"
>}}

[Insha ya Martin na Mark]({{<trurl>}}/numbers/wikipedia-language-geography{{</trurl>}}) ina uchanganuzi zaidi na taswira za data kwenye Wikipedia katika lugha tofauti, lakini kinachoonekana wazi kutoka kwa takwimu hizi ni kinadhihirishwa na uhalisia wa maisha ya wachangiaji wetu kote ulimwenguni.

Ungandamizaji na kutengwa kwa lugha ambazo kimsingi si lugha za kikoloni za Ulaya ziko katika ulimwengu wa kawaida na mtandaoni, pamoja na lugha zinazotawala na za kimataifa kama Kiarabu. Ili kuandika Wikipedia katika lugha ya mtu binafsi, kwa kutumia marejeleo ambayo yanatokana na muktadha wa lugha hiyo, tunahitaji vyanzo vya kuaminika na vya kina vilivyochapishwa, ambavyo (kama tulivyopata hapo awali) ni nadra katika lugha nyingi za ulimwengu. Kama mwandishi wa Wikipedia, [Emna]({{<trurl>}}/stories/write-in-another-language{{</trurl>}}) anaeleza jinsi ilivyo vigumu kupata rasilimali na marejeleo katika lugha tofauti barani Afrika: “...Jitihada kwa mfano kwangu kama mwandishi wa Wikipedia kutafuta marejeleo katika lugha zetu wenyewe, ninaposema lugha zetu wenyewe, sio tu lugha ya Tunisia, lahaja yetu, au lugha ya Kiarabu lakini pia tunapotembea Afrika nzima tunapata pengo kubwa kwa upande wa rasilimali na marejeleo.”

Hata huko Uropa, wazungumzaji wa lugha za wachache wanatatizika kutumia au kuhariri Wikipedia katika lugha zao. Ingawa [Claudia]({{<trurl>}}/stories/decolonizing-minority-language{{</trurl>}}) aligundua kwamba wengi wa waliojibu katika Kibretoni walijua kuhusu kuwepo kwa Wikipedia ya Kibretoni, huku “19% yao hata wakichangia kwa kuhariri makala zilizopo au. kuandika mpya (8%),” anahisi kwamba wazungumzaji wengi wa lugha za wachache watabadilika na kutumia lugha inayotawala kwa sababu ni rahisi zaidi: “kupatikana hakumaanishi kwamba huduma, violesura, programu na Wikipedia zinatumika. Baadhi ya tafiti zinaonyesha kwamba wazungumzaji wa lugha za wachache hubadilika kwa urahisi hadi lugha yao kuu wanapotumia teknolojia ya kidijitali inayotegemea lugha, ama kwa sababu teknolojia hiyo ni bora zaidi, au kwa sababu huduma mbalimbali zinazopatikana ni pana zaidi.”

Wikipedia na mkusanyiko wake wa bure na [chanzo-wazi](https://en.wikipedia.org/wiki/Open_source) (Ambapo kanuni inapatikana kwa uwazi na kujengwa kwa pamoja) miradi ya maarifa ni mojawapo ya nafasi zenye matumaini na msaada kwa maarifa ya lugha nyingi mtandaoni. Kwa mfano, jumuiya zake za kujitolea zinajua na kuelewa kwamba hakuna aina moja ya Kiingereza au Kiarabu au Kichina, lakini kueleza wingi huu wa muktadha wa lugha na maudhui si rahisi kila wakati. Kama tunavyoona kutoka kwa uchambuzi na uzoefu wetu, [Wikipedia pia inakabiliwa na miundo ya kihistoria na inayoendelea](https://wikipedia20.pubpub.org/pub/myb725ma/release/2) uwezo na upendeleo unaopotosha njia na miundo ambamo tunaunda na kushiriki maarifa katika lugha tofauti, na ndani ya familia za lugha.

Je, ni baadhi ya njia zipi kwa ajili yetu kama watu binafsi, mashirika na jumuiya zinazotaka mtandao wa lugha nyingi zaidi? Katika sehemu hizi zinazofuata na za mwisho, tunatumia maarifa kutoka kwa [nambari]({{<trurl>}}/numbers{{</trurl>}}) na [Hadithi]({{<trurl>}}/stories{{</trurl>}}) ambazo tumeshiriki nawe kufikia sasa, ili kutoa muhtasari wa kile tumejifunza, na miktadha, uelewaji, na vitendo ambavyo vinaweza kutuongoza kwenye mtandao wa lugha nyingi.

## Tumejifunza nini kuhusu mtandao wa ujumuia wa lugha?

Tumejifunza mengi kuhusu lugha, intaneti, na lugha kwenye mtandao, kama tukibuni ripoti hii. Huu hapa ni muhtasari mfupi wa maarifa muhimu zaidi ya safari yetu kufikia sasa.

**Kujifunza:** Lugha ni wakala wa maarifa na njia muhimu ya kuwa ulimwenguni, sio tu chombo cha mawasiliano. Hii ndiyo sababu lugha nyingi ni muhimu sana, kwa hivyo tunaheshimu na kuthibitisha utajiri kamili na muundo wa nafsi zetu nyingi na ulimwengu wetu tofauti bora zaidi.

**Muktadha:** Watu wanajua ulimwengu wao na kujieleza katika zaidi ya lugha 7000.

Lugha zetu zaweza kuwa za mdomo (kutamkwa na kusainiwa), kuandikwa, au kupitishwa kupitia sauti.

Bado usaidizi wa lugha kwenye mifumo na programu kuu za teknolojia ni kwa sehemu ya lugha hizi 7000, na takriban 500 kati ya lugha hizi zikiwakilishwa mtandaoni katika aina yoyote ya taarifa au maarifa. Baadhi ya lugha zinazozungumzwa zaidi ulimwenguni hazina usaidizi mwingi wa lugha au habari mtandaoni. Usaidizi bora zaidi wa lugha, maelezo ya kina zaidi kwenye mtandao (pamoja na Ramani za Google na Wikipedia), na tovuti nyingi, ziko kwa Kiingereza.

**Tafakari:** **Mtandao hauko karibu na lugha nyingi jinsi tunavyofikiria au kuhitaji uwe.**

**Uchambuzi:** Watu wengi wanapaswa kutumia lugha ya karibu ya kikoloni ya Uropa (Kiingereza, Kihispania, Kireno, Kifaransa…) au lugha inayotawala kieneo (Kichina, Kiarabu...) ili kufikia mtandao. Miundo ya kihistoria na inayoendelea ya mamlaka na mapendeleo ni msingi wa jinsi lugha zinavyoweza kufikiwa (au la) mtandaoni.

## Tunawezaje kufanya vyema zaidi?: Muktadha na Vitendo kwa Mtandao wa ujumuia wa lugha

{{< summary-quote
    text="Katika hali nyingi, kutumia lugha ya wachache kunahitaji uvumilivu mzuri, utashi, na uthabiti, kwa kuwa uzoefu wa mtumiaji katika kutumia lugha za wachache huingiliana na dosari na shida."
    author="Claudia Soria"
    link="/sw/stories/decolonizing-minority-language/"
>}}

{{< summary-quote
    text="Lengo la mtandao wa lugha nyingi unaojumuisha na uwakilishi wa watu wa kiasili lazima izingatie na kuzingatia urithi wa kijamii na uzoefu unaoendelea wa ukandamizaji wa wakoloni. Mtandao wa lugha nyingi hauwezi tu kutamani kuwa uwakilishi, lakini, kwa kuzingatia historia ya ukoloni, lazima pia itafute kukuza mazingira ambayo yanaboresha uhai na ujifunzaji wa lugha za Wenyeji kwa na kwa watu wa kiasili."
    author="Jeffrey Ansloos and Ashley Caranto Morford"
    link="/sw/stories/learning-and-reclaiming-indigenous-languages/"
>}}

{{< summary-quote
    text="Vijana na watoto wa Mapuche hukua pamoja na teknolojia na Intaneti. Mtandao ni mahali ambapo watu hawa wanaweza kukaribia Mapuzugun…Hadithi za watu wetu lazima ziandikwe, na zinahitaji kuandikwa au kuzungumzwa katika Mapuzugun… Hadithi zetu si lazima ziwe za mashujaa wakuu, huku mataifa ya kikoloni na baada ya ukoloni yanaposherehekea. Historia yetu ni hadithi ya kila Mmapuche ambaye alinusurika na dhiki na jeuri. Mwanamke ambaye alilazimika kuhamia mjini ili kupata mshahara, wanawake na wanaume waliorudi kutoka mjini, lakini hapakuwa na nafasi tena katika nyumba zao “lofs”, na iliwabidi warudi mijini na mizizi yao ikiwa imekatwa na bila mahali pa kurudi. Matukio haya na kumbukumbu za kila mtu wa Mapuche hujumuisha kumbukumbu yetu ya pamoja kama watu."
    author="Kimeltuwe project"
    link="/sw/stories/use-of-our-ancestral-language/"
>}}

Katika sehemu hii ya muhtasari wa ripoti yetu ya Hali ya Lugha za Mtandao, tunaleta pamoja maarifa tofauti kutoka kwa wachangiaji wetu kwenye ripoti hii, na vile vile kutoka kwa mwaka wetu wa 2019 [Kuondoa Ukoloni kwa Lugha za Mtandao unaokutana](https://whoseknowledge.org/resource/dtil-report/), kuelewa miktadha, changamoto na fursa mbalimbali zinazohusu lugha nyingi duniani na mtandaoni. Kujiuliza na kujiuliza maswali manne muhimu kunaweza kutupa njia za kufikiria na kubuni mtandao wa lugha nyingi zaidi.

* **Nguvu na rasilimali za nani?**
* **Maadili na maarifa ya nani?**
* **Teknolojia na viwango vya nani?**
* **Miundo na mawazo ya nani?**

### Nguvu na rasilimali za nani?

#### Muktadha

Uchanganuzi wetu wa takwimu na uzoefu wa maisha wa watu huweka wazi kuwa kutengwa kwa lugha katika ulimwengu halisi na wa mtandaoni hakutegemei tu idadi ya watu wanaozungumza lugha hiyo.

Lugha za kiasili huzungumzwa na raia wa [zaidi ya mataifa 6000 ya kiasili](https://www.cwis.org/2015/05/how-many-indigenous-people/) duniani kote, ambao walikuwa wakazi wa msingi wa sehemu kubwa ya dunia hadi ukoloni na mauaji ya halaiki yalipoharibu au kupunguza watu na lugha zao. Lugha kuu katika baadhi ya mabara yenye lugha nyingi tofauti kama vile Asia na Afrika, zinazozungumzwa na kusainiwa na mamilioni ya watu, haziwakilishwi vyema, au wakati mwingine hata kidogo, mtandaoni. Ikiwa tutazingatia ughaibuni ya watu wanaozungumza lugha kama aina nyingi tofauti za Kiarabu, Kichina, Kihindi, Kibengali, Kipunjabi, Kitamil, Kiurdu, Kiindonesia (Bahasa Indonesia), Malay, Kiswahili, Hausa na kadhalika, katika nchi na mabara tofauti, ni wazi kwamba hizi ni lugha zilizotengwa mtandaoni, hata kama zinaweza kutawala zingine ndani ya maeneo yao.

Aina hizi za kutengwa kwa dijiti na kutengwa sio bahati mbaya; husababishwa na miundo ya kihistoria na inayoendelea na michakato ya mamlaka na upendeleo. Pia wanamaanisha kuwa rasilimali zinazoingia katika miundombinu ya lugha — kutoka kwa uchapishaji na wasomi, serikali na kampuni za teknolojia — tayari zimeelekezwa kwa maeneo fulani (Ulaya na Amerika Kaskazini) na lugha fulani (Kiingereza na lugha zingine za Ulaya Magharibi).Hata ndani ya Uropa na Amerika Kaskazini, Jamii za Wenyeji, Weusi na zilizotengwa zinapata ugumu kuhifadhi lugha zao katika vizazi vyote.

Hasa, nguvu za ukoloni na ubepari husababisha na kuingiliana na mifumo mingine ya ubaguzi na ukandamizaji kama vile ubaguzi wa rangi, mfumo dume, chuki ya watu wa jinsia moja, uwezo, utabaka na utabaka.Hii ina maana kwamba lugha fulani — hasa lugha za kikoloni za Ulaya — ndizo zinazojulikana zaidi mtandaoni, iwe zinazungumzwa au la na idadi kubwa ya watu duniani. Inamaanisha pia kwamba kunapokuwa na habari na maarifa katika lugha zingine zilizotengwa zaidi, yaliyomo katika lugha hizo hupunguzwa na nani anayeweza kuzifikia na kuziunda, au kuzuia wengine kutoa habari mbadala. Kwa mfano, ukosefu wa maudhui ya mtandaoni yanayotetea haki za wanawake katika Kisinhala, au ukosefu wa maudhui chanya kwa watu wakuchu na walemavu katika Kibengali au Kiindonesia (Bahasa Indonesia).

Kwa sababu lugha ndiyo kiini cha sisi ni nani, kutoweza kujieleza kwa lugha zetu wenyewe na kwa ukamilifu wa nafsi zetu nyingi ni aina ya vurugu. Lakini matokeo ya kutengwa huku ni vurugu kwa njia zingine. Kama [Uda]({{<trurl>}}/stories/flickering-hope{{</trurl>}}) anasema, “Ukosefu wa maudhui ya kidijitali ambayo ni ya haki za wanawake, ya kirafiki ya haki za binadamu, na yenye heshima, hufanya nafasi za mtandaoni ambapo mawasiliano hasa hufanyika katika lugha za kienyeji, chuki dhidi ya wanawake, watu wakuchu na walio wachache. Kuna ukosefu wa wazi wa vyombo vya habari vya kidijitali ambavyo vinapinga masimulizi ya kawaida ambayo yanasalia kuathiriwa na dhana potofu mbaya. Hii inazidisha matamshi ya chuki na unyanyasaji wa kingono na kijinsia mtandaoni.” Aina hizi za ukatili zinaenea hadi kwa watu wa asili, [waliodhulumiwa](https://www.equalitylabs.org/facebookindiareport) na jumuiya za dini ndogo, kwa walemavu na watu wanao jiweza tofauti, na jumuiya nyingine zilizotengwa.

Wakati huo huo, jamii hizi zinatumia mtandao kupigana dhidi ya aina tofauti za unyanyasaji wa vizazi dhidi yao na lugha zao [Jeffrey na Ashley](({{<trurl>}}/stories/signs-across-generations{{</trurl>}})) ilichanganua kuhusu twiti 3800 zenye zaidi ya lebo 35 za Lugha ya Kiasili na maneno muhimu 57, katika vikundi 60 vya lugha za Wenyeji vinavyotambulika na serikali nchini Kanada. Waligundua kuwa, kupitia lebo za reli kwenye Twitter, watu wa kiasili kote Kanada na kwingineko duniani wanaungana, wanashiriki, na wanashirikiana katika ufufuo na kustawi kwa lugha za Asilia.

Kama walivyosema, “Katika muktadha wa kijamii ambapo lugha za kiasili kote Kanada zimelengwa kufutika chini ya [sera za ukoloni za kuiga](https://indigenousfoundations.arts.ubc.ca/the_residential_school_system/), Mitandao ya alama za reli ya Twitter imekusanya muktadha wa kipekee na wa maana kwa watu wa kiasili kushiriki maarifa kuhusu lugha za Asilia. Katika mitandao mbalimbali iliyojumuishwa katika utafiti wetu, kuna mifano ya urejeshaji wa lugha na uunganisho kwa waathirika wa vizazi vya sera za uigaji wa kikoloni.”

{{< summary-side-fig
    src="/media/stories-content/Learning_and_Reclaiming_Indigenous_Languages_image1.webp"
    alt="In Dene, the word for Caribou also means Stars. I love that (emoji with heart-shaped eyes) #denecosmology #advancedlanguagesclass)"
    caption="Dene Melissa Daniels katika kutoa ridhaa ya kutaja tweet hii alitamani kumtambua mwalimu wa lugha aliyetoa mafundisho haya, mzee wa Dene na mwalimu Eileen Beaver."
>}}

#### Vitendo

* Kutambua miundo na michakato ya mamlaka na mapendeleo katika taasisi na michakato mbalimbali inayosaidia miundo msingi ya lugha.
* Hakikisha rasilimali za urejeshaji katika lugha na jamii zilizotengwa, ikijumuisha kujifunza lugha na upangaji lugha.
* Wezesha rasilimali kuelekea kuunda na kupanua taarifa na maarifa kutoka na kwa jamii zinazopitia aina nyingi za ukandamizaji na unyanyasaji, katika lugha na aina wanazochagua.

### Maadili na maarifa ya nani?

#### Muktadha

Historia na teknolojia ya mtandao inategemea mtazamo wa ulimwengu kutoka kwa epistemolojia za Magharibi, au njia za kujua na kufanya. Hasa zaidi, mtandao uliundwa na unaendelea kutengenezwa na kutawaliwa hasa na weupe (na [sasa hudhurungi](https://www.theguardian.com/technology/2014/apr/11/powerful-indians-silicon-valley)) wanaume wenye upendeleo. Hii ina maana kwamba maadili ambayo mara nyingi huwa msingi wa usanifu na miundo msingi ya mtandao ni maadili ya uamuzi wa kiteknolojia — ambapo teknolojia inaonekana kama sababu ya msingi (na yenye manufaa) ya mabadiliko yoyote ya kijamii — na ubinafsi, ambapo lengo la msingi. na dereva ni mtu binafsi, si kikundi.

Kwa kuongezea, mtazamo huu wa ulimwengu unatokana na Enzi ya Mwangaza, karne ya 18 katika Ulimwengu wa Kaskazini kuelekea aina fulani ya sayansi na teknolojia inayotegemea busara. Tunachosahau ni kwamba hisabati na sayansi ilistawi kote Kusini mwa Ulimwengu kabla ya karne ya 18. Mifumo ya kwanza ya uandishi na nambari, kwa mfano, ilitoka Mesopotamia, Iran na Iraq ya leo. Hata kwa umakinifu zaidi, tunasahau kwamba rasilimali za Kutaalamika, "zama hii ya dhahabu" ya sayansi na teknolojia katika Kaskazini ya Ulimwenguni, ilikuwa Enzi ya Dola katika Ulimwengu wa Kusini, na harakati nyingi za ukoloni, utumwa, mauaji ya kimbari na uchimbaji wa rasilimali. kutoka Asia, Afrika, Amerika Kusini na Visiwa vya Caribbean na Visiwa vya Pasifiki. Misingi ya asili ya uziduaji ya ubepari wa kisasa imejikita katika historia hizi za zamani za ukoloni, na inaendelea kuwa sehemu ya mtaji wa teknolojia.

Pamoja na rasilimali za nyenzo, kile kilichoharibiwa, kupuuzwa, au kudhoofishwa katika michakato hii ni aina zingine za kujua, kufanya, na kuwa — ambayo ni, vitabu visivyo vya Magharibi, kama maarifa asilia, au maarifa ya wale kutoka sehemu duni za ulimwengu. Matokeo mabaya zaidi ya hii kwa lugha, ambayo ni wakala muhimu wa maarifa, kama tulivyokwisha sema hapo awali, ilikuwa kushuka kwa thamani kamili ya lugha zisizo za Magharibi mwa Ulaya, na uharibifu mkubwa au upuuzaji mbaya wa aina za mdomo na zisizo za maandishi aina za lugha. Upendeleo wa maudhui yaliyoandikwa katika seti ndogo ya lugha za upendeleo unaendelea kutupotosha kuelekea aina fulani ya "maarifa" yaliyoandikwa katika uchapishaji na taaluma, ambayo inaathiri uandikaji wa kimsingi na data ya usindikaji wa lugha asilia, au baadhi ya mifumo ya kiotomatiki ya lugha. ambazo ni sehemu ya miundomsingi ya mtandao, kama vile Google Tafsiri.

Kama [Ana]({{<trurl>}}/stories/dill-wlhall-on-the-web{{</trurl>}}) anavyo fafanua, "Licha ya ukweli kwamba chini ya nusu ya lugha za ulimwengu ​​ni lugha ​​ambazo hazina mfumo wa uandishi na kudumisha mapokeo marefu ya simulizi, lugha ​​zilizo na mfumo wa alfabeti unaotambulika sana na ulioenea hutawala wavuti. Wavuti huimarisha kutengwa kwa utaratibu, ambapo ni zile tu lugha ​​zilizoandikwa zinaweza kuhifadhiwa kwa siku zijazo.”

Kupitia upotevu wa lugha, tunapoteza zaidi ya maisha yetu ya baadaye kuliko tunavyotambua. Tunapoteza aina zote mbili za usemi katika lugha tofauti na mitazamo mizima ya ulimwengu na maarifa yaliyo katika lugha hizi. Wakati ambapo ubinadamu uko kwenye hatihati ya kuporomoka kwa sayari, jumuiya za Wenyeji na ujuzi wao wanalinda bayoanuwai yetu na kuhifadhi uhai kama tunavyojua. Haishangazi, tunajua kwamba [kupotea kwa lugha kunahusishwa moja kwa moja na kupotea kwa bioanuwai](http://www.unesco.org/new/en/culture/themes/endangered-languages/biodiversity-and-linguistic-diversity/) na uharibifu wa mifumo ikolojia duniani kote.

Mtandao unaweza kuwa miundombinu ya kusisimua ya kuhifadhi na kupanua aina mbalimbali za lugha na maarifa, kwa sababu miundo yake tajiri ya midia inaweza kuiga na kuwakilisha lugha shirikishi zinazozungumzwa, na za ishara, na zaidi ya maandishi. Lakini ni muhimu kwamba ahadi hii kali ya mtandao haitegemei tena maadili ya ukoloni-bepari na mfumo dume. Je! ni jinsi gani jumuiya za watu huhifadhi na kuhuisha lugha na utambulisho wao, na kushiriki maarifa yao kwa masharti yao wenyewe? Kwa mfano, katika jamii nyingi za Wenyeji, ujuzi fulani ni takatifu na haupaswi kushirikiwa waziwazi.

Juhudi moja kama hizo zinazoongozwa na jamii ni [Papa Reo](https://papareo.nz/), teknolojia ya utambuzi wa usemi kwa te reo Māori (lugha ya Kimaori) ya Aotearoa/New Zealand Jumuiya ya Wamaori iliunda na kudumisha teknolojia na data ya mpango huu, na inaamini kuwa aina hii ya [uhuru wa data](https://www.wired.co.uk/article/maori-language-tech) ni muhimu katika kuhakikisha kwamba ujuzi unaoshirikiwa kupitia lugha unatumiwa na Wamaori, badala ya makampuni yanayotafuta faida. Cha kufurahisha, wakati wa kutambua thamani ya teknolojia huria, timu ya Papa Reo pia iliamua kutoongeza data zao kwenye hifadhidata huria, kwa sababu jumuiya ya Wamaori imenyimwa rasilimali na mapendeleo ya jumuiya nyingi za chanzo huria. Kwa upande mwingine, [Mukurtu](https://mukurtu.org/) ni mfano wa jukwaa huria ambalo limejengwa na jumuiya za Wenyeji ili kudhibiti data ya lugha zao.

#### Vitendo

* Unda, shirikiana na ushiriki miundo msingi ya lugha kwa ajili ya mtandao ambayo ni kwa ajili ya manufaa ya umma, iliyoundwa kwa maadili ya pamoja na ya jamii mioyoni mwao, na ambayo inazingatia dhana za ufeministi na Wenyeji za ukuu na umiliki.
* Endelea kutoa changamoto na kukosoa miundo msingi ya teknolojia ya lugha ambayo inakandamiza asili: ubepari, umiliki, uchimbaji wa kibinadamu na uharibifu wa mazingira.
* Tambua kwamba teknolojia za lugha huria na huria pia zinahitaji kuzingatia mapendeleo yao binafsi, na kuheshimu jinsi jumuiya zilizotengwa zinavyojiamulia na kufafanua "wazi" na kile wanachotaka kushiriki na ulimwengu.

### Teknolojia na viwango vya nani?

#### Muktadha

Sekta ya teknolojia haiwajibiki kikamilifu kwa ukosefu wa sasa wa uwakilishi na usaidizi kwa idadi kubwa ya lugha za ulimwengu. Hata hivyo, teknolojia kutoka Global North inawajibika kuendelea kudumisha na kuongeza ukosefu wa usawa katika lugha na ukoloni wa kidijitali mtandaoni.

Kampuni kubwa za teknolojia — ambazo zinabuni na kuunda mifumo mingi ya kidijitali, zana, maunzi na programu tunazotumia — zinaweza kupuuza hitaji hilo kuunda mtandao wa lugha nyingi kweli, kwa sababu hawazingatii au hawaoni lugha zetu nyingi 7000 zinazozungumzwa kama sehemu muhimu ya miundombinu ya mtandao. Baada ya yote, wanajua kwamba wanahitaji kutoa usaidizi wa lugha pale tu inapoleta maana ya biashara: ama kwa lugha za kikoloni za Ulaya, au lugha katika kile wanachoita "soko zinazoibuka". Kwa hakika, lugha kuu za Kusini na Kusini Mashariki mwa Asia zimeanza kupata [msaada bora wa lugha](https://blog.google/inside-google/googlers/shachi-dave-natural-language-processing/) kwenye majukwaa makubwa ya teknolojia huku yakiwa msingi mkubwa wa wateja kwa kampuni hizi.

Wakati huo huo, baadhi ya teknolojia za lugha zilizoenea zaidi tunazo kwenye mtandao zinaundwa na kudhibitiwa na makampuni haya, kwa sababu wana rasilimali na uwezo. Wikipedia ni ubaguzi mashuhuri, kwa sababu ni chanzo huria na inaungwa mkono na jumuiya zake za kujitolea kutoka duniani kote. Kwa ujumla, motisha za umiliki na zinazotokana na faida hazisababishi zana na teknolojia zinazohitajika kwa maudhui ya kina na yasiyoeleweka katika lugha tofauti zilizotengwa, na kutoka kwa jamii zilizotengwa. Kuzidi teknolojia za lugha ambazo zinajengwa kwa sasa na kampuni hizi, ni kubwa [mifumo otomatiki](https://dl.acm.org/doi/pdf/10.1145/3442188.3445922) ambazo zinategemea kiasi kikubwa cha data ya lugha kutoka kwa aina yoyote ya chanzo — hata kama chanzo hicho kinaweza kuwa maneno ya vurugu na yaliyojaa chuki au maandishi dhidi ya makundi yaliyotengwa.

Kama [Jeffrey na Ashley](({{<trurl>}}/stories/signs-across-generations{{</trurl>}})) wanaeleza, “Katika lugha nyingi za kiasili [kuishi](https://en.wikipedia.org/wiki/Survivance) na mitandao ya kujifunza, ubaguzi wa rangi umetambuliwa kama changamoto kubwa ya kijamii ndani ya ikolojia ya Twitter. Hasa, kuna njia mbalimbali ambazo mitandao hii inalengwa kikamilifu na maandishi ya uchochezi na maudhui ya medianuwai, wakati mwingine hujumuisha matamshi ya chuki, na aina mbalimbali za watumiaji, ikiwa ni pamoja na watu halisi, na roboti otomatiki. Kwa upande wa watumiaji wa roboti, akaunti hizi zinaonekana kufuata mifumo sawa ya usambazaji wa habari potofu, na mara nyingi husambaza maandishi yasiyo ya maana yanayoakisi uchanganuzi wa jumla na uzalishaji wa yaliyomo.”

Ikiwa kampuni haijali vya kutosha kuhusu matokeo ya ukosefu wao wa utaalamu wa lugha ya ndani na muktadha, basi inaweza kusababisha madhara yasiyopimika na vurugu tendaji. Nchini Myanmar, ambapo Facebook (au Meta) kimsingi ndiyo mtandao, wanaharakati walionya kampuni hiyo kuhusu matamshi ya chuki kwa miaka mingi kabla ya kuanzisha timu ya lugha ya Kiburma. Mnamo 2015, Facebook ilikuwa na [wazungumzaji wanne wa Kiburma](https://www.reuters.com/investigates/special-report/myanmar-facebook-hate/) kukagua maudhui, na watumiaji milioni 7.3 wanaofanya kazi nchini Myanmar. Ukosefu huu wa umakini kwa lugha na muktadha ulisababisha nini? Ilimaanisha kuwa Umoja wa Mataifa uligundua kuwa Facebook ilikuwa sehemu ya mauaji ya halaiki dhidi ya Waislamu wa Rohingya nchini Myanmar, na kampuni hiyo ndiyo kiini cha kesi dhidi ya serikali ya Myanmar kwenye [Mahakama ya Kimataifa ya Haki](https://thediplomat.com/2020/08/how-facebook-is-complicit-in-myanmars-attacks-on-minorities/).

Vile vile, [hotuba ya chuki nchini India](https://www.ndtv.com/india-news/facebook-officials-played-down-hate-in-india-reveals-internal-report-2607365) ya Waislamu, Dalits na jumuiya nyingine zilizotengwa inaendelea na udhibiti mdogo sana, licha ya India kuwa soko kubwa zaidi la Facebook, na kuwa na baadhi ya lugha zinazozungumzwa zaidi duniani. Kwa hakika, kampuni inatumia [84% ya “tangazaji wake wa kimataifa wa malipo/lugha”](https://www.washingtonpost.com/technology/2021/10/24/india-facebook-misinformation-hate-speech/) kuhusu taarifa potofu nchini Marekani, ambayo ina chini ya 10% ya watumiaji wake. 16% iliyobaki ni ya ulimwengu wote.

Kampuni za teknolojia zinahitaji kutambua kwamba ujenzi wa teknolojia ya lugha unahitaji upana na kina cha rasilimali, muktadha wa kijamii na kisiasa, na kujitolea kwa matumizi salama na ya kukaribisha ya dijitali ya lugha nyingi.

Wachangiaji wetu wanazungumza juu ya anuwai ya mahitaji, changamoto na fursa katika kuunda uzoefu kama huo. Hasa, ukosefu wa miundombinu (kutoka kwa ufikiaji wa mtandao hadi vifaa bora) na teknolojia kwa lugha zote hufanya matumizi ya kidijitali ya lugha zilizotengwa kuwa ya kuchosha, ngumu, polepole na isiyowezekana. Tunatoa mifano michache yenye nguvu.

**Teknolojia ya lugha haijaundwa mara chache kwa lugha iliyotengwa.**

[Donald]({{<trurl>}}/stories/challenges-of-expressing-chindali{{</trurl>}}) azungumza kuhusu jinsi ilivyo nadra kwa jamii yake nchini Malawi kupata mtandao na vifaa vya mawasiliano kwa urahisi katika lugha zao. Aliwahoji wazungumzaji 20 wa Chindali,10 kati yao wakiwa wanafunzi na 10 walikuwa wazee wa jumuiya hiyo. Kati ya 20 aliowahoji, ni watano tu waliokuwa na simu mahiri au simu za sifa, na saba hawakuwa na kifaa chochote. Wanne tu kati ya 20 — wanafunzi wote — walikuwa na kompyuta ya mkononi. Kuhusu upatikanaji wa mtandao, ni wanafunzi wa chuo pekee waliokuwa na uwezo wa kufikia kupitia vyuo vyao au mipango ya data ya kibinafsi.

Mara tu mtandaoni, ni nadra kwa watu wengi duniani kuweza kufikia kibodi katika lugha zao. Jumuiya nyingi zinapaswa kufanyia kazi kibodi iliyoundwa kwa ajili ya lugha za Ulaya, na kubandika herufi kutoka kwa lugha zao kwenye kibodi.Hii ni ngumu ya kutosha kwa kibodi ya kompyuta ya kibinafsi, lakini haiwezekani kwa kibodi ndogo ya simu. Pia ni ngumu zaidi kwa lugha ambazo kimsingi ni za mdomo na hazina mfumo wa uandishi uliokubaliwa.

Vile [Ana]({{<trurl>}}/stories/dill-wlhall-on-the-web{{</trurl>}}) anasema kuhusu lugha yake, “kibodi hazina alama sahihi za Zapotec kuwakilisha sauti na toni za lugha zetu. Juhudi za kupata lugha za kiasili ​​zimeandikwa zimekuwa katika miaka ya majadiliano kujaribu kufikia muafaka wa umbizo sanifu kama vile Kilatini, umbizo lililowekwa na kuathiriwa kwa kiasi fulani na nchi za Magharibi na kwa namna fulani kukubaliwa na kuhitajika na sekta ya wazungumzaji.”Hii ni ngumu zaidi kwa lugha kama vile Arrernte, ambazo huchanganya sauti na ishara, kama [Joel na Caddie]({{<trurl>}}/stories/signs-across-generations{{</trurl>}}) walituambia kupitia mradi wa Indigemoji.

Iwapo unatoka kwenye jamii iliyo katika hali ya tahadhari au isiyo salama na ufikiaji wa mtandao haujaundwa katika lugha yao wenyewe, kuna uwezekano kwamba utoweza kuandaa na kuonyesha vipindi kwa na pamoja na jamii .[Paska](http:///stories/flickering-hope-en/) anapendekeza, "Watu wengi wa jamii ya upinde nchini Indonesia bado wana sintofahamu ya technolojia ya tovuti, wala hawana ujuzi wa kutosha kuhusu jinsi injini ya utafutaji inavyofanya kazi."

Jamii zilizotengwa katika maeneo ya upendeleo ya Kaskazini mwa Ulimwengu pia zinakabiliwa na changamoto hizi za kutokuwa na uthibitisho wa maisha na wakati mwingine maudhui ya kuokoa maisha katika lugha zao wenyewe. [Jeffrey na Ashley](({{<trurl>}}/stories/signs-across-generations{{</trurl>}})) wanaeleza: “Mojawapo ya changamoto kuu… ni mapungufu ya kiufundi ya teknolojia ya sasa ya utafsiri kwa lugha za kiasili katika muktadha wa Kanada. Lugha za kiasili kama vile Hul'qumi'num, Sḵwx̱wú7mesh (Squamish), Lewkungen, na Neheyawewin (Cree) zinatambulika kimakosa kuwa Kijerumani, Kiestonia, Kifini, Kivietinamu, na Kifaransa na teknolojia ya utafsiri ya Twitter."

Kwa ujumla, [Uda]({{<trurl>}}/stories/flickering-hope{{</trurl>}}) inathibitisha,"kuunda maudhui ya dijitali katika lugha za kienyeji bado ni changamoto kutokana na kutopatikana kwa zana, na utata wa zana zinazopatikana. Kuunda maudhui katika lugha ya kienyeji kunahitaji zana na ujuzi maalum. Changamoto hii inachangia ukosefu wa maudhui ya kimaendeleo katika lugha za kienyeji.”

**Teknolojia ya lugha imeundwa zaidi katika mkabala wa juu chini, ikiweka kipaumbele faida kuliko usawa na usalama.** [Claudia]({{<trurl>}}/stories/decolonizing-minority-language{{</trurl>}}) anafafanua mbinu ya kampuni nyingi za teknolojia kwa uwazi sana, wakati ambapo anasema: "[Ni] utoaji wa teknolojia na vyombo vya habari unamiminwa juu chini na makampuni makubwa, na ushiriki mdogo au kutokuwepo kabisa kwa jumuiya za wazungumzaji.Katika kesi hii, mbinu ya kufadhili inaweza pia kuonekana:kwa kuwa kidogo sana inapatikana, chochote kinachotolewa lazima kiwe kizuri na kinakaribishwa kwa ufafanuzi. Mara nyingi kampuni hutoa suluhu zilizotengenezwa tayari bila kuzingatia mahitaji halisi, matamanio na matarajio ya wazungumzaji wa lugha wachache. Ni kana kwamba dhana ilikuwa kwamba wazungumzaji hawa wanapaswa kushukuru kwa bidhaa au fursa yoyote wanayopewa, haijalishi ikiwa ni ya kuvutia au muhimu kwa maisha yao. Vighairi mashuhuri kwa tabia hii vinaonyeshwa na van Esch et al. (2019), ambao wanasisitiza mara kwa mara hitaji la ushirikiano wa karibu na wazungumzaji wa lugha wakati wa kupanga uundaji wa matumizi ya Usindikaji wa Lugha Asilia.”

Mbinu hii ni nadra kuelewa miktadha ambamo jamii za lugha zilizotengwa huishi, na tofauti za muundo zinazohitajika ili kuleta lugha zao mtandaoni kwa njia tajiri na isiyoeleweka. Wakati [Emna]({{<trurl>}}/stories/write-in-another-language{{</trurl>}}) alipohojiwa na Gamil kutoka Sudan, alizungumza kwa Kiarabu cha Sudan kusema, “Sudan ni nchi yenye makabila mengi na aina mbalimbali za mila na desturi.Kwa hiyo Kaskazini inazungumza lahaja ya Kiarabu (hiyo hiyo ninayotumia) na hiyo ni kwa sababu ya ukoloni bila shaka.Ingawa kwa Mashariki na Magharibi, makabila yanazungumza lugha tofauti ya kienyeji, ni wao tu wanaoweza kuizungumza na kuielewa. Kwa watu kutoka Kaskazini, ni nadra sana kupata mtu anayeielewa isipokuwa aliishi huko na kuingiliana nao. Asili ya lugha yetu ni lugha ya Kikushi. Ustaarabu wa Kikushi na Kinubi ndio marejeleo yetu. Wakati High Dam ilipozama, tulipoteza utambulisho wa Kikushi, hatukupata kamusi ya kusimbua lugha na kuitafsiri katika lugha nyingine. Lugha ya Kinubi, hata hivyo, inajulikana na kutafsiriwa. Lugha ya Kinubi inapatikana hata kwenye simu za rununu za Huawei kama mojawapo ya lugha katika mipangilio ya rununu. Kwa lugha za Mashariki na Magharibi, hazijaandikwa (au labda zimeandikwa, sijui, ninahitaji kuthibitisha).Kwa upande wa Kaskazini, tunazungumza Kiarabu. Sisi ni Waafrika na wazungumzaji wa Kiarabu, si Waarabu.”

Mahojiano ya Emna yalimsukuma kusema, "Wavuti inahitaji watumiaji wake wote, wanaoandika na wasioandika. Hata hivyo, mabadiliko si kitu ambacho ni wajibu madhubuti ya watumiaji, makampuni ambayo kubuni na kuendeleza programu lazima pia kuchukua jukumu kwa ajili ya kubuni ya baadaye ya mtandao. Inaonekana kwamba kila mtu leo anazungumza kuhusu kuwa mjumuisho na kupiga vita ubaguzi wa rangi, ubaguzi, na aina nyinginezo za ukoloni. Hata hivyo, ili kufanya mabadiliko kwenye wavuti, mashirika yanapaswa kujadili jinsi yanavyoendeleza kutengwa;wabunifu wa wavuti, wahandisi wa teknolojia za wavuti, wamiliki wa kampuni za teknolojia wanapaswa pia kuchangia katika kufanya programu yao ipatikane kwa watumiaji wote.”

Njia moja ya kuchanganua kwa kina aina hizi tofauti za kutengwa ni kutambua kwamba msingi wa teknolojia ya kidijitali — kanuni yenyewe — mara nyingi iko katika Kiingereza.Kuna [lugha za programu](https://www.wired.com/story/coding-is-for-everyoneas-long-as-you-speak-english/) chache sana kulingana na lugha zingine, ambayo ina maana kwamba wanateknolojia wenyewe haja ya kujua Kiingereza vizuri vya kutosha kuweka ndani yake. [Lugha ya programu Qalb](https://www.afikra.com/talks/conversations/ramseynasser), kulingana na sintaksia ya Kiarabu na calligraphy, ni mojawapo ya mifano michache inayojaribu kuvunja mtindo huu. Kwa ujumla, ingawa, tasnia ya teknolojia inahitaji kuelewa jinsi upendeleo wa lugha unavyosimbwa katika kila teknolojia na wanateknolojia wengi.

Kama [Jeffrey na Ashley](({{<trurl>}}/stories/signs-across-generations{{</trurl>}})) walivyoweka, “Utafiti wetu unaweka wazi kwamba ukuzaji wa lugha za kiasili kwenye mtandao lazima ujikite ndani ya uchanganuzi wa kina wa mtandao. teknolojia yenyewe, pamoja na michakato ya kijamii inayozunguka utumiaji wake.”

#### Vitendo

* Tambua kwamba teknolojia ya lugha nyingi na maudhui, yaliyoundwa na kuundwa na wazungumzaji wa lugha wenyewe, ni haki ya msingi ya binadamu ambayo inahitaji kupewa kipaumbele na makampuni ya teknolojia na mashirika ya viwango, na kuungwa mkono na taasisi za kimataifa kama vile [UNESCO](https://en.unesco.org/news/tell-us-about-your-language-play-part-building-unescos-world-atlas-languages).
* Jenga kielelezo cha usimamizi unaoongozwa na jamii,ya kimataifa ya miundo msingi ya lugha ambayo inafanya kazi kwa ushirikiano wa uaminifu na heshima na makampuni ya teknolojia na taasisi nyinginezo.
* Idhini ya jumuiya na maadili katika kuunda data na teknolojia ya lugha, kuhakikisha kwamba jumuiya za lugha zina uwezo na usalama juu ya kile na jinsi wanashiriki, hasa kwa wale waliotengwa zaidi na mifumo tofauti ya kuingiliana ya ukandamizaji na ubaguzi.

### Miundo na mawazo ya nani?

Kutoka kwa nambari na hadithi ambazo tumeshiriki, tunajua kwamba miundo msingi ya lugha yenye maana na inayofaa inawezekana tu tunapozingatia mahitaji, miundo, na mawazo ya jumuiya ya lugha yenyewe. Lugha zilizotengwa zitastawi na kupanuka tu wakati watu walio wachache duniani watakuwa sehemu ya kujenga teknolojia.

Wachangiaji wetu wanapendekeza njia mbalimbali za kuhakikisha hili, ikiwa ni pamoja na kampuni za teknolojia zinazoajiri wanateknolojia na wataalamu wengine kutoka kwa jumuiya za lugha zilizotengwa, pamoja na kuwasilisha rasilimali kwa kuwajibika kwa jumuiya katika kazi hii. Hupendekeza wingi wa miundo na michakato inayozingatia miktadha ya lugha, badala ya mkabala mmoja wa kimfumo. Kama [Claudia]({{<trurl>}}/stories/decolonizing-minority-language{{</trurl>}}) anavyosema, “Wazungumzaji wa lugha ya wachache hawahitaji masuluhisho yaliyotayarishwa tayari: mahitaji na mahitaji yao mahususi lazima yasikilizwe na kujumuishwa katika bidhaa ambazo zimeundwa kulingana na zile. mahitaji. Miktadha ya kiisimu jamii ya lugha mbalimbali za walio wachache inaweza kutofautiana sana, na pia lazima masuluhisho yanayotolewa.”

Wachangiaji wetu pia wanaelezea wajibu na mbinu za jumuiya zao za kuhifadhi na kupanua lugha zao, kwa kutumia teknolojia zinazopatikana kwao. [Ishan]({{<trurl>}}/stories/marginality-within-marginality{{</trurl>}}) anasema, “Kama mtu kuchu, mlemavu, ninaamini kwamba ni lazima tutumie vyema rasilimali zetu zilizopo za kijamii, kiutamaduni-kiuchumi ili kufanya uzoefu, matarajio yetu. na madai yaliyosikika na kuonekana. Sisi, watumiaji wa mtandao, lazima tuchukue jukumu la kufanya mtandao kuwa jumuishi na kufikiwa.”

[Ana]({{<trurl>}}/stories/dill-wlhall-on-the-web{{</trurl>}}) fafanua jinsi majukwaa ambayo yana miundo msingi bora ya simulizi na taswira inavyotumiwa zaidi na jumuiya zake kuliko yale yanayotegemea maandishi.Leo, kwa lugha ya Zapotec, mazungumzo hutumiwa zaidi ya maandishi katika mtandao. Jukwa la kijamii kama vile YouTube, Facebook, WhatsApp na Instagram huonekana kuwa rasilimali na zakirafiki zinazopatikana kwa lugha simulizi. Majukwaa haya huruhusu watumiaji kupakia maudhui yanayoonekana yanayoboresha ujumbe, kwa jinsi watumiaji wanavyotaka uwasilishwe, bila kuangukia katika ukali wa kuandika. Ni majukwaa haya ambayo yana watumiaji wengi zaidi ulimwenguni na yanatumiwa na jamii asilia. Kwa upande wa jumuiya za Sierra Zapotec, watumiaji wengi huunganisha mtandao kupitia Facebook, ambapo hutangaza tamasha za kitamaduni, dansi, muziki, masimulizi ya matukio muhimu na matangazo kwa wahamiaji na jumuiya za wenyeji. Kabla ya Uviko-19, Facebook ilikuwa tayari ina jukumu muhimu katika kuomboleza familia za wahamiaji, kwani mazishi na mila zilipitishwa kupitia jukwaa. Jukwaa hili hili linatumiwa na baadhi ya jumuiya za Zapotec kusambaza upya vipindi vya redio, na hivyo kufanya daraja muhimu kati ya njia ya mawasiliano ya analogi inayotumika sana katika maeneo ya vijijini kama vile redio, yenye nafasi ya ulimwenguni pote na inayopatikana kila mahali kama vile kwa mtandao.”

Kwa wale wanaoweza kutumia njia za maandishi za lugha, lebo za reli zimekuwa njia ya kusisimua kwa jumuiya kuunganishwa kidijitali kujifunza na kueneza lugha zao wenyewe, na kuhamasisha jamii nyingine pia. Kama [Jeffrey na Ashley](({{<trurl>}}/stories/signs-across-generations{{</trurl>}})) wanavyoelezea: “Katika muktadha wa kijamii ambapo lugha za kiasili kote Kanada zimelengwa kufutwa chini ya sera za ukoloni za uigaji, mitandao ya hashtag ya Twitter iliitisha muktadha wa kipekee na wa maana kwa watu wa kiasili kushiriki maarifa kuhusu lugha za Asilia… Kwa mfano, mtandao wa hashtag wa wanafunzi wanaojifunza lugha ya Gwichin uliwahimiza wanafunzi wa Anishnaabemowin (Ojibway) kuunda mtandao wao wa reli. Na vile vile, mtandao wa lugha wa Neheyawewin (Cree) kwenye Twitter uliwatia moyo wanafunzi wa Hul'qumi'num kuanzisha Neno la Siku lenye msingi wa Twitter."

Matumizi mapana na ya kiubunifu ya majukwaa haya ya umiliki na jumuiya zilizotengwa ni sababu muhimu kwa makampuni ya teknolojia kufanya kazi nao, badala ya kuwapinga.

Wakati huo huo, [Claudia]({{<trurl>}}/stories/decolonizing-minority-language{{</trurl>}}) anaonya kwamba wanaharakati wa lugha kutoka jamii zilizotengwa wanahitaji kufahamishwa na kuratibiwa vyema zaidi ili wasipoteze nishati na rasilimali katika mchakato huo. “Ingawa ni ya kupongezwa, mipango hii [ya wanaharakati] inaelekea kuteseka kutokana na ukosefu wa uratibu, mipango midogo, na hata ugunduzi mdogo. Hii inasababisha tatizo kubwa sana kwa jamii ambapo rasilimali hazina kikomo: kupunguzwa kwa juhudi. Katika hali zote mbili, tatizo kuu liko katika ujuzi mdogo wa kile ambacho tayari kinapatikana na kile kinachohitajika. Ili kuondoa ukoloni wa teknolojia ya lugha kwa lugha za wachache, ni muhimu kupata picha wazi ya kiwango ambacho lugha za wachache hutumiwa kwenye vyombo vya kidigitali, kwa mara ngapi, na kwa madhumuni gani. Muhimu vile vile ni kujua kuhusu vikwazo ambavyo wazungumzaji wa lugha ya wachache hukabilianapo (ikiwa) wanapojaribu kutumia lugha hizi: je, wanapata matatizo ya kiufundi? Je, wamezuiwa na aina fulani ya paranoia ya kujiletea wenyewe? Kwa vile kuandika kwa lugha ya walio wachache ni aina ya kufichuliwa na ulimwengu wa nje, je, watu hujiepusha nayo kwa kuogopa kudhihakiwa au kunyanyapaliwa? Vile vile, ni machache yanayojulikana kuhusu hamu ya wazungumzaji wa lugha za wachache kuhusu fursa za kidijitali: ni nini wanachotaka au wanatarajia kupatikana?”

Ripoti hii imekuwa jaribio moja la kuelewa changamoto hizi, na njia za kusonga mbele. Kufanya vitu sawa tena na tena kwa lugha tofauti haitafanya kazi. Kwa mfano, kuunda programu kwa Kiingereza na kudhani kuwa itafanya kazi vivyo hivyo kwa Kiindonesia (Bahasa Indonesia) ni shida sana. Kufanya mtandao kuwa bora ni kuhusu kubadilisha mienendo ya nguvu kati ya watu, sio tu kurekebisha masuala ya kiufundi. Na lugha nyingi kwenye mtandao ni mkusayiko wa masuala changamano wa kijamii na kiufundi na kisiasa. Tunahitaji kuweka mahitaji ya jumuia za lugha kwanza, si teknolojia ya mtandao — kufanya hivyo kwa njia hii kutafanya teknolojia ya lugha kuwa bora na muhimu zaidi.

Kimsingi zaidi, tunajua kwamba ni miundo na mawazo ya kile tunachokiita "watu walio wachache" duniani ambayo yatabadilisha miundo msingi ya lugha yetu kuwa bora. “[[Indigemoji](https://www.indigemoji.com.au/ayeye-akerte-about)] inakuja wakati muhimu wa matumizi ya haraka ya teknolojia na muunganisho mapya katika Australia ya Kati. Inawaalika wenyeji kufikiria kile wangeweza kufanya na mifumo hii mipya. Vile, si tu nguvu nyingine za ukoloni? Na tunawezaje kupachika lugha na utamaduni wetu ndani yake, ili kuzifanya kuwa zetu?”

#### Vitendo

* Teknolojia ya lugha katikati katika miktadha, mahitaji, miundo, na mawazo ya jumuiya za lugha zilizo na msingi wa ndani lakini zilizounganishwa kimataifa, badala ya teknolojia inayochukua lugha moja kuwa inafaa-yote.
* Kuwa mbunifu kuhusu kutumia masafa kamili ya teknolojia ya mtandao ili kuchunguza anuwai kamili ya lugha zilizojumuishwa (simulizi, picha, ishara, maandishi…) ili aina tofauti za maarifa ziweze kuonyeshwa na kushirikiwa kwa urahisi na kwa urahisi.
* Jifunze kutoka kwa Mataifa yetu ya Asili kuunda teknolojia ya lugha kwa kuheshimu kumbukumbu za pamoja na za jamii wakati huo huo kupanga kile kitakachotokea katika siku sijazo. [Hebu tutembee nyuma katika mustakabali wetu](https://www.rnz.co.nz/national/programmes/morningreport/audio/2018662501/ka-mua-ka-muri-walking-backwards-into-the-future).

## Hatimaye, unaweza kufanya nini?

Ikiwa mtu hazungumzi Kiingereza kama wewe, haimaanishi kuwa yeye ni mjinga. Ina maana kwamba wanazungumza vizuri zaidi katika mojawapo ya lugha nyingine 7000 za ulimwengu.

Sisi sote tulio na ujuzi na uzoefu tofauti tunahitaji kufanya kazi pamoja ili kuunda na kupanua mtandao wa lugha nyingi. Pia tunatakiwa kuhakikisha kwamba taarifa na maarifa tunayoshiriki katika lugha hizi nyingi hayasababishi madhara, bali yanaleta manufaa ya pamoja ya ulimwengu wetu. Tunahitaji kile tunachokiita ‘mshikamano katika vitendo’.

### Iwapo uko katika teknolojia:

* Tambua jinsi sera za kampuni yako zinavyochangia (au la) kwa wingi wa lugha mtandaoni, na ukuzaji wa maarifa wa pamoja wa binadamu.
* Usanifu wa lugha katikati (uliotengwa) na ujanibishaji hufanya kazi katika mikakati yako, badala ya kuziona kama za pembeni. Fanya hivyo kwa mtazamo wa washirika wa jumuiya, badala ya mbinu ya juu-chini, isiyo na muktadha.
* Kubali ukosoaji uliofanyiwa utafiti wa kina wa miundo mikubwa ya lugha na teknolojia za lugha otomatiki, na madhara makubwa yanayoweza kusababisha bila uangalifu mzuri wa kibinadamu.
* Jenga katika michakato makini ya kibinadamu ya muktadha, uratibu, na udhibiti wa kazi zote za lugha, kwa kutumia mkusanyiko mdogo wa data iliyotawaliwa na jumuiya.
* Fanya kazi kwa heshima na jamii, hasa zile zilizotengwa zaidi na ambazo zina uwezekano wa kuumizwa zaidi na ukosefu wowote wa utunzaji na uangalifu.
* Rasilimali wale wanaokupa muda na utaalamu kutoka kwa jumuiya hizi za lugha.

### Iwapo uko katika shirika la viwango vya teknolojia:

* Tambua jinsi viwango vya lugha vyenye muktadha vinapaswa kuwa.
* Jenga uhusiano bora na michakato na jamii za lugha zilizotengwa ili viwango zaidi viweze kuwa na washirika wa jamii, ikiwa sio kuongozwa na jamii.
* Alika kwa nia wanachama zaidi kutoka kwa jumuiya za lugha zilizotengwa ili wawe sehemu ya utawala, na uwape nyenzo zinazohitajika ili kushiriki kikamilifu.

### Iwapo uko serikalini:

* Tambua kwamba maudhui katika lugha za raia wako yanahitaji kupatikana kwa wote, si tu wachache waliobahatika.
* Kusaidia upanuzi wa maudhui katika lugha zako kutoka na kwa wale wanaotengwa au kubaguliwa, ndani ya mikoa yako.
* Kusaidia uhifadhi na uwekaji dijitali wa lugha zilizotengwa katika eneo lako, badala ya lugha kuu pekee.

### Iwapo uko katika teknolojia huria na huria na maarifa huria:

* Tambua kwamba teknolojia huria na huria na maarifa pia ina usawa na mipaka yake ya nguvu, ingawa inakusudiwa kwa manufaa ya pamoja.
* Heshimu mipaka ambayo jamii zilizotengwa ziliweka katika kushiriki kwao maarifa, kwa sababu ya jinsi maarifa yao yalivyotumiwa na kutumiwa katika historia hapo awali.
* Fanya kazi na jumuiya za lugha zilizotengwa ili kuunda teknolojia na maarifa wanayohitaji, badala ya wale wanaofikiri wanahitaji.

### Iwapo uko katika taasisi ya GLAM (matunzio, maktaba, kumbukumbu, makumbusho na kumbukumbu):

* Tambua kuwa lugha ndio kiini cha maarifa na tamaduni unazohifadhi, kuhifadhi na kuonyesha.
* Fanya kazi na jumuiya za lugha zilizotengwa ili kuhakikisha kwamba historia na lugha zao zimeidhinishwa, zinakubaliwa na kuimarishwa wanavyotaka, kupitia njia unazoweka alama za asili (au umiliki na eneo la nyenzo). Hii ni pamoja na haki za jamii zilizotengwa kuchagua kutoshiriki maarifa na nyenzo fulani hadharani. Hii ni muhimu kwa sababu taasisi nyingi za GLAM, haswa Kaskazini mwa Ulimwengu, zimejikita katika historia ngumu za ukoloni na ubepari.
* Hakikisha kuwa nyenzo za lugha ambazo zimehifadhiwa katika mikusanyo yako zinapatikana kwa urahisi na kwa urahisi kwa jamii zilizotengwa na washirika wao, ili tuweze kujenga miundombinu ya lugha ya pamoja.

### Ikiwa uko kwenye elimu:
* Tambua jinsi elimu yetu ilivyo na upendeleo kwa vyanzo vya maandishi, na lugha fulani.
* Panua njia zako za kufundisha na kujifunza ili kujumuisha lugha nyingi na aina tofauti za lugha na maarifa zinazojumuisha.
* Soma, sikiliza, na unukuu kazi ya kutafsiri inapowezekana na uwatie moyo wengine wafanye hivyo pia.

### Ikiwa uko katika uchapishaji:

* Tambua jinsi lugha za kikoloni za Uropa zinazochapishwa zaidi ulimwenguni zilivyo kinyume cha sheria.
*Panua idadi ya lugha unazochapisha, na uweke dijiti katika lugha hizi zote.
* Chapisha vitabu na nyenzo zaidi za lugha nyingi.
* Jaribio na aina nyingi za uchapishaji, ili aina tofauti za lugha ya mdomo, inayoonekana, na maandishi inaweza kushirikiwa kwa urahisi zaidi.
* Waheshimu na uwatambue watafsiri wako.

### Ikiwa uko katika uhisani:

* Tambua kwamba lugha ndiyo kiini cha utaalamu wa binadamu, uzoefu na ujuzi wa kila aina, bila kujali unafadhili suala gani.
* Nyenzo za tafsiri za lugha nyingi katika matukio na mikutano mbalimbali ya kimataifa na ya kikanda unayounga mkono.
* Saidia utayarishaji, uhifadhi na uwekaji dijiti wa nyenzo katika lugha za jumuiya unazohudumia, na hakikisha nyenzo zako mwenyewe ziko katika lugha za jumuiya unazohudumia.

### Iwapo uko katika jumuiya ya lugha iliyotengwa:

* Tambua kwamba hauko peke yako.
* Jua kuwa ni haki ya jumuiya yako kuamua ni maarifa gani ungependa kushiriki na ulimwengu na jinsi gani.
* Fanya kazi na wazee, wasomi na vizazi vijana katika jumuiya yako, pamoja na marafiki kutoka jumuiya nyingine, kukusanya na kushiriki ujuzi huu.
* Ikiwa ungependa kuunganishwa na wengine wanaofanya kazi kama hiyo, wasiliana nasi!

### Ikiwa wewe ni mpenzi wa lugha na unajiuliza la kufanya:

* Tambua kwamba lugha ndio kiini chetu na kile tunachofanya, na ni msingi wa maarifa na tamaduni tofauti — ikijumuisha yako mwenyewe!
* Fanya mazungumzo na familia yako, marafiki na jumuiya ili kuona jinsi na kwa nini Kiingereza na lugha nyingine chache hutawala ufikiaji wa mtandao na maudhui, na jinsi ya kubadilisha hayo pamoja.
* Tafuta, soma, sikiliza na ushiriki kikamilifu michango ya jamii za lugha zilizotengwa (pamoja na ripoti hii!)
* Ikiwa ungependa kupokea sasisho kutoka kwa mpango wetu, tufuate kwenye mitandao ya kijamii!

## Shukrani

Tunashikilia kwa upendo, heshima, na mshikamano jamii nyingi zilizotengwa kote ulimwenguni (Wazawa na kwingineko) ambao huona lugha katika kiini cha utambulisho wao na njia zao za kuwa. Jitihada zao za kuhifadhi, kuhuisha na kupanua lugha hizi na miundo ya misemo kwa njia zenye maana, hututia moyo kufikiria mtandao(za) zenye lugha nyingi na wingi ambamo tunaweza kuwa kamili na matajiri zaidi kati ya nafsi zetu nyingi. Pia tunathamini sana jumuiya na wasomi wote wa taasisi na wanateknolojia wanaopenda lugha kama sisi, na wanaofanya kazi kwa bidii kila siku ili kufanya intaneti iwe ya lugha nyingi kama ulimwengu wetu halisi.

Kwa [wachangiaji, watafsiri]({{<trurl>}}/about{{</trurl>}}), na jumuiya zetu nyingi ulimwenguni (hasa wale waliojiunga na mazungumzo yetu ya [Kuondoa ukoloni kwa Lugha za Mtandao mnamo 2019](https://whoseknowledge.org/resource/dtil-report/)): asante kwa yote unayofanya na uliyopo ulimwenguni, na kwa kuwa na subira nasi tulipojaribu kuishi miaka miwili iliyopita! Asante hasa kwa mchoraji wetu kwa taswira za ubunifu za insha, na kwa mwigizaji wetu, ambaye alitupa zawadi ya uhuishaji wake wa vielelezo hivi.

Shukrani nyingi kwa marafiki na jumuiya zetu zote ambao [walikagua]({{<trurl>}}/about{{</trurl>}}) kazi yetu kutoka mitazamo tofauti, na katika lugha tofauti. Makosa yote ni yetu, lakini msaada wako na mshikamano ulisaidia kufanya kazi hii inayoendelea kuwa bora zaidi. Na hatimaye, kwa kila mmoja na kwa familia zetu za damu na chaguo: hatungeweza kufanikiwa miaka michache iliyopita (haswa 2019-21) bila kushirikiana, hata kama tu mtandaoni. Upendo na uaminifu ndio lugha bora kuliko zote.

## Ufafanuzi

Kuna njia nyingi tofauti za kufafanua vipengele tofauti vya lugha, na historia ambazo tumekuwa tukijadili. Sio fasili hizi zote zinazokubaliana! Tumetumia istilahi na misemo fulani kwa njia mahususi katika ripoti hii yote. Hizi ndizo fasili zetu za maneno na vishazi hivi muhimu.

* **Lugha kuu**: Lugha ambazo ama ni lugha zinazozungumzwa na watu wengi katika eneo fulani, au zinazotawala kupitia aina mahususi za nguvu za kihistoria na uthibitishaji, kupitia nguvu za kisheria, kisiasa au kitamaduni. Kwa mfano, Kihindi ni lugha inayotawala katika Asia ya Kusini, kwa kulinganisha na lugha nyingine nyingi, hasa ikizingatiwa kwamba Kihindi chenyewe ni familia ya lugha au kile ambacho wengine huita "lahaja". Vile vile, Kichina cha Mandarin ni lugha inayotawala nchini Uchina, kupitia sera ya serikali, kwa kulinganisha na aina zingine za Kichina na vile vile lugha zingine za asili katika eneo hilo. Baadhi ya lugha kuu pia ni lugha "rasmi" au "kitaifa" katika eneo au nchi.
* **Lugha za kikoloni za Ulaya**: Lugha kutoka Ulaya Magharibi zilizoenea kote Afrika, Asia, Amerika, Karibea na Visiwa vya Pasifiki kupitia michakato ya ukoloni wa makampuni na serikali za Ulaya Magharibi, kuanzia karne ya 16 na kuendelea. Hizi ni pamoja na Kiingereza, Kihispania, Kifaransa, Kireno, Kiholanzi, na Kijerumani. Ni muhimu kutambua kwamba lugha hizi pia zilikuwa lugha za "ukoloni" kwa watu wa Asili wa Amerika Kaskazini, sio tu Amerika ya Kusini (Amerika ya Kati na Kusini).
* **Kusini mwa Ulimwengu na Kaskazini mwa Ulimwengu:** Neno "Kusini mwa Ulimwengu" linamaanisha maeneo ya Afrika, Asia, Amerika ya Kusini na Visiwa vya Karibea na Pasifiki ambavyo vilitawaliwa na nchi za Ulaya Magharibi. Haikusudiwi kuwa neno la jiografia.Badala yake, inakusudiwa kuakisi hali ya kihistoria na inayoendelea kijamii na kiuchumi na kisiasa ambayo inaashiria nchi na kanda hizi, na kuitofautisha na nchi zilizo katika nchi mashuhuri za Uropa na Amerika Kaskazini, au "Kaskazini mwa Ulimwengu". Iliundwa na kuimarishwa na wasomi na wanaharakati kutoka Kusini mwa Ulimwengu ili kwenda kinyume ya kile walichohisi kuwa ni tabia ya dharau na isiyofaa ya maneno kama vile mataifa "yaliyoendelea" na "zinazoendelea", na "Ulimwengu wa Tatu". Kwa sababu ukoloni ulisababisha mauaji ya halaiki au uharibifu wa Mataifa mengi ya Wenyeji katika Kusini mwa Ulimwengu, na kwa sababu baadhi ya watu binafsi na jamii katika Ukanda wa Kusini walifaidika na kushiriki katika ukoloni wa watu wao wenyewe, wakati mwingine tunasema kwamba kuna Kusini mwa Ulimwengu Kaskazini na Kaskazini mwa Ulimwengu katika Kusini mwa Ulimwengu.Miundo na taratibu hizi huathiri hadhi ya lugha katika maeneo haya pia. (tazama neno “walio wachache”)
* **Lugha za kiasili**: Lugha zinazozungumzwa na Mataifa ya kiasili ya eneo au sehemu fulani ni lugha za kiasili. Watu wa kiasili wanaonekana kama "watu wa kwanza" au wakaaji wa kwanza wa maeneo kote ulimwenguni ambayo baadaye yalitawaliwa na koloni na kukaa na kikundi tofauti cha kitamaduni. Lugha nyingi zaidi ya 7000 duniani zinazungumzwa na jamii za wenyeji.
* **Lugha na lahaja**: Tunachukulia mfumo wowote uliopangwa wa kujieleza kati ya wanadamu, iwe kwa sauti, mlio, dokezo, ishara, au maandishi, kuwa lugha. Baadhi ya wanaisimu hufafanua "lahaja" ili kuelezea kile kinachosikika kama aina tofauti za lugha moja, lakini hiyo inaweza "kueleweka" — inayoeleweka na wazungumzaji wote wa aina hizi tofauti ambao wanaweza kuzungumza wao kwa wao. Hata hivyo, kwa sababu tofauti kati ya jinsi "lugha" na "lahaja" inavyofafanuliwa kwa kawaida ni chaguo la kisiasa (badala ya lugha) — kulingana na michakato ya kihistoria ya mamlaka na mapendeleo — mara chache hatujatumia neno lahaja katika ripoti hii. Tunapendelea kutumia neno "familia ya lugha", kuonyesha kwamba kuna lugha nyingi ambazo zinaweza kuwa na historia sawa lakini ambazo pia zina sifa tofauti, kama vile familia ya lugha ambazo ni Kiarabu, Kichina, au Kihindi.
* **Lugha za kienyeji**: Katika ripoti hii, tumefafanua lugha za kienyeji kuwa lugha zinazozungumzwa na idadi kubwa zaidi ya watu katika nchi au eneo.
* **Lugha zilizotengwa**: Katika ripoti hii, lugha zilizotengwa ni zile lugha ambazo si maarufu kwenye mtandao kwa suala la usaidizi wa lugha au maudhui, yaani taarifa na maarifa katika lugha hiyo. Lugha hizi zimetengwa na miundo ya kihistoria na inayoendelea na michakato ya mamlaka na upendeleo, pamoja na ukoloni na ubepari, badala ya idadi ya watu au idadi ya wazungumzaji. Baadhi ya lugha zilizotengwa tayari ziko hatarini duniani (kama vile lugha nyingi za kiasili). Lakini baadhi ya lugha zilizotengwa zinazungumzwa na watu wengi sana katika eneo lao au dunia, na bado haziwakilishwi mtandaoni (kama vile Kipunjabi na Kitamil, au Kihausa na Kizulu, kama mifano michache kutoka lugha nyingi zinazotawala katika Asia na Afrika).
* **Lugha za walio wachache na walio wengi**: Lugha za walio wachache ni zile zinazozungumzwa na wachache (kulingana na idadi) ya idadi ya watu, katika eneo au eneo lolote lililoelezwa, huku lugha ya wengi inazungumzwa na wengi (kwa idadi) kati ya hizo. idadi ya watu.
* **Wachache walio wengi duniani**: Miundo ya kihistoria na inayoendelea ya mamlaka na mapendeleo husababisha ubaguzi na ukandamizaji wa jamii na watu wengi tofauti, kote ulimwenguni. Aina hizi za mamlaka na upendeleo mara nyingi hufungamana na kuingiliana, kwa hivyo baadhi ya jamii hupungukiwa au kukandamizwa kwa njia nyingi: kwa mfano, kwa jinsia, rangi, ujinsia, tabaka,  dini, eneo, uwezo, na bila shaka, kwa lugha. Iwe mtandaoni au katika ulimwengu wa kimwili, jumuiya hizi zinaunda sehemu kubwa ya ulimwengu kulingana na idadi ya watu, lakini mara nyingi hawako katika nyadhifa za mamlaka, na kwa hivyo wanachukuliwa kama wachache. Kwa maneno mengine, wao ni “Wachache walio wengi” duniani.

[Zaidi kuhusu jinsi ya kutaja na kutumia ripoti hii.]({{<trurl>}}/license{{</trurl>}})

[Zaidi kuhusu rasilimali na misukumo yetu.]({{<trurl>}}/resources{{</trurl>}})

[Pakua ripoti hii (PDF 1.8 MB).](/media/pdf-summary/SW-STIL-SummaryReport.pdf)