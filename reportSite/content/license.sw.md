---
author: Whose Knowledge
title: Leseni na Manukuu
date: 2019-05-13
toc: false
---

Tunasawazisha uwazi wa maudhui, data na muundo na hadhi na usalama wa jamii zilizotengwa. Tunataka ripoti ya Hali ya Lugha za Mtandao ishirikiwe kwa urahisi na kwa uhuru, kwa sababu tunaamini kuwa kazi hii ni ya msingi kwa mtandao ulioondoa ukoloni wa lugha nyingi na teknolojia za kidijitali. Wakati huo huo, kwa sababu ripoti hii inajumuisha kazi kutoka na pamoja na jamii zilizotengwa ambao kihistoria wameona ujuzi wao ukitumiwa vibaya na wengine, **tunaheshimu yote wanayoshiriki nasi na ulimwengu kwa ukarimu**.

Tumejitolea kutoa leseni ya **maudhui, ikiwa ni pamoja na sanaa na muundo** kwenye tovuti hii kwa **leseni ya wazi**: [CC NC-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/). Hii ina maana kwamba isipokuwa kubainishwa vinginevyo, maelezo na maudhui kwenye tovuti hii yanaweza kushirikiwa, kubadilishwa au kutumika tena bila malipo (lakini huwezi kuitumia kwa faida au sababu yoyote ya kibiashara). Ni lazima utambue Ujuzi ni wa Nani?, Taasisi ya Mtandao ya Oxford, Kituo cha Mtandao na Jamii, na waundaji wengine wa maudhui haya unapofanya hivyo, iwe jumuiya, mashirika au watu. Pia lazima usambaze kazi yako mpya na leseni sawa. 

Tunatoa leseni ya **data** kwenye tovuti hii [ODbL](https://opendatacommons.org/licenses/odbl/summary/) leseni wazi. Hii ina maana kwamba unaweza kutumia, kuunda, na kurekebisha kutoka kwa hifadhidata ambayo imeundwa kwa ajili ya ripoti hii. Ni lazima utanbue Martin Dittus, Mark Graham na Taasisi ya Mtandao wa Oxford, pamoja na ripoti ya Hali ya Lugha za Mtandaoni, unapofanya hivyo, na lazima pia usambaze kazi yako mpya kwa leseni sawa.

Tumejitolea pia kutoa leseni **msimbo wa muundo** wa tovuti hii kwa kutumia [MIT](https://choosealicense.com/licenses/mit/) leseni, ambayo ina maana kwamba unaweza kutumia tena, kubadilisha na kurekebisha msimbo kwa madhumuni yoyote, mradi tu ujumuishe nakala asili ya leseni ya MIT unaposambaza kazi yako mpya.

Tunapendekeza umbizo hili litunukuu:

_“Whose Knowledge?, et al. “Ripoti ya Lugha ya Hali ya Mtandao”, mpango uliofanyiwa utafiti na kurekodiwa kwa ushirikiano, Tarehe, Muda,internetlanguages.org._

Tunashukuru [Sara Ahmed](https://feministkilljoys.com/2013/09/11/making-feminist-points/), [P. Gabrielle Foreman](https://coloredconventions.org/about/team/), [Jessica Marie Johnson](https://twitter.com/jmjafrx) na [Kelly Foster](https://kellyfoster.co.uk/)kwa uongozi wao katika vitendo vya uadilifu na vikali vya kunukuu. Kufuatia hayo, tunakuomba ufurahie kushiriki ripoti hii kwa madondoo au kwa ujumla wake, lakini usisahau kuitaja. Hii inatumika kwa ripoti na vile vile kwenye mitandao ya kijamii inayotaja kuihusu. Tafadhali tutambulishe au utujulishe ikiwa unaishiriki, ili tuweze kufuatilia manufaa na athari zake (@whoseknowledge). Ikiwa una maswali kuhusu leseni au manukuu, [tafadhali wasiliana nasi](mailto:languages@whoseknowledges.org).