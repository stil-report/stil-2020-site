---
Author1: Anasuya Sengupta
Author2: Abbey Ripstra
Author3: Adele Vrana
Title: 'Ringkasan Laporan'
Date: 2020-01-15
hasaudio: true
hasboxes: true
translationKey: 
Imageauthor1: Anasuya_Sengupta.webp
Imageauthor2: Abbey_Ripstra.webp
Imageauthor3: Adele_Vrana.webp
haspdf: true
PDFsummary: /media/pdf-summary/ID-STIL-SummaryReport.pdf
hasaudio: true
Audio1: https://archive.org/download/STIL-Summary-IND/01_Indonesian.mp3
Audio2: https://archive.org/download/STIL-Summary-IND/02_Indonesian.mp3
Audio3: https://archive.org/download/STIL-Summary-IND/03_Indonesian.mp3
Audio4: https://archive.org/download/STIL-Summary-IND/04_Indonesian.mp3
Audio5: https://archive.org/download/STIL-Summary-IND/05_Indonesian.mp3
Audio6: https://archive.org/download/STIL-Summary-IND/06_Indonesian.mp3
Audio7: https://archive.org/download/STIL-Summary-IND/07_Indonesian.mp3
Audio8: https://archive.org/download/STIL-Summary-IND/08_Indonesian.mp3

---

## Mengapa laporan ini? Siapakah kami?

Menurut kamus dan tata bahasa, bahasa adalah cara yang terstruktur untuk mengungkapkan informasi, terutama di antara manusia. Namun lebih jauh dari itu, bahasa adalah warisan fundamental yang kita tawarkan satu sama lain, seringkali diberikan kepada kita oleh leluhur kita, dan jika kita beruntung, kita dapat meneruskannya pada mereka yang hadir setelah kita. Ketika kita berpikir, ketika kita berbicara, ketika kita mendengar, ketika kita berimajinasi... kita menggunakan bahasa untuk kita sendiri dan satu sama lain. Bahasa adalah inti dari diri kita, bagaimana kita berada di dunia ini. Bahasa membantu kita bercerita dan berbagi tentang apa yang kita ketahui tentang diri kita sendiri dan satu sama lain. Bahasa apa yang dipakai? Apakah bermimpi dengan bahasa tersebut? Apakah berpikir dengan bahasa yang berbeda dari yang kita pakai untuk bekerja? Apakah musik yang kita sukai memakai bahasa yang berbeda dan tidak selalu kita mengerti?

Setiap bahasa adalah sebuah sistem keberadaan, praktek dan komunikasi dalam dunia, dan yang terpenting sebagai sistem pengetahuan dan imajinasi. Setiap bahasa adalah sistem pengetahuan tersendiri: bahasa kita adalah cara fundamental bagi kita untuk memahami dunia kita dan untuk menjelaskannya kepada yang lain. Bahasa kita dapat berupa oral (tuturan atau tanda), atau ditransmisikan melalui suara seperti [peluit atau drum](https://pages.ucsd.edu/~rose/Whistled%20and%20drum%20languages.pdf)! Dalam semua bentuk ini, **bahasa adalah proxy untuk pengetahuan**. Dengan kata lain, bahasa adalah cara yang paling jelas untuk mengungkapkan apa yang kita pikirkan, kita yakini, dan kita ketahui.

Sekarang, pikirkan bahasa yang Anda pakai untuk bicara, berpikir, bermimpi atau menulis. Dari bahasa tersebut, berapa banyak yang dapat dibagikan dan dikomunikasikan dalam ruang-ruang digital? Apa pengalaman Anda dalam menggunakan bahasa atau bahasa-bahasa secara daring? Apakah hardware yang digunakan mempunyai huruf-huruf dalam bahasa Anda? Apakah perlu memodifikasi keyboard untuk menggunakannya dalam bahasa Anda? Ketika mencari informasi menggunakan mesin pencarian, apakah hasilnya ditemukan dalam bahasa yang diinginkan? Apakah Anda harus belajar bahasa yang berbeda dari bahasa sendiri untuk mengakses dan berkontribusi di internet? Jika jawaban pada beberapa atau banyak dari pertanyaan ini adalah “tidak”, maka Anda adalah salah satu dari orang-orang yang beruntung di dunia yang bisa menggunakan internet dalam bahasa sendiri dengan mudah. Dan mungkin bahasa Anda adalah bahasa... Inggris.

Internet dan berbagai ruang digital menyediakan satu dari infrastruktur yang paling penting dalam pengetahuan, komunikasi dan tindakan saat ini. Namun demikian dengan lebih dari 7000 bahasa yang dipakai di dunia (termasuk bahasa lisan dan bahasa isyarat), **berapa dari bahasa-bahasa ini yang dapat digunakan seutuhnya secara daring? Bagaimana bentuk, rasa dan suara sebenarnya dari internet yang multilingual?**

Laporan ini adalah salah satu cara kami mencoba menjawab pertanyaan tersebut. Kami adalah [kolaborasi]({{<trurl>}}/about{{</trurl>}}) dari tiga organisasi, yaitu: Whose Knowledge?, Oxford Internet Institute, dan The Centre for Internet and Society (India). Kami bersama-sama menawarkan berbagai pandangan, pengalaman dan analisa tentang keadaan bahasa di internet dan bekerjasama dengan pihak lain yang peduli dengan isu ini. Kami berharap dapat menciptakan internet yang lebih multilingual dengan teknologi digital dan lebih praktis.

Laporan ini bermaksud melakukan tiga hal:

* **Memetakan status bahasa-bahasa dalam internet saat ini**: Kami mencoba memahami bahasa mana yang saat ini merupakan representasi dalam internet dan bagaimana representasi tersebut. Kami melakukannya melalui data kuantitatif (melihat angka yang terbentang dari berbagai platform digital, alat dan ruang), serta data kualitatif (mempelajari dari cerita dan pengalaman orang-orang dalam bahasa daring).
* **Meningkatkan kesadaran tentang tantangan dan kesempatan dalam membentuk internet lebih multilingual**: Menciptakan dan mengelola teknologi, konten dan komunitas bagi bahasa-bahasa di dunia memiliki tantangan yang berat namun juga memiliki kemungkinan dan kesempatan yang seru. Laporan ini akan membahas beberapa tantangan dan kemungkinan.
* **Memajukan agenda aksi:** Dengan masukan dan kesadaran ini, kami dan banyak lainnya yang bekerja dalam isu ini di seluruh dunia menawarkan cara-cara untuk merencanakan dan bertindak untuk memastikan internet lebih multilingual.

### Apakah yang ada dan tidak ada dalam laporan ini?

Laporan ini adalah kinerja yang berkembang dan berproses.

Banyak individu, komunitas dan institusi yang berbeda telah bekerja dalam berbagai aspek bahasa dalam jangka waktu yang lama dan aspek yang berbeda dari bahasa daring, baru-baru ini. Mereka menginspirasi kami, tetapi laporan ini tidak dimaksudkan untuk menjadi survei yang lengkap dari semua kinerja setiap orang. Kami juga tidak mengetahui setiap orang yang bekerja terkait bahasa dan kinerja mereka, meskipun kita sudah mencoba untuk memasukkan sebagian besar yang kami ketahui dan yang menginspirasi, dengan beberapa cara, dengan memasukkan mereka dalam bagian [sumber]({{<trurl>}}/resources{{</trurl>}}) dan [penghargaan](#toc_6_H2).

Kami memiliki keterbatasan dengan data yang bisa kami kumpulkan, dan kami membahas beberapa hambatan dalam bagian [angka]({{<trurl>}}/numbers{{</trurl>}}). Kami menyambut baik komentar dan usulan untuk memperbaiki dan memperbaharui informasi yang kami tawarkan disini, dan kami sangat ingin mendengar dari mereka yang sudah bekerja dalam isu ini dan yang ingin dimasukkan dalam pembaharuan laporan ini di masa depan.

Kami sudah melakukan yang terbaik untuk menulis laporan agar dapat diakses semudah mungkin. Kami ingin beberapa generasi dan komunitas untuk bergabung dalam kerja kami, dan kami juga tidak ingin ada jargon atau bahasa “akademis” yang menjadi hambatan untuk membaca dan merefleksikannya. Kami juga ingin laporan ini diterjemahkan dalam sebanyak mungkin bahasa (penerjemah: [hubungi kami]({{<trurl>}}/engage{{</trurl>}})!), dan meskipun kami awalnya menulis laporan ini dalam bahasa Inggris, kami tidak ingin bahasa Inggris menjadi hambatan baik untuk menjadi refleksi atau untuk bertindak.

Kami berharap bahwa laporan ini dapat digunakan sebagai “dasar” untuk riset, diskusi dan aksi terkait isu-isu ini, sambil membangun usaha-usaha yang sudah ada sebelumnya.

### Siapa kami, dan mengapa kami bersama mengerjakan laporan ini?

Tiga organisasi bersama mengerjakan riset untuk laporan ini: The Centre for Internet and Society, Oxford Internet Institute, dan Whose Knowledge? Kami semua tertarik dengan implikasi internet dan teknologi digital dari berbagai perspektif riset, kebijakan dan advokasi.

Selama beberapa tahun terakhir, kami bekerja dengan cara kami sendiri untuk memahami ketidaksetaraan pengetahuan dan ketidakadilan di internet: siapa dan bagaimana berkontribusi pada konten daring? Kami segera menyadari bahwa hanya terdapat sedikit sekali data tentang pengetahuan dalam bahasa yang berbeda di internet. Maka, kami ingin mengetahui lebih dalam sejauh mana bahasa-bahasa di dunia ada di internet saat ini. Eksplorasi kami dibatasi pada beberapa area dimana kami bisa menemukan informasi publik dan terbuka, tetapi kami berharap ini bisa menjadi kontribusi untuk kita semua yang mengusahakan internet multilingual.

*Catatan singkat tentang Covid-19 dan laporan ini:* Kami memulai kerja untuk laporan ini pada tahun 2019 sebelum Covid-19, tetapi kebanyakan kerja analisis, wawancara dan penulisan dilakukan selama pandemi global yang telah mengubah hidup kita baik secara individu maupun kolektif. Setiap orang yang berkontribusi dalam laporan ini mengalami dampaknya dan memerlukan waktu yang lebih lama dari antisipasi kami untuk berbagi dengan dunia. Namun Covid-19 juga membantu kita mengingat bagaimana kita saling terhubung, bagaimana pentingnya bagi kita untuk dapat menyampaikan ide yang kompleks dalam berbagai bahasa di dunia, dan bagaimana pentingnya untuk memiliki infrastruktur (digital) yang dapat bertahan dan dapat diakses yang sungguh-sungguh multilingual.

## Bagaimana membaca laporan ini?

Laporan ini adalah yang kita sebut “digital first”. Cara terbaik untuk membaca, mendengar dan mempelajarinya adalah melalui website ini, karena laporan ini memiliki beberapa lapisan dan tingkatan. Laporan kami menyajikan bersama [angka]({{<trurl>}}/numbers{{</trurl>}}) dan [pengalaman]({{<trurl>}}/stories{{</trurl>}}). Mempelajari tentang kondisi bahasa daring dari perspektif statistik memberikan kita gambaran tentang isu dan membantu kita memahami konteks yang berbeda dari pengalaman orang-orang. Namun pengalaman orang-orang tentang bahasa di internet dari seluruh dunia, dalam konteks yang berbeda, membantu kita mempelajari lebih dalam tentang seberapa mudah atau sulitnya bagi orang-orang untuk menggunakan internet dalam bahasa mereka. Baik dengan cerita maupun angka, kita mulai menanggapi konteks, tantangan dan kesempatan yang mendasarinya.

Maka dari itu lapisan ini memiliki tiga lapisan utama:

* Ringkasan tentang kondisi laporan bahasa dan bagaimana kami menyusunnya (yang Anda baca saat ini!)
* [Angka]({{<trurl>}}/numbers{{</trurl>}}) yang menganalisa beberapa isu penting dalam beberapa platform, aplikasi dan gawai yang kita gunakan setiap hari. Kawan-kawan dari Oxford Internet Institute memimpin kerja ini dan akan menemukan visualisasi data dan analisa yang menakjubkan disini. Perlu dicatat bahwa analisis ini terbatas pada data yang dapat kami akses dan materi bersifat publik yang tersedia. Hambatan metodologi lain dibahas lebih detail dalam esai ini, namun yang terpenting adalah sulitnya menemukan satu cara yang konsisten untuk mengidentifikasi bahasa. Demikian juga sulit untuk memperkirakan berapa orang yang menggunakan bahasa tertentu, terutama karena bahasa dan penggunaannya adalah hal yang dinamis dan berubah dari waktu ke waktu.
* [Pengalaman]({{<trurl>}}/stories{{</trurl>}}) yang memberi kita pemahaman yang lebih mendalam tentang bagaimana orang-orang dan komunitas dari seluruh dunia mengalami internet dalam bahasa mereka sendiri, dan dalam banyak kasus adalah bagaimana sulitnya menemukan informasi yang mereka butuhkan dalam bahasa mereka sendiri saat ini. Kami [mengundang](https://whoseknowledge.org/initiatives/callforcontributions/) cerita-cerita ini dalam bentuk tertulis dan lisan, jadi Anda akan menemukan esai berupa teks namun juga wawancara audio dan video. Kawan-kawan dari The Centre for Internet and Society memimpin kerja ini, mengumpulkan hamparan yang kaya dari pengalaman berbahasa dari seluruh dunia. Kami memiliki kontribusi tentang bahasa masyarakat asli seperti Chindali, Cree, Ojibway, Mapuzugun, Zapotec, dan Arrernte dari Afrika, Amerika, dan Australia; bahasa minoritas seperti Breton, Basque, Sardinian, dan Karelian di Eropa; serta bahasa dominan secara regional dan global seperti Bengali, bahasa Indonesia dan Sinhala di Asia, dan berbagai bentuk bahasa Arab di Afrika Utara.

Yang terpenting, kontributor kami menulis dan berbicara dalam bahasa mereka sekaligus dalam bahasa Inggris, dan ringkasan kami juga disampaikan secara lisan dan tulisan dalam berbagai bahasa, jadi kami berharap Anda menikmati, membaca dan mendengarkan dalam lebih dari satu bahasa!

Kami juga melakukan yang terbaik untuk menghidupkan kontribusi ini dalam bentuk visual, dengan ilustrasi imajinatif dan animasi yang mengumpulkan aspek teknis dan sosial dari bahasa dengan hati-hati. Sebagaimana semua hal lain dalam laporan ini, ilustrasi juga berkembang dengan kolaborasi para ilustrator dalam percakapan dengan kontributor kami.

## Seberapa multilingual kah internet?

Internet belum (dan sayangnya jauh) dari multilingual dibandingkan dengan kehidupan nyata. Kita mencoba memahami alasannya dengan melihat baik [angka]({{<trurl>}}/numbers{{</trurl>}}) maupun [pengalaman]({{<trurl>}}/stories{{</trurl>}}) dari semua orang di seluruh dunia. Di sini kami memberikan hanya ringkasan pendek dan analisa kekayaan dan kedalaman dari kerja yang dilakukan oleh kontributor kami dari seluruh dunia. Silakan melihat esai mereka untuk inspirasi dan detailnya.

Pertama kami melihat konteks dimana orang-orang menggunakan internet di seluruh dunia dalam bahasa yang berbeda. Kami melihat cara-cara dimana informasi dan pengetahuan didistribusikan atau tidak, dalam berbagai bahasa dan letak geografis. Kemudian kami melihat lebih dekat pada platform dan aplikasi utama yang kita gunakan untuk menciptakan konten, berkomunikasi, dan berbagi informasi secara daring, dan ada berapa bahasa yang tersedia. Kami melihat secara detail di Google Maps dan Wikipedia sebagai dua ruang konten multilingual yang digunakan oleh banyak pengguna internet setiap hari dan bagaimana penerapannya dalam berbagai bahasa.

Sepanjang jalan, kami berbagi cerita dan pengalaman dari orang-orang yang mengakses dan berkontribusi pada pengetahuan di internet dalam bahasa mereka sendiri. Sebagaimana kita pelajari, kebanyakan kontributor kami menyadari kebutuhan untuk beralih dari pilihan bahasa pertama ke bahasa lain agar dapat mengakses dan berkontribusi pada isu yang mereka hargai.

### Konteks bahasa: ketidaksetaraan geografis dan pengetahuan digital

{{< summary-quote
    text="Bahasa dengan tradisi lisan tidak sesuai dengan web yang kita miliki saat ini."
    author="Ana Alonso"
    link="/id/stories/dill-wlhall-on-the-web/"
>}}

{{< summary-quote
    text="Kami merasa bahwa platform-platform ini, secara umum, melanggengkan ide kolonialis bahwa ada bahasa dengan nilai dan kapasitas yang lebih besar untuk berkomunikasi, yang merupakan pandangan yang merugikan bahasa minoritas seperti Mapuzugun."
    author="Kimeltuwe project"
    link="/id/stories/use-of-our-ancestral-language/"
>}}

Kita tahu bahwa [lebih dari 60% populasi dunia](https://www.internetworldstats.com/stats.htm) diperkirakan terhubung secara digital, kebanyakan lewat telepon dan perangkat. Dari mereka yang daring, tiga perempat dari kelompok tersebut berasal dari Global South: dari Asia, Afrika, Amerika Latin, serta Kepulauan Karibia dan Pasifik. Namun seberapa bermakna dan setarakah akses kita? Apakah kita dapat menciptakan dan memproduksi pengetahuan daring publik seluas kita mengkonsumsinya?.

Dalam survei Martin dan Mark tentang populasi global dibandingkan dengan jumlah pengguna internet, kita menemukan bahwa populasi tertentu dapat mengakses internet dengan lebih bermakna dari lainnya, termasuk dalam ruang digital yang terkenal. Misalnya, meskipun kebanyakan dari kita yang menjadi pengguna internet berasal dari Global South, kita tidak dapat mengakses internet sebagai pencipta dan produsen pengetahuan, tetapi hanya sebagai konsumen. Kebanyakan edit Wikipedia, mayoritas akun Github (tempat penyimpanan kode pemrograman) dan pengguna paling banyak dari Tor (browser atau peramban aman) berasal dari Eropa dan Amerika Utara.

{{< summary-side-fig-fancybox
    src_small="/media/summary/STIL-Internet-regions-500px.webp"
    src_full="/media/summary/STIL-Internet-regions-1000px.webp"
    alt="Mengukur partisipasi digital berdasarkan wilayah di dunia. (Data: World Bank 2019, Wikimedia Foundation 2019, Wikipedia 2018, Github 2020, Tor 2019)"
    caption="Mengukur partisipasi digital berdasarkan wilayah di dunia. (Data: World Bank 2019, Wikimedia Foundation 2019, Wikipedia 2018, Github 2020, Tor 2019)"
    PDFlink="/media/pdf/STIL-Internet-regions.pdf"
>}}

Dan apakah arti akses yang tidak merata ini pada bahasa? Apakah kita semua dapat mengakses internet dalam bahasa kita sendiri? Apakah kita dapat menciptakan konten dan informasi dalam bahasa kita sendiri?.

Sebagaimana [proyeksi lain](https://www.internetworldstats.com/stats7.htm) menunjukkan, lebih dari 75% orang yang mengakses internet menggunakan 10 bahasa saja — kebanyakan dari bahasa tersebut adalah bahasa dengan sejarah kolonial (Inggris, Prancis, Jerman, Portugis, Spanyol …) atau bahasa yang dominan dalam wilayah tertentu dimana bahasa lain harus berjuang untuk tetap relevan (Cina, Arab, Rusia...). Pada tahun 2020, diperkirakan bahwa [25.9% pengguna internet daring menggunakan bahasa Inggris](https://www.statista.com/statistics/262946/share-of-the-most-common-languages-on-the-internet/), sementara 19.4% mengakses dalam bahasa Cina. Cina adalah negara dengan pengguna internet terbesar di seluruh dunia dan layak untuk diingat bahwa apa yang kita sebut sebagai “Cina” bukanlah satu bahasa namun [ sebuah rumpun dari berbagai bahasa](https://www.oxfordhandbooks.com/view/10.1093/oxfordhb/9780199856336.001.0001/oxfordhb-9780199856336-e-1).

Menariknya, bahasa-bahasa internet saat ini berasal dari Eropa, tetapi Eropa memiliki paling sedikit bahasa dibanding benua lainnya. [Dari lebih dari 7000+ bahasa di dunia, 4000 lebih berasal dari Asia dan Afrika](https://www.ethnologue.com/guides/how-many-languages#population) (masing-masing lebih dari 2000 bahasa), dan kepulauan Pasifik dan Amerika memiliki masing-masing lebih dari 1000 bahasa. Papua Nugini dan Indonesia adalah [negara dengan bahasa terbanyak](https://www.ethnologue.com/guides/countries-most-languages), dengan lebih dari 800 bahasa di PNG dan lebih dari 700 bahasa di Indonesia.

{{< summary-side-fig-fancybox
    src_small="/media/summary/living_languages_vs_population__pies_500px.webp"
    src_full="/media/summary/living_languages_vs_population__pies_1000px.webp"
    alt="Jumlah bahasa dan populasi total penutur berdasarkan wilayah di seluruh dunia. Sumber:"
    caption="Jumlah bahasa dan populasi total penutur berdasarkan wilayah di seluruh dunia. Sumber:"
    source="Ethnologue"
    link="https://www.ethnologue.com/guides/continents-most-indigenous-languages"
    PDFlink="/media/pdf/living_languages_vs_population__pies.pdf"
>}}

Banyak bahasa dari Asia Selatan (Hindi, Bengali, Urdu...) [berada di 10 bahasa teratas](https://www.ethnologue.com/guides/ethnologue200) menurut jumlah penutur asli, namun bahasa ini bukan bahasa yang digunakan oleh orang-orang di Asia Selatan untuk mengakses internet. Dan tentu saja, seperti kita pelajari dari [Ishan]({{<trurl>}}/stories/marginality-within-marginality{{</trurl>}}) yang bahasa pertamanya adalah Bengali tentang marginalisasi di dalam marginalisasi, bahkan ketika anda bisa mengakses pengetahuan digital yang anda pilih, jenis informasi yang anda cari mungkin tidak ada di dalamnya; dalam kasus Ishan, adalah konten tentang disabilitas dan hak seksual. Situasi di Asia Tenggara—di mana beberapa negara termasuk ke dalam daftar negara dengan pengguna internet tertinggi di dunia dan memiliki keberagaman bahasa terbesar—juga tidak jauh berbeda. [Paska]({{<trurl>}}/stories/flickering-hope{{</trurl>}}) menemukan masalah yang sama terkait konten hak-hak seksualitas dalam bahasa Indonesia sebagaimana masalah yang ditemukan Ishan dalam bahasa Bengali.

Kami juga mengetahui bahwa dari 7000 bahasa di dunia, [hanya ada 4000](https://www.ethnologue.com/enterprise-faq/how-many-languages-world-are-unwritten-0) yang mempunyai sistem tulisan atau aksara. Namun demikian, kebanyakan aksara ini tidak dikembangkan oleh penutur dalam bahasa itu sendiri, tapi sebagai bagian dari banyak proses kolonisasi di seluruh dunia. Hanya dengan memiliki aksara tidak berarti bahasa ini digunakan atau dipakai secara luas. Banyak bahasa di dunia disampaikan dengan tuturan atau bentuk tanda dan tidak melalui tulisan. Bahkan dalam bahasa yang memiliki aksara tertulis, penerbitan cenderung dilakukan dalam bahasa kolonial Eropa, dan jauh lebih sedikit lagi dalam bahasa dominan regional. Pada tahun 2010, Google memperkirakan ada [130 juta buku yang pernah diterbitkan](https://www.pcworld.com/article/508405/google_129_million_different_books_have_been_published.html), dengan proporsi yang signifikan dalam 480 bahasa. Jurnal akademis yang paling mapan dalam [ilmu pasti](https://www.theatlantic.com/science/archive/2015/08/english-universal-language-science-research/400919/) atau [ilmu sosial](https://www.tandfonline.com/doi/abs/10.1080/01639269.2014.904693) ada dalam bahasa Inggris. Buku [yang paling banyak diterjemahkan](https://en.wikipedia.org/wiki/Bible_translations) adalah Injil (ke dalam lebih dari 3000 bahasa), dan [dokumen yang paling diterjemahkan](https://www.ohchr.org/EN/UDHR/Pages/Introduction.aspx) di dunia adalah [Dekalarasi Hak Asasi Manusia](https://en.wikipedia.org/wiki/Universal_Declaration_of_Human_Rights) dari PBB (ke dalam lebih dari 500 bahasa).

Mengapa hal ini penting? Karena teknologi bahasa digital bergantung pada proses otomatis dari materi yang dipublikasi dalam berbagai bahasa untuk mengembangkan dukungan pada bahasa dan konten. Jadi ketika teks yang dipublikasi itu sendiri cenderung bias pada bahasa tertentu — dan tidak termasuk bahasa non tulis — hal ini bergantung pada ketidaksetaraan bahasa yang kita alami. Dan tentu saja, bahasa yang berdasar pada non-teks — yang berdasar pada isyarat, suara, sikap dan gerakan tubuh— sepenuhnya hilang dari industri penerbitan dan maka dari itu seringkali hilang dalam teknologi bahasa digital.

Misalnya,[Ana]({{<trurl>}}/stories/dill-wlhall-on-the-web{{</trurl>}}) bercerita bahwa, “web tidak dirancang untuk merespon pengguna yang hanya memiliki tradisi lisan saja.” Dalam dominasi bahasa tulis di internet, konten dalam tradisi bahasa lisan dan visual sangat sulit ditemukan. Kita tidak mudah mencari sikap, isyarat dan siulan, misalnya. Dalam wawancara kami dengan [Joel dan Caddie]({{<trurl>}}/stories/signs-across-generations{{</trurl>}}), mereka menceritakan emoji pertama Australia dari masyarakat adat diciptakan di Arrernte land di Mparntwe/Alice Springs, dan bagaimana gerakan fisik seringkali dikombinasikan dengan kata-kata lisan untuk membentuk makna di Arrernte. [Emna]({{<trurl>}}/stories/write-in-another-language{{</trurl>}}) menyatakan hal yang sama tentang Tunisia, dan bahasa lisan yang dipakai orang-orang disana : “ketika kita ingin melestarikan bahasa, kita tidak bisa hanya berfokus pada tulisan, kita perlu melakukannya dalam bentuk lisan, sikap, isyarat, siulan dan sebagainya, dan hal ini tidak dapat ditangkap sepenuhnya dalam bahasa tulis.

Teknologi digital menawarkan kemungkinan pada kita untuk merepresentasikan pluralisme bentuk bahasa yang berdasarkan pada tulisan, suara, sikap dan masih banyak lagi. Teknologi juga bisa membantu kita melestarikan dan menghidupkan kembali bahasa-bahasa yang terancam punah: [lebih dari 40% bahasa](https://www.endangeredlanguages.com/about/). Setiap bulan, [2 bahasa masyarakat adat]((https://news.un.org/en/story/2019/12/1053711)) dan pengetahuan yang diungkapkannya punah dan hilang dari kita.

Mengapa konteks bahasa yang berbeda ini tidak direpresentasikan lebih baik secara daring?.

Dalam esainya, [Claudia]({{<trurl>}}/stories/decolonizing-minority-language{{</trurl>}}) menawarkan pada kita tiga dimensi untuk memahami hubungan antara bahasa dan teknologi: ketersediaan, penggunaan dan bagaimana teknologi dikembangkan. Seperti yang kami temukan sepanjang laporan, apa yang disebut Claudia sebagai “bahasa mayoritas” (dan kami menemukan bahwa sebagian besar adalah bahasa Eropa kolonial dan bahasa dominan regional) mempunyai sederet media, jasa, interface dan aplikasi yang tersedia sementara bahasa lain sangat tidak tersedia, termasuk dalam kerangka infrastruktur seperti keyboards, mesin penerjemahan atau pengenalan ucapan. Perusahaan teknologi juga lebih menghabiskan waktu dan sumber daya untuk penggunaan bahasa mayoritas ini karena mereka melihat keuntungan yang lebih besar. Akhirnya, Claudia menemukan bahwa teknologi bahasa dikembangkan dengan proses dari atas ke bawah dengan sedikit kolaborasi dengan komunitas bahasa, atau beberapa usaha untuk bekerja dengan komunitas tidak terencana dan tidak terkoordinasi dengan baik.

Keprihatinan dan tantangan konteks juga memberikan kami langkah maju untuk menciptakan internet yang multilingual, dan kami [akan kembali dengan kemungkinan ini](#toc_4_H2).

### Ketersediaan bahasa: platform dan aplikasi pesan

{{< summary-quote
    text="Ketika anda menulis “selamat pagi” dalam bahasa Inggris, sebelum anda selesai menulis, telepon atau komputer akan mengusulkan kata. Ketika saya menulis selamat pagi dalam bahasa Chindali “mwalamusha”), saya harus mengetik seluruh kata dan perlu waktu lama, dan akan digarisbawahi karena komputer atau telepon tidak mengenali kata ini."
    author="Donald Flywell Malanga"
    link="/id/stories/challenges-of-expressing-chindali/"
>}}

{{< summary-quote
    text="Keyboards dengan huruf Sinhala dan Tamil jarang ditemukan. Orang tua kami mencetak huruf Sinhala yang sangat kecil, digunting dan ditempelkan di sebelah huruf Inggris. Meskipun banyak huruf Sinhala telah dikembangkan, namun tidak ada yang berfungsi sebagus huruf Unicode."
    author="Uda Deshapriya"
    link="/id/stories/amidst-virtual-impunity/"
>}}

{{< summary-quote
    text="Jika aplikasi populer dan kunci perangkat lunak interfaces tidak disediakan dalam bahasa Breton segera, karena tidak bisa bersaing dengan aplikasi berbahasa Prancis, bahasa ini dipastikan tidak akan menarik lagi bagi generasi yang lebih muda."
    author="Claudia Soria"
    link="/id/stories/decolonizing-minority-language/"
>}}

Kami memahami lebih dalam bagaimana internet belum multilingual seperti dalam kehidupan dunia nyata. Kami melihat jenis pendukung bahasa — seperti tampilan pengguna dalam berbagai bahasa — yang disediakan oleh platform digital dan aplikasi besar untuk berkomunikasi, menciptakan dan berbagi konten dalam bahasa kita.

{{< summary-gallery-fancybox
    caption_all="Interface Wikipedia dalam berbagai bahasa"

    src_small1="/media/summary/Bengali-Wikipedia-500px.webp"
    src_full1="/media/summary/Bengali-Wikipedia-1000px.webp"
    caption1="Interface Wikipedia dalam bahasa Bengali"

    src_small2="/media/summary/Breton-Wikipedia-500px.webp"
    src_full2="/media/summary/Breton-Wikipedia-1000px.webp"
    caption2="Interface Wikipedia dalam bahasa Breton"

    src_small3="/media/summary/English-Wikipedia-500px.webp"
    src_full3="/media/summary/English-Wikipedia-1000px.webp"
    caption3="Interface Wikipedia dalam Bahasa Inggris"

    src_small4="/media/summary/Spanish-Wikipedia-500px.webp"
    src_full4="/media/summary/Spanish-Wikipedia-1000px.webp"
    caption4="Interface Wikipedia dalam bahasa Spanyol"

    src_small5="/media/summary/Zapotec-Wikipedia-500px.webp"
    src_full5="/media/summary/Zapotec-Wikipedia-1000px.webp"
    caption5="Interface wikipedia dalam bahasa Zapoteco"
>}}

[Martin dan Mark]({{<trurl>}}/numbers/a-platform-survey{{</trurl>}}) menganalisa pendukung bahasa untuk 11 websites, 12 aplikasi android dan 16 aplikasi iOS. Mereka memilih platform yang digunakan secara luas dan memiliki spesialisasi dalam koleksi dan berbagi pengetahuan, terutama yang berusaha hadir secara global dengan audiensi dari seluruh dunia. Mereka juga harus bergantung pada data yang tersedia secara publik untuk platform dan aplikasi ini, serta berfokus pada konten dan tampilan teks daripada suara.

Platform-platform ini dikelompokkan dalam empat kategori luas (dengan catatan bahwa kategori ini tumpang tindih):

* **Knowledge access** (platform pengetahuan dan informasi, termasuk mesin pencarian) : Google Maps, Google Search, Wikipedia, YouTube.
* **Language learning** (platform belajar mandiri): DuoLingo, dan platform edukasi Coursera, Udacity, Udemy.
* **Social media** (platform sosial media untuk umum): Facebook, Instagram, Snapchat, TikTok, Twitter.
* **Messaging** (aplikasi pesan privat dan berkelompok): imo, KakaoTalk, LINE, LINE Lite, Messenger, QQ, Signal, Skype, Telegram, Viber, WeChat, WhatsApp, Zoom.

{{< summary-side-fig-fancybox
    src_small="/media/data-survey/PS_Figure_1-platform_summary-500px.webp"
    src_full="/media/data-survey/PS_Figure_1-platform_summary-1000px.webp"
    alt="Jumlah interface bahasa dari tiap platform menurut kategori."
    caption="Jumlah interface bahasa dari tiap platform menurut kategori."
    PDFlink="/media/pdf/PS_Figure_1-platform_summary.pdf"
>}}

Kami menemukan bahwa distribusi bahasa berbasis teks sangat tidak merata di berbagai platform digital. Platform web utama seperti Wikipedia, Google Search, dan Facebook saat ini menawarkan paling banyak pendukung bahasa. Menariknya, Wikipedia (yang merupakan nirlaba, dan diedit oleh relawan di seluruh dunia) adalah platform yang secara keseluruhan paling banyak diterjemahkan sejauh ini. Antarmuka pengguna dasar dari Wikipedia didukung oleh lebih dari 400 bahasa, dengan 300 bahasa mempunyai setidaknya 100 artikel. Pencarian Google didukung oleh 150 bahasa, sementara Facebook didukung oleh 70-100 bahasa. Signal memimpin aplikasi pesan dengan lebih dari 70 bahasa di Android dan 50 di iOS. Di sisi lain, kebanyakan platform hanya fokus pada sedikit bahasa yang dikuasai oleh paling banyak orang sehingga kebanyakan bahasa tidak tersedia. Misalnya aplikasi pesan QQ, hanya tersedia dalam bahasa Cina.

Beberapa bahasa yang cenderung tersedia di kebanyakan platform yang disurvei yaitu bahasa Eropa, termasuk bahasa Inggris, Spanyol, Portugis, dan Prancis, serta beberapa bahasa Asia seperti Mandarin, bahasa Indonesia, Jepang dan Korea. Bahasa utama seperti bahasa Arab dan Malaysia kurang tersedia, dan bahasa lain yang dipakai oleh puluhan sampai ratusan juta orang tidak terepresentasi dengan baik dalam antarmuka.

Apa artinya kurang tersedianya pendukung bahasa ini pada kebanyakan orang di dunia? Pada tahun 2021, kami memperkirakan terdapat [7,9 milyar orang](https://www.worldometers.info/) di dunia, kebanyakan hidup di Asia (hampir 4.7 milyar) dan Afrika (hampir 1.4 milyar). Namun sebagian besar dari penduduk dunia tidak terakomodasi oleh bahasa-bahasa yang sering digunakan di internet:

* _Orang yang menguasai bahasa-bahasa Afrika_: Mayoritas luas bahasa Afrika tidak tersedia sebagai bahasa antarmuka di platform manapun yang disurvei, akibatnya 90% orang Afrika perlu mengerti bahasa kedua untuk menggunakan platform. Artinya, kebanyakan dari mereka menggunakan bahasa kolonial Eropa atau bahasa yang lebih dominan di wilayahnya.
* _Orang yang menguasai bahasa-bahasa Asia Selatan_: Di Asia selatan, hampir setengah dari platform yang disurvei tidak menyediakan bahasa regional, dan bahkan tidak dalam bahasa mayoritas Asia Selatan seperti Hindi dan Bengali, yang dikuasai oleh ratusan juta orang dan sangat tertinggal dibanding bahasa lainnya.
* _Orang yang menguasai bahasa-bahasa Asia Tenggara_: Bahasa Indonesia, Vietnam, dan Thailand cenderung sangat tersedia dalam platform yang kami survei, sedangkan kebanyakan bahasa lain di Asia Tenggara tidak tersedia pada kebanyakan platform yang kami survei.

Temuan Martin dan Mark diperkuat dengan realitas keseharian mereka yang berada di bagian dunia tersebut. [Donald]({{<trurl>}}/stories/challenges-of-expressing-chindali{{</trurl>}}) dari Malawi menemukan misalnya ketika ia menanyakan pada penutur bahasa Chindali, bahasa Bantu yang terancam, tentang bagaimana mereka berkomunikasi via telepon, mereka semua menjelaskan bagaimana sulitnya dan memakan waktu untuk berbahasa Chindali, karena kebanyakan telepon hanya menyediakan bahasa Inggris, Prancis dan Arab dan tidak mengenali bahasa Chindali. Tantangan teknologi ini tentu saja menambah hambatan ekonomi dan sosial yang membatasi kemampuan penutur bahasa Chindali untuk membeli smartphone atau paket data. Bahkan mereka yang menggunakan bahasa nasional Malawi, Chichewa, mengalami kesulitan karena kurangnya ketersediaan bahasa: “Mengapa saya harus membeli telepon mahal dan membuang waktu untuk mengakses internet ketika hanya bisa dipakai dengan bahasa Inggris yang saya tidak mengerti?”

Sebenarnya, kekurangan bahasa pada kebanyakan bahasa Afrika tercatat pada tahun 2018, ketika [Twitter pertama kali mengakui bahasa Swahili](https://www.theafricancourier.de/culture/swahili-makes-history-as-first-african-language-recognized-by-twitter/), bahasa yang dipakai oleh sekitar 50-150 juta orang di seluruh Afrika Timur dan lebih luas lagi (sebagai bahasa pertama atau kedua). Sebelumnya, bahasa Swahili dan kebanyakan bahasa Afrika lainnya dirujuk sebagai bahasa Indonesia pada platform tersebut. Pengakuan kata-kata dalam bahasa Swahili dan pendukung penerjemahannya tidak dimulai oleh perusahaan teknologi tetapi merupakan hasil dari kampanye penutur Swahili di Twitter.

Situasi di Amerika Latin tidak lebih baik untuk bahasa masyarakat adat. Dalam wawancara dengan [proyek Kimeltuwe]({{<trurl>}}/stories/use-of-our-ancestral-language{{</trurl>}}) yang bekerja untuk bahasa Mapuzugun, yang dipakai oleh masyarakat saat ini di Chili dan Argentina, mereka menyatakan, “akan sangat baik jika bisa memposting dalam bahasa Mapuzugun di platform seperti YouTube atau Facebook. Bahkan istilah-istilah dalam interface pun tidak diterjemahkan, menu tidak menyediakan fasilitas yang menyatakan bahwa ini adalah bahasa Mapuzugun. Misalnya, ketika mengunggah video di YouTube atau Facebook, kita tidak bisa menambahkan transcript dalam bahasa Mapuzugun karena tidak muncul dalam daftar bahasa yang sudah ditentukan. Jadi jika kita mengunggah transcript dalam bahasa Mapuzugun, anda harus mengatakannya sebagai Spanyol atau Inggris.”

Martin dan Mark tidak menganalisa ketersediaan bahasa dalam peralatan khusus seperti telepon seluler, tapi kami tahu bahwa keyboard digital merupakan salah satu dari ruang penting dimana para ahli bahasa dan teknologi membuat kemajuan. Gboard, keyboard telepon Google untuk sistem operasi Android, misalnya menyediakan [lebih dari 900 bahasa](https://arxiv.org/pdf/1912.01218.pdf) berdasarkan hasil yang signifikan dari berbagai komunitas bahasa dan akademisi. Namun demikian kita hanya bisa mengakses keyboard ini hanya jika mampu memiliki telepon pintar yang relatif canggih.

Pada saat yang sama, [pengalaman Uda dengan Sinhala]({{<trurl>}}/stories/amidst-virtual-impunity{{</trurl>}}) — bahasa yang dipakai oleh lebih dari 20 juta orang di Sri Lanka baik sebagai bahasa pertama atau kedua — menunjukkan bahwa menciptakan konten dalam bahasa yang aksaranya tidak dengan mudah ditangkap oleh teknologi pendukung bahasa, masih sangat sulit, terutama jika bentuknya sangat berbeda dengan aksara latin bahasa Eropa Barat. Ia mengatakan, “masalah utama Unicode Sinhala berhubungan dengan aturan bahwa berbagai aksara harus dimasukkan untuk menciptakan huruf. Anda harus mengikuti konsonan dengan diakritik untuk memenuhi aturan ini. Pemikiran linguistik ini mengikuti aturan bahasa Eropa yang berdasarkan aksara latin. Namun dalam bahasa Sinhala, kadang-kadang diakritik mendahului konsonan.”

[Unicode](https://home.unicode.org/) adalah standar teknologi untuk coding teks yang diekspresikan dalam bahasa tulisan atau aksara. Versi 13 mempunyai [143,859 karakter](https://www.unicode.org/faq/basic_q.html) untuk lebih dari 30 sistem penulisan yang digunakan hari ini, karena satu sistem aksara dapat digunakan untuk lebih dari satu bahasa. (Misalnya aksara latin untuk kebanyakan bahasa Eropa, aksara Han untuk bahasa Jepang, China dan Korea, dan Devanagari untuk berbagai bahasa Asia Selatan). Unicode juga memiliki huruf untuk teks sejarah dari bahasa yang sudah tidak digunakan lagi. Konsorsium Unicode (organisasi non profit yang berpusat di California) juga memutuskan [emoji](https://home.unicode.org/emoji/about-emoji/) — simbol yang kita gunakan setiap hari melalui berbagai antarmuka.

[Survei Martin dan Mark]({{<trurl>}}/numbers/a-platform-survey{{</trurl>}}) tentang pendukung bahasa dan pengalaman [kontributor]({{<trurl>}}/stories{{</trurl>}}) lain dari seluruh dunia memberikan lebih banyak detail pada penjelasan yang terbatas ini dan tidak meratanya pendukung teknis pada kebanyakan bahasa di platform dan aplikasi saat ini. Silakan dibaca!

### Bahasa konten: aksesibilitas dan produksi

{{< summary-quote
    text="Konten feminis terutama tidak dapat diakses dalam bahasa lokal. Women’s Development Foundation adalah kelompok perempuan pedesaan yang sudah bekerja untuk isu hak perempuan sejak tahun 1983. Namun baru di tahun 2019, kami memulai berbagi konten feminis tentang isu sosial politik dan ekonomi secara daring."
    author="Uda Deshapriya"
    link="/id/stories/amidst-virtual-impunity/"
>}}

{{< summary-quote
    text="Sayangnya dari dulu sampai sekarang sangat sulit untuk menemukan konten queer yang positif dalam bahasa Indonesia di internet... jika kita mencari “LGBT” atau “homoseksualitas” di Google– mesin pencarian terbesar dan terpopuler – kita akan menemukan hasil yang berisikan kata “penyimpangan”, “dosa”, dan “penyakit”."
    author="Paska Darmawan"
    link="/id/stories/flickering-hope/"
>}}

{{< summary-quote
    text="Informasi yang berisi irisan tentang queer dan disabilitas (atau bahkan ketiadaannya) yang tersedia dalam bahasa Bengali di internet sebagian besar terbentuk oleh dan menyebabkan homofobia dan ableisme."
    author="Ishan Chakraborty"
    link="/id/stories/marginality-within-marginality/"
>}}

Kami ingin memahami konten di internet dengan menganalisa versi dunia seperti apa dan pengetahuan siapa yang kita alami ketika kita daring. Bagaimanapun juga [lebih dari 63% situs web](https://w3techs.com/technologies/overview/content_language) menggunakan bahasa Inggris sebagai bahasa utama.

Dalam esai dan interview mereka, kontributor kami mendiskusikan berbagai konstelasi sejarah, sosial, politik, ekonomi dan tantangan teknologi dalam melakukan akses yang bermakna di internet dalam bahasa mereka. Lebih signifikan lagi, mereka menanggapi tantangan untuk menemukan konten yang relevan di internet dalam bahasa mereka dan menciptakan konten yang bermakna untuk mereka dalam bahasa tersebut. Dengan kata lain, tidak cukup bagi kita untuk dapat mengakses informasi dan pengetahuan yang diciptakan untuk kita dalam bahasa lain oleh orang yang mungkin tidak memahami konteks dan pengalaman kita dan, lebih buruk lagi, mereka yang memusuhi kita. Kita perlu memiliki kemampuan untuk memproduksi pengetahuan yang bermakna untuk kita sendiri dan komunitas kita, atau setidaknya untuk dapat menunjang produksi dan ekspansi konten pada bahasa yang berbeda.

Hal ini terutama benar untuk mereka yang memiliki isu aksesibilitas dan untuk mereka yang mengalami bentuk pengucilan karena marjinalisasi dan pengasingan.

[Joel]({{<trurl>}}/stories/signs-across-generations{{</trurl>}}) menjelaskan pada kita hasil wawancara tentang proyek Indigemoji. Proyek ini dimulai suatu hari ketika ia sedang menulis tweet dan frustasi di mobilnya, lalu ia menepi di pinggir jalan dan mulai memasukkan kata-kata Arrernte dengan emoji untuk menjelaskan maknanya. Puluhan tahun setelah emoji pertama kali diperkenalkan di internet, satu bangsa atau masyarakat adat menyampaikan petisi dan tidak berhasil untuk menggunakannya sebagai cara mengekspresikan bahasa lisan dan visual mereka seperti Arrernte. Seperti kami katakan sebelumnya, Unicode Consortium mempertimbangkan permintaan publik untuk emoji baru dan petisi seperti emoji untuk bendera aborigin Australia [ditolak](https://unicode.org/emoji/emoji-requests.html). Bagi Joel, Caddie dan lainnya, proyek Indigemoji merupakan usaha multigenerasi untuk menolak berbagai bentuk marginalisasi fisik dan virtual, dan menciptakan konten mereka sendiri dengan cara yang lebih bermakna untuk identitas dan bahasa masyarakat adat mereka.

{{< summary-side-fig
    src="/media/summary/Indigemoji-tweet.webp"
    alt="Tweet yang menampilkan daftar Emoji dengan bahasa Arrernte di sebelahnya. Sumber:"
    caption="Tweet yang menampilkan daftar Emoji dengan bahasa Arrernte di sebelahnya. Sumber:"
    source="Indigemoji."
    link="www.indigemoji.com.au"
>}}

Penting untuk diingat bahwa bahasa masyarakat adat menjadi bahasa “minoritas” di dunia saat ini karena adanya genosida massal sepanjang sejarah kolonialisme dimana bangsa asli dihancurkan atau menjadi populasi minoritas setelah menjadi penduduk utama pada wilayah atau tanah tertentu. Dan proses kolonisasi ini juga berpengaruh pada bahasa dominan yang dipakai jutaan orang di seluruh dunia.

[Ishan]({{<trurl>}}stories/marginality-within-marginality{{</trurl>}}) adalah seorang akademisi queer disabilitas. Baginya, dapat berkomunikasi daring pun sudah melewati segala rintangan. Kemudian ia berjuang untuk menemukan informasi yang relevan dalam bahasa Bengali tentang disabilitas dan queer, dan terlebih lagi tentang irisan di kedua isu tersebut. Hal ini membawa kita pada yang kita sebut sebagai ‘marjinalisasi di dalam marjinalitas’: “di satu sisi, sikap homofobia dan ableisme dalam masyarakat dan di sisi lain internalisasi homofobia dan/atau ableisme pada individu (queer dan/atau disabilitas) — bersama, kondisi ini saling melengkapi untuk meneruskan mekanisme marjinalisasi. Posisi dalam masyarakat dari seorang individu queer disabilitas mungkin bisa dijelaskan sebagai marjinalisasi di dalam marjinalisasi.”

Dengan kata lain, proses-proses kritis dari akses dan informasi, bahkan pada bahasa dominan seperti bahasa Bengali — yang dipakai oleh sekitar 300 juta orang di seluruh dunia — tidak ditemukan di internet.

Martin dan Mark memutuskan untuk mendalami analisa mengenai jangkauan dan jenis konten dalam bahasa yang berbeda, di dua platform informasi dan pengetahuan yang berbeda: Google Maps and Wikipedia.

#### Google Maps

Dapatkah kita mengakses Google Maps dengan banyak bahasa yang berbeda? Apakah bahasa yang kita gunakan mengubah versi dunia yang kita lihat melalui Google Maps?.

Untuk menjawab pertanyaan di atas, Martin and Mark mengumpulkan data tentang konten global [Google Maps]({{<trurl>}}/numbers/google-maps-language-geography{{</trurl>}}) yang mencakup 10 bahasa yang paling dipakai: Inggris, Cina Mandarin, Hindi, Spanyol, Prancis, Arab, Bengali, Rusia, Portugis dan bahasa Indonesia. Mereka mengumpulkan puluhan juta pencarian individu dalam bahasa-bahasa ini dan mengidentifikasi seta memetakan sekitar tiga juta tempat unik (situs dan lokasi lainnya).

Tidak mengejutkan bahwa peta dengan paling banyak konten adalah ketika Google Maps diakses dalam bahasa Inggris. Google Map dalam bahasa Inggris meliputi seluruh dunia, meskipun lebih padat di dunia utara dengan fokus di Eropa dan Amerika Utara (dengan lebih banyak informasi). Peta juga meliputi Asia Selatan, sebagian Asia Tenggara dan sebagian besar Amerika Latin, meskipun dalam perbandingan, konten masih jarang di banyak bagian di Afrika.

{{< summary-side-fig-fancybox
    src_small="/media/data-survey/GM_Figure_1-English-500px.webp"
    src_full="/media/data-survey/GM_Figure_1-English-1000px.webp"
    alt="Kepadatan informasi pada Google Maps untuk penutur bahasa Inggris. Tanda gelap menunjukkan dimana hasil penelitian memasukkan lebih banyak jumlah tempat."
    caption="Kepadatan informasi pada Google Maps untuk penutur bahasa Inggris. Tanda gelap menunjukkan dimana hasil penelitian memasukkan lebih banyak jumlah tempat."
    PDFlink="/media/pdf/GM_Figure_1-English.pdf"
>}}

Dibandingkan dengan peta yang relatif komprehensif dalam bahasa Inggris, kami menemukan bahwa peta dalam bahasa Bengali (bahasa utama [Ishan]({{<trurl>}}/stories/the-unseen-story{{</trurl>}})) sangat berlawanan — cakupannya terbatas di Asia Selatan terutama India and Bangladesh, dan Google Maps hanya memiliki sedikit atau tidak ada konten dalam bahasa Bengali di wilayah lain di dunia. Untuk menemukan konten tambahan dan menemukan tempat di luar India dan Bangladesh, penutur Bengal perlu mengakses ke bahasa keduanya, misalnya bahasa Inggris. Hal ini juga benar untuk Google Maps dalam bahasa Hindi (bahasa dengan penutur terbesar ketiga di dunia setelah bahasa Inggris dan Cina Mandarin).

{{< summary-side-fig-fancybox
    src_small="/media/data-survey/GM_Figure_2-Bengali-500px.webp"
    src_full="/media/data-survey/GM_Figure_2-Bengali-1000px.webp"
    alt="TKepadatan informasi pada Google map untuk pembicara Bengali, Warna gelap menunjukkan hasil pencarian dimana lebih banyak tempat ditemukan."
    caption="Kepadatan informasi pada Google map untuk pembicara Bengali, Warna gelap menunjukkan hasil pencarian dimana lebih banyak tempat ditemukan."
    PDFlink="/media/pdf/GM_Figure_2-Bengali.pdf"
>}}

Lebih jauh tentang Google Maps dalam bahasa yang berbeda dapat dilihat di [detail esai Martin dan Mark]({{<trurl>}}/numbers/google-maps-language-geography{{</trurl>}}).

#### Wikipedia

Seperti ditunjukkan dalam [penelitian platform survey]({{<trurl>}}/numbers/a-platform-survey{{</trurl>}}) oleh Martin dan Mark, Wikipedia ada di garis depan pendukung bahasa di internet, dengan tampilan yang diterjemahkan ke paling banyak bahasa dibanding platform komersial manapun yang kami lihat, termasuk di Google dan Facebook.

Dalam kerangka konten aktual — informasi dan pengetahuan dalam artikel Wikipedia — Wikipedia memiliki lebih dari 300 edisi bahasa, meskipun penutur bahasa-bahasa ini tidak mendapatkan akses pada konten yang sama atau jumah informasi yang sama. Kami ingin bertanya dan menjawab lebih dalam: seberapa baik konten Wikipedia tercakup dalam edisi tiap bahasa? Apakah beberapa bahasa lebih terepresentasi dari bahasa lainnya? Apakah komunitas bahasa tertentu memiliki akses konten yang lebih besar dari lainnya? Kami menjawab beberapa pertanyaan ini dengan mendetail di [Analisa Wikipedia Martin dan Mark]({{<trurl>}}/numbers/wikipedia-language-geography{{</trurl>}}).

Kami menggunakan data 2018 dengan geotags (cara untuk melekatkan referensi geografis seperti koordinat dalam artikel wikipedia), dan menganalisa jumlah artikel dan perkembangan konten di berbagai bahasa. Kami juga mendasarkan analisis pada bahasa “lokal”. Selain itu, kami mendefinisikan bahasa yang tidak diklasifikasikan dalam bahasa resmi dalam [Unicode CLDR](https://cldr.unicode.org/) (kode yang mendukung bahasa dalam internet) atau yang digunakan oleh setidaknya 30% populasi dari suatu negara.

Kami mengidentifikasi bahasa lokal yang paling umum, misalnya yang digunakan oleh paling banyak orang di setiap negara. Kami menemukan 73 bahasa yang paling umum digunakan setidaknya di satu negara. Bahasa Inggris merupakan bahasa yang paling luas dipakai dan menjadi bahasa paling umum di 34 negara. Diikuti oleh bahasa Arab dan Spanyol (18 negara), Prancis (13 negara), Portugis (tujuh negara), Jerman (empat negara), dan Belanda (tiga negara). Cina, Italia, Melayu, Rumania, Yunani, dan Rusia adalah bahasa yang paling umum di dua negara, dengan 60 bahasa menjadi paling umum di satu negara.

Untuk membandingkan distribusi bahasa lokal dengan konten Wikipedia di setiap negara, kami mengidentifikasi edisi bahasa Wikipedia dengan jumlah artikel paling besar tentang negara tersebut. Kami menemukan bias pada konten berbahasa Inggris. Bahasa Inggris merupakan bahasa dominan Wikipedia di 98 negara, diikuti Prancis (sembilan negara), Jerman (delapan negara), Spanyol (tujuh negara), Catalan dan Russia (empat negara), Italia dan Serbia (tiga negara), dan Belanda, Yunani, Arab, Serbo-Kroasia, Swedia, dan Rumania (dua negara). 21 bahasa Wikipedia lainnya merupakan bahasa umum di satu negara.

Meskipun [jumlah artikel Wikipedia dalam tiap bahasa](https://en.wikipedia.org/wiki/List_of_Wikipedias) dinamis dan terus tumbuh, jelas bahwa edisi bahasa Wikipedia sangat bervariasi baik dalam ukuran maupun skala — baik dalam jumlah artikel maupun dalam ukuran komunitas editor. Wikipedia bahasa Inggris sejauh ini adalah yang terbesar dengan lebih dari enam juta artikel dan hampir 40 juta kontributor terdaftar. Komunitas terbesar berikutnya adalah Wikipedia edisi Spanyol, Jerman dan Prancis, masing-masing dengan antara empat sampai enam juta kontributor dan lebih dari dua juta artikel. Edisi lain berukuran kecil jika dibandingkan: hanya sekitar 20 edisi bahasa yang mempunyai lebih dari satu juta artikel dan hanya 70 yang mempunyai lebih dari 100,000 artikel. Kebanyakan edisi Wikipedia hanya memiliki bagian kecil dibandingkan konten Wikipedia bahasa Inggris.

{{< summary-side-fig-fancybox
    src_small="/media/data-survey/WP_Figure_2-English-500px.webp"
    src_full="/media/data-survey/WP_Figure_2-English-1000px.webp"
    alt="Kepadatan informasi dalam Wikipedia berbahasa Inggris pada awal 2018. Warna gelap menunjukkan artikel dengan lebih banyak jumlah geotag."
    caption="Kepadatan informasi dalam Wikipedia berbahasa Inggris pada awal 2018. Warna gelap menunjukkan artikel dengan lebih banyak jumlah geotag."
    PDFlink="/media/pdf/WP_Figure_2-English.pdf"
>}}

{{< summary-side-fig-fancybox
    src_small="/media/data-survey/WP_Figure_3-ar,_bn,_hi,_es-500px.webp"
    src_full="/media/data-survey/WP_Figure_3-ar,_bn,_hi,_es-1500px.webp"
    alt="Kepadatan informasi dalam Wikipedia berbahasa Arab, Bengali, dan Spanyol pada awal 2018. Warna gelap menunjukkan jumlah lebih besar pada artikel dengan tag geo."
    caption="Kepadatan informasi dalam Wikipedia berbahasa Arab, Bengali, dan Spanyol pada awal 2018. Warna gelap menunjukkan jumlah lebih besar pada artikel dengan tag geo."
    PDFlink="/media/pdf/WP_Figure_3-ar,_bn,_hi,_es.pdf"
>}}

Yang menakjubkan adalah bagaimana konten di berbagai bahasa di Wikipedia menggemakan distribusi yang serupa di Google Maps, yang kita lihat sebelumnya.

Hal ini juga benar ketika kita melihat jumlah artikel di Wikipedia dengan bahasa yang berbeda dibandingkan dengan jumlah penutur dari bahasa tersebut (baik sebagai bahasa pertama atau kedua). Kami menemukan bahwa dalam bahasa Eropa seperti Inggris, Prancis, Spanyol, Rusia dan Portugis, jumlah artikel dalam Wikipedia proporsional dengan jumlah penutur. Namun hal ini tidak berlaku bagi bahasa yang banyak dipakai lainnya seperti Cina Mandarin, Hindi, Arab, Bengali dan bahasa Indonesia yang dipakai oleh ratusan juta orang namun memiliki edisi Wikipedia dengan jumlah artikel yang jauh lebih kecil dibanding edisi bahasa Eropa. Ada lebih banyak artikel dalam bahasa Prancis, Spanyol atau Portugis dibanding dalam bahasa Mandarin, Hindi atau Arab, meskipun bahasa ini termasuk [lima bahasa teratas](https://en.wikipedia.org/wiki/List_of_languages_by_total_number_of_speakers) di dunia dengan penutur lebih banyak dari Prancis dan Portugis.

{{< summary-side-fig-fancybox
    src_small="/media/data-survey/WP_Figure_1-language_ranking-500px.webp"
    src_full="/media/data-survey/WP_Figure_1-language_ranking-1000px.webp"
    alt="Konten Wikipedia dan jumlah penutur 10 bahasa yang paling banyak dipakai di seluruh dunia.(Perkiraan populasi : Ethnologue 2019, termasuk penutur bahasa kedua)"
    caption="Konten Wikipedia dan jumlah penutur 10 bahasa yang paling banyak dipakai di seluruh dunia.(Perkiraan populasi : Ethnologue 2019, termasuk penutur bahasa kedua)"
    PDFlink="/media/pdf/WP_Figure_1-language_ranking.pdf"
>}}

[Esai Martin dan Mark]({{<trurl>}}/numbers/wikipedia-language-geography{{</trurl>}}) memiliki lebih banyak analisis dan visualisasi data tentang Wikipedia di berbagai bahasa, namun temuan dari angka ini juga digaungkan oleh apa yang ditemukan dalam pengalaman dari kontributor di seluruh dunia.

Marjinalisasi dan pengasingan bahasa di luar bahasa kolonial Eropa terjadi di dunia fisik dan virtual, termasuk bahasa global dan dominan seperti bahasa Arab. Untuk menulis Wikipedia dalam bahasanya sendiri, menggunakan referensi yang berbasis pada konteks bahasa tersebut, kita membutuhkan sumber yang sudah diterbitkan secara luas dan dapat diandalkan,dimana hal ini (seperti yang kami temukan sebelumnya) jarang ditemukan dalam kebanyakan bahasa di dunia. Sebagai Wikipedian, [Emna]({{<trurl>}}/stories/write-in-another-language{{</trurl>}}) membicarakan tentang sulitnya menemukan sumber dan referensi dalam bahasa-bahasa Afrika: “... bagi saya sebagai Wikipedian, sulit menemukan referensi dalam bahasa saya sendiri. Ketika saya mengatakan bahasa kami sendiri, bukan berarti bahasa Tunisia, tapi dialek kami atau bahasa Arab. Ketika kami berjalan melintasi Afrika, kami juga menemukan kesenjangan besar dalam sumber dan referensi.”

Bahkan di Eropa, penutur bahasa minoritas kesulitan menggunakan atau mengedit Wikipedia dalam bahasa mereka. Ketika [Claudia]({{<trurl>}}/stories/decolonizing-minority-language{{</trurl>}}) menemukan bahwa banyak responden di Breton mengetahui adanya Wikipedia Breton, dengan “19% dari mereka bahkan berkontribusi dengan mengedit atau menulis artikel baru (8%),” ia merasa bawa kebanyakan penutur bahasa minoritas akan berpindah pada bahasa dominan karena terasa lebih mudah: “Ketersediaan tidak berarti bahwa jasa, antarmuka, aplikasi, dan Wikipedia digunakan. Beberapa kajian mengungkapkan bahwa penutur bahasa minoritas berpindah dengan mudah ke bahasa dominan ketika menggunakan teknologi digital karena teknologi yang dimiliki pada dasarnya lebih bagus atau karena jangkauan jasa yang tersedia lebih luas.”

Wikipedia dan konstelasi proyek pengetahuan di dalamnya yang gratis dan [open-source](https://en.wikipedia.org/wiki/Open_source) (dimana kodenya tersedia secara terbuka dan dibangun secara kolektif) adalah salah satu ruang yang memberikan harapan dan paling membantu dalam pengetahuan multilingual daring. Misalnya, komunitas relawan mengetahui bahwa tidak ada bentuk tunggal dalam bahasa Inggris, Arab atau Cina, namun mengungkapkan pluralitas konteks dan konten bahasa tidak selalu mudah. Sebagaimana kita ketahui dalam analisis dan pengalaman kami, [Wikipedia juga memiliki masalah struktur kekuasaan dan privilese](https://wikipedia20.pubpub.org/pub/myb725ma/release/2) yang mengubah cara dan bentuk bagi kita untuk menciptakan dan berbagi pengetahuan dalam bahasa yang berbeda, dan dalam bahasa serumpun.

Apa yang dapat kita lakukan untuk melangkah maju sebagai individu, organisasi dan komunitas menuju internet yang lebih multilingual? Dalam bagian berikutnya dan bagian akhir, kita akan mengambil pandangan dari semua [statistik]({{<trurl>}}/numbers{{</trurl>}}) dan [cerita]({{<trurl>}}/stories{{</trurl>}}) yang telah kami bahas sejauh ini, dan menawarkan tinjauan tentang apa yang sudah kami pelajari beserta konteks, pemahaman dan tindakan yang bisa membawa kita pada internet yang sungguh-sungguh multilingual.

## Apa yang sudah kita pelajari dari internet multilingual?

Kami mempelajari banyak hal tentang bahasa, internet dan bahasa dalam internet ketika kami mengerjakan laporan ini. Berikut, adalah ulasan singkat tentang wawasan terpenting dari perjalanan kami sejauh ini.

**Belajar:** Bahasa adalah proxy dari pengetahuan dan cara penting untuk menjadi manusia di dunia ini, bukan sekedar alat komunikasi. Itulah mengapa multilingualisme sangat penting, sehingga kita dapat menghargai dan memberikan afirmasi terhadap kekayaan dan tekstur dari banyak kepribadian dan perbedaan dunia kita dengan lebih baik.

**Konteks:** Orang-orang mengetahui dunianya dan mengungkapkan dirinya dalam lebih dari 7000 bahasa. Bahasa kita dapat berupa lisan (tutur dan tanda), tertulis, atau disampaikan melalui bunyi.

Namun demikian, ketersediaan bahasa pada platform teknologi utama dan aplikasi hanyalah sebagian kecil dari 7000 bahasa ini, dengan hanya sekitar 500 bahasa terepresentasi secara daring dalam bentuk informasi atau pengetahuan apapun. Beberapa dari bahasa dengan penutur terbanyak di dunia tidak tersedia atau tidak ada informasinya secara daring. Bahasa yang paling tersedia, dengan informasi paling luas di internet (termasuk Google Maps dan Wikipedia), serta kebanyakan website adalah dalam bahasa Inggris.

**Refleksi: Internet sangat tidak multilingual sebagaimana yang kita bayangkan atau kita perlukan.**

**Analisis:** Kebanyakan orang menggunakan bahasa kolonial Eropa terdekat (Inggris, Spanyol, Portugis, Prancis …) atau bahasa dominan regional (Cina, Arab...) untuk mengakses internet. Struktur kekuasaan yang terus berlangsung dan sejarah serta hak istimewa tersirat dalam bagaimana bahasa dapat diakses atau tidak secara online.


## Bagaimana kita bisa melakukannya lebih baik?: Konteks dan Aksi untuk Internet Multilingual

{{< summary-quote
    text="Dalam mayoritas kasus, menggunakan bahasa minoritas memerlukan kegigihan yang besar, kemauan, dan ketahanan, karena pengguna mengalami kekurangan dan kesulitan dalam menggunakan bahasa minoritas."
    author="Claudia Soria"
    link="/id/stories/decolonizing-minority-language/"
>}}

{{< summary-quote
    text="Tujuan dari internet multilingual adalah inklusi dan representasi masyarakat adat dalam mempertimbangkan dan memperhitungkan warisan sosial dan pengalaman penindasan kolonial yang masih berlangsung. Internet multilingual tidak hanya perlu menjadi cita-cita untuk direpresentasikan, tetapi dengan mempertimbangkan sejarah kolonial, kita harus menciptakan lingkungan yang menguatkan pelestarian dan pembelajaran bahasa masyarakat adat oleh masyarakat adat."
    author="Jeffrey Ansloos and Ashley Caranto Morford"
    link="/id/stories/learning-and-reclaiming-indigenous-languages/"
>}}

{{< summary-quote
    text="Generasi muda dan anak-anak Mapuche tumbuh seiring teknologi dan internet. Internet menjadi ruang dimana orang-orang bisa mendekati Mapuzugun… cerita kami harus dituliskan dan cerita ini perlu dituliskan dan dibicarakan dalam bahasa Mapuzugun… cerita kami tidak harus selalu tentang pahlawan besar, seperti yang dirayakan oleh negara kolonial dan poskolonial. Sejarah kami adalah cerita dari setiap Mapuche yang bertahan dari permusuhan dan kekerasan. Perempuan yang harus bermigrasi ke kota untuk mendapat penghasilan, perempuan dan laki-laki yang kembali dari kota namun tidak punya ruang dan [lof](https://en.wikipedia.org/wiki/Lof) lagi, dan mereka harus kembali lagi ke kota dengan akar mereka yang sudah tercabut tanpa tempat untuk kembali. Pengalaman-pengalaman ini dan memori dari tiap orang Mapuche menyusun memori kolektif sebagai masyarakat."
    author="Kimeltuwe project"
    link="/id/stories/use-of-our-ancestral-language/"
>}}

Dalam bagian ringkasan laporan kondisi bahasa di Internet ini, kami membawa berbagai pandangan dari kontributor kami dalam laporan ini seperti yang kami lakukan di tahun 2019 melalui laporan [Decolonizing the Internet’s Languages](https://whoseknowledge.org/resource/dtil-report/), untuk memahami berbagai konteks, tantangan dan kesempatan seputar multilingualitas di dunia dan secara daring. Kami bertanya pada diri sendiri dan saling mempertanyakan empat pertanyaan kunci yang mendorong kami melangkah maju untuk membayangkan dan merancang internet yang jauh lebih multilingual.

* **Siapa yang memiliki kekuasaan dan sumberdaya?** 
* **Siapa yang memiliki nilai dan pengetahuan?** 
* **Siapa yang memiliki teknologi dan standar?** 
* **Siapa yang merancang dan memiliki imajinasi?** 

### Siapa yang memiliki kekuasaan dan sumberdaya?

#### Konteks

Baik analisa statistik maupun pengalaman orang-orang memperjelas bahwa marginalisasi bahasa secara fisik dan dalam dunia virtual tidak berdasarkan hanya dari jumlah penutur sebuah bahasa.

Bahasa masyarakat adat dituturkan oleh [lebih dari 6000 suku bangsa](https://www.cwis.org/2015/05/how-many-indigenous-people/) secara global, yang sebelumnya menjadi penduduk utama dunia sampai masa kolonisasi dan genosida yang menghancurkan dan mengurangi jumlah orang dan bahasa. Bahasa dominan di benua dengan bahasa yang paling beragam seperti Asia dan Afrika, diucapkan dan diisyaratkan oleh jutaan penutur, tidak terwakili dengan baik atau kadang tidak sama sekali secara daring. Jika kita mempertimbangkan diaspora orang yang menggunakan bahasa dengan banyak variasi seperti Arab, Cina, Hindi, Bengali, Punjabi, Tamil, Urdu, bahasa Indonesia, bahasa Melayu, Swahili, Hausa dan sebagainya melintasi negara dan benua, jelas bahwa bahasa mereka termarjinalisasi secara daring, meskipun mereka mendominasi di wilayah mereka.

Bentuk marginalisasi dan pengasingan digital ini bukan sebuah kebetulan, hal ini disebabkan oleh sejarah dan struktur serta proses kekuasaan dan keistimewaan yang terus berlangsung. Hal ini juga berarti bahwa sumberdaya dalam infrastruktur bahasa — dari bahasa dan akademia, pemerintah dan perusahaan teknologi — cenderung dikuasai oleh beberapa wilayah (Eropa dan Amerika Utara) dan beberapa bahasa (bahasa Inggris dan bahasa Eropa Barat). Bahkan di Eropa dan Amerika Utara sendiri, masyarakat adat, masyarakat kulit hitam dan komunitas marjinal lainnya sangat sulit melestarikan bahasa mereka dari generasi ke generasi.

Secara khusus, kekuasaan kolonialisme dan kapitalisme menyebabkan dan bertautan dengan sistem diskriminasi dan penindasan seperti rasisme, patriarki, homofobia, ableisme, kelas dan kasta. Hal ini berarti bahwa beberapa bahasa — terutama bahasa kolonial Eropa — menjadi bahasa yang terpenting secara daring, terlepas dari jumlah penutur dan pemakaiannya di dunia. Hal ini juga berarti bahwa jika ada informasi dan pengetahuan di bahasa marjinal lainnya, konten bahasa tersebut dibatasi oleh siapa yang memiliki akses atau kekuasaan untuk menciptakannya atau mencegah yang lain memproduksi informasi alternatif. Misalnya kekurangan konten feminis dalam bahasa Sinhala, atau kurangnya konten positif untuk queer dan disabilitas di bahasa Bengali atau bahasa Indonesia.

Karena bahasa adalah esensi dari siapa kita, ketidakmampuan untuk mengungkapkan diri kita dalam bahasa kita sendiri secara penuh adalah suatu bentuk kekerasan. Maka dari itu, konsekuensi dari marginalisasi merupakan sebuah bentuk lain dari kekerasan. Sebagaimana [Uda]({{<trurl>}}/stories/amidst-virtual-impunity{{</trurl>}}) menyatakan, “Kekurangan konten digital yang feminis, ramah hak asasi manusia dan menghargai, membuat ruang daring dimana komunikasi berlangsung dalam bahasa lokal menjadi rawan bagi perempuan, queer dan minoritas. Media digital jelas kekurangan dalam menanggulangi narasi utama yang masih terkontaminasi dengan stereotip negatif. Ujaran kebencian dan kekerasan seksual dan gender meruncing secara daring.” Bentuk kekerasan ini meluas pada masyarakat adat, [kasta tertindas](https://www.equalitylabs.org/facebookindiareport) dan komunitas agama minoritas, masyarakat disabilitas dan komunitas termarjinal lainnya.

Pada saat yang sama, komunitas ini menggunakan internet untuk melawan berbagai bentuk kekerasan transgenerasi; kekerasan terhadap diri dan bahasa mereka. [Jeffrey dan Ashley]({{<trurl>}}/stories/learning-and-reclaiming-indigenous-languages{{</trurl>}}) menganalisa 3800 tweets dari lebih dari 35 tagar bahasa masyarakat adat dan 57 kata kunci pada 60 bahasa masyarakat yang diakui di Kanada. Mereka menemukan bahwa melalui tagar di Twitter, masyarakat adat di seluruh Kanada dan di daerah lain di dunia terhubung, berpartisipasi dan berkolaborasi dalam bahasa masyarakat adat yang hidup dan berkembang.

Mereka menyatakan, “Dalam konteks sosial dimana masyarakat adat di seluruh Kanada ditargetkan untuk dihapus dibawah [kebijakan asimilasi kolonial](https://indigenousfoundations.arts.ubc.ca/the_residential_school_system/), jaringan tagar Twitter membawa konteks yang unik dan bermakna pada masyarakat adat untuk berbagi pengetahuan tentang bahasa mereka. Melintasi berbagai jaringan yang masuk dalam kajian kami, terdapat beberapa contoh pengakuan dan penghubungan kembali bahasa bagi penyintas antar generasi dari kebijakan asimilasi kolonial.”

{{< summary-side-fig
    src="/media/stories-content/Learning_and_Reclaiming_Indigenous_Languages_image1.webp"
    alt="In Dene, the word for Caribou also means Stars. I love that (emoji with heart-shaped eyes) #denecosmology #advancedlanguagesclass)"
    caption="Dene Melissa Daniels dalam menyediakan konsen untuk mengutio tweet ini berharap untuk mengakui guru bahasa yang telah mengajarnya, Dene elder dan pendidik Eileen Beaver."
>}}

#### Aksi

* Mengakui struktur dan proses kekuasaan dan keistimewaan dalam berbagai institusi dan proses dukungan infrastruktur bahasa.
* Memastikan sumber daya untuk memperbaiki bahasa yang termarjinalisasi dan komunitasnya, termasuk pembelajaran bahasa dan pemrogramannya.
* Memberdayakan sumber daya untuk menciptakan dan memperluas informasi dan pengetahuan dari dan untuk komunitas yang mengalami bentuk penindasan dan kekerasan berganda dalam bahasa dan karena pilihan mereka.

### Siapa yang memiliki nilai dan pengetahuan?

#### Konteks

Sejarah dan teknologi internet berdasar pada epistemologi barat atau cara mengetahui dan melakukan sesuatu dari barat. Lebih spesifik lagi, internet dulu sampai sekarang dirancang dan dikelola oleh laki-laki kulit putih yang memiliki hak istimewa (dan [kini oleh sedikit kulit coklat](https://www.theguardian.com/technology/2014/apr/11/powerful-indians-silicon-valley)). Hal ini berarti bahwa nilai yang seringkali mendasari arsitektur dan infrastruktur internet adalah nilai determinisme teknologi — dimana teknologi dilihat sebagai sebab (dan keuntungan) utama dari setiap perubahan sosial — dan individualisme, dimana fokus dan pendorong utama adalah individu, bukan kolektif.

Ditambah lagi, pandangan ini berakar dari masa pencerahan, gerakan abad ke-18 di dunia utara yang menuju pada ilmu dan teknologi berbasis rasionalitas. Kita lupa bahwa matematika dan ilmu pasti berkembang di selatan sebelum abad ke-18. Penulisan pertama dan sistem angka misalnya berasal dari Mesopotamia, yang kini adalah Iran dan Iraq. Lebih kritis lagi, kita melupakan bahwa sumber untuk pencerahan, masa emas ilmu dan teknologi di dunia utara berasal dari masa kejayaan selatan dimana gerakan kolonisasi masal, perbudakan, genosida, dan penggalian sumber daya dari Asia, Afrika, Amerika Latin dan Karibia serta Kepulauan Pasifik. Dasar dari sifat penggalian kapitalisme modern berakar pada sejarah kolonisasi yang berlanjut menjadi kapital teknologi.

Bersama dengan sumber daya material yang diabaikan dan dirusak dalam proses ini, bentuk pengetahuan, cara hidup dan cara mengerjakan sesuatu — yang bukan epistemologi barat, seperti pengetahuan masyarakat adat atau pengetahuan dari mereka dari bagian dunia yang termarjinalisasi, juga hilang. Konsekuensi yang paling merusak adalah pada bahasa yang merupakan proxy signifikan dari pengetahuan, sebagaimana yang kita bahas sebelumnya yaitu penurunan nilai dari bahasa yang bukan bahasa Eropa Barat dan perusakan aktif atau pengabaian bahasa lisan dan non-tulisan. Bias pada konten tertulis dari bahasa tertentu yang diistimewakan mengarahkan kita pada pengetahuan jenis tertentu dalam penerbitan dan akademi, yang kemudian berdampak pada dokumen dan data yang menjadi dasar untuk memproses bahasa, atau beberapa sistem bahasa otomatis yang menjadi bagian dari infrastruktur internet seperti penerjemahan Google Translate.

[Ana]({{<trurl>}}/stories/dill-wlhall-on-the-web{{</trurl>}}) menjelaskan, “Meskipun fakta menunjukkan bahwa setengah dari bahasa di dunia adalah bahasa tanpa sistem tulisan dengan tradisi lisan yang panjang, akan tetapi bahasa dengan sistem alfabet yang diakui dan dipakai secara luas yang mendominasi dalam web. Web memperkuat pengasingan secara sistematis dimana hanya bahasa tertulis yang dapat dilestarikan untuk masa depan.”

Melalui hilangnya bahasa, kita kehilangan masa depan lebih dari yang kita sadari. Kita kehilangan bentuk ekspresi dalam berbagai bahasa dan seluruh pandangan dunia dan pengetahuan yang ada dalam bahasa tersebut. Saat kemanusiaan ada di ambang kejatuhan, komunitas masyarakat adat dan pengetahuannya menjaga keanekaragaman hayati dan melestarikan kehidupan yang kita ketahui sekarang. Sudah dapat diduga bahwa [kehilangan bahasa berarti secara langsung hilangnya keanekaragaman hayati](http://www.unesco.org/new/en/culture/themes/endangered-languages/biodiversity-and-linguistic-diversity/) dan perusakan ekosistem di seluruh dunia.

Internet dapat menjadi infrastruktur yang menarik untuk pelestarian dan pengembangan berbagai bentuk bahasa dan pengetahuan karena bentuk media yang kaya dan dapat menirukan dan merepresentasikan bahasa berupa tutur dan tanda dan melampaui teks. Namun perlu dikritisi bahwa janji radikal internet ini tidak sekali lagi berbasis pada kapitalis kolonial dan nilai patriarkis. Bagaimana komunitas melestarikan dan merevitalisasi bahasa dan identitas mereka, berbagi pengetahuan dengan cara mereka sendiri? Misalnya, banyak komunitas adat menjaga pengetahuan yang sakral dan tidak bisa dibagi secara terbuka.

Komunitas yang sudah memulai usaha ini adalah [Papa Reo](https://papareo.nz/), dengan teknologi pengenalan pembicaraan untuk te reo Māori (bahasa Māori) dari Aotearoa/Selandia Baru. Komunitas Māori menciptakan dan mempertahankan teknologi dan data untuk inisiatif ini dan percaya pada [kedaulatan data](https://www.wired.co.uk/article/maori-language-tech). Hal ini penting untuk memastikan bahwa pengetahuan yang dibagi melalui bahasa ini digunakan oleh dan untuk orang Māori, dan bukan untuk perusahaan yang mencari keuntungan. Menariknya, sementara mengakui nilai teknologi _open-source_, tim Papa juga memutuskan untuk tidak menambahkan data mereka ke database _open-source_, karena komunitas Māori telah ditolak komunitas _open-source_ yang bersumberdaya besar dengan privilesenya. Di sisi lain, [Mukurtu](https://mukurtu.org/) adalah contoh platform_ open-source_ yang dibangun dengan komunitas masyarakat adat untuk mengelola data bahasa sendiri.

#### Aksi

* Menciptakan, bekerjasama dan berbagi infrastruktur bahasa untuk internet yang merupakan barang publik, dirancang dengan nilai komunitas dan kolektif sebagai dasar dan mengarusutamakan konsep feminisme dan mewujudkan kedaulatan masyarakat adat.
* Melanjutkan tantangan dan kritik terhadap infrastruktur teknologi bahasa yang bersifat menindas, kapitalis, kepemilikan, menjarah kemanusiaan dan merusak lingkungan.
* Mengenali teknologi bahasa _open-source_ dan bebas dengan memperhatikan privilese mereka dan menghormati keputusan komunitas yang termarjinalisasi dan menentukan “keterbukaan” yang ingin mereka bagi kepada dunia.

### Teknologi dan standar milik siapa?

#### Konteks

Industri teknologi tidak sepenuhnya bertanggung jawab terhadap kurangnya representasi dan dukungan pada mayoritas bahasa di dunia. Namun demikian, teknologi dari Global North bertanggung jawab karena melanggengkan dan meningkatkan ketidaksetaraan berbasis bahasa dan kolonialisme digital secara daring.

Perusahaan teknologi besar — yang merancang dan menciptakan kebanyakan platform digital, alat, piranti keras dan lunak yang kita gunakan — dapat mengabaikan kebutuhan untuk menciptakan internet yang sungguh-sungguh multilingual karena mereka tidang menganggap atau melihat mayoritas dari 7000 bahasa yang dituturkan sebagai bagian penting dari infrastruktur internet. Bagaimanapun juga mereka mengetahui bahwa mereka memerlukan dukungan bahasa jika ada kaitan bisnis baik untuk bahasa kolonial Eropa atau bahasa dimana mereka menyebut sebagai “pasar yang berkembang” sebenarnya. [Dukungan terhadap bahasa](https://blog.google/inside-google/googlers/shachi-dave-natural-language-processing/) dominan Asia dan Asia Selatan mulai membaik pada platform teknologi besar karena mereka berdasar pada konsumen terbesar.

Pada saat yang sama, teknologi bahasa yang paling banyak tersebar luas di internet diciptakan dan dikendalikan oleh perusahaan-perusahaan ini karena mereka memiliki sumber daya dan kapasitas. Wikipedia adalah pengecualian penting karena merupakan _open-source_ dan didukung oleh komunitas relawan dari seluruh dunia. Pada umumnya, motivasi kepemilikan dan keuntungan tidak menghasilkan teknologi dan alat yang diperlukan untuk konten yang mendalam dan bernuansa dalam bahasa yang termarjinalisasi dan dari komunitas marjinal. Lebih parah lagi, perusahaan ini membangun bahasa teknologi dengan skala besar dan [sistem otomatis](https://dl.acm.org/doi/pdf/10.1145/3442188.3445922) yang bergantung pada data bahasa yang besar dari sumber mana saja — bahkan jika sumbernya mungkin berupa kekerasan dan ujaran kebencian atau tulisan yang memusuhi kelompok marjinal.

[Jeffrey dan Ashley]({{<trurl>}}/stories/learning-and-reclaiming-indigenous-languages{{</trurl>}}) menjelaskan, “Pada jajaran [survivance](https://en.wikipedia.org/wiki/Survivance) dan jaringan pembelajaran dari bahasa masyarakat adat, rasisme telah teridentifikasi sebagai tantangan sosial utama dalam ekologi Twitter. Hal ini khususnya terkait bagaimana jaringan secara aktif menjadi target dari teks dan konten multimedia yang mengacaukan, kadang berupa ujaran kebencian dari berbagai tipe pengguna termasuk orang-orang sebenarnya dan bot otomatis. Pada kedua pengguna ini, akun-akun ini tampaknya mengikuti pola serupa untuk penyebaran misinformasi yang seringkali menyebarkan teks yang tidak masuk akal yang merefleksikan analisa agregat dan pembentukan konten.”

Jika perusahaan tidak cukup peduli tentang konsekuensi dari kurangnya ahli dan konteks bahasa lokal, maka hal ini dapat mengarah pada kekerasan dan kerugian yang tidak terukur. Di Myanmar, dimana Facebook (atau Meta) pada dasarnya adalah internet, aktivis sudah memperingatkan perusahaan tentang ujaran kebencian bertahun-tahun sebelum perusahaan membentuk tim bahasa Birma. Tahun 2015, Facebook mempunyai [empat penutur bahasa Birma](https://www.reuters.com/investigates/special-report/myanmar-facebook-hate/) yang meninjau konten dengan 7.3 juta pengguna aktif di Myanmar. Apa akibat kurangnya perhatian pada bahasa dan konteks ini? PBB menemukan bahwa Facebook menjadi bagian dari genosida terhadap Muslim Rohingya di Myanmar, dan perusahaan ini sedang terlibat dalam kasus melawan pemerintah Myanmar di [tribunal internasional](https://thediplomat.com/2020/08/how-facebook-is-complicit-in-myanmars-attacks-on-minorities/).

Hal serupa terjadi dengan, [ujaran kebencian di India](https://www.ndtv.com/india-news/facebook-officials-played-down-hate-in-india-reveals-internal-report-2607365) terhadap Muslim, Dalit dan komunitas termarjinalisasi lainnya yang berlangsung dengan sedikit moderasi aktif, meskipun India adalah pasar terbesar Facebook dan memiliki salah satu penutur bahasa terbesar di dunia. Fakta menunjukkan bahwa perusahaan ini menghabiskan [84% dari pengiriman uang global terkait bahasa](https://www.washingtonpost.com/technology/2021/10/24/india-facebook-misinformation-hate-speech/) untuk misinformasi di Amerika Serikat, yang hanya merupakan 10% dari seluruh pengguna, sedangkan hanya 16% dialokasikan untuk bagian dunia lainnya.

Perusahaan teknologi perlu mengakui bahwa membangun teknologi bahasa membutuhkan sumber daya yang luas dan dalam untuk memahami konteks sosial politik, serta komitmen untuk pengalaman multilingual digital.

Kontributor kami membicarakan jangkauan kebutuhan, tantangan dan kesempatan dalam memutuskan pengalaman seperti ini. Terutama kurangnya infrastruktur (dari akses internet sampai alat yang efektif) dan teknologi untuk semua bahasa membuat penggunaan untuk bahasa marjinal sulit, melelahkan, lamban dan tidak praktis. Berikut kami berikan contoh-contoh yang mendukung kuat.

**Teknologi bahasa jarang dirancang untuk bahasa marjinal.**

[Donald]({{<trurl>}}/stories/challenges-of-expressing-chindali{{</trurl>}}) membicarakan tentang betapa langkanya bagi komunitasnya di Malawi untuk dapat mengakses internet dan alat yang dapat digunakan untuk berkomunikasi dengan mudah dalam bahasa mereka. Ia melakukan wawancara pada 20 penutur Chindali, 10 diantaranya pelajar dan 10 lainnya adalah lansia dalam komunitas. Dari 20 orang tersebut, hanya lima yang memiliki smartphone atau feature phone, dan tujuh diantaranya tidak memiliki alat komunikasi. Hanya empat dari 20 — dan dari semua pelajar — memiliki laptop. Sementara untuk akses internet, hanya mahasiswa yang memiliki akses internet melalui kampusnya atau dengan pulsa data pribadi.

Setelah terhubung secara daring, kebanyakan orang di dunia tidak bisa mengakses keyboardnya dalam bahasa mereka. Kebanyakan komunitas harus memakai keyboard yang dirancang terutama untuk bahasa-bahasa Eropa dan menyalin huruf bahasa mereka ke keyboard. Hal ini cukup sulit dilakukan untuk komputer pribadi dan tidak mungkin dilakukan di keyboard telepon. Kesulitan bertambah untuk bahasa yang terutama merupakan bahasa lisan dan tidak sesuai dengan sistem tulisan.

[Ana]({{<trurl>}}/stories/dill-wlhall-on-the-web{{</trurl>}}) mengatakan tentang bahasanya, “Keyboard tidak memiliki simbol Zapotec untuk merepresentasikan suara dan nada dalam bahasa kami. Usaha untuk membuat bahasa masyarakat adat tertulis sudah didiskusikan selama bertahun-tahun untuk mencapai konsensus format standar seperti huruf latin, sebuah format yang dipaksakan dan dipengaruhi oleh barat dan bagaimanapun diterima dan dibutuhkan oleh beberapa sektor penutur.” Hal ini juga sangat sulit untuk bahasa seperti Arrernte, yang mengkombinasikan suara dan sikap, seperti yang disampaikan oleh [Joel dan Caddie]({{<trurl>}}/stories/signs-across-generations{{</trurl>}}) melalui proyek Indigemoji.

Jika Anda berasal dari komunitas yang sudah merasa tidak aman dan tidak terjamin di dunia, dan akses internet tidak dirancang di dalam bahasa sendiri, maka Anda juga tidak akan merasa nyaman untuk memproduksi dan meningkatkan visibilitas konten yang penting dan relevan untuk komunitas Anda. [Paska]({{<trurl>}}/stories/flickering-hope{{</trurl>}}) berpendapat, “banyak individu LGBTQIA+ di Indonesia masih belum mengenal aspek teknis website atau belum punya cukup pengetahuan tentang bagaimana mesin pencarian bekerja.”

Komunitas marjinal di wilayah utara juga menghadapi tantangan karena tidak memiliki konten yang membantu hidup atau kadang menyelamatkan hidup dalam bahasa mereka. [Jeffrey dan Ashley]({{<trurl>}}/stories/learning-and-reclaiming-indigenous-languages{{</trurl>}}) menjelaskan: “salah satu dari tantangan utama … adalah pembatasan teknis dari teknologi penerjemahan yang ada untuk bahasa masyarakat adat dalam konteks Kanada. Bahasa masyarakat adat seperti Hul'qumi'num, Sḵwx̱wú7mesh (Squamish), Lewkungen, dan Neheyawewin (Cree) selalu disalah artikan secara teknis sebagai bahasa Jerman, Estonia, Finlandia, Vietnam, dan Prancis oleh teknologi penerjemahan Twitter.”

Secara keseluruhan, [Uda]({{<trurl>}}/stories/amidst-virtual-impunity{{</trurl>}}) membenarkan bahwa, “menciptakan konten digital dalam bahasa lokal masih merupakan tantangan karena ketiadaan alat dan kompleksitas alat yang ada. Menciptakan konten dalam bahasa lokal membutuhkan alat dan keahlian khusus. Tantangan ini berkontribusi pada konten progresif dalam bahasa lokal.”

**Teknologi bahasa kebanyakan dirancang dengan pendekatan atas ke bawah, mengutamakan keuntungan di atas keadilan dan keamanan.** [Claudia]({{<trurl>}}/stories/decolonizing-minority-language{{</trurl>}}) menjelaskan pendekatan oleh kebanyakan perusahaan teknologi dengan sangat jelas ketika ia mengatakan: “penyediaan teknologi dan media diluncurkan dari atas ke bawah oleh perusahaan besar dengan sedikit keterlibatan atau tidak sama sekali dari komunitas penutur. Dalam kasus ini, pendekatan yang merendahkan juga dapat terlihat: oleh karena teknologi yang terbatas, apapun yang diberikan harus sesuai ketentuan. Seringkali perusahaan menawarkan solusi yang sudah jadi tanpa mempertimbangkan kebutuhan, keinginan dan harapan yang sesungguhnya dari penutur bahasa minoritas. Seakan ada asumsi bahwa penutur ini seharusnya berterima kasih dengan apapun produk atau kesempatan yang diberikan pada mereka, tanpa memandang apakah hal tersebut menarik atau relevan bagi hidup mereka. Pengecualian dari kebiasaan ini ditunjukkan oleh van Esch et al. (2019), yang berulangkali menekankan kebutuhan untuk kolaborasi erat dengan penutur bahasa saat merencanakan pengembangan aplikasi pemrosesan bahasa secara alami.”

Pendekatan ini jarang memahami konteks dimana bahasa marjinal hidup, serta rancangan yang diperlukan untuk membawa bahasa mereka secara daring dalam cara yang kaya dan bernuansa. Ketika [Emna]({{<trurl>}}/stories/write-in-another-language{{</trurl>}}) mewawancarai Gamil dari Sudan, ia berbicara bahasa Arabic Sudan dan mengatakan, “Sudan adalah negara dengan banyak suku dan variasi tradisi dan adat. Jadi di bagian utara berbicara dialek Arab (seperti yang saya gunakan) dan tentu saja ini terjadi karena kolonialisme. Sementara di Timur dan Barat, suku-suku berbicara bahasa lokal yang berbeda, hanya mereka yang bisa berbicara bahasa itu dan memahaminya. Orang dari utara jarang ada yang bisa mengerti kecuali yang sudah hidup dan berinteraksi disana. Bahasa kami berasal dari bahasa Cushitic. Referensinya adalah bahasa dan peradaban Cushitic dan Nubian. Ketika High Dam tenggelam, kami kehilangan identitas Cushitic, kami tidak menemukan kamus untuk mendeskripsikan bahasa dan menerjemahkan dalam bahasa lain. Namun demikian, bahasa Nubian sudah dikenal dan diterjemahkan. Bahasa Nubian bahkan dapat ditemukan dalam telepon Huawei dalam pengaturan bahasa. Untuk bahasa timur dan barat, bahasa mereka bukan bahasa tulis (atau mungkin juga ditulis, saya tidak tahu, saya harus mengecek lagi). Untuk wilayah utara, kami berbicara bahasa Arab. Kami orang Afrika dan berbahasa Arab, bukan orang Arab.”

Emna menyimpulkan, “Web membutuhkan penggunanya, baik yang menulis maupun yang tidak. Namun demikian, perubahan bukan sesuatu yang merupakan tanggung jawab pengguna. Perusahaan yang mendesain dan mengembangkan piranti lunak juga harus bertanggung jawab demi desain masa depan web. Semua orang tampaknya saat ini membicarakan sikap inklusif dan melawan rasisme, diskriminasi dan bentuk kolonialisme lainnya. Namun demikian, untuk mengubah web, korporasi harus mendiskusikan bagaimana mereka melanggengkan eksklusi; desainer web, ahli web dan teknologi serta pemilik perusahaan teknologi seharusnya berkontribusi untuk membuat piranti yang dapat diakses oleh semua.”

Satu cara yang menganalisis secara kritis tentang berbagai bentuk eksklusi adalah mengakui bahwa dasar dari teknologi digital — kode itu sendiri — kebanyakan dalam bahasa Inggris. Hanya ada sedikit [bahasa pemrograman](https://www.wired.com/story/coding-is-for-everyoneas-long-as-you-speak-english/) yang memakai bahasa lain, yang berarti bahwa ahli teknologi itu sendiri perlu memahami bahasa Inggris dengan cukup baik untuk dapat melakukan koding. [Bahasa pemrograman Qalb](https://www.afikra.com/talks/conversations/ramseynasser), bahasa pemrograman berdasarkan sintaksis dan kaligrafi Arabic, adalah salah satu dari beberapa contoh yang mencoba memutus kecenderungan ini. Secara umum, industri teknologi perlu memahami bagaimana privilese dari sebuah bahasa telah terkode di setiap teknologi dan dalam sebagian besar ahli teknologi.

[Jeffrey dan Ashley]({{<trurl>}}/stories/learning-and-reclaiming-indigenous-languages{{</trurl>}}) menyatakan, “kajian kami memperjelas bahwa kemajuan bahasa masyarakat adat di internet harus berakar dalam analisis kritis tentang teknologi internet itu sendiri, sebagaimana proses sosial yang melingkari pemakaiannya.”

#### Aksi

* Mengakui bahwa mendesain dan menciptakan konten dan teknologi multilingual adalah hak fundamental dari penutur bahasa itu sendiri yang perlu menjadi prioritas bagi perusahaan teknologi, organisasi standar dan perlu didukung oleh institusi global seperti [UNESCO](https://en.unesco.org/news/tell-us-about-your-language-play-part-building-unescos-world-atlas-languages).
* Membangun model yang dipimpin komunitas, tata kelola global infrastruktur bahasa yang bekerjasama dengan kepercayaan dan penghargaan perusahaan teknologi dan institusi lainnya.
* Memusatkan persetujuan komunitas dan etika dalam menciptakan data dan teknologi bahasa, memastikan bahwa bahasa komunitas mempunyai kekuatan dan keamanan atas apa yang mereka bagikan, terutama mereka yang lebih jauh terpinggirkan oleh sistem terkait penindasan dan diskriminasi.

### Siapa pemilik design dan imajinasi?

Dari angka dan cerita yang sudah dibahas, kami mengetahui bahwa infrastruktur bahasa yang bermakna dan efektif hanya mungkin ketika kita fokus pada kebutuhan, rancangan dan imajinasi dari komunitas bahasa itu sendiri. Bahasa yang terpinggirkan akan berkembang dan meluas jika mayoritas di dunia yang terpinggirkan ini menjadi bagian dari pembangunan teknologi.

Kontributor kami mengusulkan berbagai cara untuk memastikannya, termasuk bagi perusahaan untuk mempekerjakan ahli teknologi dan ahli lainnya dari komunitas bahasa marjinal, sekaligus mencari sumber daya dari komunitas untuk mengerjakannya. Mereka merekomendasikan struktur yang plural dan proses berbasis konteks bahasa daripada hanya pendekatan dari satu formula. [Claudia]({{<trurl>}}/stories/decolonizing-minority-language{{</trurl>}}) menyatakan, “penutur bahasa minoritas tidak membutuhkan solusi siap pakai: mereka menekankan bahwa kebutuhan dan persyaratan harus didengarkan dan diterapkan dalam produk yang disesuaikan dengan kebutuhan tersebut. Konteks sosiolinguistik bisa sangat bervariasi dalam bahasa minoritas sehingga solusi yang disediakan juga harus beragam.”

Kontributor kami juga menjelaskan tanggung jawab dan pendekatan komunitas mereka untuk melestarikan dan memperluas bahasa mereka, menggunakan teknologi yang tersedia. [Ishan]({{<trurl>}}/stories/marginality-within-marginality{{</trurl>}}) mengatakan, “sebagai individu queer dan disabilitas, saya percaya bahwa kita harus menggunakan sumber daya sosial budaya dan ekonomi sebaik-baiknya agar pengalaman, aspirasi dan tuntutan kita didengarkan dan diperhatikan. Kami, pengguna internet, harus bertanggung jawab untuk membuat internet yang inklusif dan dapat diakses oleh semua.”

[Ana]({{<trurl>}}/stories/dill-wlhall-on-the-web{{</trurl>}}) menjelaskan bagaimana platform yang memiliki infrastruktur lisan dan visual yang lebih baik lebih sering dipakai oleh komunitasnya daripada platform lain yang bergantung pada teks. “Saat ini, dalam kasus bahasa Zapotec, bahasa lisan lebih sering dipakai daripada bahasa teks di dalam dunia maya. Platform media sosial seperti YouTube, Facebook, WhatsApp, dan Instagram lebih memungkinkan menjadi sarana bahasa lisan. Platform ini memungkinkan pengguna untuk mengunggah konten visual agar mengandung pesan sesuai keinginan pengguna tanpa terjebak pada kerumitan tulis menulis. Platform-platform inilah yang banyak digunakan oleh masyarakat adat secara global. Dalam kasus komunitas Sierra Zapotec, mayoritas pengguna terhubung ke internet melalui Facebook, di mana mereka menyiarkan festival tradisional, tarian, musik, peristiwa penting serta informasi bagi migran dan komunitas lokal. Sebelum Covid-19, Facebook berperan penting dalam penyebaran informasi bagi keluarga migran untuk program ritual dan pemakaman. Platform ini pun digunakan oleh beberapa komunitas Zapotec dalam siaran ulang program radio yang menjadi peran penting penghubung media komunikasi di banyak daerah pedesaan, yang menggunakan radio secara universal seperti di Internet.”

Bagi mereka yang dapat menggunakan bahasa tertulis, tagar menjadi penghubung komunitas secara digital untuk belajar serta memperkenalkan bahasa mereka sendiri, hingga menjadi inspirasi bagi komunitas lain. Seperti yang telah dijelaskan oleh [Jeffrey dan Ashley]({{<trurl>}}/stories/learning-and-reclaiming-indigenous-languages{{</trurl>}}): “Dalam konteks sosial di mana bahasa adat di seluruh Kanada telah ditargetkan untuk dihapus di bawah kebijakan asimilasi kolonial, jaringan tagar Twitter telah menyelenggarakan konteks yang unik dan bermakna bagi masyarakat adat dalam berbagi pengetahuan tentang bahasa adat… Misalnya, jaringan tagar pelajar bahasa Gwichin menginspirasi pelajar Anishnaabemowin (Ojibway) untuk membuat tagar dalam membangun jaringan mereka. Demikian pula, bahasa Neheyawewin (Cree) di jaringan Twitter telah menginspirasi pelajar Hul'qumi'num untuk mengembangkan “Word of the Day” (Kata Hari Ini) berbasis Twitter versi mereka sendiri.”

Penggunaan platform milik perusahaan secara luas dalam kreativitas komunitas yang terpinggirkan menjadi alasan penting bagi perusahaan teknologi untuk bekerja dengan mereka, bukan melawan mereka.

Pada saat yang sama, [Claudia]({{<trurl>}}/stories/decolonizing-minority-language{{</trurl>}}) mengingatkan bahwa aktivis bahasa dari komunitas yang terpinggirkan perlu diberikan koordinasi serta informasi yang lebih baik sehingga mereka tidak kehilangan semangat sumber daya dalam prosesnya. “Bagaimanapun layak dihargai, inisiatif [aktivis] tersebut cenderung kurang koordinasi, minim perencanaan, bahkan informasinya cukup minim ditemukan. Hal inilah yang menjadi masalah serius bagi masyarakat yang memiliki sumber daya tidak tak terbatas: redundansi upaya. Dalam kedua kasus tersebut, masalah utama terletak pada terbatasnya pengetahuan tentang apa yang sudah tersedia dan apa yang dibutuhkan. Untuk mendekolonisasi teknologi bahasa bagi bahasa minoritas, penting adanya standar yang lebih jelas tentang sejauh mana bahasa minoritas digunakan melalui media digital, sesering apa penggunaannya, dan memiliki tujuan apa. Yang tidak kalah penting adalah mengetahui hambatan-hambatan yang akan dihadapi penutur bahasa minoritas ketika mencoba menggunakan bahasa-bahasa tersebut: apakah mereka mengalami kesulitan secara teknis? Apakah mereka mengalami paranoia yang dipicu oleh diri sendiri? Karena menulis dalam bahasa minoritas adalah salah satu bentuk eksposur ke dunia luar, apakah orang-orang cenderung menghindari hal tersebut karena khawatir akan adanya hinaan maupun stigmatisasi? Begitu pula, sedikit yang diketahui tentang apa yang diingini oleh penutur bahasa minoritas terkait kesempatan digital: apa keinginan atau ekspektasi yang mereka harapkan untuk dapat dipenuhi?”

Laporan ini menjadi salah satu upaya memahami tantangan-tantangan selama ini, dan bagaimana cara untuk melangkah ke depan. Melakukan hal yang sama berulang-ulang dengan bahasa yang berbeda tidak akan berhasil. Contohnya, membuat aplikasi dalam bahasa Inggris dan berasumsi bahwa hal yang sama juga akan bekerja untuk bahasa Indonesia adalah sesuatu yang sangat problematik. Membangun jaringan internet yang lebih baik adalah tentang mengubah dinamika pemahaman di antara orang-orang, bukan sekedar memperbaiki masalah teknis. Dan multilingual di internet adalah serangkaian masalah teknis dalam sosial dan politik yang kompleks. Kita harus mengutamakan kebutuhan komunitas bahasa, bukan teknologi internet — dengan melakukan pola tersebut akan membuat teknologi bahasa lebih efektif dan berguna.

Secara kritis, kita harus memahami bahwa desain dan imajinasi dunia yang kita sebut sebagai “mayoritas minoritas” akan mengubah kerangka dasar bahasa kita menjadi lebih baik. “[[Indigemoji]](https://www.indigemoji.com.au/ayeye-akerte-about) hadir pada saat penyerapan teknologi yang cepat dan adanya konektivitas yang masih baru di Australia Tengah. Hal ini mengundang masyarakat lokal dalam mengembangkan usaha mereka dengan platform ini. Bagaimana platform tersebut bukan menjadi bentuk penjajahan lain? Dan bagaimana mereka dapat  menempatkan bahasa dan budaya dalam platform tersebut, dan menjadikannya hak milik mereka?”

#### Aksi

* Pusatkan teknologi bahasa dalam konteks kebutuhan, desain, dan imajinasi komunitas berbasis lokal tetapi terhubung secara global, daripada mengasumsikan teknologi dalam satu bahasa untuk semua.
* Jadilah kreatif dalam menggunakan teknologi internet dengan cakupan bahasa (lisan, visual, gestural, teks...) terlengkap untuk menjelajah, sehingga berbagai bentuk pengetahuan dapat diungkapkan dan akses dapat dibagikan dengan mudah.
* Belajar dari Masyarakat Adat kami untuk merancang teknologi bahasa dengan menghormati pengalaman kolektif dan komunitas dalam merencanakan apa yang dijalankan di masa depan. [Mari kita berjalan menuju masa depan kita](https://www.rnz.co.nz/national/programmes/morningreport/audio/2018662501/ka-mua-ka-muri-walking-backwards-into-the-future).

## Akhirnya, apa yang dapat anda lakukan?

Jika seseorang tidak berbahasa Inggris sebaik Anda, bukan berarti dirinya bodoh. Artinya, mereka berbicara salah satu dari 7000 bahasa lain di dunia dengan lebih baik.

Kita semua yang memiliki beragam keterampilan dan pengalaman yang berbeda perlu bekerja sama dalam menciptakan serta memperluas internet yang multilingual. Kita juga perlu memastikan bahwa informasi dan pengetahuan yang telah dibagikan dalam banyak bahasa tidak menyebabkan kerugian, melainkan mengarahkan pada kebaikan kolektif di dunia. Kita membutuhkan apa yang disebut 'solidaritas berupa tindakan'.

### Jika Anda berada di bidang teknologi:

* Kenali bagaimana kebijakan perusahaan Anda berkontribusi (atau tidak) pada internet yang multilingual, dan pendalaman dari pengetahuan bersama tentang kemanusiaan.
* Fokuskan internasionalisasi dan lokalisasi bahasa (yang termarjinalkan) sebagai strategi Anda, daripada melihatnya sebagai sesuatu yang periferal. Lakukan pendekatan kemitraan dengan komunitas, bukan pendekatan top-down yang tidak memperhatikan konteks.
* Terima kritik secara teliti tentang gaya bahasa secara luas dan teknologi bahasa secara otomatis, dan kerugian yang ditimbulkannya tanpa pengawasan yang bijaksana.
* Membangun proses konteks, kurasi, dan moderasi manusia secara cermat dari seluruh kinerja bahasa, menggunakan kumpulan data yang diatur oleh komunitas yang lebih kecil.
* Bekerja dengan menghormati masyarakat, terutama mereka yang paling terpinggirkan dan kemungkinan paling dirugikan oleh kurangnya pemahaman dan pengertian.
* Berdayakan komunitas Anda kepada mereka yang telah memberikan waktu serta keahliannya.

### Jika Anda berada di organisasi standar teknologi:

* Kenali bagaimana standar bahasa yang kaya akan kontext diperlukan..
* Bangun hubungan dan proses yang lebih baik dengan komunitas bahasa yang terpinggirkan sehingga lebih banyak standar yang terbentuk dari kemitraan dengan komunitas, jika tidak dipimpin oleh komunitas.
* Mengundang lebih banyak anggota dari komunitas bahasa yang terpinggirkan untuk menjadi bagian dari pemerintahan, dan berikan mereka sumber daya yang diperlukan untuk berpartisipasi secara penuh.

### Jika Anda berada di pemerintahan:

* Ketahuilah bahwa konten dalam bahasa negara Anda harus dapat diakses oleh semua orang, bukan hanya minoritas yang memiliki hak istimewa.
* Mendukung perluasan konten bahasa, dari dan untuk mereka yang terpinggirkan atau didiskriminasi, di wilayah Anda.
* Mendukung pelestarian dan digitalisasi bahasa yang terpinggirkan di wilayah Anda, bukan hanya bahasa yang dominan.

### Jika Anda berada di bidang teknologi dan pengetahuan yang terbuka dan bebas (_open source technology dan open knowledge_):


* Mengakui bahwa teknologi dan pengetahuan yang terbuka juga memiliki ketidakseimbangan dan keterbatasannya sendiri, meskipun demi kebaikan bersama.
* Hormati batasan-batasan yang ditetapkan oleh komunitas terpinggirkan dalam berbagi pengetahuan mereka, karena cara-cara pengetahuan mereka telah dieksploitasi dan dikomodifikasi secara historis di masa lalu.
* Bekerja dengan komunitas bahasa yang terpinggirkan untuk menciptakan teknologi dan pengetahuan yang mereka butuhkan, daripada mengasumsikan kebutuhan mereka menurut pikiran Anda.

### Jika Anda berada di institusi GLAM (galeri, perpustakaan, arsip, dan dokumentasi museum):

* Kenali bahwa bahasa adalah inti pengetahuan dan budaya yang Anda kurasi, lestarikan, dan tampilkan.
* Bekerja dengan komunitas bahasa yang terpinggirkan untuk memastikan bahwa sejarah dan bahasa mereka ditegaskan, diakui, dan diperkuat seperti yang mereka inginkan, melalui keberadaannya (atau kepemilikan serta lokasi dari materialnya). Hal ini termasuk hak komunitas yang terpinggirkan untuk memilih tidak membagikan pengetahuan dan material tertentu secara publik. Hal ini penting karena banyak institusi GLAM, terutama di Global North, berakar pada kompleksitas sejarah kolonisasi dan kapitalisme.
* Pastikan bahwa koleksi material bahasa yang disimpan dapat diakses secara bebas dan mudah oleh komunitas yang terpinggirkan serta jaringannya, sehingga kita dapat menciptakan program bahasa secara kolektif.

### Jika Anda dalam institusi pendidikan:

* Kenali bagaimana biasnya pendidikan kita atas bahasa yang bersumber dalam basis teks, dan bahasa tertentu.
* Perluas cara Anda mengajar dengan belajar memasukkan berbagai bentuk bahasa hingga pengetahuan yang ada di dalamnya.
* Membaca, mendengar, dan mengutip terjemahan jika memungkinkan, serta memotivasi orang lain untuk melakukannya juga.

### Jika Anda dalam dunia penerbitan:

* Kenali bagaimana bahasa kolonial Eropa cenderung mendominasi dalam kebanyakan penerbitan selama ini di dunia.
* Perluas jumlah bahasa sesuai tempat Anda menerbitkannya, dan digitalkan ke dalam seluruh program bahasa.
* Terbitkan lebih banyak buku dan material multilingual.
* Melakukan percobaan dengan bentuk penerbitan multimodal, sehingga berbagai bentuk bahasa lisan, visual, dan tekstual dapat dibagikan secara bersamaan dengan lebih mudah.
* Hormati dan kenali penerjemah Anda.

### Jika Anda terlibat dalam filantropi:

* Sadari bahwa bahasa adalah inti dari segala jenis keahlian, pengalaman, dan pengetahuan manusia, apa pun kerumitan yang sedang Anda biayai.
* Sediakan berbagai interpretasi bahasa dalam seluruh program pertemuan global dan regional yang Anda dukung.
* Mendukung produksi, pelestarian, dan digitalisasi material bahasa komunitas yang Anda layani, dan pastikan materi Anda disampaikan dalam bahasa komunitas yang Anda layani.

### Jika Anda berada dalam komunitas bahasa marjinal:

* Sadarilah bahwa Anda tidak sendirian.
* Ketahuilah bahwa komunitas Anda berhak memutuskan pengetahuan apa yang ingin Anda bagikan kepada dunia dan bagaimana caranya.
* Bekerjalah dengan para tetua, cendekiawan, dan generasi muda di komunitas Anda, serta teman-teman dari komunitas lain dalam mengumpulkan sekaligus berbagi pengetahuan.
* Jika Anda ingin terhubung dengan orang lain yang melakukan pekerjaan serupa, hubungi kami!

### Jika anda mencintai bahasa dan bertanya tentang hal yang dapat dilakukan:

* Akuilah bahwa bahasa adalah jantung dari siapa kita dan apa yang kita lakukan, dan merupakan pusat dari pengetahuan dan budaya yang berbeda — termasuk pengetahuan dan budaya anda!
* Mendiskusikannya dengan keluarga, kawan dan komunitas untuk memperhatikan bagaimana dan mengapa bahasa Inggris dan beberapa bahasa lainnya mendominasi akses internet dan kontennya, serta bagaimana mengubahnya bersama.
* Secara aktif mencari, membaca, mendengarkan dan berbagi tentang kontribusi komunitas bahasa yang termarjinalkan (termasuk laporan ini!)
* Jika anda ingin menerima berita terbaru dari inisiatif kami, ikuti kami di sosial media!


## Rasa syukur

Kami memegang cinta, rasa hormat, dan solidaritas dengan komunitas terpinggirkan di seluruh dunia (Masyarakat adat dan lainnya) yang melihat bahasa sebagai inti dari identitas dan cara hidup mereka. Upaya mereka dalam melestarikan, menghidupkan, serta memperluas bahasa sebagai bentuk ekspresi yang bermakna, mengilhami kita dalam membangun jaringan internet yang multilingual, yang menjadikan diri kita penuh keragaman. Kami juga sangat menghargai semua komunitas, cendekiawan institusional dan teknolog yang mencintai bahasa seperti kami, yang bekerja keras setiap harinya dengan menjadikan internet multilingual seperti di dunia nyata.

Kepada banyak [kontributor, penerjemah]({{<trurl>}}/about{{</trurl>}}), dan komunitas kami di seluruh dunia (terutama kepada yang telah bergabung dalam [wacana Dekolonisasi Bahasa Internet 2019](https://whoseknowledge.org/resource/dtil-report/): terima kasih untuk semua yang telah Anda lakukan bagi dunia, dan karena telah bersabar saat kami mencoba bertahan selama dua tahun terakhir! Terima kasih khususnya kepada ilustrator kami atas esai visualisasi kreatifnya, dan kepada animator kami, yang telah membuat ilustrasi animasi ini.

Terima kasih banyak kepada semua teman dan komunitas kami yang telah memberikan [ulasan]({{<trurl>}}/about{{</trurl>}}) terhadap karya kami melalui perspektif yang berbeda, sekaligus dalam bahasa yang berbeda. Segala kekurangan yang kami miliki, dengan dukungan dan solidaritas Anda, tetap membantu pekerjaan ini berjalan dengan lebih baik. Dan akhirnya, untuk beberapa hal lainnya, keluarga biologis dan keluarga pilihan kami: kami tidak dapat melewati beberapa tahun terakhir (terutama tahun 2019-2021) tanpa saling berpelukan, meskipun hanya secara virtual. Cinta dan kepercayaan menjadi bahasa terbaik atas semuanya.

## Definisi

Banyak cara dalam mendefinisikan berbagai aspek bahasa, dan sejarah yang telah kita diskusikan. Tidak semua definisi disetujui oleh satu sama lain! Kami telah menggunakan istilah dan frasa tertentu di seluruh laporan ini. Berikut merupakan definisi kami tentang kata-kata dan frasa kunci di laporan ini.

* **Bahasa dominan**: Bahasa yang merupakan bahasa dari mayoritas penduduk di wilayah tertentu, atau yang mendominasi bentuk-bentuk tertentu dari kekuatan dan validasi sejarah, melalui kekuatan hukum, politik, atau budaya. Misalnya, bahasa Hindi adalah bahasa yang dominan di Asia Selatan, dibandingkan dengan banyak bahasa lain, terutama mengingat bahasa Hindi sebagai rumpun bahasa atau "dialek". Demikian pula, Cina Mandarin adalah bahasa yang dominan di Cina, melalui kebijakan pemerintah, dibandingkan dengan bentuk-bentuk lain dari bahasa Cina serta bahasa masyarakat asli lainnya di wilayah tersebut. Beberapa bahasa dominan juga merupakan bahasa “resmi” atau “nasional” di suatu wilayah atau negara.
* **Bahasa kolonial Eropa**: Bahasa dari Eropa Barat yang tersebar di Afrika, Asia, Amerika, Karibia, dan Kepulauan Pasifik melalui proses kolonisasi oleh perusahaan dan pemerintah Eropa Barat, sejak abad ke-16 dan seterusnya. Ini termasuk bahasa Inggris, Spanyol, Prancis, Portugis, Belanda, dan Jerman. Penting digarisbawahi bahwa bahasa-bahasa ini juga merupakan bahasa “penjajah” bagi penduduk asli Amerika Utara, tidak hanya Amerika Latin (Amerika Tengah dan Selatan).
* **Global South dan Global North:** Istilah “Global South” mengacu pada wilayah Afrika, Asia, Amerika Latin dan Karibia, serta Kepulauan Pasifik yang dijajah oleh negara-negara Eropa Barat. Frasa ini tidak dimaksudkan sebagai istilah geografi. Sebaliknya, ini dimaksudkan sebagai cerminan kondisi sosio-ekonomi dan politik historis yang berkelanjutan yang menjadi ciri negara dan wilayah, dan membedakannya dari negara-negara lain yang memiliki privilese di Eropa dan Amerika Utara, atau "Global North". Terminologi ini dibuat dan diperkuat oleh para sarjana serta aktivis dari Global South untuk meninggalkan beberapa terminologi yang dianggap merendahkan, seperti negara-negara “terbelakang” dan “berkembang”, dan “Dunia Ketiga”. Kolonisasi menyebabkan genosida atau pemusnahan banyak Masyarakat adat di Global North, yang karenanya, beberapa individu serta komunitas di Global South telah diuntungkan dalam kolonisasi bangsa mereka sendiri. Kami terkadang mengatakan bahwa ada Global South di Global South dan Global North di Global South. Struktur dan proses ini mempengaruhi status bahasa di wilayah tersebut. (lihat istilah “mayoritas dunia yang diminoritaskan”).
* **Bahasa masyarakat adat**: Bahasa yang digunakan oleh Masyarakat adat di wilayah atau tempat tertentu adalah bahasa Masyarakat adat. Masyarakat adat dipandang sebagai “masyarakat pertama” atau penghuni pertama tempat-tempat di seluruh dunia yang kemudian dijajah dan dihuni oleh kelompok budaya yang berbeda. Mayoritas lebih dari 7000 bahasa di dunia dituturkan oleh komunitas Masyarakat adat.
* **Bahasa dan dialek**: Kami menganggap sistem pengungkapan terstruktur antara manusia, baik dengan ucapan, suara, tanda, isyarat, atau tulisan, sebagai bahasa. Beberapa ahli bahasa mendefinisikan "dialek" untuk menggambarkan apa yang terdengar seperti varietas yang berbeda dari bahasa yang sama, tetapi yang mungkin "saling dimengerti" — dipahami oleh semua pembicara dari varietas bahasa yang berbeda ini yang kemudian dapat berbicara satu sama lain. Namun, karena perbedaan antara bagaimana "bahasa" dan "dialek" didefinisikan umumnya adalah pilihan politik (daripada linguistik) — berdasarkan proses sejarah kekuasaan dan privilese — kami jarang menggunakan istilah dialek dalam laporan ini. Kami lebih suka menggunakan istilah “rumpun bahasa”, menunjukkan bahwa ada banyak bahasa yang mungkin memiliki sejarah yang sama tetapi juga memiliki karakteristik yang berbeda, seperti rumpun bahasa Arab, Cina, atau Hindi.
* **Bahasa lokal**: Dalam laporan ini, kami telah mendefinisikan bahasa lokal sebagai bahasa yang digunakan oleh sebagian besar orang di suatu negara atau wilayah.
* **Bahasa marjinal**: Dalam laporan ini, bahasa yang terpinggirkan adalah bahasa yang tidak menonjol di internet dalam hal dukungan atau konten bahasa, yaitu informasi dan pengetahuan dalam bahasa tersebut. Bahasa-bahasa ini terpinggirkan oleh struktur historis dan berkelanjutan serta proses kekuasaan dan privilese, termasuk kolonisasi dan kapitalisme, daripada oleh populasi atau jumlah penutur. Beberapa bahasa marjinal sudah terancam punah di dunia (seperti banyak bahasa masyarakat adat). Tetapi beberapa bahasa yang terpinggirkan dituturkan oleh sebagian besar masyarakat di wilayah mereka atau di dunia, namun kurang terwakili secara daring (seperti Punjabi dan Tamil, atau Hausa dan Zulu, sebagai beberapa contoh dari banyak bahasa dominan di Asia dan Afrika).
* **Bahasa minoritas dan mayoritas**: Bahasa minoritas adalah bahasa yang digunakan oleh minoritas (dalam hal jumlah) populasi, di yurisdiksi atau wilayah yang dijelaskan, sementara bahasa mayoritas digunakan oleh mayoritas (dalam hal jumlah) dari populasi tersebut.
* **Mayoritas dunia yang diminoritaskan**: Struktur kekuasaan dan privilese yang historis dan berkelanjutan mengakibatkan diskriminasi dan penindasan terhadap banyak komunitas dan bangsa yang berbeda, di seluruh dunia. Bentuk-bentuk kekuasaan dan privilese ini seringkali saling terkait dan bersilangan, sehingga beberapa komunitas dirugikan atau tertindas dalam berbagai cara: misalnya, berdasarkan jenis kelamin, ras, seksualitas, kelas, kasta, agama, wilayah, kemampuan, dan tentu saja, oleh bahasa. Baik secara daring atau di dunia fisik, komunitas-komunitas ini membentuk mayoritas dunia berdasarkan populasi atau jumlah, tetapi mereka sering tidak dalam posisi berkuasa, dan oleh karena itu mereka diperlakukan seperti minoritas. Dengan kata lain, mereka adalah “mayoritas minoritas” di dunia.

[Lebih jauh tentang bagaimana mengutip dan menggunakan laporan ini.]({{<trurl>}}/license{{</trurl>}})
 
[Lebih jauh tentang sumber dan inspirasi kami.]({{<trurl>}}/resources{{</trurl>}})

[Mengunduh laporan ini (PDF 1.8 MB).](/media/pdf-summary/ID-STIL-SummaryReport.pdf)