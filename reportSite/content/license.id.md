---
author: Whose Knowledge
title: Lisensi dan Kutipan
date: 2019-05-13
toc: false
---

Kami menyeimbangkan keterbukaan konten, data, dan desain dalam menjaga keamanan komunitas yang terpinggirkan. Kami ingin laporan State of the Internet's Languages ​​dibagikan secara bebas, karena kami yakin pekerjaan ini adalah dasar dari internet dan teknologi digital multibahasa yang di dekolonisasi. Pada saat yang sama, laporan ini mencakup pekerjaan bersama komunitas marjinal yang secara historis melihat pengetahuan mereka dieksploitasi oleh orang lain, **kami menghormati semua hal yang mereka bagikan dengan murah hati kepada kami dan dunia**.
 
Kami berkomitmen untuk melisensikan **konten, termasuk seni dan desain** di situs ini dengan **lisensi terbuka**: [CC NC-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.id). Kecuali ada ketentuan lain di situs ini, informasi serta konten dapat dibagikan, diubah, atau digunakan kembali secara bebas (namun Anda tidak dapat menggunakannya untuk mendapatkan keuntungan atau untuk alasan komersial) Anda harus mengakui Whose Knowledge? Oxford Internet Institute , , dan pembuat konten lain saat melakukannya, baik komunitas, organisasi, atau orang. Anda juga harus mendistribusikan karya terbaru Anda dengan lisensi yang sama.
 
Kami melisensikan **data** di situs ini dengan lisensi terbuka [ODbL](https://opendatacommons.org/licenses/odbl/summary/). Artinya Anda dapat menggunakan, membuat, dan mengadaptasi melalui database yang telah dibuat laporannya. Anda harus mengakui Martin Dittus, Mark Graham dan Oxford Internet Institute, serta melaporkannya kepada State of the Internet's Languages, saat melakukan dan mendistribusikan karya terbaru Anda dengan lisensi yang sama.
 
Kami juga berkomitmen untuk melisensikan **kode desain** untuk situs web ini dengan lisensi [MIT](https://choosealicense.com/licenses/mit/), yang berarti Anda dapat menggunakan kembali, mengubah, dan memodifikasi kode untuk tujuan apa pun, selama menyertakan salinan asli lisensi MIT saat mendistribusikan karya baru Anda.
 
Kami menyarankan format kutipan kami:

_“Whose Knowledge?, et al. “Laporan Bahasa dari Internet”, inisiatif yang diteliti dan didokumentasikan secara kolaboratif, Tanggal, Waktu, internetlanguages.org._
 
Kami berterima kasih kepada [Sara Ahmed](https://feministkilljoys.com/2013/09/11/making-feminist-points/), [P. Gabrielle Foreman](https://coloredconventions.org/about/team/), [Jessica Marie Johnson](https://twitter.com/jmjafrx) dan [Kelly Foster](https://kellyfoster.co.uk/ ) untuk kepemimpinan mereka dalam mempraktikan kutipan yang radikal sekaligus bijaksana. Mengikuti panduannya, kami meminta Anda menikmati dalam berbagai laporan sesuai kutipan atau secara keseluruhan, tetapi jangan lupa untuk mengutipnya. Ini berlaku sebagai laporan pada setiap media sosial yang menyebutkan hal tersebut. Silakan tag kami atau beri tahu kami jika Anda membagikannya, sehingga kami dapat melacak kegunaan dan dampaknya (@whoseknowledge). Jika Anda memiliki pertanyaan tentang lisensi atau kutipan, [silakan hubungi kami](mailto:languages@whoseknowledges.org).