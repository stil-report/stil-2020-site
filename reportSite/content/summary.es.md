---
Author1: Anasuya Sengupta
Author2: Abbey Ripstra
Author3: Adele Vrana
Title: 'Resumen'
IsSummary: true
Date: 2020-01-15
hasboxes: true
translationKey: 
Imageauthor1: Anasuya_Sengupta.webp
Imageauthor2: Abbey_Ripstra.webp
Imageauthor3: Adele_Vrana.webp
haspdf: true
PDFsummary: /media/pdf-summary/ES-STIL-SummaryReport.pdf
hasaudio: true
Audio1: https://archive.org/download/STIL-Summary-ES/01_Spanish.mp3
Audio2: https://archive.org/download/STIL-Summary-ES/02_Spanish.mp3
Audio3: https://archive.org/download/STIL-Summary-ES/03_Spanish.mp3
Audio4: https://archive.org/download/STIL-Summary-ES/04_Spanish.mp3
Audio5: https://archive.org/download/STIL-Summary-ES/05_Spanish.mp3
Audio6: https://archive.org/download/STIL-Summary-ES/06_Spanish.mp3
Audio7: https://archive.org/download/STIL-Summary-ES/07_Spanish.mp3
Audio8: https://archive.org/download/STIL-Summary-ES/08_Spanish.mp3
---

## ¿Por qué este informe? ¿Quiénes somos?

Los diccionarios y las gramáticas nos muestran que el lenguaje es un sistema estructurado de expresar información que se usa principalmente entre seres humanos. Pero el lenguaje es mucho más que eso: es la herencia primordial que las personas nos ofrecemos las unas a las otras. Cuando pensamos, cuando hablamos, cuando escuchamos, cuando imaginamos… estamos usando el lenguaje con nosotras mismas y con las demás. ¿Qué idioma hablas? ¿Sueñas en esa lengua? ¿Piensas en un idioma distinto al que usas cuando estás en el trabajo? Tus canciones favoritas ¿suelen estar en una lengua que entiendes?

Cada lengua es una manera de ser, de hacer, de comunicarse en el mundo. Y lo que es más importante, de saber e imaginar. Cada idioma es un sistema de conocimiento en sí mismo: nuestra lengua es la manera esencial en la que le encontramos sentido a nuestro mundo y lo explicamos a las demás personas. Nuestras lenguas pueden ser orales (habladas y de señas), escritas ¡o transmitidas a través de sonidos emitidos [por un silbato o un tambor](https://pages.ucsd.edu/~rose/Whistled%20and%20drum%20languages.pdf)! En todas sus formas, **el lenguaje es una transmisión de información**. En otras palabras, el lenguaje es la forma más directa de expresar lo que pensamos, creemos y sabemos.

Ahora piensa en las lenguas en las que hablas, piensas, sueñas o escribes. ¿En cuántas de ellas te sientes plenamente capaz de compartir información y comunicarte en espacios digitales? ¿Cuál ha sido tu experiencia al usar lenguaje o lenguas en la red? ¿Dispone el hardware que usas de los caracteres de tu lengua? ¿Has tenido que modificar tu teclado para poder usarlo en la lengua que querías? ¿Has tenido que aprender un idioma distinto al tuyo para acceder y participar en la red? Si tu respuesta a una o más de estas preguntas es “no”, entonces estás entre las pocas personas privilegiadas en el mundo que pueden usar el internet con facilidad en tu propia lengua. Y probablemente esta lengua sea… el inglés.

El internet y sus variados espacios digitales ofrecen una de las infraestructuras más importantes de información, comunicación y acción que existen hoy en día. Aunque existen más de 7000 lenguas distintas en el mundo (incluidas las lenguas habladas y de señas), **¿cuántas de estas lenguas podemos encontrar y usar en línea? ¿Cómo sería un internet verdaderamente multilingüe? ¿Cómo se leería, sentiría y sonaría?**

Este informe es un intento de responder a estas preguntas. Somos una [alianza]({{<trurl>}}/about{{</trurl>}}) entre tres organizaciones: Whose Knowledge?, el Oxford Internet Institute y The Centre for Internet and Society (India). Nos hemos unido para aportar diferentes perspectivas, experiencias y análisis del estado de las lenguas en el internet, y junto a otras entidades a las que les preocupan estos temas, esperamos crear un internet y unas tecnologías y prácticas digitales más multilingües.

Este informe tiene tres objetivos principales:

* **Mapear el estado actual de las lenguas en el internet**: estamos intentando entender qué lenguas son las que están actualmente representadas en el internet y cómo lo están. Hacemos esto mediante datos cuantitativos (estudiando las estadísticas de diferentes plataformas, herramientas y espacios digitales) y datos cualitativos (aprendiendo de las historias y experiencias que la gente tuvo con distintas lenguas en la red).
* **Concientizar sobre los obstáculos y las oportunidades que supondría construir un internet más multilingüe**: la creación y gestión de las tecnologías, el contenido y las comunidades de acuerdo a las lenguas que existen en el mundo implica unos retos considerables, así como posibilidades y oportunidades fascinantes. Este informe expone algunos de estos retos y posibilidades.
* **Promover un plan de acción:** en base a estas consideraciones y conocimientos, ofrecemos algunas propuestas con las que podemos (tanto nosotres como toda persona en el mundo que trabaje con estas cuestiones) planificar y pasar a la acción para garantizar un internet más multilingüe.

### ¿Qué es y qué no es este informe?

¡Este informe es un proyecto en desarrollo o un proyecto en curso!

Una gran cantidad de personas, comunidades e instituciones han dedicado mucho tiempo a estudiar distintos aspectos de las lenguas, y más recientemente, el estado de las lenguas en la red. Nos hemos inspirado en estas personas, pero este informe no pretende ser un estudio completo y exhaustivo de cada una de ellas y de su trabajo. Tampoco conocemos a todas las personas que trabajan con las lenguas y con el internet, aunque hemos tratado de incluir a la mayoría de las que conocemos y que nos inspiran, de un modo u otro, en las secciones [“Recursos”]({{<trurl>}}/resources{{</trurl>}}) y [“Agradecimientos”](#toc_6_H2).

Este informe está limitado por los datos que hemos podido recabar, y hablamos de algunas de estas limitaciones en la sección de [Cifras]({{<trurl>}}/numbers{{</trurl>}}). Apreciamos los comentarios y sugerencias para mejorar y actualizar la información que ofrecemos aquí, y nos encantaría escuchar de aquellas personas que ya están trabajando en estas cuestiones y a las que les gustaría estar incluidas en futuras entregas de este informe.

Hemos hecho todo lo posible para escribir este informe en el estilo más accesible posible. Queremos que múltiples generaciones y comunidades se unan a nuestro proyecto, y no queremos que la jerga o el lenguaje “académico” interfieran en su lectura y reflexión. También nos encantaría que se tradujera en la mayor cantidad de idiomas posible (traductores: ¡[contacten con nosotres]({{<trurl>}}/engage{{</trurl>}})!), pues mientras escribíamos partes de este informe en inglés, no queríamos que el inglés supusiera una barrera para la reflexión ni la acción.

Esperamos que este informe sirva como punto de referencia para futuras investigaciones, debates y acciones sobre estas cuestiones, del mismo modo que los esfuerzos pasados han servido de base para la elaboración de este.

### ¿Quiénes somos y por qué hemos decidido unirnos para elaborar este informe?

Tres organizaciones se han unido para llevar a cabo la labor de investigación de este informe: The Centre for Internet and Society, el Oxford Internet Institute y Whose Knowledge?. Las tres estamos muy preocupadas por las implicaciones del internet y las tecnologías digitales desde distintas perspectivas académicas, políticas y de incidencia. 

En los últimos años hemos estado trabajando en nuestras propias formas de entender las desigualdades y la injusticia en el internet: ¿quién contribuye al contenido en línea y cómo? Pronto nos dimos cuenta de que en todo el internet, los datos y la información solo estaban disponibles en unas pocas lenguas. Entonces quisimos indagar más: actualmente, ¿cuán presentes están en el internet las lenguas que se hablan en el mundo? ¿Cuán multilingüe es el internet? Nuestro campo de exploración estaba limitado a las pocas áreas en las que podíamos encontrar información útil, abierta y de acceso público, pero tenemos la esperanza de poder contribuir al trabajo de todas las personas que luchamos por un internet multilingüe.

_Una nota breve sobre la COVID-19 y este informe_: Comenzamos a trabajar en este informe en 2019, antes de la COVID-19, pero la mayoría de la labor de análisis, entrevistas y redacción se llevó a cabo durante la pandemia que ha cambiado nuestras vidas de manera individual y colectiva. Todas las personas que contribuyeron a este informe se han visto afectadas de un modo u otro, y compartir este informe con el resto del mundo nos llevó mucho más tiempo del que estimábamos. Sin embargo, la COVID-19 también nos ha ayudado a recordar que todas las personas estamos interconectadas, que es esencial poder transmitir ideas complejas en diferentes lenguas y lo importante que es disponer de una infraestructura (digital) resiliente y accesible que sea verdaderamente multilingüe.

## ¿Cómo leer este informe?

Este informe es lo que se conoce como “digital first”, es decir, que la mejor manera de leer, escuchar o aprender de este informe es a través de esta misma página web, ya que el informe tiene diferentes niveles. Nuestro informe reúne [Cifras]({{<trurl>}}/numbers{{</trurl>}}) e [Historias]({{<trurl>}}/stories{{</trurl>}}). Aprender sobre el estado de las lenguas en la red desde una perspectiva estadística nos da una visión general de sus problemas y nos ayuda a entender los diversos contextos en los que vive la gente. Pero son las historias, narradas desde diferentes contextos, las nos ayudan a entender en mayor profundidad lo fácil o difícil que le resulta a la gente usar el internet en sus propias lenguas. Con las historias y las estadísticas, podemos comenzar a abordar algunos de los contextos, obstáculos y oportunidades subyacentes.

Por esta razón, en este informe hay tres niveles distintos:

* El Resumen del Informe del Estado de las Lenguas en Internet y cómo lo realizamos (¡que es justo lo que estás leyendo ahora mismo!)
* [Cifras]({{<trurl>}}/numbers{{</trurl>}}), que analiza varias cuestiones clave respecto a las lenguas en algunas plataformas digitales, aplicaciones y dispositivos que usamos diariamente. Nuestres amigues del Oxford Internet Institute se encargaron de esta labor, y en esta sección encontrarás sus fascinantes visualizaciones de datos y análisis. Ten en cuenta que este análisis está limitado a los datos que pudimos obtener a partir de bases de datos y materiales abiertos y de acceso público. En esta parte del informe también se habla con más detalle de otras limitaciones metodológicas, pero sobre todo de lo difícil que es encontrar un único procedimiento consistente para identificar lenguas, y del mismo modo, estimar cuántas personas usan idiomas específicos, ya que las lenguas y su uso son dinámicos y cambian a lo largo del tiempo.
* [Historias]({{<trurl>}}/stories{{</trurl>}}) que nos hacen entender en mayor profundidad cómo diferentes personas y comunidades alrededor del mundo experimentan el internet en sus propias lenguas, y en muchos casos, lo difícil que es hoy en día encontrar la información que necesitan en sus propios idiomas. [Hicimos una convocatoria](https://whoseknowledge.org/initiatives/callforcontributions/) en la cual solicitamos estas historias en formato escrito y oral, así que las encontrarás tanto en forma de textos como en entrevistas de audio y vídeo. Nuestres amigues del Centre for Internet and Society llevaron a cabo esta labor, reuniendo experiencias lingüísticas alrededor del mundo para formar este rico tapiz.

Contamos con aportaciones sobre lenguas indígenas como el chindali, cree, ojibwe, mapuche, zapoteco y arrente de África, las Américas y Australia; algunas lenguas minoritarias como el bretón, euskera, sardo y carelio de Europa; así como lenguas regional y globalmente dominantes como el bengalí, el indonesio y cingalés de Asia, y diferentes variantes de árabe del norte de África.

Y lo más importante, nuestres colaboradores han escrito o hablado tanto en sus propias lenguas como en inglés, y nuestro resumen también está escrito y grabado en diferentes lenguas, ¡así que esperamos que disfrutes de leerlo o escucharlo en más de un idioma!

Hemos hecho todo lo posible para representar vívidamente estas aportaciones en formato visual, con creativas ilustraciones y una animación, que combinan diversos aspectos sociales y técnicos del lenguaje. Al igual que el resto del informe, estas también fueron elaboradas colaborativamente a partir del diálogo entre nuestra ilustradora y nuestres colaboradores.

## ¿Cuán multilingüe es el internet?

El internet aún no es (ni está cerca de serlo, desgraciadamente) tan multilingüe como lo somos las personas en la vida real. Intentamos entender el porqué mediante el estudio de algunas [estadísticas]({{<trurl>}}/numbers{{</trurl>}}) y [experiencias]({{<trurl>}}/stories{{</trurl>}}) de personas a lo largo del mundo. Aquí ofrecemos solamente un breve resumen y análisis de toda esa riqueza y de la profundidad de la labor de nuestres colaboradores de distintas partes del mundo, así que por favor, no dudes en consultar sus historias si buscas más detalles o inspiración.

Primero estudiamos los contextos en los que la gente usa el internet en distintas lenguas en todo el mundo. Estudiamos las maneras en las que se distribuyen (o no) la información y el conocimiento en distintas lenguas y geografías. Luego estudiamos más a fondo las principales plataformas y aplicaciones que la gente usa para crear contenido, comunicarse y compartir información en línea, así como el número de lenguas en las que están disponibles. Analizamos en profundidad Google Maps y Wikipedia, dos espacios de contenido multilingüe que todo el mundo usa diariamente, y estudiamos cómo funcionan en diferentes idiomas.

Mientras tanto, compartimos las historias y experiencias de la gente que accede y aporta información en la red en sus propias lenguas. Hemos llegado a la conclusión de que la mayoría de nuestres colaboradores tienen necesariamente que cambiar de la primera lengua que escogieron en un principio a otra para así poder acceder y contribuir al contenido que les interesa.

### Contexto lingüístico: desigualdades de información en los planos geográfico y digital

{{< summary-quote
    text="Las lenguas con tradición oral no tienen cabida en la red de hoy en día"
    author="Ana Alonso"
    link="/es/stories/dill-wlhall-on-the-web/"
>}}

{{< summary-quote
    text="Creemos que estas plataformas, por lo general, perpetúan la idea colonialista de que algunas lenguas tienen mayor valor y capacidad comunicativas que otras, lo cual es una visión dañina para lenguas minoritarias como el mapudungún"
    author="Proyecto Kimeltuwe"
    link="/es/stories/use-of-our-ancestral-language/"
>}}

Se estima que [más del 60 % del mundo](https://www.internetworldstats.com/stats.htm) está conectado digitalmente, la mayoría a través de celulares o dispositivos móviles. De todas aquellas personas que están conectadas a la red, tres cuartas partes provienen del Sur global: de Asia, África, Latinoamérica, el Caribe y las Islas del Pacífico. ¿Pero podemos acceder todas las personas de igual manera? ¿Somos capaces de crear y producir información pública en línea en la misma medida en la que la consumimos?

En el estudio comparativo de Martin y Mark, en el que compararon la población global con el número de personas usuarias del internet, encontramos que ciertas poblaciones pueden acceder al internet de manera más sustancial que otras, incluyendo espacios digitales muy conocidos. Por ejemplo, a pesar de que la mayoría de las personas usuarias en línea provenimos del Sur global, no podemos acceder al internet como creadoras y productoras de información, solamente como consumidoras. La mayoría de las ediciones que tienen lugar en la Wikipedia, la mayoría de las cuentas de Github (un repositorio de código), y la inmensa mayoría de usuarios de Tor (un navegador seguro) son de Europa y Norteamérica.

{{< summary-side-fig-fancybox
    src_small="/media/summary/STIL-Internet-regions-500px.webp"
    src_full="/media/summary/STIL-Internet-regions-1000px.webp"
    alt="Cifras de participación digital por regiones del mundo. (Fuente: Banco Mundial, 2019, Fundación Wikimedia 2019, Wikipedia 2018, Github 2020, Tor 2019)"
    caption="Cifras de participación digital por regiones del mundo. (Fuente: Banco Mundial, 2019, Fundación Wikimedia 2019, Wikipedia 2018, Github 2020, Tor 2019)"
    PDFlink="/media/pdf/STIL-Internet-regions.pdf"
>}}

¿Y qué supone esta desigualdad de acceso para las lenguas? ¿Podemos todas las personas acceder al internet en nuestras propias lenguas? ¿Podemos crear contenido e información en nuestras propias lenguas?

Tal y como reflejan [otras estadísticas](https://www.internetworldstats.com/stats7.htm), más del 75% de las personas que acceden al internet lo hacen en tan solo 10 idiomas —la mayoría tienen una historia colonial europea (inglés, francés, alemán, portugués, castellano…) o son lenguas dominantes en regiones específicas en las que otras lenguas minoritarias luchan por sobrevivir (chino, árabe, ruso…). En 2020 se estimó que [el 25,9% de todas las personas usuarias de internet accedían a la red en inglés](https://www.statista.com/statistics/262946/share-of-the-most-common-languages-on-the-internet/), mientras que el 19,4% accedía al internet en chino. China es el país con más personas usuarias de internet de todo el mundo, y es preciso recordar que cuando decimos “chino” no nos referimos a un único idioma, sino a [una familia de lenguas distintas entre sí](https://www.oxfordhandbooks.com/view/10.1093/oxfordhb/9780199856336.001.0001/oxfordhb-9780199856336-e-1), con el chino mandarín siendo el más dominante.

Resulta revelador que las lenguas del internet hoy en día sean mayormente de origen europeo, ya que Europa es el continente con menor número de lenguas del mundo. De las más de 7000 lenguas que existen en el mundo, [más de 4000 son de Asia y África](https://www.ethnologue.com/guides/how-many-languages#population) (con más de 2000 lenguas respectivamente), mientras que las Islas del Pacífico y las Américas albergan más de 1000 lenguas cada una. Papúa Nueva Guinea e Indonesia son los [países con más lenguas](https://www.ethnologue.com/guides/countries-most-languages), con más de 800 en Papúa Nueva Guinea y 700 en Indonesia.

{{< summary-side-fig-fancybox
    src_small="/media/summary/living_languages_vs_population__pies_500px.webp"
    src_full="/media/summary/living_languages_vs_population__pies_1000px.webp"
    alt="Número de lenguas y población total de hablantes por región, a nivel mundial. Fuente:"
    caption="Número de lenguas y población total de hablantes por región, a nivel mundial. Fuente:"
    source="Ethnologue"
    link="https://www.ethnologue.com/guides/continents-most-indigenous-languages"
    PDFlink="/media/pdf/living_languages_vs_population__pies.pdf"
>}}

Muchas lenguas de Asia meridional (hindi, bengalí, urdu…) están entre [las 10 lenguas más habladas del mundo](https://www.ethnologue.com/guides/ethnologue200) según el número de hablantes que las tienen como lengua materna (o nativa), pero estas no son las lenguas en las que la gente del Asia meridional puede acceder al internet. Y por supuesto, tal y como veremos en [la historia de Ishan]({{<trurl>}}/stories/marginality-within-marginality{{</trurl>}}), cuya lengua materna es el bengalí, incluso cuando podemos acceder a información digital en nuestro idioma preferido, es posible que el tipo de información que buscamos no exista en esa lengua. En el caso de Ishan, se trataba de contenido sobre discapacidad y derechos sexuales. La situación en el Sudeste Asiático, una de las zonas con mayor uso de internet del mundo y mayor diversidad de lenguas, es parecida. Mientras buscaba contenido sobre derechos sexuales en indonesio, [Paska]({{<trurl>}}/stories/flickering-hope{{</trurl>}}) se encontró con las mismas dificultades que Ishan tuvo con el bengalí.

También sabemos que, de entre las más de 7000 lenguas que existen en el mundo, [unas 4000](https://www.ethnologue.com/enterprise-faq/how-many-languages-world-are-unwritten-0) tienen sistemas de escritura. Sin embargo, la mayoría de estas escrituras no fueron desarrolladas por los propios hablantes de las lenguas, sino como parte de los numerosos procesos de colonización en todo el mundo. El simple hecho de tener una escritura no significa que se entienda o se utilice ampliamente. La mayoría de las lenguas del mundo se transmiten de forma oral o por señas, y no a través de la escritura. Creemos que al menos la mitad de las lenguas del mundo se transmiten principalmente por tradición oral, sin forma escrita. Y aquellas que sí tienen forma escrita suelen publicar sus textos en lenguas coloniales europeas, y en mucho menor grado, en las lenguas que predominaban en la región. En 2010, Google calculó que había aproximadamente [unos 130 millones de libros publicados](https://www.pcworld.com/article/508405/google_129_million_different_books_have_been_published.html), y una gran proporción de ellos estaban en tan sólo 480 lenguas. Las publicaciones académicas más consolidadas sobre [ciencia](https://www.theatlantic.com/science/archive/2015/08/english-universal-language-science-research/400919/) o [ciencias sociales](https://www.tandfonline.com/doi/abs/10.1080/01639269.2014.904693) están en inglés. [El libro más veces traducido en todo el mundo](https://en.wikipedia.org/wiki/Bible_translations) es la Biblia (a más de 3000 lenguas), y el [documento más traducido](https://www.ohchr.org/EN/UDHR/Pages/Introduction.aspx) en todo el mundo es [la Declaración Universal de Derechos Humanos de las Naciones Unidas](https://en.wikipedia.org/wiki/Universal_Declaration_of_Human_Rights) (a más de 500 lenguas).

¿Por qué es importante esto? Porque las tecnologías digitales del lenguaje se basan en el procesamiento automatizado de materiales publicados en diferentes lenguas para mejorar la calidad de su soporte y contenido. Por eso mismo, cuando la publicación de textos a escala mundial favorece a ciertas lenguas (mientras excluye a aquellas que no tienen forma escrita), se acentúan las injusticias lingüísticas que vivimos. Y por supuesto, las lenguas que no tienen un sistema de escritura (las que están basadas en signos, sonidos, gestos, movimiento) quedan completamente excluidas de la industria de la publicación, y por lo tanto, también de las tecnologías del lenguaje.

Por ejemplo, tal y como nos cuenta [Ana]({{<trurl>}}/stories/dill-wlhall-on-the-web{{</trurl>}}), “la web no está diseñada para adaptarse a personas usuarias de lenguas de tradición oral”. En el predominio de lenguas escritas en la red, es difícil encontrar contenido de tradiciones orales o visuales. Por ejemplo, no es fácil buscar gestos, signos o silbidos. En nuestra entrevista con [Joel y Caddie]({{<trurl>}}/stories/signs-across-generations{{</trurl>}}), nos hablan del primer paquete de emojis indígenas en Australia, realizados en territorio arrente, en Mparntwe/Alice Springs, y cómo en arrente los gestos físicos a menudo se combinan con la palabra hablada para construir el significado. [Emna]({{<trurl>}}/stories/write-in-another-language{{</trurl>}}) nos cuenta lo mismo sobre Túnez y las diversas lenguas que hablan sus gentes: “cuando se trata de conservar una lengua, no deberíamos preocuparnos solamente de la escritura, también es necesario preservar sus formas orales, sus gestos, signos, silbidos, etc, y ninguna de estas cosas se pueden expresar plenamente por escrito”.

Las tecnologías digitales poseen un gran potencial para representar la pluralidad de lenguas basadas en texto, sonido, gestos y mucho más. También pueden ayudarnos a conservar y reavivar lenguas que están en peligro de extinción: [más del 40 % del total de lenguas](https://www.endangeredlanguages.com/). Cada mes, [dos lenguas indígenas](https://news.un.org/en/story/2019/12/1053711) y el conocimiento que entrañan mueren y se pierden para siempre.

¿Por qué no está la pluralidad de lenguas y sus contextos mejor representada en la red?

En su ensayo, [Claudia]({{<trurl>}}/stories/decolonizing-minority-language{{</trurl>}}) nos expone tres dimensiones para entender la relación entre las lenguas y la tecnología: disponibilidad, usabilidad, y cómo se desarrolla la tecnología. Como iremos descubriendo a lo largo de este informe, lo que Claudia llama “lenguas mayoritarias” (y que resultan ser sobre todo lenguas coloniales europeas o lenguas que predominan en una región) disponen de toda una serie de medios, servicios, interfaces y aplicaciones, mientras que otras lenguas tienen mucha menos disponibilidad, especialmente en términos de infraestructura como teclados, traducción automatizada o reconocimiento de voz. Además, las empresas tecnológicas emplean más tiempo y recursos en la usabilidad de estas lenguas mayoritarias, ya que de ellas reciben más beneficios. Por último, nos cuenta que la mayor parte de las tecnologías del lenguaje se desarrollan o bien a través de procesos verticales con muy poca colaboración con las comunidades de hablantes, o bien con pocos y mal coordinados esfuerzos por trabajar con estas comunidades.

Estos problemas y obstáculos contextuales también nos indican el camino hacia el que avanzar en el proceso de articulación de un internet más multilingüe, y regresaremos a estas posibilidades [más adelante](#toc_4_H2).

### Soporte lingüístico: plataformas y aplicaciones de mensajería

{{< summary-quote
    text="Cuando escribes la palabra “buenos días”, antes de que termines de teclear, el celular o la computadora ya te habrán sugerido la palabra. Cuando escribo la misma palabra en chindali (”mwalamusha”), tengo que teclearla entera, y esto lleva más tiempo; además, aparecerá subrayada porque la computadora o el celular no reconoce la palabra."
    author="Donald Flywell Malanga"
    link="/es/stories/challenges-of-expressing-chindali/"
>}}

{{< summary-quote
    text="Los teclados en cingalés o tamil son muy difíciles de encontrar. Nuestros padres imprimían pequeñas letras cingalesas, las recortaban, y las pegaban en las teclas junto a los caracteres ya existentes en inglés. Aunque se han desarrollado muchas fuentes tipográficas cingalesas, ninguna funciona tan bien como las fuentes Unicode."
    author="Uda Deshapriya"
    link="/es/stories/amidst-virtual-impunity/"
>}}

{{< summary-quote
    text="Si las aplicaciones más populares y las interfaces de software más usadas no están disponibles en bretón pronto, las generaciones más jóvenes empezarán a perder interés por esta lengua, que no podrá competir con las aplicaciones en francés."
    author="Claudia Soria"
    link="/es/stories/decolonizing-minority-language/"
>}}

Hemos intentado entender mejor cómo es posible que el internet todavía no sea tan multilingüe como el mundo en el que vivimos. Para ello, hemos analizado el tipo de soporte lingüístico (por ejemplo, interfaces de usuario en diferentes lenguas) que nos ofrecen las mayores plataformas digitales y aplicaciones para comunicarnos, crear y compartir contenido en nuestras lenguas.

{{< summary-gallery-fancybox
    caption_all="Interfaz de Wikipedia en diferentes lenguas."

    src_small1="/media/summary/Bengali-Wikipedia-500px.webp"
    src_full1="/media/summary/Bengali-Wikipedia-1000px.webp"
    caption1="Interfaz de Wikipedia en bengalí."

    src_small2="/media/summary/Breton-Wikipedia-500px.webp"
    src_full2="/media/summary/Breton-Wikipedia-1000px.webp"
    caption2="Interfaz de Wikipedia en bretón."

    src_small3="/media/summary/English-Wikipedia-500px.webp"
    src_full3="/media/summary/English-Wikipedia-1000px.webp"
    caption3="Interfaz de Wikipedia en inglés."

    src_small4="/media/summary/Spanish-Wikipedia-500px.webp"
    src_full4="/media/summary/Spanish-Wikipedia-1000px.webp"
    caption4="Interfaz de Wikipedia en castellano."

    src_small5="/media/summary/Zapotec-Wikipedia-500px.webp"
    src_full5="/media/summary/Zapotec-Wikipedia-1000px.webp"
    caption5="Interfaz de Wikipedia en zapoteco."
>}}

[Martin y Mark]({{<trurl>}}/numbers/a-platform-survey{{</trurl>}}) analizaron el soporte lingüístico de 11 sitios web, 12 aplicaciones de Android y 16 aplicaciones de iOS. Seleccionaron plataformas muy usadas y especializadas en recopilar y compartir información, en particular aquellas que buscan tener una presencia y audiencia a escala global, basándose en los datos de acceso público de estas plataformas y apps.

Las plataformas fueron agrupadas en cuatro grandes categorías (que se superponen entre sí):

* **Acceso a la información** (plataformas de información y conocimiento, incluyendo buscadores): Google Maps, Google Search, Wikipedia, YouTube.
* **Aprendizaje de idiomas** (plataformas de aprendizaje autodidacta de idiomas): DuoLingo, y las plataformas educativas Coursera, Udacity y Udemy.
* **Redes sociales** (plataformas de redes sociales de contenido público): Facebook, Instagram, Snapchat, TikTok, Twitter.
* **Mensajería** (mensajería privada y en grupo): imo, KakaoTalk, LINE, LINE Lite, Messenger, QQ, Signal, Skype, Telegram, Viber, WeChat, WhatsApp, Zoom.

{{< summary-side-fig-fancybox
    src_small="/media/data-survey/PS_Figure_1-platform_summary-500px.webp"
    src_full="/media/data-survey/PS_Figure_1-platform_summary-1000px.webp"
    alt="Número de idiomas admitidos en la interfaz para cada plataforma, por tipo de plataforma."
    caption="Número de idiomas admitidos en la interfaz para cada plataforma, por tipo de plataforma."
    PDFlink="/media/pdf/PS_Figure_1-platform_summary.pdf"
>}}

Descubrimos que el soporte lingüístico basado en texto está distribuido de manera muy desigual a lo largo de las distintas plataformas digitales. Grandes plataformas web como Wikipedia, Google Search y Facebook ofrecen el soporte lingüístico más completo. Curiosamente, Wikipedia (que es sin ánimo de lucro y editada por voluntarios de todo el mundo) es la plataforma más ampliamente traducida al día de hoy. Wikipedia ofrece una interfaz básica en más de 400 lenguas, y las Wikipedias de unas 300 lenguas tienen al menos 100 artículos. Google Search soporta 150 lenguas, mientras que Facebook admite entre 70 y 100 lenguas. Signal encabeza la lista de aplicaciones de mensajería en este aspecto, incluyendo casi 70 lenguas en Android y 50 en iOS. Por otro lado, la mayor parte de las plataformas solo enfocan su soporte lingüístico en un pequeño número de lenguas de uso extendido, dejando sin soporte a la mayoría de lenguas. La aplicación de mensajería QQ, por ejemplo, solo está en chino.

Dentro de esa pequeña variedad de lenguas que están incluidas en casi todas las plataformas analizadas, encontramos lenguas europeas como el inglés, el castellano, el portugués y el francés, así como ciertas lenguas asiáticas tales como el chino mandarín, el indonesio, el japonés y el coreano. Otras lenguas mayoritarias como el árabe y el malayo no cuentan con un soporte tan bueno, y lenguas habladas por decenas y cientos de millones de hablantes no están bien representadas en términos de interfaces soportadas.

¿Qué significa esta ausencia de soporte lingüístico para la mayoría de las lenguas del mundo? Para 2021 calculamos una cantidad de [7,9 mil millones de seres humanos](https://www.worldometers.info/) sobre la tierra, cuya mayoría vive en Asia (casi 4,7 mil millones) y África (casi 1,4 mil millones). Y aún así, las lenguas de internet están al servicio de tan solo una pequeña minoría de personas en el mundo.

* _Hablantes de lenguas africanas_: la inmensa mayoría de las lenguas africanas no están incluidas como lenguaje de interfaz en ninguna de las plataformas analizadas, y por lo tanto, más del 90% de las personas africanas necesitan cambiar a una segunda lengua o a una lengua predominante en su región.
* _Hablantes de lenguas de Asia meridional_: En Asia meridional, la interfaz de casi la mitad de las plataformas analizadas carecen de soporte en lenguas regionales. Incluso lenguas mayoritarias de Asia meridional como el hindi o el bengalí, habladas por millones de personas, están menos representadas que otras lenguas.
* _Hablantes de lenguas del Sudeste Asiático_: El soporte para lenguas del Sudeste Asiático es igualmente contradictorio. Mientras que el indonesio, el vietnamita y el tailandés tienden a estar muy bien representados en las plataformas analizadas, la mayoría de las lenguas del Sudeste Asiático no están incluidas.

A los resultados del estudio de Martin y Mark se suman las realidades cotidianas de quienes viven en estas regiones del mundo. Por ejemplo, [Donald]({{<trurl>}}/stories/challenges-of-expressing-chindali{{</trurl>}}) de Malawi descubrió que cuando preguntaba a hablantes de chindali — una lengua bantu en peligro de extinción —, cómo se comunicaban a través de sus celulares, todos mencionaron que en chindali la tarea les llevaba mucho tiempo y era muy laboriosa, ya que la mayoría de sus teléfonos tenían incorporado soporte lingüístico en inglés, francés o árabe, y no reconocían el chindali. Estos obstáculos tecnológicos, por supuesto, vienen acompañados de restricciones económicas y sociales que dificultan que los hablantes de chindali se compren un smartphone o adquieran un servicio de datos. Incluso para aquellos que usan lenguas nacionales como el malawi o chichewa, la ausencia de soporte lingüístico hace las cosas muy difíciles: “¿Por qué he de comprar un celular caro o malgastar tiempo tratando de acceder al internet cuando todo está en inglés, que es una lengua que no comprendo?”.

De hecho, la falta de soporte linguístico para la mayoría de las lenguas africanas se hizo muy notable en 2018, cuando [Twitter reconoció por primera vez el swahili](https://www.africanews.com/2018/05/09/swahili-makes-history-as-first-african-language-recognized-by-twitter//), una lengua hablada por más de 50-150 millones de personas a lo largo de África oriental y más allá (ya sea como primer o segunda lengua). Hasta entonces, el swahili y la mayoría de las otras lenguas africanas, eran redirigidas al indonesio en la plataforma. Sin embargo, no fue la compañía la que inició el reconocimiento de las palabras o el soporte de traducción en swahili, sino que todo se debe al resultado de una campaña de usuarios y usuarias hablantes de swahili en Twitter.

La situación no es mucho mejor en Latinoamérica. En la entrevista con el [proyecto Kimeltuwe](({{<trurl>}}/stories/use-of-our-ancestral-language{{</trurl>}})), que trabajan con el mapudungún, hablado hoy en día por el pueblo mapuche a lo largo de todo Chile y Argentina, puntualizaron que “sería maravilloso poder escribir una publicación en mapuche en plataformas como YouTube o Facebook. Ni siquiera hablamos de traducción de interfaz, sino simplemente de poder seleccionar el mapuche como lenguaje en el menú existente. Por ejemplo, cuando subimos un vídeo a YouTube o Facebook, no puedes añadir una transcripción en mapuche porque no aparece como opción en la lista de lenguas predeterminadas. Así que si quieres subir una transcripción en mapuche, tienes que seleccionar el castellano o el inglés”.

Martin y Mark no analizaron el soporte lingüístico en dispositivos específicos, tales como celulares, pero sabemos que los teclados digitales son uno de los espacios clave en los que lingüistas y desarrolladores han realizado mayores progresos. Por ejemplo, Gboard, la aplicación de teclado virtual de Google para el sistema operativo Android, admite [más de 900 variedades linguísticas](https://arxiv.org/pdf/1912.01218.pdf) y es el resultado de un trabajo considerable con diferentes comunidades lingüísticas y académicas. Es posible acceder a un teclado de teléfono inteligente con estas características, pero solo si te puedes permitir un smartphone de alta gama.

Mientras tanto, [la experiencia de Uda con el cingalés]({{<trurl>}}/stories/amidst-virtual-impunity{{</trurl>}}), (una lengua hablada por más de 20 millones de personas en Sri Lanka como primer o segunda lengua) nos muestra que sigue siendo muy difícil crear contenido en una lengua si los desarrolladores y desarrolladoras que trabajan en el soporte lingüístico no entienden su sistema de escritura, especialmente cuando la forma escrita de esta lengua no se parece en nada al alfabeto del latín o de las lenguas de Europa occidental. Uda nos cuenta “el principal problema con el cingalés Unicode tiene que ver con el orden en el que los diferentes caracteres se insertan para formar una letra. Este orden requiere que se coloquen signos diacríticos detrás de las consonantes. Esta lógica sigue las reglas de las lenguas europeas basadas en el alfabeto latino. Sin embargo, en cingalés, a veces los signos diacríticos preceden a la consonante”.

[El Unicode](https://home.unicode.org/) es la tecnología estándar para codificar un texto expresado en el sistema de escritura de una lengua. La versión 13 dispone de [143 859 caracteres](https://www.unicode.org/faq/basic_q.html) para más de 20 sistemas de escritura que están en uso en la actualidad, ya que el mismo sistema de escritura puede compartirse entre varias lenguas (por ejemplo, el alfabeto latino en la mayoría de las lenguas de Europa occidental, el han para el japonés, chino y coreano y el devanagari para una serie de lenguas del Asia meridional). También incluye caracteres de sistemas de escritura antiguos que están en desuso. El Consorcio Unicode (una organización sin ánimo de lucro con sede en California) también decide sobre los [emojis](https://home.unicode.org/emoji/about-emoji/), los símbolos que usamos a diario a través de diferentes interfaces.

[El estudio de Martin y Mark]({{<trurl>}}/numbers/a-platform-survey{{</trurl>}}) sobre soporte lingüístico y las experiencias de otras personas [colaboradoras]({{<trurl>}}/stories{{</trurl>}}) de diferentes partes del mundo, enriquecen esta breve descripción de lo limitado y desigual que es el soporte técnico para un gran número de lenguas en plataformas y aplicaciones hoy en día. ¡No dejes de leerlo!


### Contenido lingüístico: accesibilidad y producción

{{< summary-quote
    text="Los contenidos feministas son especialmente poco accesibles en las lenguas locales. La Fundación para el Desarrollo de las Mujeres (WDF por sus siglas en inglés) es un grupo de mujeres rurales que lleva trabajando en cuestiones relacionadas con los derechos de las mujeres desde 1983, pero no fue hasta el año 2019 cuando empezamos a compartir contenidos feministas en línea sobre cuestiones sociopolíticas y económicas en cingalés."
    author="Uda Deshapriya"
    link="/es/stories/amidst-virtual-impunity/"
>}}

{{< summary-quote
    text="Por desgracia, encontrar en internet contenidos educativos y positivos sobre cultura queer en bahasa indonesio era (y sigue siendo) una tarea complicada... si buscamos “LGBT” o “homoseksualitas” (homosexualidad) en Google, el buscador más grande y conocido, encontraremos muchísimos resultados con las palabras “penyimpangan” (desviación), “dosa” (pecado) y “penyakit” (enfermedad)."
    author="Paska Darmawan"
    link="/es/stories/flickering-hope/"
>}}

{{< summary-quote
    text="La información sobre la intersección entre la homosexualidad y la discapacidad (o incluso la falta de información) que se encuentra disponible en internet en lengua bengalí está determinada en gran medida por la homofobia y el capacitismo, a los que a su vez contribuye."
    author="Ishan Chakraborty"
    link="/es/stories/marginality-within-marginality/"
>}}

Nuestro objetivo era comprender los contenidos de internet analizando a qué versión del mundo, y a quién, pertenece el conocimiento al que accedemos cuando buscamos información en línea. A fin de cuentas, [más del 63% de las páginas web](https://w3techs.com/technologies/overview/content_language) emplean la lengua inglesa como principal idioma de sus contenidos.

A través de sus ensayos y entrevistas, las personas que colaboran con nosotras analizan las múltiples constelaciones de desafíos históricos, sociopolíticos, económicos y tecnológicos que se plantean a la hora de acceder a internet de manera eficaz en sus lenguas. Y, lo que es más importante, todas estas personas abordan dificultades para encontrar contenidos relevantes en línea y crear contenido que les es relevante en sus propios idiomas. En otras palabras: no basta con poder acceder a la información y los conocimientos creados en otras lenguas por aquellas personas que quizás ignoran nuestros contextos y experiencias o, peor aún, por personas que nos odian. Debemos ser capaces de producir conocimiento relevante para nosotres mismes y nuestras comunidades, o por lo menos ser capaces de apoyar la producción y difusión de estos contenidos en todas nuestras diversas lenguas.

Esto es especialmente importante en el caso de quienes tienen problemas de acceso y quienes sufren formas entrecruzadas de marginación y exclusión.

Tal y como [Joel]({{<trurl>}}/stories/signs-across-generations{{</trurl>}}) nos relató en su entrevista sobre el proyecto Indigemoji, todo empezó con un tuit lleno de frustración que escribió un día desde su coche. Detenido a un lado de la carretera, empezó a escribir el significado de cada emoji en arrernte. Tras la aparición inicial de los emojis en internet, las Primeras Naciones y los pueblos Indígenas estuvieron por décadas solicitando, sin éxito, poder usarlas para expresar sus lenguas orales y visuales, como el arrernte. Como hemos mencionado anteriormente, el Consorcio Unicode decide sobre las peticiones públicas para la incorporación de nuevos emojis, y solicitudes como la de incluir un emoji para la bandera del pueblo aborigen de Australia fueron [rechazadas](https://unicode.org/emoji/emoji-requests.html). Para Joel, Caddie y otras personas, el proyecto Indigemoji se convirtió en un esfuerzo multigeneracional para sobrepasar estas formas distintas de marginación física y virtual, y crear sus propios contenidos de manera que sean relevantes para sus identidades y lenguas indígenas.


{{< summary-side-fig
src="/media/summary/Indigemoji-tweet.webp"
alt="Un tuit con una lista de emojis y palabras en arrernte. Fuente:"
caption="Un tuit con una lista de emojis y palabras en arrernte. Fuente:"
source="Indigemoji."
link="www.indigemoji.com.au"
>}}

Cabe recordar que las lenguas indígenas son lenguas “minoritarias” en nuestro mundo actual como consecuencia del genocidio masivo provocado por la colonización, que destruyó las Naciones Indígenas o las convirtió en poblaciones minoritarias después de ser los habitantes principales de una región o zona específica. Y estos procesos de colonización también repercuten en las lenguas dominantes que hablan millones de personas en todo el mundo.

[Ishan]({{<trurl>}}/stories/marginality-within-marginality{{</trurl>}}) es un académico queer con discapacidad visual a quien el mundo virtual le supone un verdadero esfuerzo. Tiene dificultades a la hora de encontrar información relevante en bengalí sobre discapacidad, identidades queer y, todavía más, sobre la interrelación entre ambos temas. Esto desemboca en lo que él denomina “marginación dentro de la marginación”: “por un lado, las actitudes homófobas y capacitistas de la sociedad y, por el otro, la homofobia y/o el capacitismo interiorizado de las personas a nivel individual (queer y/o discapacitadas). En conjunto, estas condiciones se complementan unas a otras y perpetúan el mecanismo de marginación. La situación social de una persona queer y discapacitada puede definirse como “marginación dentro de la marginación”.

En otras palabras, los procesos básicos de acceso e información brillan por su ausencia en internet, incluso en una lengua dominante como es el bengalí, con cerca de 300 millones de hablantes en todo el mundo.

Martin y Mark decidieron profundizar y analizar la magnitud y tipo de contenidos en diferentes lenguas en dos plataformas de información y conocimiento distintas: Google Maps y Wikipedia.

#### Google Maps

¿Podemos acceder a Google Maps en todas nuestras lenguas? ¿El idioma que utilizamos cambia la versión del mundo que vemos a través de Google Maps?

Para responder a estas preguntas, Martin y Mark recabaron datos sobre la cobertura global de contenidos de [Google Maps]({{<trurl>}}/numbers/google-maps-language-geography{{</trurl>}}) en las 10 lenguas más habladas del mundo: inglés, chino mandarín, hindi, castellano, francés, árabe, bengalí, ruso, portugués e indonesio. Recopilaron decenas de millones de resultados de búsquedas individuales en estos idiomas y con ellos identificaron y catalogaron cerca de 3 millones de lugares concretos (locales y otras ubicaciones).

Como era de esperar, los mapas tienen más contenido cuando accedemos a Google Maps en inglés. El mapa de Google en inglés abarca todo el mundo, aunque el Norte global es mucho más denso (es decir, tiene más información), sobre todo en Europa y América septentrional. También comprende el sur de Asia y partes del Sudeste asiático, así como parte importante de Latinoamérica. Sin embargo, el contenido de muchas áreas de África es escaso en comparación.

{{< summary-side-fig-fancybox
src_small="/media/data-survey/GM_Figure_1-English-500px.webp"
src_full="/media/data-survey/GM_Figure_1-English-1000px.webp"
alt="La densidad de información de Google Maps para los angloparlantes. Las zonas más oscuras indican que los resultados de las búsquedas incluyen un número de lugares mayor."
caption="La densidad de información de Google Maps para los angloparlantes. Las zonas más oscuras indican que los resultados de las búsquedas incluyen un número de lugares mayor."
PDFlink="/media/pdf/GM_Figure_1-English.pdf"
>}}

A diferencia del mapa en inglés, que es relativamente completo, el bengalí (la primera lengua de [Ishan]({{<trurl>}}/stories/the-unseen-story{{</trurl>}})) se sitúa al otro lado del espectro: su cobertura se limita en su mayor parte al sur de Asia, especialmente a la India y Bangladesh, y Google Maps tiene muy pocos o nulos contenidos para los hablantes de bengali en el resto el mundo. Para que los hablantes de bengalí puedan descubrir contenidos adicionales y navegar en lugares más allá de India y Bangladesh, se ven obligados a cambiar a una segunda lengua como el inglés. Lo mismo sucede con Google Maps en hindi, la tercera lengua más hablada del mundo después del inglés y el chino mandarín.

{{< summary-side-fig-fancybox
src_small="/media/data-survey/GM_Figure_2-Bengali-500px.webp"
src_full="/media/data-survey/GM_Figure_2-Bengali-1000px.webp"
alt="La densidad de información de Google Maps para los hablantes de bengalí. Las zonas más oscuras indican que los resultados de las búsquedas incluyen un número de lugares mayor."
caption="La densidad de información de Google Maps para los hablantes de bengalí. Las zonas más oscuras indican que los resultados de las búsquedas incluyen un número de lugares mayor."
PDFlink="/media/pdf/GM_Figure_2-Bengali.pdf"
>}}

Para leer mucho más sobre Google Maps en diferentes idiomas, lee el [informe detallado de Martin y Mark]({{<trurl>}}/numbers/google-maps-language-geography{{</trurl>}}).

#### Wikipedia

Tal y como demuestra el [análisis de plataformas]({{<trurl>}}/numbers/a-platform-survey{{</trurl>}}) de Martin y Mark, Wikipedia se sitúa a la vanguardia del soporte lingüístico en internet. Su interfaz de usuario se ha traducido a más idiomas que cualquiera de las plataformas comerciales que hemos consultado, incluidas Google y Facebook.

En cuanto al contenido en sí (la información y conocimiento recogido en los artículos de la enciclopedia), Wikipedia cuenta con más de 300 versiones en distintos idiomas. Sin embargo, los hablantes de estas lenguas no tienen acceso al mismo contenido ni a la misma cantidad de información. Queríamos ahondar en esta cuestión y poder responder a las siguientes preguntas: ¿Cuál es el nivel de la cobertura de contenidos de Wikipedia en sus distintas versiones lingüísticas? ¿Hay algunos idiomas mejor representados que otros? ¿Hay algunas comunidades en particular que tengan acceso a más contenido que otras? En el [análisis de Wikipedia de Martin y Mark]({{<trurl>}}/numbers/wikipedia-language-geography{{</trurl>}}) se responden a algunas de estas preguntas de forma detallada.

Utilizamos la información de 2018 con etiquetas geográficas (una forma de incorporar referencias geográficas como coordenadas en los artículos de Wikipedia) y analizamos el número de artículos y el crecimiento del contenido en distintos idiomas. También basamos nuestro análisis en las lenguas “locales”, que son las lenguas que están clasificadas como lenguas oficiales en [Unicode CLDR](https://cldr.unicode.org/) (el código que soporta los idiomas en internet) o bien las lenguas utilizadas por al menos el 30% de la población de cualquier país.

Después identificamos la lengua local predominante de cada país, es decir, la lengua hablada por un mayor número de personas, y encontramos 73 lenguas, que son las más prevalentes en al menos un país. El inglés es la lengua más hablada y es la más extendida en 34 países. Le siguen el árabe y el castellano (18 países), el francés (13 países), el portugués (7 países), el alemán (4 países) y el neerlandés (3 países). El chino, el italiano, el malayo, el rumano, el griego y el ruso son las lenguas con mayor presencia en dos países, y las 60 lenguas restantes son predominantes en un único país.

El objetivo era comparar esta distribución lingüística local con el contenido de Wikipedia en cada país, así que relacoinamos la versión lingüística de Wikipedia con el mayor número de artículos de ese país. Descubrimos que existe un sesgo hacia el contenido en lengua inglesa. El inglés es el idioma predominante de Wikipedia en 98 países, seguido por el francés (9 países), el alemán (8 países), el castellano (7 países), el catalán y el ruso (4 países), el italiano y el serbio (3 países) y el neerlandés, el griego, el árabe, el serbocroata y el rumano (2 países). Las 21 lenguas restantes de Wikipedia son predominantes en un único país.

Si bien el [número de artículos en cada idioma en Wikipedia](https://en.wikipedia.org/wiki/List_of_Wikipedias) es dinámico y sigue aumentando, es evidente que las ediciones en los distintos idiomas en Wikipedia son muy variables en cuanto a tamaño y escala, tanto por el número de artículos como por el tamaño de sus comunidades de editores. La Wikipedia inglesa es claramente la más grande, y cuenta con más de 6 millones de artículos y casi 40 millones de colaboradores registrados. Las siguientes comunidades de Wikipedia por mayor número de colaboradores son la castellana, la alemana y la francesa; y todas ellas cuentan con entre 4 y 6 millones de colaboradores y alrededor de 2 millones de artículos. El resto de versiones lingüísticas son pequeñas en comparación: tan solo 20 versiones tienen más de un millón de artículos y solo 70 tienen más de 100 000 artículos. La mayoría de las versiones lingüísticas de Wikipedia tienen una pequeña parte del contenido de la Wikipedia en inglés.

{{< summary-side-fig-fancybox
src_small="/media/data-survey/WP_Figure_2-English-500px.webp"
src_full="/media/data-survey/WP_Figure_2-English-1000px.webp"
alt="La densidad de información de la Wikipedia en inglés a principios de 2018. Las zonas más oscuras indican un número mayor de artículos con geoetiquetas."
caption="La densidad de información de la Wikipedia en inglés a principios de 2018. Las zonas más oscuras indican un número mayor de artículos con geoetiquetas."
PDFlink="/media/pdf/WP_Figure_2-English.pdf"
>}}

{{< summary-side-fig-fancybox
src_small="/media/data-survey/WP_Figure_3-ar,_bn,_hi,_es-500px.webp"
src_full="/media/data-survey/WP_Figure_3-ar,_bn,_hi,_es-1500px.webp"
alt="La densidad de información de las Wikipedias en árabe, bengalí, hindi y castellano a principios de 2018. Las zonas más oscuras indican un número mayor de artículos con geoetiquetas."
caption="La densidad de información de las Wikipedias en árabe, bengalí, hindi y castellano a principios de 2018. Las zonas más oscuras indican un número mayor de artículos con geoetiquetas."
PDFlink="/media/pdf/WP_Figure_3-ar,_bn,_hi,_es.pdf"
>}}

Es fascinante ver cómo el contenido de las distintas Wikipedias lingüísticas sigue el mismo patrón de distribución que vimos anteriormente con Google Maps.

Lo mismo ocurre cuando revisamos el número de artículos en las Wikipedias de diferentes idiomas en comparación con el número de hablantes de esas lenguas (tanto primera como segunda lengua). El número de artículos de las Wikipedias en idiomas europeos como el inglés, el francés, el castellano, el ruso y el portugués es proporcional al número de hablantes. Pero no es así en otras lenguas mayoritarias como el chino mandarín, el hindi, el árabe, el bengalí y el indonesio, unas lenguas que a pesar de contar con cientos de millones de hablantes, cuentan con versiones de Wikipedia mucho más pequeñas y con menos artículos en comparación con las versiones en idiomas europeos. Hay más artículos en las versiones de Wikipedia en francés, castellano o portugués que en sus homólogas en mandarín, hindi o árabe, a pesar de que estas tres se encuentren entre las [cinco lenguas más habladas](https://en.wikipedia.org/wiki/List_of_languages_by_total_number_of_speakers) del mundo, con más hablantes que el francés o el portugués.

{{< summary-side-fig-fancybox
src_small="/media/data-survey/WP_Figure_1-language_ranking-500px.webp"
src_full="/media/data-survey/WP_Figure_1-language_ranking-1000px.webp"
alt="Contenidos de Wikipedia y número de hablantes de las 10 lenguas más habladas del mundo. (Estimación de la población: Ethnologue 2019, que incluye los hablantes de segundas lenguas.)"
caption="Wikipedia content and number of speakers for the 10 most widely spoken languages in the world. (Population estimate: Ethnologue 2019, which includes second-language speakers.)"
PDFlink="/media/pdf/WP_Figure_1-language_ranking.pdf"
>}}

[El informe de Martin y Mark]({{<trurl>}}/numbers/wikipedia-language-geography{{</trurl>}}) recoge más análisis y visualizaciones de datos sobre Wikipedia en diferentes idiomas, pero lo que es evidente a partir de estas cifras, es que reflejan las experiencias vividas por quienes colaboran con nosotres en todo el mundo.

La marginación y la exclusión de las lenguas que no son idiomas esencialmente europeos y coloniales, son profundas en el mundo físico y virtual, incluso en el caso de lenguas globales y dominantes como el árabe. Para escribir una Wikipedia en el lenguaje propio y con referencias basadas en ese contexto lingüístico, es necesario contar con abundantes fuentes fiables publicadas, algo que es poco frecuente en la mayoría de los idiomas del mundo, como ya vimos anteriormente. Como wikipedista, [Emna]({{<trurl>}}/stories/write-in-another-language{{</trurl>}}) describe el desafío que supone encontrar recursos y referencias en distintos idiomas africanos: “Por ejemplo, la lucha a la que me enfrento como wikipedista para encontrar referencias en nuestras propias lenguas —y cuando digo nuestras lenguas no solo me refiero al tunecino, a nuestro dialecto, o a la lengua árabe, sino a la gran brecha que existe en términos de recursos y referencias en toda África”.

Incluso en Europa los hablantes de lenguas minoritarias tienen dificultades a la hora de acceder o editar Wikipedia en sus idiomas. Aunque [Claudia]({{<trurl>}}/stories/decolonizing-minority-language{{</trurl>}}) descubriera que muchas de las personas bretonas a las que preguntó sabían de la existencia de la Wikipedia en bretón y que “el 19% de ellas incluso colaboraban mediante la edición de artículos existentes o la creación de otros nuevos (8%)”. Claudia considera que la mayoría de los hablantes de lenguas minoritarias cambian a un idioma dominante porque les resulta más fácil: “La disponibilidad no implica que los servicios, interfaces, aplicaciones y Wikipedia se utilicen realmente. Algunos estudios demuestran que los hablantes de lenguas minoritarias cambian sin dudar a su lengua dominante cuando utilizan tecnologías digitales basadas en el lenguaje, ya sea porque la tecnología es intrínsecamente mejor o porque el abanico de prestaciones es mucho más amplio”.

Wikipedia y su constelación de proyectos de conocimiento libre y de [código abierto](https://en.wikipedia.org/wiki/Open_source) (en los que el acceso al código es libre y se construye de manera colectiva) conforman uno de los espacios más esperanzadores y cooperativos para el conocimiento multilingüe en línea. Por ejemplo, sus comunidades de personas voluntarias saben y entienden que no existe una única variante de inglés, árabe o chino y que plasmar esta pluralidad de contextos y contenidos lingüísticos no es siempre tarea fácil. Como hemos podido observar en nuestro análisis y experiencias propias, [Wikipedia también es víctima de unas estructuras históricas y prevalentes](https://wikipedia20.pubpub.org/pub/myb725ma/release/2) de poder y privilegio que distorsionan la forma en la que creamos y compartimos conocimientos en diferentes idiomas y dentro de las familias de lenguas.

¿Qué camino debemos seguir como personas, organizaciones y comunidades que aspiran a un internet más multilingüe? En las siguientes y últimas secciones nos valdremos de las conclusiones de las [cifras]({{<trurl>}}/numbers{{</trurl>}}) e [historias]({{<trurl>}}/stories{{</trurl>}}) que hemos compartido con ustedes hasta ahora, con el fin de ofrecer una visión general de lo que hemos descubierto y los contextos, acuerdos y acciones que pueden llevarnos hacia un internet verdaderamente multilingüe.

## ¿Qué hemos aprendido sobre el internet multilingüe?

Durante la elaboración de este informe hemos aprendido mucho sobre lenguas, internet y las lenguas de internet. Este es un breve resumen de las ideas más importantes de nuestro recorrido hasta ahora.

**Aprendizaje:** La lengua no es solo una herramienta de comunicación, es una representación del conocimiento y una forma esencial de existencia en el mundo. Esa es la razón por la que el multilingüismo es tan importante para poder honrar y reafirmar la complejidad y texturas de nuestros múltiples seres y mundos.

**Contexto:** Las personas conocen sus mundos y se expresan en más de 7000 lenguas distintas. Nuestras lenguas pueden ser orales (habladas y de señas), escritas o transmitidas a través de sonidos.

Y, sin embargo, el soporte lingüístico en las principales plataformas y aplicaciones tecnológicas abarca solamente una fracción de esas 7000 lenguas. Tan solo unos 500 de esos idiomas se encuentran representados en línea en cualquier forma de información o conocimiento. Algunas de las lenguas más habladas del mundo no tienen demasiado soporte lingüístico ni información en la red. La lengua inglesa cuenta con el apoyo lingüístico más completo y la información más abundante en internet (incluidos Google Maps y Wikipedia). La mayoría de páginas web están en inglés.

**Observación:** **Internet no es ni de lejos tan multilingüe como imaginamos o necesitamos que sea.**

**Análisis:** La mayoría de personas se ven obligadas a usar la lengua colonial europea más cercana (inglés, castellano, portugués, francés, etc.) o la lengua dominante a nivel local (chino mandarín, árabe, etc.) para acceder a internet. Las estructuras históricas prevalentes de poder y privilegio son intrínsecas a la manera en que los idiomas son accesibles (o no) en internet.

## ¿Cómo podemos mejorar? Contextos y acciones para conseguir un internet multilingüe

{{< summary-quote
    text="En la mayoría de los casos el uso de una lengua minoritaria requiere una gran cantidad de paciencia, voluntad y capacidad de adaptación, ya que que la experiencia del usuario al emplear lenguas minoritarias está repleta de deficiencias y complicaciones."
    author="Claudia Soria"
    link="/es/stories/decolonizing-minority-language/"
>}}

{{< summary-quote
    text="El objetivo de un internet multilingüe inclusivo y representativo para los pueblos indígenas debe considerar y reconocer los legados sociales y la experiencia actual de la opresión colonial. Un internet multilingüe no puede limitarse a aspirar a ser representativo, sino que, en vista de la historia colonial, también debe intentar promover entornos que mejoren la supervivencia y el aprendizaje de las lenguas indígenas por y para la población indígena."
    author="Jeffrey Ansloos and Ashley Caranto Morford"
    link="/es/stories/learning-and-reclaiming-indigenous-languages/"
>}}

{{< summary-quote
    text="Los niños, niñas y jóvenes mapuches crecen junto a la tecnología y el internet. La red es un espacio en el que estas personas pueden acercarse al mapudungun. Las historias de nuestro pueblo deben escribirse y tienen que escribirse o contarse en mapudungun. Nuestras historias no son siempre historias de grandes héroes, como las de los Estados coloniales y postcoloniales. La nuestra es la historia de cada mapuche que sobrevivió a la adversidad y la violencia. La mujer que tuvo que emigrar a la ciudad en busca de un salario, las mujeres y hombres que volvieron de la ciudad, pero que encontraron que no había más sitio en sus “levos” y tuvieron que volver a las ciudades, desarraigados y sin un lugar al que regresar. Estas experiencias y los recuerdos de cada persona mapuche constituyen nuestra memoria colectiva como pueblo."
    author="Kimeltuwe project"
    link="/es/stories/use-of-our-ancestral-language/"
>}}

En esta parte de nuestro resumen ejecutivo del Informe sobre el Estado de las Lenguas en Internet, reunimos las diferentes perspectivas de quienes han colaborado con nosotras para la elaboración de este documento, así como nuestro encuentro de 2019 titulado [Descolonizar las Lenguas de Internet](https://whoseknowledge.org/resource/dtil-reporte/?lang=es), con el objetivo de comprender los diferentes contextos, desafíos y oportunidades que rodean al multilingüismo en el mundo físico y virtual. Es posible que las siguientes cuatro preguntas clave iluminen nuestro camino y nos ayuden a imaginar y diseñar un internet mucho más multilingüe:

* **¿De quién es el poder y los recursos?**
* **¿De quién son los valores y los conocimientos?**
* **¿De quién son las tecnologías y los estándares?**
* **¿De quién son los diseños y la imaginación?**

### ¿De quién es el poder y los recursos?

#### Contexto

Tanto nuestro análisis estadístico como las experiencias vividas por las personas que contribuyeron sus historias, evidencian que la marginación de las lenguas en el mundo físico y virtual no se basa únicamente en el número de personas que las hablan.

Las lenguas indígenas son habladas por [más de 6000 Naciones Indígenas](https://www.cwis.org/2015/05/how-many-indigenous-people/) en todo el mundo, quienes fueron las primeras habitantes de gran parte del mundo hasta que la colonización y el genocidio destruyeron o redujeron sus poblaciones y lenguas. A pesar de ser usadas por millones de personas , las lenguas dominantes en algunos de los continentes con mayor diversidad lingüística (como Asia o África) están poco o nada representadas en línea. Si miramos a la diáspora de personas que hablan idiomas como las múltiples variantes de árabe, chino, hindi, bengalí, punjabi, tamil, urdu, indonesio, malayo, swahili, hausa, etc, en diferentes países y continentes, resulta evidente que estas lenguas son minorizadas en la red, aunque prevalezcan sobre otras en sus propias regiones.

Estas formas de marginación y exclusión digital no son fortuitas sino consecuencia de estructuras y procesos, históricos y actuales, de poder y privilegio. Esto también significa que los recursos destinados a la infraestructura lingüística (desde el sector editorial y entornos académicos hasta los gobiernos y empresas tecnológicas) se encuentran orientadas hacia ciertas regiones (Europa y América septentrional) y ciertas lenguas (el inglés y otras lenguas europeas occidentales). Las comunidades indígenas, negras y muchas otras comunidades marginadas tienen dificultades a la hora de transmitir sus lenguas de generación en generación, incluso en Europa y en Norteamérica.

Más concretamente, la colonización y el capitalismo provocan y se entrelazan con otros sistemas de discriminación y opresión tales como el racismo, el patriarcado, la homofobia, el capacitismo, el clasismo y el sistema de castas. Esto supone que ciertas lenguas (lenguas coloniales europeas, principalmente) sean más predominantes en internet, independientemente de que tengan un mayor número de hablantes a nivel mundial. Además, esto significa que cuando existe información y conocimientos en otros idiomas minorizados, el contenido en esas lenguas se ve limitado por quien tiene acceso y poder para crearlo, o para impedir que otras personas generen información alternativa. Por ejemplo, la ausencia de contenidos feministas en la red en cingalés, o de contenido positivo para las personas queer y discapacitadas en bengalí o indonesio.

Dado que la lengua es una parte esencial de nuestra idiosincrasia, el hecho de no poder expresarnos en nuestros propios idiomas y de forma plena según nuestras múltiples identidades es (ciertamente) una forma de violencia. Las consecuencias de estas marginaciones son violentas de otras maneras también. Como afirma [Uda]({{<trurl>}}/stories/amidst-virtual-impunity{{</trurl>}}), “la ausencia de contenidos digitales feministas y respetuosos de los derechos humanos, hace que los espacios en línea en los que la comunicación tiene lugar principalmente en idiomas locales, sean hostiles para las mujeres, las personas queer y las minorías. Existe una clara carencia de medios de comunicación digitales que contrarresten la narrativa convencional, que continúa estando contaminada por estereotipos negativos. Esto acentúa el discurso de odio y la violencia de género y sexual en entornos virtuales”. Estas formas de violencia incluyen a las comunidades indígenas, las [castas oprimidas](https://www.equalitylabs.org/facebookindiareport), las minorías religiosas, las personas discapacitadas y con diversidad funcional y otras comunidades desfavorecidas.

Por otra parte, estas comunidades utilizan internet para luchar contra diversas formas de violencia transgeneracional ejercida contra ellas mismas y sus lenguas. [Jeffrey y Ashley]({{<trurl>}}/stories/learning-and-reclaiming-indigenous-languages{{</trurl>}}) analizaron cerca de 3800 tuits con más de 35 hashtags de lenguas indígenas y 57 palabras clave en 60 grupos lingüísticos indígenas reconocidos a nivel federal en Canadá. Descubrieron que, gracias a los hashtags de Twitter, los pueblos indígenas de Canadá y del resto del mundo pueden conectar, participar y colaborar de forma activa en la recuperación y el desarrollo de las lenguas indígenas.

En sus propias palabras, “en un contexto social en el que las lenguas indígenas de toda Canadá han sido objeto de cancelación por las [políticas coloniales de asimilación](https://indigenousfoundations.arts.ubc.ca/the_residential_school_system/), las redes de hashtags de Twitter han conformado un importante y singular espacio para que los pueblos indígenas compartan sus conocimientos sobre lenguas indígenas. Entre las múltiples redes incluidas en nuestro estudio se encuentran ejemplos de recuperación y reconexión lingüística para los supervivientes intergeneracionales de las políticas asimilacionistas coloniales”.

{{< summary-side-fig
src="/media/stories-content/Learning_and_Reclaiming_Indigenous_Languages_image1.webp"
alt="En Dene, la palabra para Caribou también significa Estrellas. Me encanta. (emoji con ojos en forma de corazón) #denecosmology #advancedlanguagesclass)"
caption="Al dar su consentimiento para citar este tuit, Melissa Daniels quiso reconocer la labor de la profesora de idiomas que impartió esta enseñanza, la anciana dene y educadora Eileen Beaver."
>}}

#### Acciones

* Reconocer estructuras de poder y privilegio en las distintas instituciones y procesos que sustentan las infraestructuras lingüísticas.
* Garantizar recursos reparadores para las comunidades y lenguas desfavorecidas que incluyan el aprendizaje y la programación lingüística.
* Destinar recursos a la creación y difusión de información y conocimientos de y para las comunidades que sufren múltiples e interseccionales formas de opresión y violencia, en sus propias lenguas y de las maneras que ellas elijan.

### ¿De quién son los valores y los conocimientos?

#### Contexo

La historia y la tecnología de internet se fundamentan en una visión del mundo basada en las epistemologías occidentales, es decir, en sus formas de conocer y hacer. Más concretamente, internet fue y sigue estando diseñado y gobernado en gran parte por hombres blancos privilegiados (y, [ahora, algunas personas morenas](https://www.theguardian.com/technology/2014/apr/11/powerful-indians-silicon-valley)). Esto significa que los valores que suelen constituir los cimientos de la arquitectura e infraestructura de internet, son los valores del determinismo tecnológico (según los cuales la tecnología se considera como inherentemente beneficiosa y la causa principal de cualquier cambio social) y del individualismo, en el que el principal objetivo y motor es el individuo y no el colectivo.

Además, esta visión del mundo tiene sus raíces en la Ilustración, el movimiento del siglo XVIII en el Norte global caracterizado por un cierto tipo de ciencia y tecnología basadas en la razón. Pero nos olvidamos de que las matemáticas y la ciencia florecieron en el Sur global mucho antes del siglo XVIII. Por ejemplo, los primeros sistemas numéricos y de escritura se originaron en Mesopotamia, donde se sitúan Irán e Irak en la actualidad. Y lo que resulta aún más crucial es que olvidamos que los recursos para la Ilustración, esa “edad dorada” de la ciencia y tecnología del Norte global, se basaron en lo que fue la “edad del imperio” en el Sur global, con movimientos masivos de colonización, esclavitud, genocidio y extracción de recursos de Asia, África, Latinoamérica, el Caribe y las islas del Pacífico. Los cimientos de la naturaleza extractiva del capitalismo moderno tienen su origen en este pasado colonial, y continúan formando parte de su capital tecnológico.

Además de los recursos materiales, en estos procesos se destruyeron, ignoraron y socavaron otras formas de saber, hacer y ser, es decir, las epistemologías no occidentales que conformaban los conocimientos indígenas o los saberes de las personas de los lugares menos privilegiados del mundo. La consecuencia más devastadora de todo esto para la lengua como representación importante del conocimiento (como hemos mencionado anteriormente) fue la devaluación total de las lenguas europeas no occidentales y la destrucción activa o el abandono indolente de las formas orales y no escritas de la lengua. El sesgo hacia el contenido escrito en un pequeño conjunto de lenguas privilegiadas sigue desviándonos hacia un tipo particular de “conocimiento” escrito en el entorno editorial y académico, que luego repercute en la documentación e información subyacente necesarias para el procesamiento de la lengua natural, o en algunos de los sistemas lingüísticos automáticos que forman parte de las infraestructuras de internet, como Google Translate.

Tal y como [Ana]({{<trurl>}}/stories/dill-wlhall-on-the-web{{</trurl>}}) explica, “a pesar de que algo menos de la mitad de los idiomas del mundo sean lenguas que no disponen de un sistema escrito y que guardan una larga tradición oral, las lenguas que dominan el mundo virtual son lenguas que cuentan con un sistema alfabético ampliamente reconocido y extendido. La red refuerza una exclusión sistemática en la que solo los idiomas escritos pueden preservarse para el futuro”.

Esta pérdida de lenguas conlleva una pérdida mayor para nuestro futuro de la que no somos plenamente conscientes. Estamos perdiendo completamente no solo las formas de expresarnos en distintos idiomas, sino también los conocimientos y las perspectivas que son inherentes a esas lenguas. En un momento en el que la humanidad se encuentra al borde del colapso planetario, las comunidades indígenas y sus conocimientos salvaguardan nuestra biodiversidad y protegen la vida tal y como la conocemos. No es de extrañar, por tanto, que la [pérdida de lenguas se encuentre relacionada de forma directa con la pérdida de biodiversidad](http://www.unesco.org/new/en/culture/themes/endangered-languages/biodiversity-and-linguistic-diversity/) y la destrucción de ecosistemas en todo el mundo.

Internet podría ser una infraestructura extraordinaria para la preservación y difusión de distintas formas de lenguaje y conocimiento, ya que sus variados medios de comunicación pueden reproducir y plasmar lenguajes que son orales, visuales y extratextuales. Pero es esencial que esta promesa radical de internet no se fundamente una vez más en valores patriarcales, coloniales y capitalistas. ¿Cómo hacen las comunidades para preservar y revitalizar sus lenguas e identidades, y compartir sus conocimientos en sus propios términos? Muchas comunidades indígenas, por ejemplo, tienen conocimientos que son sagrados y que no pueden compartirse públicamente.

[Papa Reo](https://papareo.nz/) es una iniciativa de tales características liderada comunitariamente. Consiste en una tecnología de reconocimiento de voz para el reo maorí (la lengua maorí) de Aotearoa/Nueva Zelanda. La comunidad maorí creó y mantiene la tecnología y datos de este proyecto y considera que esta forma de [soberanía de datos](https://www.wired.co.uk/article/maori-language-tech) es indispensable para garantizar que el conocimiento que se comparte mediante el lenguaje se utilice por y para la comunidad maorí y no por empresas con ánimo de lucro. Curiosamente, a pesar de reconocer el valor de la tecnología de código abierto, el equipo de Papa Reo decidió no añadir sus datos a bases de datos de código abierto, porque a la comunidad maorí se le han negado los recursos y privilegios de la mayoría de comunidades de código abierto. Por otra parte, [Mukurtu](https://mukurtu.org/) es un ejemplo de plataforma de código abierto que ha sido desarrollada junto con comunidades indígenas para gestionar sus propios datos lingüísticos.

#### Acciones

* Crear, colaborar y compartir infraestructuras lingüísticas para internet, que sean para el bien público, enraizadas en unos valores colectivos y comunitarios que giren en torno a conceptos feministas e indígenas de soberanía y conocimiento encarnado.
* Seguir desafiando y denunciando las infraestructuras tecnológicas lingüísticas de naturaleza opresiva: capitalista, privativa, humanamente extractiva y ambientalmente destructiva.
* Reconocer que las tecnologías lingüísticas libres y de código abierto también deben prestar atención a sus propios privilegios relativos, y respetar la manera en que las comunidades desfavorecidas se autodeterminan, cómo definen “abierto” y lo que quieren compartir con el mundo.

### ¿De quién son las tecnologías y los estándares?

#### Contexto 

La industria tecnológica no es la única responsable de la actual falta de representación de la gran mayoría de lenguas del mundo y su falta de soporte. Sin embargo, la tecnología del Norte global sí juega un papel crucial a la hora de mantener e incrementar las desigualdades lingüísticas y el colonialismo digital en la red. 

Las grandes empresas tecnológicas — que están diseñando y creando la mayoría de plataformas digitales, herramientas, equipos, componentes y software que usamos — pueden ignorar la necesidad de crear un internet realmente multilingüe porque no consideran ni visualizan la mayoría de nuestras 7000 lenguas como parte esencial de la infraestructura de Internet. Al fin y al cabo, saben que pueden permitirse ofrecer soporte lingüístico tan solo en los casos en los que entienden que puede ser útil desde un punto de vista comercial, es decir: para las lenguas coloniales europeas o para las lenguas de lo que consideran “mercados emergentes”.  De hecho, las lenguas dominantes de Asia meridional y el Sudeste Asiático están empezando a recibir un [mejor apoyo lingüístico](https://blog.google/inside-google/googlers/shachi-dave-natural-language-processing/) en las grandes plataformas tecnológicas a medida que van convirtiéndose en su principal base de clientes.

Cabe añadir que algunas de las tecnologías más extendidas en la red han sido creadas y están controladas por estas empresas, ya que son las únicas con la capacidad y los recursos necesarios para hacerlo. Wikipedia es la notable excepción: es de código abierto y está gestionada por sus comunidades de voluntaries repartidas por todo el mundo. En general, las iniciativas orientadas a la propiedad y el lucro, no dan como resultado herramientas y tecnologías necesarias para la creación de un contenido complejo y con matices en lenguas minorizadas y desde comunidades desfavorecidas. Es incluso más grave: las tecnologías lingüísticas que estas empresas están desarrollando ahora mismo son [sistemas automatizados](https://dl.acm.org/doi/pdf/10.1145/3442188.3445922) a gran escala que dependen de enormes volúmenes de datos obtenidos de cualquier tipo de fuente —incluso si esa fuente está plagada de violencia, odio o de retórica en contra de grupos desfavorecidos.

Tal y como describen [Jeffrey y Ashley]({{<trurl>}}/stories/learning-and-reclaiming-indigenous-languages{{</trurl>}}) “la mayoría de las comunidades de aprendizaje, conservación y revitalización de lenguas indígenas _[survivance](https://en.wikipedia.org/wiki/Survivance)_ identifican el racismo como el mayor reto al que se enfrentan dentro del ecosistema social que es Twitter. Hay varias maneras concretas en las que este tipo de comunidades son atacadas: mediante contenido incendiario, tanto escrito como multimedia, que en muchos casos cumple todos los criterios para ser considerado discurso de odio, y por una gran variedad de tipos de usuarios que incluye tanto a personas reales como a bots automatizados. En cuanto a los bots, esas cuentas parecen seguir patrones similares de diseminación de desinformación, y muy frecuentemente comparten textos sin sentido a partir de datos analíticos y generación de contenido agregados”.

Que una empresa no se preocupe lo suficiente de conocer el contexto y la lengua local puede tener consecuencias nefastas y derivar en violencia activa. Activistas de Myanmar, donde el internet consiste básicamente en Facebook (o Meta), avisaron durante años a la empresa sobre los discursos de odio que circulaban por la red antes de que esta estableciera un equipo de lengua birmana. En 2015 Facebook empleaba a [cuatro personas](https://www.reuters.com/investigates/special-report/myanmar-facebook-hate/) para revisar el contenido en birmano que producían sus 7,3 millones de usuarios activos en Myanmar. ¿Qué implica esta falta de atención en lo que respecta al lenguaje y su contexto? Implica que las Naciones Unidas consideraron que Facebook fue determinante en el genocidio contra la población rohinyá en Myanmar, y su rol tiene un papel central en el caso contra el gobierno de Myanmar que se está juzgando actualmente en la [Corte Internacional de Justicia](https://thediplomat.com/2020/08/how-facebook-is-complicit-in-myanmars-attacks-on-minorities/). 

Un caso similar es el de la [incitación al odio contra musulmanes, dalits y otras comunidades desfavorecidas en India](https://www.ndtv.com/india-news/facebook-officials-played-down-hate-in-india-reveals-internal-report-2607365), que continúa ocurriendo, sin apenas moderación activa, a pesar de que India es el mayor mercado de Facebook y que cuenta con algunas de las lenguas más habladas del mundo. De hecho, la empresa destina el [84% de su “alcance global/cobertura lingüística”](https://www.washingtonpost.com/technology/2021/10/24/india-facebook-misinformation-hate-speech/) a desinformación en Estados Unidos, que cuenta con tan solo el 10% de sus usuarios. El 16% restante de sus esfuerzos se reparte por todo el resto del mundo.

Las empresas tecnológicas deben entender que el desarrollo de tecnologías lingüísticas necesita un amplio y profundo despliegue y articulación de recursos, contexto sociopolítico, y compromiso hacia una experiencia digital multilingüe segura y acogedora.

Nuestres colaboradores describen toda una serie de necesidades, retos y oportunidades que condicionan el acceso a una experiencia de este tipo. La falta de infraestructura (tanto de acceso a internet como de equipos funcionales) y de tecnologías para todas las lenguas parece ser un factor clave que dificulta el uso de lenguas minorizadas en espacios digitales, haciendo que sea difícil, lento, cansado e inviable. Ofrecemos a continuación unos pocos pero elocuentes ejemplos.

**Son muy pocas las ocasiones en que las tecnologías lingüísticas se diseñan con las lenguas minorizadas en mente**

[Donald]({{<trurl>}}/stories/challenges-of-expressing-chindali{{</trurl>}}) muestra lo difícil que es para su comunidad en Malawi acceder a una conexión a internet y a equipos que les permitan comunicarse en sus lenguas. Entrevistó a 20 hablantes de chindali: 10 estudiantes y 10 miembros ancianos de la comunidad. De estas 20 personas entrevistadas sólo cinco tenían un teléfono celular básico o uno inteligente, y siete no tenían acceso a ningún aparato. Solo cuatro de esos 20 —todos estudiantes— tenían una computadora portátil. En cuanto al internet, únicamente los estudiantes universitarios tenían acceso, a través de sus universidades o de su contrato de datos móviles personal. 

Una vez que consigue conectarse a la red, la gran mayoría de la población mundial no tiene acceso a teclados en sus propias lenguas. La mayoría de las comunidades de hablantes se ven obligadas a adaptar teclados diseñados principalmente para las lenguas europeas, copiando y pegando los caracteres de sus propias lenguas sobre los originales. Esto, que ya es lo suficientemente complicado de hacer sobre teclados de computadoras personales, se vuelve casi imposible en el caso de los celulares. Y es todavía más difícil para las lenguas principalmente orales, que no tienen un sistema de escritura acordado. 

Tal y como explica [Ana]({{<trurl>}}/stories/dill-wlhall-on-the-web{{</trurl>}}) sobre su lengua, “los teclados no tienen los símbolos zapotecos necesarios para representar los sonidos y los tonos de nuestras lenguas. Los esfuerzos para que las lenguas indígenas se escriban, llevan años de discusión tratando de alcanzar un consenso para un formato estandarizado como el latino, un formato un tanto impuesto e influenciado por Occidente pero de algún modo aceptado y requerido por un sector de hablantes”. Esto es aún más difícil para lenguas como el arrernte, que combinan voz y gestos, tal y como explican [Joel y Caddie]({{<trurl>}}/stories/signs-across-generations{{</trurl>}}) en su proyecto Indigemoji.

Si eres una persona que pertenece a una comunidad que ya se siente insegura en el mundo y además no puedes acceder a internet en tu propia lengua, es poco probable que puedas producir y compartir cómodamente contenido relevante y crítico para tu comunidad. [Paska]({{<trurl>}}/stories/flickering-hope{{</trurl>}}) en su ensayo sobre el contenido LGBTQIA+ en indonesio, explica: “Muchas personas LGBTQIA+ en Indonesia siguen sin estar familiarizadas con los aspectos técnicos de una página web ni tienen suficiente conocimiento sobre cómo funciona un buscador”. 

Las comunidades desfavorecidas en las regiones privilegiadas que conforman el Norte global también se enfrentan al reto que supone no tener un contenido en su idioma que les permita celebrar su experiencia vital y que, en algunas ocasiones, incluso salvar sus vidas. [Jeffrey y Ashley]({{<trurl>}}/stories/learning-and-reclaiming-indigenous-languages{{</trurl>}}) cuentan que “uno de los principales desafíos [...] son las limitaciones técnicas de las tecnologías de traducción automática para las lenguas indígenas en el contexto de Canadá. La función de traducción de Twitter identifica sistemáticamente de forma errónea las lenguas indígenas hul'qumi'num, sḵwx̱wú7mesh (squamish), lewkungen y neheyawewin (cree) como alemán, estonio, finés, vietnamita y francés”. 

En general, tal y como corrobora [Uda]({{<trurl>}}/stories/amidst-virtual-impunity{{</trurl>}}), “la creación de contenido digital en lenguas locales continúa siendo un reto debido a la falta de herramientas y la complejidad de las herramientas que hay disponibles. Crear contenido en lenguas locales requiere herramientas y habilidades especiales. Esta dificultad contribuye a la escasez de contenido progresista en lenguas locales”.

**Las tecnologías lingüísticas están fundamentalmente diseñadas con una perspectiva jerárquica, de arriba hacia abajo, que prioriza el lucro por encima de la igualdad y la seguridad.** [Claudia]({{<trurl>}}/stories/decolonizing-minority-language{{</trurl>}}) describe muy certeramente la estrategia de la mayoría de las empresas tecnológicas cuando afirma que “la provisión de tecnología y de medios digitales es vertida desde arriba por grandes compañías sobre las comunidades de hablantes con poca o nula participación de estas últimas. En este caso también podemos detectar un enfoque paternalista: como hay tan poca oferta, cualquier nueva aportación debe ser automáticamente aplaudida. Muy frecuentemente las empresas ofrecen soluciones prefabricadas sin tener en cuenta los deseos, necesidades y expectativas de les hablantes de lenguas minorizadas. Es como si se diera por sentado que estas personas deben estar agradecidas por cualquier producto u oportunidad que se les ofrezca, independientemente de si es algo realmente interesante o relevante para sus vidas. Podemos encontrar algunas excepciones a este comportamiento en van Esch et al. (2019), donde se señala repetidamente la necesidad de una colaboración estrecha con los hablantes a la hora de desarrollar aplicaciones de procesamiento de lenguaje natural”.

Este enfoque raramente entiende el contexto en el que viven las comunidades hablantes de lenguas minorizadas, y los cambios de diseño necesarios para trasladar a la red la riqueza y los matices de sus lenguas. Cuando [Emna]({{<trurl>}}/stories/write-in-another-language{{</trurl>}}) entrevistó a Gamil, de Sudán, él le respondió en árabe sudanés: “Sudán es un país con muchas tribus y una gran variedad de tradiciones y costumbres. Así que en el norte se habla un dialecto del árabe (el mismo que estoy usando) y esto se debe al colonialismo, claro. En cuanto al este y al oeste, las tribus hablan diferentes lenguas locales que solo ellas pueden hablar y entender. Es muy raro encontrar alguien del norte que las entienda, a no ser que haya vivido ahí e interactuado con ellas. Nuestra lengua proviene del cushítico. Las civilizaciones cushítica y nubia son nuestra referencia. Cuando se inundó la represa alta [de Asuán] perdimos la identidad cushítica, no encontramos un diccionario para descifrar la lengua y traducirla a otros idiomas. La lengua nubia, en cambio, es conocida y traducida. Incluso aparece en los teléfonos celulares Huawei como uno de los idiomas en el menú de ajustes. En cuanto a las lenguas del este y del oeste, no se escriben (o quizás sí, no soy consciente, debería confirmarlo). En el norte hablamos árabe. Somos africanos y hablantes de árabe, no somos árabes”.

En base a esta entrevista, Emna concluye que “la red necesita a todas las personas usuarias, tanto las que escriben como las que no. A pesar de esto, el cambio no es responsabilidad exclusiva de las usuarias, las empresas que diseñan y desarrollan software tienen que asumir también su responsabilidad en el futuro diseño de la red. Hoy en día parece que todo el mundo habla de la inclusividad y de la lucha contra el racismo, la discriminación y otras formas de colonialismo. Sin embargo, si queremos un cambio real en la red, las empresas deben hacer frente a la manera en que perpetúan la exclusión: toda persona que se dedique al diseño web, la ingeniería de tecnologías web o que sea propietaria de empresas tecnológicas debe también contribuir a hacer su software accesible para todas las personas”.

Una manera de analizar críticamente todas estas diferentes formas de exclusión es reconociendo que la base misma de la tecnología digital —el código en sí mismo— está fundamentalmente en inglés. Hay muy pocos [lenguajes de programación](https://www.wired.com/story/coding-is-for-everyoneas-long-as-you-speak-english/) basados en otros idiomas, lo que implica que es necesario dominar el inglés para poder programar. El [lenguaje de programación Qalb](https://www.afikra.com/talks/conversations/ramseynasser), basado en la sintaxis y la caligrafía arábiga, es uno de los pocos ejemplos que intenta romper esta tendencia. En cualquier caso, en términos generales, es importante que la industria tecnológica entienda como el privilegio lingüístico está codificado en la misma tecnología y las personas que la crean. 

Tal y como lo expresan [Jeffrey y Ashley]({{<trurl>}}/stories/learning-and-reclaiming-indigenous-languages{{</trurl>}}), “nuestro estudio deja claro que la promoción de lenguas indígenas en el internet debe estar basada en un análisis crítico de la tecnología de la red en sí misma, además de los procesos sociales que articulan su uso”. 

#### Acciones

* Reconocer que el acceso a una tecnología y un contenido multilingüe diseñados por los propios hablantes es un derecho humano fundamental que debe convertirse en una prioridad de las empresas tecnológicas y los organismos de normalización, y recibir el apoyo de instituciones globales como la [Unesco](https://en.unesco.org/news/tell-us-about-your-language-play-part-building-unescos-world-atlas-languages).
* Construir un modelo de gobernanza global para infraestructuras lingüísticas liderado por las comunidades, que funcione a través de alianzas basadas en la confianza y el respeto con las empresas tecnológicas y demás instituciones.
* Poner la ética y el consentimiento comunitario en el centro de la creación de tecnología y la recolección de datos lingüísticos, garantizando que las comunidades de hablantes puedan decidir de manera autónoma y segura cómo y qué información comparten, especialmente en el caso de aquellos grupos atravesados por otros de los muchos sistemas de opresión y discriminación que se entrelazan entre sí.

### ¿De quién son los diseños y la imaginación?

En base a las cifras y las historias que hemos compartido, sabemos que una infraestructura lingüística relevante y efectiva solo será posible cuando pongamos las necesidades, los diseños y el imaginario de las propias comunidades lingüísticas en el centro. Las comunidades desfavorecidas solo podrán crecer y fortalecerse cuando la mayoría minorizada del mundo forme parte integral del proceso de creación tecnológico.

Nuestres contribuidores sugieren diversas maneras de garantizar que esto ocurra, desde la contratación por parte de las empresas tecnológicas de especialistas y personal técnico procedente de comunidades lingüísticas minorizadas, hasta la asignación razonable de recursos a estas comunidades para realizar este trabajo. Recomiendan una pluralidad de estructuras y procesos basada en contextos lingüísticos en lugar de un enfoque “de fórmula”. Como dice [Claudia]({{<trurl>}}/stories/decolonizing-minority-language{{</trurl>}}), las comunidades de hablantes de una lengua minoritaria no necesitan soluciones prefabricadas: sus necesidades y requisitos concretos deben ser escuchados e incorporados en productos diseñados específicamente para ellas. Los contextos sociolingüísticos de diferentes lenguas minoritarias pueden ser muy diferentes entre sí, e igual de diferentes deben ser las soluciones”. 

Nuestres colaboradores también describen la responsabilidad de sus comunidades y sus intentos de conservar y ampliar sus lenguas mediante la tecnología de la que disponen. [Ishan]({{<trurl>}}/stories/marginality-within-marginality{{</trurl>}}) afirma que, “como persona queer y discapacitada creo que debemos aprovechar al máximo de los recursos socioeconómicos y culturales de los que disponemos para que nuestras experiencias, ambiciones y demandas sean vistas y escuchadas. Nosotras, las personas usuarias, debemos responsabilizarnos de hacer de internet un espacio inclusivo y accesible”. 

[Ana]({{<trurl>}}/stories/dill-wlhall-on-the-web{{</trurl>}}) explica que las comunidades a las que pertenece prefieren las plataformas que cuentan con una mejor infraestructura oral y visual frente a las que dependen del texto. “Hoy en día, en el caso de las lenguas zapotecas, la oralidad ha ido cobrando relevancia en el ciberespacio frente a la comunicación escrita. Plataformas sociales como YouTube, Facebook, WhatsApp o Instagram se alzan como recursos disponibles y amigables para las lenguas orales. Estas plataformas permiten crear contenido visual, lo cual enriquece el mensaje que se transmite sin caer en el rigor de la escritura. Son justamente estas plataformas las que tienen más usuaries a nivel global y las que están usando las comunidades indígenas. En el caso de las comunidades hablantes de zapoteco serrano, la mayoría de las personas se conectan a internet a través de Facebook, donde se retransmiten festivales tradicionales, bailes, música, narraciones de eventos importantes y anuncios para comunidades migrantes y locales. Antes de la COVID-19, Facebook ya jugaba un papel importante en el luto de familias migrantes, ya que los funerales y ceremonias se retransmitían a través de la plataforma. Esta misma plataforma es usada por algunas comunidades zapotecas para retransmitir la programación radiofónica, estableciendo importantes puentes entre un medio de comunicación analógico ampliamente usado en las zonas rurales como es la radio, y el espacio universal y omnipresente que es internet”. 

Para aquellas personas que pueden usar formas escritas del lenguaje, los hashtags se han convertido en maneras estimulantes de entrar en contacto digitalmente para aprender y difundir su propia lengua, e inspirar también a otras comunidades. [Jeffrey y Ashley]({{<trurl>}}/stories/learning-and-reclaiming-indigenous-languages{{</trurl>}}) explican como “en un contexto social en el que las lenguas indígenas en el territorio de Canadá han sido atacadas y están en peligro de ser borradas mediante políticas coloniales de asimilación, los sistemas de hashtags de Twitter se han convertido en espacios relevantes y singulares en los que las personas indígenas pueden compartir su conocimiento sobre lenguas indígenas… Por ejemplo, una red de hashtags de estudiantes de la lengua gwichʼin inspiró a estudiantes de anishinaabemowin (ojibwe) a crear su propia red. Y, de forma parecida, una red lingüística de lengua neheyawewin (cree) en Twitter fue la inspiración detrás de la iniciativa “la palabra del día” por parte de unos estudiantes de hul'qumi'num en esta misma plataforma”.

El uso extenso y creativo que las comunidades desfavorecidas hacen de estas plataformas privadas, es una razón importante por la cual las empresas tecnológicas deberían trabajar con ellas, y no contra ellas.

A la vez, [Claudia]({{<trurl>}}/stories/decolonizing-minority-language{{</trurl>}}) advierte que les activistas de las lenguas de comunidades desfavorecidas deben informarse y coordinarse mejor para no perder energías ni recursos en el proceso. “Aunque son encomiables, este tipo de iniciativas [activistas] generalmente adolecen de falta de coordinación, poca planificación y escasa visibilidad. Esto deriva en un problema serio para comunidades que no disponen de recursos ilimitados: la duplicación del esfuerzo. El principal problema en ambos casos es la falta de conocimiento sobre lo que ya existe o lo que es necesario. Con el fin de descolonizar las tecnologías lingüísticas para los idiomas minoritarios, es importante que nos podamos hacer una idea más clara de hasta qué punto se utilizan los idiomas minoritarios en los medios digitales, con qué frecuencia y con qué objetivos. Identificar los obstáculos a los que se enfrentan las personas hablantes de lenguas minoritarias cuando (si acaso) intentan usar esas lenguas, es igual de importante. ¿Se topan con dificultades técnicas? ¿Se bloquean debido a una suerte de paranoia autoinducida? Teniendo en cuenta que escribir en una lengua minoritaria es de algún modo una manera de exponerse al mundo exterior, ¿es posible que la gente se abstenga de hacerlo por miedo a la burla o el estigma? De forma parecida, se sabe poco sobre los deseos de las personas hablantes de lenguas minoritarias con respecto a las oportunidades que ofrece el ámbito digital. ¿Qué quieren o qué esperan que se ponga a su disposición?”.

Este informe ha sido un intento de entender estos desafíos y las posibles maneras de avanzar. Hacer lo mismo una y otra vez para lenguas diferentes no va a funcionar. Por ejemplo, desarrollar una aplicación en inglés y asumir que va a funcionar de manera similar en bahasa es profundamente problemático. Mejorar el internet implica cambiar las dinámicas de poder entre las personas, no solo solucionar cuestiones técnicas. Y el multilingüismo en internet está atravesado por toda una serie de complejas cuestiones políticas y sociotecnológicas. Debemos poner las necesidades de las comunidades lingüísticas por delante de las tecnologías de la red — solo así conseguiremos unas tecnologías lingüísticas realmente más efectivas y útiles.

La conclusión más importante es esta: son los diseños y la imaginación de lo que hemos llamado la “mayoría minorizada” del mundo lo que va a transformar positivamente nuestras infraestructuras lingüísticas. “[[Indigemoji]](https://www.indigemoji.com.au/ayeye-akerte-about) llega en un momento crucial de rápida absorción tecnológica y nueva conectividad en Australia central, e invita a la gente local a imaginar lo que podrían hacer con estas nuevas plataformas. ¿Cómo podemos conseguir que no se conviertan en una nueva fuerza colonizadora? ¿Y cómo podemos incorporar en ellas nuestras lenguas y nuestras culturas para hacerlas nuestras?”.

#### Acciones

* Articular las tecnologías digitales en torno al contexto, las necesidades, los diseños y la imaginación de comunidades lingüísticas locales pero conectadas a nivel global, en lugar de intentar forzar la diversidad lingüística a encajar en un modelo tecnológico único.
* Emplear la creatividad y aprovechar al máximo las posibilidades que ofrecen las tecnologías de la red para explorar el amplio rango de lenguajes corporales (orales, visuales, gestuales, textuales…), de manera que sea posible expresar y compartir diferentes tipos de conocimiento de manera sencilla y accesible.
* Aprender de nuestras Naciones Indígenas a diseñar tecnologías lingüísticas respetando la memoria colectiva y comunitaria a la vez que nos preparamos para el futuro. [Caminemos hacia atrás en dirección a nuestro futuro](https://www.rnz.co.nz/national/programmes/morningreport/audio/2018662501/ka-mua-ka-muri-walking-backwards-into-the-future).

## Por último, ¿qué puedes hacer tú?

Que alguien no hable inglés tan bien como tú no significa que esa persona sea estúpida. Significa que habla mejor en una de las otras 7000 lenguas del mundo.

Debemos trabajar conjuntamente y poner en común nuestras diferentes habilidades y experiencias para crear y expandir un internet realmente multilingüe. También debemos asegurarnos de que la información y el conocimiento que compartimos en todas estas lenguas no sean perjudiciales ni causen daño, sino que contribuyan en todo caso al bien común de nuestro mundo. Necesitamos eso que hemos llamado “solidaridad en acción”.

### Si trabajas en el ámbito de la tecnología:

* Sé consciente de cómo las políticas de tu empresa están contribuyendo (o no) a la multilingualidad del internet y a profundizar en el conocimiento humano común o compartido.
* Pon la internacionalización y la localización de las lenguas (minorizadas) en el centro de tus estrategias, no las consideres cuestiones periféricas. Para ello utiliza estrategias de cooperación comunitaria en lugar de un enfoque jerárquico y descontextualizado. 
* Acepta la crítica ampliamente contrastada de los grandes modelos de lenguaje y las tecnologías lingüísticas automatizadas, así como del gran daño que causan en el caso de carecer de una supervisión humana rigurosa.
* Crea a partir de procesos humanos y minuciosos, basados en el contexto, la gestión cuidadosa, la moderación, y bases de datos relativamente pequeñas controladas de manera comunitaria.
* Trabaja con las comunidades desde el respeto y con especial atención a las más desfavorecidas y aquellas más susceptibles a sufrir ante una falta de atención y cuidado.
* Dota de recursos a las personas de esas comunidades lingüísticas que te dan su tiempo y su pericia.

### Si trabajas en un organismo de normalización tecnológica:

* Reconoce la importancia de una riqueza contextual para la estandarización lingüística. 
* Construye y mejora relaciones y procesos con comunidades de hablantes de lenguas minorizadas para que cada vez más estándares puedan establecerse de manera colaborativa, e incluso bajo su liderazgo.
* Actúa de manera proactiva a la hora de invitar a formar parte de los órganos de gobernanza a más personas de comunidades de hablantes de lenguas minorizadas y dótalas con los recursos necesarios para participar plenamente en el proceso.

### Si trabajas en el gobierno:

* Reconoce que toda la ciudadanía, y no solo una minoría privilegiada, tiene derecho a acceder a contenido en su propia lengua. 
* Apoya la expansión de contenido en las lenguas locales producido por y para las personas desfavorecidas o víctimas de marginación social en la región.
* Apoya la preservación y digitalización de las lenguas minorizadas en tu región y no solo la de las lenguas dominantes. 

### Si trabajas en el ámbito de la tecnología y el conocimiento libre y abierto:

* Reconoce que, a pesar de estar orientado al bien común, el ámbito de la tecnología y el conocimiento libre y abierto también está atravesado por desequilibrios de poder y tiene sus limitaciones. 
* Respeta los límites que las comunidades desfavorecidas ponen a la hora de compartir su conocimiento. Recuerda todas las maneras en las que este ha sido históricamente explotado y mercantilizado en el pasado.
* Trabaja de manera conjunta con las comunidades de hablantes de lenguas minorizadas para crear las tecnologías y conocimientos que necesitan, no los que tú crees que necesitan.

### Si trabajas en una institución GLAM (galerías, bibliotecas, archivos, museos y memoria): 

* Reconoce que la lengua está en el corazón de los conocimientos y las culturas con las que trabajas y que intentas preservar y divulgar.
* Trabaja con las comunidades de hablantes de lenguas marginalizadas para garantizar que la manera en la que custodias e identificas artefactos culturales reconozca, celebre y amplifique sus historias y sus lenguas del modo que las comunidades desean. Esto es crucial, ya que muchas instituciones GLAM, sobre todo las del Norte global, están estrechamente ligadas a la historia del colonialismo y el capitalismo.
* Trabaja para garantizar que los materiales lingüísticos archivados en tus colecciones sean accesibles de manera gratuita y sencilla para las comunidades desfavorecidas y sus aliades, de manera que contribuya a la construcción de una infraestructura lingüística colectiva.

### Si trabajas en el ámbito educativo:

* Reconoce cómo nuestro sistema educativo prioriza las fuentes textuales y ciertas lenguas.
* Amplía tus métodos de enseñanza y de aprendizaje para que incluyan varias lenguas y las distintas formas de comunicar y conocer que representan.
* Lee, escucha, y cita obras traducidas siempre que sea posible y anima a otras personas a hacer lo mismo.

### Si trabajas en el ámbito editorial:

* Reconoce cómo la mayor parte del sector editorial mundial está atravesado por un profundo sesgo hacia las lenguas coloniales europeas.
* Amplía el número de lenguas en las que publicas y digitaliza en todas ellas.
* Publica más libros y materiales multilingües. 
* Experimenta con formas multimodales de publicar, de manera que sea más fácil compartir de manera simultánea formas de comunicación lingüística oral, visual y textual. 
* Reconoce y honra a tus traductores. 

### Si trabajas en el ámbito de la filantropía: 

* Reconoce que la lengua está en el centro mismo de las capacidades, la experiencia y el conocimiento humanos, independientemente del área en que trabajas. 
* Dota de recursos para la interpretación multilingüe a todos los distintos eventos y encuentros que organices o en los que participes, tanto a nivel global como regional.
* Apoya la producción, conservación y digitalización de materiales en la lengua de las comunidades con las que trabajas, y asegúrate que también tus propios materiales están en esas lenguas.

### Si perteneces a la comunidad de una lengua minorizada:

* Reconoce que no estás sole.
* Sé consciente de que tu comunidad tiene el derecho de decidir qué conocimiento quiere compartir con el mundo y cómo hacerlo.
* Trabaja con las personas mayores, las personas académicas y las generaciones más jóvenes de tu comunidad, así como con amigues de otras comunidades, para recopilar y compartir conocimiento.
* Si quieres ponerte en contacto con personas que están haciendo este tipo de trabajo ¡ponte en contacto con nosotras!

### Si simplemente te gustan los idiomas y te preguntas qué hacer:

* Reconoce que la lengua está en el centro de lo que somos y de lo que hacemos, y que es fundamental para los diversos conocimientos y culturas, ¡incluida la tuya!
* Conversa con tu familia, tus amigues y tu comunidad para darte cuenta de cómo y por qué el inglés y un puñado de lenguas dominan el acceso y los contenidos de Internet, y cómo cambiar esto juntes.
* Busca, lee, escucha y comparte activamente las contribuciones de las comunidades lingüísticas marginadas (¡incluyendo este informe!)
* Si quieres recibir actualizaciones de nuestra iniciativa, ¡síguenos en las redes sociales!


## Agradecimiento 

Queremos expresar nuestro amor, respeto y solidaridad hacia las muchas comunidades desfavorecidas alrededor del mundo (indígenas y más allá) que conciben la lengua como el centro de su identidad y sus formas de existir. Sus importantes esfuerzos para preservar, revitalizar y ampliar estas lenguas y formas de expresión nos inspiran para imaginar internet(s) más multilingües y plurales en los que podamos expresar al máximo nuestras mejores y más plenas identidades. También queremos expresar nuestro profundo agradecimiento hacia todas las personas involucradas en la academia o el ámbito técnico, tanto comunitario como institucional, que aman las lenguas tanto como nosotres y trabajan duro cada día para construir un internet tan multilingüe como nuestros mundos físicos.

A todes nuestres muches [colaboradores, traductores]({{<trurl>}}/about{{</trurl>}}) y comunidades a lo largo y ancho del mundo (especialmente aquelles que participaron en nuestro [encuentro Descolonizar las Lenguas de Internet en 2019](https://whoseknowledge.org/resource/dtil-reporte/?lang=es)): ¡Gracias por todo lo que hacen y son en el mundo, y por la paciencia que han tenido con nosotres mientras intentábamos sobrevivir estos dos últimos años! Gracias especialmente a nuestra ilustradora por sus creativas visualizaciones de los ensayos y a nuestro técnico de animación, que nos ha regalado una hermosa animación de estas ilustraciones.

Muchas, muchas gracias a todes nuestres amigues y a la comunidad que ha [revisado]({{<trurl>}}/about{{</trurl>}}) nuestro trabajo desde diferentes perspectivas y en diferentes lenguas. Todos los errores son nuestros, pero el apoyo y solidaridad nos han ayudado a mejorar infinitamente este trabajo en continuo desarrollo. Y, finalmente, a todes nosotres y a nuestras familias de sangre y elegidas: no podríamos haber sobrevivido los últimos años (especialmente entre 2019 y 2021) si no nos hubiéramos mantenido unides, aunque haya sido virtualmente. El amor y la confianza son los mejores idiomas entre todos. 

## Definiciones

Hay muchas maneras de definir los diferentes aspectos del lenguaje y de las historias de las que hablamos en este informe. De hecho, ¡algunas de estas definiciones se contradicen! A lo largo del informe hemos usado algunos términos y expresiones de maneras muy específicas. Estas son nuestras definiciones particulares de algunas de esas palabras y expresiones más determinantes.

* **Lenguas dominantes**: lenguas que son, o bien habladas por la mayoría de la población de una área concreta, o bien dominantes, a través de formas específicas de poder político y reconocimiento legal, político o cultural. El hindi, por ejemplo, es una lengua dominante en Asia meridional, sobre todo si la comparamos con muchas otras lenguas y tenemos en cuenta que, de hecho, el hindi es en sí mismo una familia de idiomas que algunas personas califican como “dialectos”. Otro ejemplo es el chino mandarín, que la política de Estado ha convertido en lengua dominante en China, en detrimento de otras variedades lingüísticas del chino y de otras lenguas indígenas de la región. Algunas lenguas dominantes son también las lenguas “oficiales” o “nacionales” de una región o un país. 
* **Lenguas coloniales europeas**: lenguas de Europa occidental que se propagaron por África, Asia, las Américas, el Caribe y las Islas del Pacífico a través de los procesos de colonización llevados a cabo por las compañías comerciales y los imperios europeos a partir del siglo XVI. Incluyen el inglés, el castellano, el francés, el portugués, el neerlandés y el alemán. Es importante señalar que estas lenguas también fueron lenguas “colonizadoras” para los pueblos indígenas de América del Norte y no solo en América Latina (América Central y del Sur). 
* **Sur global y Norte global:** el término “Sur global” hace referencia a las regiones de África, Asia, América Latina, el Caribe y las Islas del Pacífico que fueron colonizadas por los países de Europa occidental. No es un término geográfico; pretende reflejar las condiciones socioeconómicas y políticas históricas que todavía hoy en día diferencian a estos países y regiones de los países privilegiados de Europa y América septentrional, o “Norte global”. El término fue creado y su uso fue extendido por académicos y activistas del Sur global con el objetivo de sustituir otros términos que consideraban peyorativos e inapropiados como “naciones subdesarrolladas”, “en vías de desarrollo” o “tercer mundo”. Cabe recordar que los procesos de colonización implicaron el genocidio y la aniquilación de muchas naciones y pueblos indígenas en el Norte global, y que algunas personas y comunidades en el Sur global participaron en y se beneficiaron de la colonización de su propia gente. Por eso a veces hablamos de un Sur global en el Norte global y de un Norte global en el Sur global. Estas estructuras y procesos también afectan el estatus de las lenguas en esas regiones. (Consultar el término “mayoría minorizada”).
* **Lenguas indígenas**: las lenguas habladas por los pueblos y las Naciones Indígenas de una región o lugar particular se llaman lenguas indígenas. Los pueblos indígenas son considerados “pueblos originarios”, es decir, los primeros habitantes de lugares a lo largo y ancho del mundo que fueron posteriormente colonizados y ocupados por grupos culturales diferentes. La mayoría de las más de 7000 lenguas que existen en el mundo son usadas por comunidades indígenas.
* **Lenguas y dialectos**: el término “lengua” define cualquier sistema estructurado de expresión entre humanos, ya sea mediante voz, sonido, signos, gestos o representación gráfica. Algunos lingüistas usan la palabra “dialecto” para referirse a lo que entienden como diferentes variedades de una misma lengua que son "mutuamente inteligibles", es decir, todos los hablantes de las diferentes variedades pueden entenderse y hablar entre ellos. A pesar de esto, debido a que los criterios usados para diferenciar una “lengua” de un “dialecto” son generalmente políticos (basados en procesos históricos de poder y privilegio) apenas hemos usado el término “dialecto” en este informe. Preferimos el término “familia lingüística”, que refleja cómo muchas lenguas comparten parte de su historia a pesar de tener características diferentes, como es el caso de la familia lingüística que forman el árabe, el chino o el hindi. 
* **Lenguas locales**: en este informe hemos definido como lenguas locales las lenguas habladas por el mayor número de personas en un país o región. 
* **Lenguas minorizadas**: en este informe hemos considerado lenguas minorizadas todas aquellas que no predominan o no son visibles en el internet, en cuanto a contenido, soporte, información y conocimiento sobre esa lengua. Estas lenguas no han sido marginalizadas por la baja magnitud de su población o su menor número de hablantes, sino mediante procesos y estructuras históricas y prevalentes de poder y privilegio que incluyen la colonización y el capitalismo. Algunas lenguas minorizadas corren el peligro de desaparecer (es el caso de muchas lenguas indígenas), pero muchas son habladas por la notable mayoría de las personas que habitan su región, o incluso el mundo, y a pesar de esto permanecen infrarrepresentadas en la red. Este es el caso del panyabí, el tamil, el hausa o el zulú, para poner tan solo unos pocos ejemplos de entre las muchas lenguas dominantes (pero minorizadas) en Asia o África.
* **Lenguas minoritarias y mayoritarias**: lenguas minoritarias son aquellas habladas por una minoría (en términos numéricos) de la población de un territorio o región determinado, mientras que lengua mayoritaria es la lengua hablada por la mayoría de personas que componen esa misma población.
* **Mayoría minorizada del mundo**: las estructuras de poder y privilegio históricas y actuales tienen como consecuencia la discriminación y opresión de muchas diferentes comunidades y poblaciones en el mundo. En muchos casos estas formas de poder y privilegio convergen y se cruzan de forma que algunas comunidades se ven desfavorecidas y oprimidas de varias maneras: en base a su género, su raza, su sexualidad, su clase, su casta, su religión, su región, sus capacidades y, por supuesto, su lengua. Tanto en el mundo físico como en la red, estas comunidades conforman la mayoría del mundo a nivel numérico y poblacional, si bien habitualmente no ocupan posiciones de poder y son, por lo tanto, tratadas como una minoría. En otras palabras: son la “mayoría minorizada” del mundo.

[Más información sobre cómo citar y usar este informe.]({{<trurl>}}/license{{</trurl>}})

[Más información sobre nuestras fuentes e inspiración.]({{<trurl>}}/resources{{</trurl>}})

[Descarga este informe (PDF 1.8 MB)](/media/pdf-summary/ES-STIL-SummaryReport.pdf)