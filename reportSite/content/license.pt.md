---
author: Whose Knowledge
title: Licenças e citações
date: 2019-05-13
toc: false
---

Levamos em consideração a abertura de conteúdo, dados e projeto com a dignidade e a segurança das comunidades marginalizadas. Queremos que o relatório do Estado das Línguas na Internet seja compartilhado de forma fácil e gratuita, pois acreditamos que este trabalho é fundamental para uma internet e tecnologias digitais descolonizadas e multilíngues. Ao mesmo tempo, como este relatório inclui trabalho de e com comunidades marginalizadas que historicamente viram seu conhecimento ser explorado por outras pessoas, **respeitamos tudo o que eles generosamente compartilham conosco e com o mundo**.

Estamos empenhados em licenciar o **conteúdo, incluindo arte e design** neste site com a **licença aberta** [CC NC-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.pt_BR). Isso significa que, a menos que especificado de outra forma, as informações e o conteúdo deste site podem ser livremente compartilhados, alterados ou reutilizados (mas você não pode usá-los para fins lucrativos ou comerciais). Você deve dar os créditos a Whose Knowledge?, Oxford Internet Institute, Center for Internet and Society e os outros criadores deste conteúdo ao fazê-lo, sejam estes comunidades, organizações ou pessoas. Você também deve distribuir seu novo trabalho com a mesma licença.

Estamos licenciando os **dados** neste site com a licença aberta: [ODbL](https://opendatacommons.org/licenses/odbl/summary/). Isso significa que você pode usar, criar e adaptar o banco de dados que foi criado para este relatório. Você deve dar os créditos a Martin Dittus, Mark Graham e Oxford Internet Institute, bem como ao relatório O Estado das Línguas na Internet ao fazê-lo, e também deve distribuir seu novo trabalho com a mesma licença.

Também estamos comprometidos em licenciar o **código de design** para este site com uma licença [MIT](https://choosealicense.com/licenses/mit/), o que significa que você pode reutilizar, alterar e modificar o código para qualquer finalidade, desde que inclua a cópia original da licença MIT ao distribuir seu novo trabalho.

Sugerimos este formato para nos citar:

_“Whose Knowledge ?, et al. “Relatório do Estado das Línguas na Internet”, uma iniciativa documentada e pesquisada em colaboração, Data, Hora, internetlanguages.org._

Somos gratos a [Sara Ahmed](https://feministkilljoys.com/2013/09/11/making-feminist-points/), [P. Gabrielle Foreman](https://coloredconventions.org/about/team/), [Jessica Marie Johnson](https://twitter.com/jmjafrx) e [Kelly Foster](https://kellyfoster.co.uk/) por sua liderança em práticas de citação justas e radicais. Fazendo coro a elas, pedimos que você compartilhe com gosto este relatório em trechos ou na íntegra, mas não se esqueça de citá-lo. Isso se aplica ao relatório, bem como a qualquer menção sobre ele nas mídias sociais. Marque-nos ou informe-nos se você compartilhá-lo, para que possamos rastrear sua utilidade e impacto (@whoseknowledge). Se você tiver dúvidas sobre as licenças ou citações, [entre em contato conosco](mailto:languages@whoseknowledges.org).