---
author: Whose Knowledge
title: Ga zá dan dzogto ganhí
date: 2019-05-13
toc: false
---

Bi llunto konganze nhen daka dzogto ganhí nha gakhe zu kuez yell´ka. Llenlheto llín nhi, dan ba bzito: Gaklhe llunlenllo llín xtishllo´ka lue dan nhe Internet gunhen  llínlen yote benhe, kanhake nhaken da blhaho enchanha zá xchínllo llenlello: gak interneten xltage yoy dill dé. Leskakse, kanhake da bento llín nhen yell ka ba blliá cha´ake nha bchuchg lan´ake yoy da dé, **llapto balhan yoy da ka ll´llhue´aklhe nha ll´chalg nhen´ake nheto nha benhe yelá lla yell líu**.
 
Llueto latg ke **yoy da niog nha bxen nzá* llín nhí nhen** latg enchanha nhuteze wak gunhenhen llín**: [CC NC-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/). Danhí llenlen nhén wak chiz lazo, kuchao, nha gunhenho llín dayuble, lete cha nhe ganhí bi gak. (Lhaoze bi´ak góton) De wayumbielho xchín ¿Nhu ke nhak yel nezen? Xlatge Internet gan nhe Oxford, llín nhi le gaklhe llunlello llín xtillo ka lue dan né Internet, nha nha yoyze benhe ka bze niá nha llín nhí, nhu yell nha benhe tu lhaze. Leskske dé chizlallo xchínhon nhen lzenzen bento lhale.

Llalhal llueto latg ke **yoy da nhéz lue nhí** nhen dan lé [ODbL](https://opendatacommons.org/licenses/odbl/summary/). Wak gunhenho llín, nha wazogo o cha guchao yoy da ka nllachao´to da benhento llín ka wiog danhí, nha de wayumbielho benhe ka lé Martin Dittus, Mark Graham, xlatge Internet gan nhe Oxford,nha llín nhí gaklhe Llunlelhllo lín xtillo lue dan nhe Internet. Leskakse dé chizlallo xchínhon nhen lzenzen bento lhale.

Leska gueto latg ke **bxen ka benhento llín** nhen dan le [MIT](https://choosealicense.com/licenses/mit/) license, enchanha wak guchaon biteze yallg lho, bi galhallo weo walo lzenz nhí.

Ká walo nheto bi llín gunho, kí wak guzogon:

_Whose Knowledge?, et al. “Gaklhe llunlelhllo llín xtillo ka lue dan nhe Internet”, llín ga btil niá nhato enchanha biogen, báte, internetlanguages.org._

Llueto yel xuken ke [Sara Ahmed](https://feministkilljoys.com/2013/09/11/making-feminist-points/), [P. Gabrielle Foreman](https://coloredconventions.org/about/team/), [Jessica Marie Johnson](https://twitter.com/jmjafrx) nha [Kelly Foster](https://kellyfoster.co.uk/) kanhake blhe´aklhe nheto nez gaklhe walto llín da niog, da guklhelhe nheto. Wak naó´ake. Llunto lhue yex enchanha guz lazo llín nhi gateze, lhat wagen o duxente, bi gá lallo walho ga blhajon. Wál gao nheto o cha chix guelho nheto kat chízon, echanha nauto wiato bale benhe ba bxí nhen (@whoseknowledge). [Cha bí llenlo gunabo wakse guzogo nheto](mailto:languages@whoseknowledges.org).