---
author1: Donald Flywell Malanga
author2:
title: "Indamyo Sho Shikwaghiwa Pakuyugha iChindali Ukwendela pa Makina gha
Intaneti: Abayughi Baku Chikaya Chakwa Chisitu, ku Misuku, mu Malawi"
date: 2020-01-01
weight: 3
slug: "challenges-of-expressing-chindali"
illustration: The_Challenges_of_Expressing_Chindali.jpg
illustrationAlt: A group of people relaxes in an open space, surrounded by trees and tall grass. A parent stands and holds the hand of a child while looking to the horizon and speaking to an elder. The elder sits at the base of a tree, holding a mobile phone in their hands. The tree has a screen in its trunk, and the elder’s mobile phone is connected to the tree by a leafy cable. The same cable-vines wrap around the tree’s trunk and branches. In the middle-distance we see another person leaning against a similar tree and reading. Behind them, gentle hills roll in the background and the sun sits low in the sky.
illustrationauthor: Illustration by Maggie Haughey
translationKey: donald
hasaudio: false
imageauthor1: Donald_Flywell_Malanga.webp
imageauthor2:
summary: "Chindali chili pamupeneshe nifiyugha fyakufumila cu chi *Bantu*. Pakabalilo aka chikuyughiwa na bandu bo bakwikala cu maghamba gha cu Misuku, mwi Boma lya Chitipa cu Malawi. Kunyuma uko chayughiwa nabandu pabu pipi 70,000 mu Malawi. Pakabalilo aka, ubwingi bwa bandu bakuyugha ichindali bwaba bunandi nongwa shafipani, ukonganikilana, nubwikalilo, nishingi sho shingabapo muchisu icha Malawi."
---

## Ukwighulira

Chindali chili pamupeneshe nifiyugha fyakufumila cu chi *"Bantu"*. Pakabalilo aka chikuyughiwa na bandu bo bakwikala cu maghamba gha cu Misuku, mwi Boma lya Chitipa cu Malawi. Kunyuma uko chayughiwa nabandu pabu pipi 70,000 mu Malawi. Pakabalilo aka, ubwingi bwa bandu bakuyugha ichindali bwaba bunandi nongwa shafipani, ukonganikilana, nubwikalilo, nishingi sho shingabapo muchisu icha Malawi. Ichindali pakabaliloshe aka, chikuyughiwashe nabandu abasongo panogwa iyi chili papi pakushimila. Amashiku agha pakafusheleka ukuti waghe iChindali chikwaghiwa pa makina agha tekinologe, sosho midia, napa intaneti.

Akapango aka kakulondashe kayughe mo iChindali chikuyughiliwa mu mamakina agha intaneti ni ndimyo sho abandali bakwaghana nasho. Ukulondesha kwakapango aka kwalangisha ukuti bakuyugha mu makina gha intenti ekwendela mu mashu, mufitushi ifya kutesha nuku pulikisha, napa kushimba. Lole, chaghiwa ukuti abandali bakutamiwa ukuyugha akisashe nongwa yandamyo sha intaneti, yapa chuma, akayughilo, ukonganikila kwa bwikalilo, nifipani, nuwu manyili mo bukwendela. Pasulo, akapango kakusakisha ishila ni mbatiko sho shingapangisha ukwafwilisha ukuti ifiyugha fyoshi fyaghiweghe pa makina agha intaneti.

## Ulubafu lwake

Ichiyugha chakulondiwa leka ukutula ichisu ni chikaya. Ichiyugha chikusunga ubwikashi bwa chikaya, chikukasha ukwikala pamumpene, ukusenga amahala, ubumanyili nu kwikalilana kwa bandu abingi. Ukukinda ifiyugha 7000, fikuyughiwa pachisu chapashi, lole ifiyuha infinandi fikwaghiwa pa makina gha intaneti. I Banki iya pachisu chapashi muchinja cha 2014, ya langisha ukuti 80 perecenti yakuyugha pa makina gha intaneti ghakwimila ifiyugha tenishe: iChisungu (10%), iChichayinizi (19.3%), iChisipanishi (7.9%), iChi-arebiki (5.2%), iChipochugizi(3.9%), iChi-indonesha/Malesha(3.9%), iChijapanisi(3.2%), iChirasha (2.5%), iChijelemani (2.1%). Po ilalusho lyolikwisa lyakuti, abayughi ba ifiyugha ifingi balikoki?

Ku Malawi kuno, ukuyana ni fisu ifya mu Afilika, abandu bo bali kubutali naku tabuni bakuyugha ukwendela mufiyugha ifya kukwabo namo chiwelile iChichewa, iChingonde, iChilambya, iChitonga, iChindali, ukyughashe finandi. Ukuyugha uku kubukukililashe, napo pakabalilo aka kwisile itekiloge ya pa intaneti yo yikwafwilisha ukuyugha, ukumanyila, ukwanisha, ukumanyisha mufya bumi, bumanyili, ubulimi nifya kwikala akisa ukuti ichisu chitukukeghe. Lole, chaghiwa ukuti ifiyugha ifingi, namo chiwelile Chindali chikubukililashe ukumala mufikaya na mutabunishe. Ichi Sungu, chocho chikulangisha ukuti chikuyughiwa pakinda fyoshi ifiyugha ifingi. Pakabalilo ka, kwaghiwa ukuti mu Malawi, intaneti mufikaya yili pa 3.4%, ama foni ghali pa 42% ukupambana nukwaghiwa kwa ma komputa (2.9%). Pobulipope, inambala ya bandu bo bandukuwa ni tekiloge ya intaneti yikukula panandi.

## Imbatiko

Ukuti tulinganye isila yakulongosolera, tukendela indaghilo yo akapang uChaudenson mu chinja cha 2003. Indaghilo yake yikuyungha ukuti ifipani, injugha, ichuuma, tekinoloje, ubumanyi, nu kwikalilana pa chikaya fingapangisha ukuti ifiyugha fyaghiwe pa intaneti. Indaghilo ishi shingalongosola, nukulangisha indamyo sho ichiyugha cha Chindali chikwaghana nasho ukwendela pa intaneti mu Malawi. Abachilumyana teni, nabasongo abaghome teni bo bakuyugha iChindali bakasaliwa ukufumila kwa Chisitu, mwamalafyale usongo uMwenemisuku kuChitipa. Ukuti basaliwe, tukeghela ubukushi bwabo, tukeghela sona ukuti mukolo pamo mulisha, imbombo yakubomba. Abandu aba bakitikisha ukuti tuyughe nabo. Umwaghamo amalalusho gho tukalalusha ghakuti: Ka makina ghaki agha intaneti gho muli nagho? Mukuwombekeshela mbombo yiki? Mukuwombela bule muchiyugha cha Chindali? Ndamyo shiki shomukwaghana nasho linga mukubombesha pamo mukuyugha iChindali pa intaneti? Indamyo sho mwayugha shingamala bulele?

## Fyo Fyaghiwa

### Ukuba na makina gha tekinoloje nakabombekeshe kake mu Chindali

Pa bandu 20 botukayugha nabo, abingi bakaba na ma foni ghabo (9), simati foni (2), ni foni yapamwanya panandi (3). Abandu 7 batakaba ni foni. Abana ba sukulu 4 bobo bakaba nama laputopu. Abaghome bo bamanyishi bakati bakaghula amakina gha intaneti mundalama sho bakasunga bene, po abalimi bakati abakamu na bana babo boba bakubomba imbombo bakabaghulila. Abana ba ku koleji bakati abapapi babo bakabaghulira ama foni na ma laputopu wawombele ifya wumanyili. Ku intaneti, abana abasukulu bakabombela ukwendela munyumba sha ma komputa, nughula intaneti ibandu. Abaghome bakitikisha ukuti wamenye ukuti intaneti yiliko lole watawombilepo lumo. Ichi chitakawa chiswigho ngani nongwa yakuti intaneti kufikaya fya ku Malawi yikali yitafikite, napowule lole iboma yibikite amaka amingi leka.

Abandu bakayugha ukuti bakuwombela ifoni ni intaneti pa kwimbila abakamu babo, kumakasitomala kubo bakughulisha ifindu. Abachilumyana, nganingani abana ba sukulu, bakubombela intaneti ifya kuhowosha nu bumanyili. Bakayugha sona ukuti bakuyugha iChindali pa intaneti ukwendela pakushimba, kutuma amashu, kupulikisha, kuketelela nu tumila abanine, abapapi, abanine baku sukulu na bamanyishi. 

## Indamyo

Abandu bo bakalalushiwa, bakayugha indamyo inyingi naloli sho bakwaghana nasho linga bakuyugha iChindali pa intaneti. Indamyo shaghiwa ekwendela mushila ishi:

### Indamyo sha tekinoloje

Aba chilumyana bakayugha ukuti napobuli amafoni numwabi wakuba ni intaneti ghulipo pala bali ku koleji, ubwisa bwa netiweki yikwenda panandi, yipangisha ukuti manye babombele akisa ichiyugha cha Chindali. Bakayugha sona ukuti nongwa yakuti amakomputa na ma foni ghakwisa ukushita ukughang'anamulila mu Chindali. Agha ghogho mashu aba chilumyana bakayughapo: *"Ama komputa amingi ghakwisa ni Chisungu ni Frenchi. Panongwa iyi iChindali chitakwaghiwa muma komputa. Ukufwanikisha, linga ukutumisha nu shimba muChindali, ukwegha akabalilo akingi. Lole linga ukushimba mu Chisungu, kwandaghashe ukushimba, ifoni ni komputa yikushakisha amashu gho kulonda ukushimba. Lole linga ukulondashe ukwimba ifoni lumo ukuyugha mu Chindali, kulondiwa ukuti ughule kabalilo akingi nughelekesha linga ukuyugha mu Chsungu" [Muyughi 2, 4, 6, 10]*.

Abaghome bakinong'ona ukuti bata nubumanyili ubwakukwanila ukuti bangabombela intaneti. Ukufwanikishashe, bamo bakayugha ukuti.*"Waketa taata, indi nifoni yabu. Ngumanyashe apakutofya linga ngwimba ifoni nu kutumula ifoni. Ndani ndawatawa isha ukuti ningawombekesha isimatifoni, ngupulikashe ukuti sili ni intaneti, fesibuku nu watisi apu".Uyungi akuti "Une ndayimenyi yo bakuti imelo. Ngupulikangashe ukuti kuli intaneti, watisi apu, lole ndamenye nukubombesha. Ichingi chakuti napo linga ningamanya ndinindalama sha kukughulira?"*

Ichi chikusanusha ukuti, ubumanyili bwa tekinoloje ya intaneti, bukwenda panandi kwa intaneti, kusita ukuba ni netiweki inyisa, ukupambana pakati pa bo bali ni tekinoloje ya intaneti nabo batanayo, ukushita kwaghiwa iChindali mu ma foni, komputa, nifibombesho ifingi fyofyo fikukanisha ukuti abandali bapotweghe ukubombesha iChindali pa intaneti.

### Indamyo sha pa chuuma

Abandu bakipulika ukuti indalama sho bakushagha bakubombera ifya kulondiwa pa bumi bwabo, po ukughula ifoni, idata ya intaneti tafyakulondiwa. Ukughula idata ya intaneti ku Malawi yikwelite leka. Abaghome bakati bate nifoni nongwa yakuti indalama sho bakushagha pa mweshi nandi leka ukuti bangaghula ifoni ni computa. Bakayugha sona ukuti inyumba shabo sho bakughona shitana mageshi, popo wakoghopa ukughula ifoni yo yikulonda sona amabatile ghakuchajira, gho batangakwanisha. Agha ghogho mashu ghamo goho bakayugha:*" Une ngupokela indalama nandi ukuba mumanyishi waku sukulu yaku pulayimale. Indalama yonguyagha ngubombera ukulisha abana nubaghulira ifya kufwala. Ngimba nongwa yiki ndaghegheshe indalama paghula intaneti bandu sho nandi pakwanisha usungilila pakaya kangu?" Abangi bakuti "Pala ndani chakulya ni wogha lya pa nyumba, ngwenera ninog'one ukuti ka ingule ama yuniti pamo ichakulya?Apo ichakulya chikuba chabwandilo" Uyungi akuti "Inyumba yangu ya bwisu, yita namageshi, linga ningaghula ifoni ningaya njaje kwighi ifoni pala ibatile lyashila?"*

### Indamyo sha kwikalilana pachikaya

Ichiyugha cha Chindali ukuyana nifiyugha ifya ku Afilika fitashimbiwe. Ukuyana nukwikalilana pachikolo, iChindali chikwendashe ukufuma kumuwilo ughu ukuya kughunine pakwendelashe ukuyugha pamulomo. Nongwa ya ndamyo iyi, abandu aba bakipulika ukuti ukushita ukushimbiwa, ndamyo ingulu yoyikupangisha ukuti iChindali chitakaghiweghe pa intaneti. Nongwa yandamyo kukula kwa tekinoloje, abamuwilo wa chilumyana batanimbombo ni Chindali nongwa yakuti chikuwoneka chakale. Panongwa iyi, chikubapangisha ukuti babonekeghe ukufwana ba pashi muchikaya. 

Ukongeleshapo, ukwinong'ona kwa bandu abangi mo iChindali chiwelile, chikupangisha ukuti bachiketeghe ububibi napa intaneti. Ifi fyofyo bamo bakayugha:*"Une nguyugha iChindali linga ngumwimbila utaata pamo ngumushimbila imelo" [muyughi 1]. "Pa sukulu, abandali banandi. Pakufwanikisha, tukwagha ukuti isukulu yoshi abana 4 pamo 5 bobo bakuyugha iChindali. Nongwa ya ichi, tukuyughaghashe twebene umwakwimama pakwendela pa foni, imelo, na pa sosho midiya. Linga abanyitu bapulikashe tukuyugha, bakutuketa kufwana twebashita kumanyikwa. Nongwa ya ichi,tukuba bakwimama nututushiwa ukuyugha iChichewa chochikweghiwa kufwana chapamwanya kuba nongwa ya muchisu cha Malawi", " Ichiyugha chitu chikuya ku bana bitu ukwendela ukuyugha pamulomo. Ndalibweni lumo ibuku lishimbiwe muChindali?*

### Indamyo yaka yughile

Abandu bakasakisha sona ukuti ukushimba iChindali mufoni, mu komputa napa intaneti, kulonda akabaliro akingi paghelekesha linga ukuyugha muChisungu. Ukuyugha muChisungu kulipo akisa nongwa yakuti chili kale nifya kung'anamulila muChisungu, chiArabeki, nichiFulenchi, nifingi ifiyugha.*"Linga ukushimba ishu lyakuti **good morning** ukali tamalishishe pakushimba, ifoni pamo ikomputa yikuba yakusakisha kale amashu. Lole linga ukushimba muChindali ukuti **mwalamusha** ubaghile ukushimba ishu lyoshi, popakwegha akabalilo akingi ngani. Ifoni pamo ikomputa yikushimba umolo mushi, choching'anamula ukuti ifoni ni komputa yipotwa ung'anamulila muChindali"*

### Indamyo sha bumanyili ni fipani

Ku Malawi tutanimbatiko ya chiyugha yoyinga sungilila ifiyugha ifinandi namo chiwelile iChindali. Panongwa iyi, ukusowa kwa iChindali pa intaneti kukubonekashe pabwelu. Ichabubili chakuti, abandu bakwinong'ona ukuti ukusoba iChindali mu bumanyili bwitu ku pulaymale na ku sekondale fipangisha ukuti chiwechikafu ukusungilila nu kuchinyamusha namaka. Bamo abayughi bakayugha ukuti batamanyile, yonongwa iyi bakupotwa ukuti bayughe nu shimba mu Chisungu. Panongwa iyi, batakuketa nukulondiwa kwa kuba ni imelo pamo ukughula ifoni yapamwanya yoyili ni intaneti, po ichiyugha chilimu Chisungu chobatukuchimanya. Ichi chikapangisha ukuti bamo baghane nindamyo ya kuyugha, kushimba, nuti wighule imelo. *"iChindali chitu chitakumanyishiwa ku sukulu sha ku pulayimale na ku sekondale. Ngimbapo iChindali chingaghiwa bule pa intaneti? Iboma litu limanyashe iChichewa ukufwana chiyugha cho chilipamwanya muchisu, po iChindali, nifiyugha fya iChitonga, iChilambya, iChingonde fitaboneka pamo". Ngimba apa choni chochingapangisha ukuti ingule ifoni ya pamwanya, nutaghashe akabalilo kangu napa intaneti po ichiyugha Chis ungu, lolengandachimenye? Lingashe chingang'amuliwa muChindali pamo muchiyugha icha mukaya mwitu"* [muyughi 11].

## Pasulo

Napobuli pope akapango aka katayughiwe pabukulu, kalangisha ukuti abandu abandali bakuyugha pa Intaneti ukwendela pafoni, pakomputa, ni sosho midiya. Lole, ukuti babombeshe pabukulu bakupotwa nongwa ya ndamyo shakupambana pambana. Ifwanikisho shakuti, ichiyugha, ukwikalinana pa chikaya, tekiloje yapa intaneti, nichuuma ni finineshe ifingi. Ichi techakuswighisha ngani nongwa yakuti indamyo sho iChindali chikwaghana nasho mubumi bwabwila shingila sona na kubumi bwa tekinoloje. Pakongelesha, ukuketesha indaghilo yakabombesheshe ka tekiloje pa chitukuko iya chinja cha 2006, ni ndaghilo ya tekinoloje iya chinja cha 2013, shapangisha ukuti ichisu cha Malawi chibikite ukuti pamung'yo wake ukuti itekinologe yise muchikaya chitu nuti abandu bamuchisu babombekeshe akisa. Lole chaghiwa ukiti ishila ni mbatiko nashimo isha kuketelela ukuti twafikishe indaghilo. Kimba, po ukwaghiwa kwa mashu agha kuli niching'anamulo mo abamasukulu amakulu, ifubutira fyotafyaboma, ifibutira fya ma tekilonoje, iboma ni chisu chapasi choshi ukuti bamanye mo intaneti yikapagisha ukuti ifiyugha fyoshi fyaghiweghe:

* Amasukulu amakulu agha bumanyi ghabaghile ukubika indalama inyingi shakupangira ishila ni nindaghilo sho shingatula ukuti ifiyugha fyaghiwe pa intaneti ukuyana nichifumo chachiyugha.
* Iboma libaghile ukuketesha sona indaghilo ya chiyugha ukuti ifiyugha ifinandi-nandi fyope fikusungiwa nukwaghiwa pabuyo bwa intaneti.
* Iboma limayishe itekinoloje abandu bobakwikala mufikaya ukwendela mubumanyili bwabasongo.
* Iboma likushe ubuyo bwakuti intaneti yaghiwe mufikaya ekwendela mufiyugha fyamufikaya.
* Ifibutila fya muchisu chapashi fikolelane nukubombera imbombo pamumpene namaboma ghakupambapambana ukubika indalama ishingi ishakuti itekilonoje ya intaneti yilipo ukuti yisunge nuku shimba ifiyugha fyofilipapipi ukushimila.
* Ifibutila fyo tafyaboma figheleghe ukusuma nu bomba pamumpene ni boma ukubanyisha abandu mobangapangila nukushimba ichiyugha chabo ukuti chaghiweghe mumabuyo amingi agha makina gha intaneti.
* Ifibutila fya ma tekinoloje gha intaneti fipange ama sofutiweya ukuti ghang'anamule amashu ukuyana nifiyugha ifingifingi nako fikufumila.
