---
author1: Kimeltuwe Project
author2:
title: The use of our ancestral language as a tool to preserve our identity in the digital age
date: 2020-01-01
weight: 9
slug: "use-of-our-ancestral-language"
illustration: Kimeltuwe.jpg
illustrationAlt: Illustrations of three Mapuche people standing behind a desk with two computers.
illustrationauthor: Illustration by Kimeltuwe Project, via kmm.cl
translationKey: kimeltuwe
hasaudio: true
imageauthor1: Kimeltuwe.webp
imageauthor2:
summary: "Kimeltuwe is an educational initiative that emerged in 2014 with the aim of becoming a visual educational project for contributing to the teaching and learning of the Mapuche language. The purpose of Kimeltuwe is to share graphic and audiovisual material in Mapuzugun around different topics of interest through different online platforms. In general, the material is aimed at teachers, but also at the dissemination and revitalization of Mapuzugun in the context of new technologies and social networks."
---

*This interview was conducted in July 2020 as a contribution to the State of the Internet's Languages report. The Kimeltuwe project focuses on online teaching of Mapuzugun language spoken by the Mapuche people in the territories known today as Chile and Argentina. In their work, the Kimeltuwe project also highlights themes related to the history and identity of the Mapuche people, which they capture in visual and audiovisual material.*

**1. Tell us about yourselves: what is the Kimeltuwe project, how did it begin and what gap is it trying to fill?**

[Kimeltuwe](https://www.facebook.com/kimeltuwe) is an educational initiative that emerged in 2014 with the aim of becoming a [visual educational project](https://rising.globalvoices.org/lenguas/investigacion/activismo-digital-de-lenguas-indigenas/estudios-de-caso/kimeltuwe/) for contributing to the teaching and learning of the Mapuche language. The purpose of Kimeltuwe is to share graphic and audiovisual material in Mapuzugun around different topics of interest through different online platforms. In general, the material is aimed at teachers, but also at the dissemination and revitalization of Mapuzugun in the context of new technologies and social networks.

We periodically publish sheets (pictures) and videos following a methodology for the progressive development of communication skills and vocabulary building of Mapuzugun language. We also address topics of a political nature and current affairs in pursuit of the recovery and revitalization of a Mapuche identity within the territory which is currently occupied by the Chilean and Argentine States.

**2. Tell us about Mapuzugun, for those of us who do not know the context: in which regions it is spoken, who the Mapuche people are, and other historical, social and cultural aspects that you deem important to share.**

The Mapuche nation inhabits a territory currently called, in Mapuzugun, *Wallmapu* (literally "territory around" in Spanish). Due to political and identity implications, there's debate around circumscribing the territory on a current map. In very general terms, it could be said that the territory corresponds to what today is the south central zone of Chile (Biobio, Araucanía, Los Lagos and Los Ríos regions) and in Argentina it would correspond to the Newken, Pampa, Río Negro and Chubut.

Notwithstanding, some people also consider that the areas where Mapuzugun has been historically spoken, are an integral part of the overall territory as well, which reinforces the idea of ​​a deep connection between language and identity.

Regardless of this, the areas where Mapuzugun is still circumscribed (or where it has survived, which is the same), correspond to different minor territories within that great *Wallmapu* territory. Intergenerational transmission is in evident decline and the spaces where it is spoken have been reduced to the formal space (expressions of religiosity, Mapuche political spaces and Mapuche national identity), and some informal places, including the family.

In recent decades, the Chilean and Argentine States have tried to reverse the policies that lead to the reduction of the number of speakers of Mapuche. This effort has been attempted through programs in which Mapuche speakers or non-native speakers would teach Mapuche language and culture in schools. Unfortunately, this has not translated into an increased number of speakers, neither has it halted the declining rate of speakers. The reasons are manifold, but it's mainly due to this effort not being accompanied by a greater political autonomy of the Mapuche people. The Mapuche people are treated like any Chilean or Argentine Spanish-speaking citizen, persisting in the view that Mapuzugun is a lower language. Then there's also the lack of seriousness from the State in the establishment of educational entities where Mapuzugun is taught in the classrooms.

**3. How is your community using Mapuzugun on the Internet?**

Online, Mapuzugun is being used very little compared to dominant languages such as English or Spanish, and even compared to other South American languages such as Quecha or Aymara. However, when comparing its presence online to other languages ​​that are about to disappear, like the Aonikenk language (also known as Tehuelche) from the Argentinian Patagonia, and many others from indigenous communities in the Amazonian rainforest, we see that there is a greater presence of Mapuzugun online today. Mapuzugun is currently being used on social media (Facebook, Twitter, Instagram) by speakers, neo-speakers, and language learners, for many communication purposes and topics, at least as far as we could notice. There are some who use it to teach and practice, others to joke and have fun (there are even some pages of memes in Mapuzugun).

{{< single-gallery
    src_img="/media/stories-content/Kimeltuwe_0.webp"
    alt=""
    source="Twitter"
    link="https://twitter.com/kimeltuwe/status/823986460407844864"
    caption="Do you drink coffee or mate? Meme in Mapuzungun. Image by @Kimeltuwe"
>}}

{{< single-gallery
    src_img="/media/stories-content/Kimeltuwe_1.webp"
    alt=""
    source="Twitter"
    link="https://twitter.com/kimeltuwe/status/1342056698022866947"
    caption="On December 25th, do you celebrate Christmas, or the victory of the mapuche people in the Tucapel battle in 1553? Meme in Mapuzungun. Image by @Kimeltuwe"
>}}

**4. What do you wish you could create or share on the Internet in Mapuzugun that is not possible today?**

It would be great to be able to post in Mapuzugun on platforms like YouTube or Facebook. Not even in terms of the interface being translated, but in terms of just being able to tag, in the menus that are available, the language as being Mapuzugun. For example, when uploading a video to YouTube or Facebook, you can't add a transcript in Mapuzugun since it does not appear in the list of predetermined languages. So, if you want to upload a transcript in Mapuzugun, you must say that it is in Spanish or English.

It seems to us that these platforms, in general, perpetuate the colonialist idea that there are languages ​​with greater value and capacity to communicate, which is a detrimental view of minority languages like Mapuzugun.

A solution to this problem is to create awareness among users, and ask them to use their own language on the big platforms instead of more dominant languages. Large volumes of currently unsupported language could make it important for companies that control the big platforms and information on social networks, to respect and better support our languages on their platforms.

In the applications that I use to give lessons, it is possible to choose the language in which the educational sheets are written, but for many minority languages ​​we only have the option "other/unknown".

**5. How does the content in Mapuzugun look on the Internet? What is present and what is not?**

Today, although there is a lot of Mapuzugun content online, most of it does not remain online for very long. For example, one can find news in Mapuzugun, but only one-off stories not about current events. It would be great if there were newscasts in Mapuzugun accounting for the events that are happening today, and projects in Mapuzugun that will endure in time.

It would also be wonderful if soap operas or series would be made in Mapuzugun, or other entertaining media. There should also be more pedagogic pages, with more audio and video in our language.

I would also like to see more translations of classical literature. I have read some works that were written in Spanish, and that were very well translated into Mapuzugun. Those works helped me a lot to advance in my study.

Importantly, Mapuzugun needs to be among the languages that are translated via Google's machine translation, and Mapuzugun should exist as an option for the "translations" proposed by Facebook and Netflix.

**6. How, where and with what technologies do you share or create content in your language?**

We use various technologies and platforms to create and share content online in Mapuzugun. We upload our content on social networks, in image, video and audio formats. Likewise, we create materials so that they can be printed or photocopied by teachers of Mapuzugun.

An example of the above is the [azumchefe.cl](http://azumchefe.cl/) page which has an online dictionary with pronunciations. The idea of ​​this page is to use it as a tool for revitalization of the language, together with lessons carried out by the [Mapuzuguletuaiñ](http://mapuzuguletuain.cl/) organization. Often, dictionaries are used in classes to teach Mapuzugun, which are not optimal. Dictionaries are not optimal either because they use a different writing system or their publication date is very old and it does not include neologisms that are used during the lessons. That is why we made this tool Azümchefe dictionary available online.

In addition, we include indications for the pronunciation of some words so that the student becomes more autonomous during their learning, and does not have to depend entirely on an educator.

Finally, the platform also uses an application called [Wordtheme](https://play.google.com/store/apps/details?id=fr.jmmoriceau.wordtheme&hl=en) that allows you to upload word listings. So, we created a database with vocabulary, which also includes grammar, examples, and definitions. If a person downloads this app, and downloads our data sheet, they carry the dictionary with them anywhere they go, on or offline, which is very practical considering that many courses are provided in remote locations without or with very poor internet connection.

The page also has basic grammatical explanations to understand the concepts that are discussed during class. Something we want to do in the future is to generate a communication study plan and a sequence of activities that students can carry out on their own. This would create a wide-spread online Mapuzugun language course, that anyone on or offline could participate in, and learn from.

For teaching Mapuzugun online one needs to use existing applications such as [Memrise](https://www.memrise.com/), [Quizlet](https://quizlet.com/), [Kahoot](https://kahoot.com/) or other learning apps, since it is more difficult to achieve an interesting learning process otherwise. With these platforms it is possible to create self-study sessions or follow-ups in between the synchronous online classes. All these platforms and the exercises for self-learning classes are uploaded to [Google Classroom](https://en.wikipedia.org/wiki/Google_Classroom) to have them stored in one place in an orderly manner. A positive aspect is that these platforms allow the teacher to create accounts for the students, create classrooms with sequences of different activities, and review the student's progress and the exercises that are yet to be done. A few of the platforms have useful features such as being able to record one's own computer screen to create explanations of exercises and other content from the teacher, like in [loom](https://www.loom.com/). Also, Kahoot and Quizlet Live support fun features like the students being able to gather online together and be organized into teams to compete and participate in vocabulary and other synchronous exercises.

**7. What is a challenge to create or share in your language on the Internet?** 

Currently, there is a difficulty regarding the normalization or standardization of writing of the Mapuzugan language. There are three important Mapuzugun alphabets. Choosing one of the alphabets implies, to some extent, that we are leaving aside what is produced in the other alphabets, or having to modify content before using it.

I believe that there is a perception regarding minority languages ​​as being strictly oral. I imagine it is thought that minority languages are particular this way, when, strictly speaking, Spanish and any language in the world, is in oral form the first time we interact with it as children. That is, all languages ​​are transmitted orally; it is at the time of schooling (and learning the writing) when our languages, like Mapuzugun, ​​are no longer present. So, in our vision, writing must be taught in order to have to have the written form of our language as a tool.

Unfortunately, due to historical processes, we do not have our own writing, since we use the letters of the Latin alphabet. The Mapuche people have a rich written tradition that dates back to the 16th century, when the first Mapuzugun grammar was written. In the 19th century, the first Mapuche authors emerged, and thanks to their written works today we have a written heritage that otherwise we might not have.

In any case, the multimedia nature of social platforms allows Mapuzugun to be broadcast by many means, and it is good that there are many forms of Mapuzugun (written, oral, etc.).

We leave you this link where we talk about the [History of Mapuche writing](https://kmm.cl/uncategorized/historia-de-la-escritura-mapuche/).

{{< single-gallery
    src_img="/media/stories-content/Kimeltuwe_2.webp"
    alt=""
    source="kmm.cl"
    link="http://kmm.cl/"
    caption="If the day is good, the heart rejoices. Image by Kimeltuwe"
>}}

**8. Why is it important to bring our native languages ​​to the Internet?**

Mapuche youth and children grow side by side with technology and the Internet. The internet is a space where these people can approach Mapuzugun. With the aforementioned platforms such as Quizlet, [Learning Apps](https://learningapps.org/) or Kahoot, you can give feedback to children through the device they use all day. Since these applications can be used on phones, tablets and computers, there are many ways to participate in lessons as students. Also, teachers can access through their devices and instead of doing only written vocabulary tests, can design more interactive activities for students even when everyone participating are in different physical locations.

However, it should be mentioned that internet access is not guaranteed in the countryside, and many students do not have an internet connection or do not have access to a computer.

The stories of our people have to be written and they need to be written or spoken in Mapuzugun. There are some groups and some people who research and write the history of our people from their perspective, but we still cannot find our history at a macro level (as in school books). The thinking and the pen of the predominant society still rules. Our stories are not necessarily those of great heroes as the colonial and postcolonial States celebrate. Our history is the story of each Mapuche who survived adversity and violence. The woman who had to migrate to the city to get a salary, the women and men who returned from the city, but there was no more room in their [*lof*](https://en.wikipedia.org/wiki/Lof), and they had to go back to the cities with their roots cut off and with no place to return to. These experiences and the memories of each Mapuche person constitute our collective memory as a people.

*This interview was conducted in Spanish. It has been translated into English by the author.*
