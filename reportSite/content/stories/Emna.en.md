---
author1: Emna Mizouni 
author2:
title: "Why do we have to write in another language?"
date: 2020-01-01
weight: 4
slug: "write-in-another-language"
illustration: Why_write_in_another_language.jpg
illustrationAlt: Four people work together in a garden. Narrow, angular waterways frame sections of greenery. In the background stands an old building with big, open, arched doorways that reveal the warm yellow sky peeking over green foliage. In the foreground, two people with long hair dig a hole to plant a USB tree. Behind them, we see two USB trees growing happily. Two people work in the middle distance to harvest fruit and care for the plants in the garden.
illustrationauthor: Illustration by Maggie Haughey
translationKey: emna
hasaudio: true
imageauthor1: Emna_Mizouni.webp
imageauthor2:
summary: "In Digital Citizenship, we were interested in supporting Whose Knowledge? in this research to reach out to African and Arab content creators and consumers to share their experiences in posting content in their own language and create more visibility and online exposure for their cultures. We were able to interview practitioners and users from Tunisia, Uganda, and Sudan. Our interest was to understand the African cultural background, and when we started to interview them, other practitioners from the Netherlands, Singapore and India, wanted to express themselves and to give their views on the topic."
---

## Introduction

*(This is a transcript from the original audio contribution)*

Personally, as an interviewer, it was a very interesting experience to interview people about their experiences, and how they find the struggle, or why they choose to write in different languages. It was very important to understand that there are burdens (like technological burdens), and also to find out more about the African cultures. It's a very forgotten culture it appears, and with this study I was very very... It's so close to my heart to showcase Africa differently. Not the cliché of Africa and its culture, not the exotic type of culture, but to showcase really what is happening in terms of languages: why we have little about the African culture, why little is being documented in our native languages.

It's very important to take this into consideration. I mean personally, I'm an Arabic speaker and it was important for me to see why people in my own language would select Arabic instead of the Tunisian dialect, which is our first, very first language. And then, I would see why the Amazigh culture is so absent from Tunisia, for example. It's so unfortunate that we couldn't interview some native Amazigh Tunisians to understand why the language is dying in Tunisia. At the same time, it was a very good experience to find people who are very eager on posting online, on using and documenting, using the Tunisian dialect which they consider as a language. And also to see that for different countries, for example, the impact of the colonization — in the way that we study it, our access to knowledge, and the way that we decide knowledge is most relevant for us.

It was very, very good to navigate through those interviews from different people, different backgrounds, people that I knew but I did not know their position in terms of languages. And people that were completely new to me, acquaintances or people I just met in the frame of this study, it was very very important. I think it’s the very beginning of an in depth-study, if we wanna do more, we would be able to do more.

I joined this study for one reason, for a personal reason at the very beginning, and it was a very good opportunity for me to do it under the hat of Digital Citizenship, which is an initiative for internet users. The most vulnerable internet users i.e. adolescent girls and women, women journalists, women in politics, women activists, all of them. What drove me into this was the need of understanding what was going wrong in terms of our languages, whether they exist online or not, what the burdens are, and then it turned into a more passionate journey to understand that. So it was not only like serving the initiative, the Digital Citizenship initiative, or serving the knowledge, but also going into a journey of knowing what was happening in this giant internet, and why we were absent, why we clearly were absent. As a knowledge advocate that meant a lot for me. I think that was the first incentive triggering...the trigger behind this was the struggle, for example for me as a Wikipedian, to find references in our own languages. When I say our own languages it’s not only the Tunisian language (our dialect), or the Arabic language; but also when we work across Africa, we find a huge gap in terms of resources and references. The source of knowledge is not our language, the source of knowledge as in print or as online is not in our languages. That’s very unacceptable, and that is why I joined this study. Definitely, this little experiment helped me a lot to see different aspects, helped me to see the struggle from the very beginning to learn how to use IT devices, when we talk about... everything is in English, few things are in Arabic.

It's very difficult to understand for illiterate people, for example, who are the ones who have the knowledge of our history, to know how to use the IT devices and the technology to document our languages, whether by audio, or typing, or whatever style... by blogging... It's very different when it comes to them, because it comes in a different language, they do not... if they are illiterate or if they are Tunisian, like all elder people, they don't speak English like the new generation for example, so it's difficult for them to understand. If they're illiterate they don't speak French sometimes, so it's very difficult for them to access, and to do a blog for example. And the materials in Arabic are limited compared to those languages such as French, which is a colonial language in Tunisia. But English is a different language that we don't have access to, we did not have access to it for years — not as much as we should.

So yeah, it's a journey. It's a little experiment that, personally, I am proud of taking part of. And also it's an added value for Digital Citizenship to understand when we deliver workshops for adolescent girls, to understand what they are going through. It's very important to take this into consideration while we prepare for workshops to edit Wikipedia, or to learn how to blog, and to learn how to protect themselves online. It's very important that we rely on translated material. But also that we rely on maybe facilitating more, and making it easier to learn for them. Because it is not their first language, they are first time learners, they are not experts, or they do not speak fluently the colonial languages at some point.

## About the interviews conducted in this research

In [Digital Citizenship](https://twitter.com/DigiCitizensOrg), we were interested in supporting [Whose Knowledge?](https://whoseknowledge.org/) in this research to reach out to African and Arab content creators and consumers, to share their experiences in posting content in their own language, and create more visibility and online exposure for their cultures. We were able to interview practitioners and users from Tunisia, Uganda, and Sudan. Our interest was to understand the African cultural background. When we started to interview them, other practitioners from the Netherlands, Singapore and India, wanted to express themselves and to give their views on the topic. We aspired to understand the reason for the massive use of colonial languages on the internet and the burdens they face.

In the period between the autumn and winter of 2019-2020, a series of interviews was conducted by Emna Mizouni in the frame of the research on “Decolonizing the Internet’s Languages”. The questions were addressed to influencers, digital practitioners and simple users about:

* Their mother tongue,
* The historic/personal background (optional)
* The language they use while sharing content online, and if it’s different from their mother language why they use it, 
* The perception of their mother tongue (Arabic or any African language) in their communities
* The struggles and obstacles they face while using any other language than English or French (colonial languages): technical, lack of internet connectivity and accessibility, lack of devices, socio-cultural barriers, etc.
* Are the search engines they use friendly or compatible with their mother tongue.

Listen to the interviews below:

{{< interview-block
    title="Interview 1 - Amal Bedhyefi"
    author="Amal Bedhyefi (Amal.Books)" 
    text1="is an English Language, Literature and Civilisation graduate, who is now pursuing a masters degree in International Relations and Diplomacy in the High Institute of Human Sciences of Tunis. Fond of reading, Amal is a part-time Editor and the first Tunisian book blogger and Bookstagrammer. She shares her love for books through social media, where she motivates people to read."
    text2="In this interview Amal talks about the massive use of English to reach out to a wider audience for online content creators."
    text3=""
    audio="https://archive.org/download/set-of-interviews/File1_%20Amal.mp3" >}}

{{< interview-block
    title="Interview 2 - Brahim Boughalmi"
    author="Brahim Boughalmi" 
    text1="is an Educator, Fulbright alumni and active member of Carthagina (Heritage and History oriented organization in Tunisia)."
    text2="This interview is in Tunisian Arabic, the following is the English translation:"
    text3="The languages that I use online most of the time are Tunsy (Tunisian dialect) and English. Tunsy for everyday topics, it’s easier for me because obviously, it’s my mother language, and it’s easier for me to communicate that way especially when in private messaging. If it’s my facebook account, most of it is in English, because I have diverse friends from other countries, so I can deliver the message. Some other times, when I speak about very personal topics I use English, because I think Tunsy could be a bit limited in emotions. We are a nation that doesn’t express its emotions. The second reason is that English gives me the feeling of detachment from myself so I became expressive without being emotionally involved especially when it’s very personal."
    audio="https://archive.org/download/set-of-interviews/File2_%20Brahim.mp3" >}}

{{< interview-block
    title="Interview 3 - Elisabeth van der Steenhoven"
    author="Elisabeth van der Steenhoven" 
    text1="is an enthusiastic member of the El-Karama community since 10 years. She is also a member of the Dutch National Advisory Committee on Foreign Affairs and currently a member of the Dutch Ministry of Health committee on digital tools against Covid-19."
    text2="In this audio interview, Elisabeth talks about the use of Dutch online and the lack of use of Arabic and African languages. The importance of the use of native languages and the preservation of that heritage."
    text3=""
    audio="https://archive.org/download/set-of-interviews/File3_%20Elisabeth.mp3" >}}

{{< interview-block
    title="Interview 4"
    author="" 
    text1="The interviewee is a digital trainer from India."
    text2="In this interview she talks about Hindi and the use of English in trainings."
    text3=""
    audio="https://archive.org/download/set-of-interviews/File4_%20Hindi_language.mp3" >}}

{{< interview-block
    title="Interview 5"
    author="" 
    text1="The interviewee is a digital trainer from India."
    text2="In this interview she talks about the struggle of finding synonyms of technical terms in the local languages while teaching IT and digital."
    text3=""
    audio="https://archive.org/download/set-of-interviews/File5_%20Language_first_learners.mp3" >}}

{{< interview-block
    title="Interview 6 - Mohannad"
    author="Mohannad"
    text1="is a university student from the South of Tunisia."
    text2="In this interview he sustains that the use of English gives more results when searching online."
    text3=""
    audio="https://archive.org/download/set-of-interviews/File6_%20Mohanad_Bennasr.mp3" >}}

{{< interview-block
    title="Interview 7 - Normalland"
    author="Normalland"
    text1="is a prominent Tunisian blogger."
    text2="This interview is in Tunisian Arabic, the following is the English translation:"
    text3="I write online in Arabic and especially in dialect. For me this is something that I worked on before and convinced myself that there is no reason to write online in a different language than what I use everyday. So that’s why even in my blog, I tried to develop the dialect (Tunsy) in a written way to make it a written language and readable. I believe the Tunsy was so undervalued and people think that the standard Arabic is more important and some think that the Tunsy isn’t a proper language but a hybrid between Maltese, Arabic and French. For me, I believe Tunsy is a rich language and similar to the Arabic language but it’s more rich because it didn’t remain closed on itself but was more open and flexible to adopt words from French, English, Italian and the near dialects. So the Tunsy is more open, that’s why I chose it and always defend its use."
    audio="https://archive.org/download/set-of-interviews/File7_%20Normalland.mp3" >}}

{{< interview-block
    title="Interview 8 - Shem"
    author="Shem"
    text1="is from Uganda."
    text2="In this interview he talks about the importance of the school you go to and the impact of colonialism in the language that his country uses."
    text3=""
    audio="https://archive.org/download/set-of-interviews/File8_%20Shem.mp3" >}}

{{< interview-block
    title="Interview 9 - Ter Yang"
    author="Ter Yang"
    text1="is from Singapore."
    text2="In this interview he sustains that the English and Chinese content online exist, there is no issue with materials in either language."
    text3=""
    audio="https://archive.org/download/set-of-interviews/File9_%20Ter_Yang.mp3" >}}

{{< interview-block
    title="Interview 10 - Mohamed Gamil"
    author="Mohamed Gamil"
    text1="is a Brown person who loves peace, from Sudan."
    text2="This interview is in Sudanese Arabic, the following is the English translation:"
    text3="Actually Sudan is a country with a lot of tribes and a variety of traditions and customs. So the North speaks Arabic dialect (the same I am using) and that’s because of colonialism of course. While for the East and West, the tribes speak a different local language, only them are able to speak and understand it. For people from the North it is very rare to find someone who understands it unless s/he lived there and interacted with them. The origin of our language is the Cushitic language.The Cushitic and Nubian civilisations are our reference. When the High Dam sank, we lost the Cushitic identity, we didn’t find a dictionary to decrypt the language and translate it into other languages. The Nubian language, however, is known and translated. Nubian language exists even on Huawei mobile phones as one of the languages in the mobile settings. For the East and West languages, they are not written (or maybe they are, I’m not aware, I need to verify). For the North, we speak Arabic. We are Africans and Arabic speakers, not Arabs."
    audio="https://archive.org/download/set-of-interviews/File10_%20Gamil.mp3" >}}

{{< interview-block
    title="Interview 11 - Mohamed Gamil"
    author="Mohamed Gamil"
    text1="talks about the Nubian Language."
    text2="This interview is in Sudanese Arabic, the following is the English translation:"
    text3="We couldn’t decrypt the codes because the High Dam sank. All the monuments were obliterated in Sudan so we were never able to translate the Cushitic language, the Nubian language is translated, however. The common point between us (Sudanese) and the Egyptians is the Nubian language. We then started to speak in Arabic because of colonialism although we were colonised by the Ottomans like the Egyptians and we were even colonised by the Egyptians and the British for some other time. That influenced our Arabic language. We have a lot of words in the Sudanese dialect that are similar to the Egyptian language. A lot of communality but generally our language is clear. We have a lot of tribes that speak a different language for each (East and West), a coded language that we don’t understand similar but not really the Nubian language depending on the tribe. The West tribes have their own language; each tribe has its own language. The same for the East. The North speaks Arabic, the South (before the division) speaks the southern language. After the division, South of Sudan has a different language and Sudan remains with the North Arabic language, East and West languages they are similar to the Amazigh language. It’s totally absent from the internet, it’s more an oral than a written language. The Nubian language is written. Huawei company installed two options of the Nubian language on their mobile phones recently; the Nubian that we speak in the Valley of the Nile. That was outstanding news for the Nubians, I am Nubian, and I know they complained a lot about not using their language on phones."
    audio="https://archive.org/download/set-of-interviews/File11_%20Nubian_language.mp3" >}}
