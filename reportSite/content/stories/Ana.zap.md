---
author1: Ana D. Alonso Ortiz
author2:
title: "Ga ze dill wlhallka lo lbaka ntil nchay lliu"
date: 2020-01-01
weight: 1
slug: "dill-wlhall-on-the-web"
illustration: Is_the_Dill_Wlhall_on_the_Web.jpg
illustrationAlt: A parent carries a child on their back as they walk through a forest, and the child grips one of their braids contentedly. Around them, birds watch them and sing. The trees part to reveal a village with a radio tower at its center. The typical radio dishes on the tower are instead brightly colored flowers, transmitting to the community below.
illustrationauthor: Illustration by Maggie Haughey
translationKey: ana
hasaudio: true
imageauthor1: Ana_Alonso_Ortiz.webp
imageauthor2:
summary: "Nhada lía Ana Alonso Ortiz, nhaka be'nn wlhall yalharg. Bzeda dan nhe Antropologia, nha' ba su nez wayull wzeda dan nzi'i linguistika lo xkuelh xhena le *Universidad de Massachusetts* da lli yell Amherst, lhall benka. Legakze nuna txhen bika nzi'i *Colectivo Dill Yelnban* gani lltallneto da wlhall ke llo enchna bi nit dill wlhallen nha' llagoto ltipe enchna gumbielhe ben zan len, le gatesen ba nllilhgllo lhao yell liuni. Gani wea dill ke xtishllo wlhall ka, ga sagseaken lo dan numbiello ka internet, wadia kuent ke dill wlhall ka shlhab *Instituto Nacional de Lenguas indígenas* INALI ka dill wlhall cha dill xhon."
---

Padiuxh yulhultelhe tzenhai nha' llak sh'lhablhe dillki, shawelha shwapa lhe diuxh lo lba ki ba ntil chay lliu. Lo da ki ba llak llayej dill kan shak nnalla ke dill wlhall ke sho ka leska llaklhenen enchnha shayunsho xbab gate se xtishllon lo lba ki nzi'i "redes sociales". 

Nhada lía Ana Alonso Ortiz, nhaka be'nn wlhall yalharg. Bzeda dan nhe Antropologia, nha' ba su nez wayull wzeda dan nzi'i linguistika lo xkuelh xhena le "Universidad de Massachusetts" da lli yell Amherst, lhall benka. Legakze nuna txhen bika nzi'i "Colectivo Dill Yelnban" gani lltallneto da wlhall ke llo enchna bi nit dill wlhallen nha' llagoto ltipe enchna gumbielhe ben zan len, le gatesen ba nllilhgllo lhao yell liuni.

Gani wea dill ke xtishllo wlhall ka, ga sagseaken lo dan numbiello ka internet, wadia kuent ke dill wlhall ka shlhab "Instituto Nacional de Lenguas indígenas" INALI ka dill wlhall cha dill xhon.

## Yel llulne ke dill wlhallka

Yiz galgayoa chine, benn'ka nhun lhallnha yel koxhchis bsogake bia ke dill wlhall ka, yichnhi benen lí enchna wayunbielhe yulhultesho ka to zak dill wlhashka. Yichnhi kakse llaklhenen lhauze nhadell llin gak.

Benn'ka llnebia llío ba shaklhenake enchnha wayogen nha' waneshon, lhaose llíon llayal kosho ltipe, llión numbiello dill wlhall ke sho ka, llíon de gaklhen lueshgsho ench waxen. Nnalla, lba ki shaklhen ba llatil llachayen lliu gatese dallo, ba dallo shlepllo bitese lo interneten, cha ba dallo tsogllo ka da shak ke llo lakse llagtielhesho lhat kat tsogllo. Nnalla wdia ga llagtielhe lliu benn' llulnhe dill wlhall kat llunenllo llin da nzi'i internet.

Nyan lliu be'nn llulne dill wlhallka, katen shnello dill wlhallen lo yichjllo nsapllo ka yitj dill wlhallen nha' gaklhe zu beshllo ka be'nn wlhall, kelhe toka llnezllonan lo xbab ke llonan nunchillon, ka' dill llwe nolhe yutlen nzi'i Yasnaya Elena Aguilar.

Yel llulne ke xhtishllonan nsap gaklhe zu bezllo, nunchin dill gaklhe bxhe tutu yell, gaklhe nzi'i no ya yew, gaklhe bayejnye xhustaoshoka yelnbanan, nha' ka llaneshon nu xin xosuasho nha' ka sagsden kon kuense.

Lliu numbiello gaklhe nzi'i lhe to to da ka, le xuxtaoshokan bneaken kuen kuense xinake ka, dill wlhashka nbanaken katen llneshon, lakze bi tzogshon, kana byojse dill wlhallen nhak nhakte ka bsogaken, nnalla lliu bill llayaxhgello wsogllon.

Nnalla, llne bia dan llneshon lhao dan wsogshonha, legakze len ba snhebialle lo interneten, kanake nha' toto bi shlpello lo youtube, facebook, whatsap nha' instagram llwello dill lhaose bi tsogllon, nha' cha ba tsogllo ba llunhenllo llin dill xtill, bi llayakuelhello ka llin gun dan llulnello dill wlhallen gullgonhi. Llaslhashllo llunenllo llin ka da ki kanhake lldeten bxhen nha' len llaklhenen shlhelhello nu bichluelljllo cha nhu da de lhao yell wlhallkellon.

Yewlhulze benn' ka zu yell wlhashka llunlhenake shi dan le "internet" nha' facebook, gani shdegake kuen kuense kat llal nu lni yell , cha kat shkuell wakuell, kat llyia nu waya, cha yixgeake nu da shunten da blhao.

Kan bi na lha yishgue lhe' (COVID 19), dan numbiesho ka facebook ba shunen guklhe llatil llachayen benn' wlhashka, katen llat benn' lhao yellen cha nu gayublhe, nha' benn'ka bill lla yellen lldegake bxen ka chak lhashbenka. Legakze dani (facebook) llaklhenen llden benhi (radio) ke yell wlhallka nuteze benn' ba shak gunlhenen shin, bill naken que tu kuenze.

Gan nunha txen bika nzi'i Dill Yelnbán, shlepto dill wlhallen enchna gumbielhe benn' yublhe dill wlhallen, nha' legakzeka llagoto ltipe enchna wpen bi llaban, cha nhu bi llenlhe wased len, llunto txen benn'ka llne lena na lhente benn' bi llak llne len. 

Nheto lladeto dill wlhallen nu lo bxen, leska yixweto da shunen blhao lhao lhashton enchna bi wnebia dill xhtilhen, bi llenlheto nit dill wlhallen kelhena llunento llin interneten enchna yolhulsho wayajniello nha' wayunbielhello shunen blhaon wanesho xtishllon gateze zello, gateze dallo.

Lbaki ndub lo intenerneten, nlla nllate ka llunenllon llin, de dan nzi'i youtube; gani shak lldello bxen ke gaklhe shak nu yelguao, cha lldello dill nbalhas nha' biteze bxen ke yellka. Dan le whatsapp shaklhenen lliu shixguen nu da shun blhao, lluelhenllo bich lueshgllo dill, cha tselsho da tzagnielhe shio, legakze nu dill nbalhaz cha biteze dill yublhe.

Dan le Instagram shalhuelhen biteze bxen nha' llakte lldallon dill wlhall, nha' leska "blog" cha "páginas web" shlepllo nu dill wlhall.

Da ki ba blhaba, bibi saken, bi yixgllon cha', nha' kelenha benzan shunlhen leaken llin. Benn'ka nao ka da ki nha' nunaken xchinake shchabake nha' llunake da le "aplicación", aplikasion le ka daka yo lo celular ke llo ka, ganha llak shlepllo dill nha' bxhen nha' llak llwellon kon bale lhas llenello, ka danhi wak waklhenen bika llenlhe wsed xhtishllo. Ka kuen dillen llwello ganhi binha gat "aplicación" ke', nha' zin shin gaklhe gak ke'.

## Bixhchen bi llalluyello wdello nha' wlhabllo xtishllo wlhallka lo interneten?

Lakse ba nyan lbaka llde dill wlhallen lo interneten kanhe ba byon nez nha' na sull da gak.

Kanhake xhtishllonhi nyanll benn'ka llnhe len ka benn'ka tsoj len, nha' youlhul lba ki ba nhunhaken enchchna wden dill ka ba nyoj, llabekillten xhtishllo wlhallka kale. Kelhe tuse dan yojenan nhak bnhi, leska su kat bi llallel lo maknka gaklhe yoj xhtishllon. Kanhate llalhale benn' wlhallkelloka llwe'ake dill gaklhe yoj xhtishllon nha' kanhate bchuyake yojen kan nyoj dill xtilhen encha gukse bayilhjlashllo gaklhe wsojllon chinake.

Kanhake kakse zed ba nhak wachallo kan ba tsojllonha, nna su kosho ltip benn' llenlhe wsoj len. Lliu benn' ba tsoj len llayal wasedello benn' yublhe nha' wpello legake enchnha wsojake dill wxhill lliu, xhthill bayixh, xhtish bichluelljllo, dill dsajnielhe lliu gá llun llin yixh xmell, wdallo lhe tiend kello ka xhtishllo. Llayalkse kollo ltipe, wsojllo nha' wlepllo da llenlhello lo interneten enchnha ka wasajnielhe kwinllo kelhe tuse dill xtilhnhan sake, tus ka saklhenen xhtill lhao ga ka, xhtill franceska, xhtill mixhka, cha konto dill.  

Kanhate byoj xhtishllon nhu le yaj, nhu le xha bayixh, nhu le yich, llion benn' kub ba bsanlashllon. Nha' lakse llyojen dan llnellonha sakase daxenulhe, ganhan nunchillo ka llajnielhello lla-yelh, gaklhe yitglhenllo yel gut nha' lhente yelnban, gaklhe llajnielhello yel korchis, gaklhe sulhenllo yell liu. Youlhul lhena ba badegake lliu konkuenze. Chinhak yegake lliu willjonhi lhan dill wlhallen enka suesello wsojllo ka tu da nbanhe xhtishllon, kelenha llayej dillnhi. Nbalhazgulhe nhun interneten llwen latj bitese yoj, lhause gate llagan dillka bi llyoj? gaklhe gak ke dill wlhallka bi llyoj. Llunksen blhao wtanello tu internet da ko chau ka llunllo shnello enchnha cha lhis ba bniten, bika ze sak wak wanezakebe gaklhe bel bnellon, gaklhe bnenhen. Gaga su lo interneten ga gak kochaullo dill wlhallen, sle dillka llyoj daxhenulhe yo chao ganha. Ka gakxha gak ke xhtishllon cha?

Bitese bi llenlhello siksen llin, kaksen de gunllo lhen interneten, bi llayal wxhenlanllo benn' yublhe, llayal nénllo enchnha kollo ltipe encha wde nha' gunchi lbaki xhtillshon.

Kanha got tu da bsiake "radioteca" dani benen shin chda yiz nha' bsapen dill llul ne benn' lhauze bill bzelhawuke llin ke, bill begake legake mesh enchnha wzelhauwake gapchiake dillka. Nha' lakze nna ba de da si bxen, bi nunake interneten enchna gun chin dill wlhall ka.

Lakse ba wukuelhello ka llinha llayal gak bikse nha kosho xhneze enchnha wachallo interneten enchna tabaye xhtishllon len, kelhe dan nhakllo yell chinkon, nha' bi nyanllo, llakello bi zak xhtishllon. Benkse lliu nha' lhebse ka sakllo. Kelenha llayal kuekllo to "audioteca " cha "videoteca" da gak kello, ga kuchaosho bxen ga kuchaullo da wezenaisho, ga youlhulsho wello dill gaklhe llak lní, ga llun llin yixh xmesh, lha nu ballixh, yay cha ya yeo. Tokse ka sak dill llyoj ka dill ka bi llyoj. Lhao gall milh dill ka de lhao yell liun, chon milh gagayua taplhaljchopen bi llyoj, ka walle lhebse nhak dill ka nyoj ka da ka bi nyoj. Nha' da ka nyojen nhaken xhtish benh wnebia ka benh ka su chla nhis yell liu, Chinoka lhente xhtill benh lhao gaka, lhause kelhe tose xhtill legaken sak.

Interneten kan numbiello nna lla bi nden kuent xhtishllo ka, bi nuen latj enchnha tabay xhtishllon len. Leska bi nunen latj enchha lliu kosho xhnese bi llenlhello wlepllo, gaklhe llenello wlepllon, chinak nden lliu, nlla ka llak ke xhtishllo ka nnalla.

## Da gunllo xhbab nha' wello dill

Enchna lhelhello kunkuenze xhtishllo wlhallka lo interneten llunen blhao tabayllon. Gunllo xhbab nhu llwya daka shlepllo? gate shlepllon? nhun chi interneten bi lello, bi llunllo, gate sullo? le bayate lenan ba shlepllo luena, nha' ke chnhalen llganaken luena. Zu bal lliu to llunhenllon llin, lhause bi neslhello benn' san llban bxhenkello nha lladegaken benn' yublhe encha gunhake da bi nhak wen, nha' bayullnha lliu lladubellon. Nhallen nhia, bi llayal to wyaxhenzllon, llayal nezlhello bi nsa to to daka nha' gaklhe llenlhe lliu chollo lo interneten.

Leska de wanablhe kwinllo, cha lliu llenlhello yoj dill wlhallen lo internet, gaklhe gunllon? Wsojllo xhtishllon to lo internet nyoj dill xtilh, cha wadello faceboken xhtishllo? cha chollo tu lo facebok wlhall nha' wsojtello dill wlhall. Ka ba llun to bi numbielhake ka Zapoteco 3.0 Ba badebe xhtillbe tu pagina nzi'i mozilla firefox nha' kat llobe lo internet ba llayilhgbe da llenhebe lo mozillan nyoj xhtishbe.

Lliusen llayal kokse xhnese ka da ki enchnha wachallo interneten. Enchnha willjonhi wasaklhe interneten xhtishllo kat bi wnhablhello len nha ka wayilhjten len, nha' ka kuekllo dill xtilhen chull kale cha ke nhu bi da yoblhe.

Ka yisen sellonha, galj gayoa galj bi nde kuent lo interneten dillka llulnhe, dill bi nha yoj kon kuense. Lllayal walla kadanhi kanhake dasangulhe dillka nhit yell liunhi bi na yojaken, nha ka walle youlhensen lan yi chi yiz. Chinhak nun interneten ga kochaullo dill ka llulnesho, da ka bi nyoj, daxhen da gak gunhenllon llin enchnha gunchillo xhtishllon. Kat ba su nez yalhan xhtishllon, wayach benn' nha' bida'o llnhelen, kat ba bayach benn'ka llne lenha llin xhenulhe sin enchnha waxuen, dasan bill llaxhue. Katen tsanllo shnello xhtishllon, ba llnhitllonha.

Lliu llunhen llin interneten de wnablhe kwinllo cha llenksello chollo lo interneten lakse neslhello nhaken nxull kanhake sute ka kwanake da ka shlepllo, nha' leska llalhalen llbekyishen xhtishlloka kale. Llunha lhe yex enchnha gunllo xhbab ka dillka ba bsala ganhi: gaklhe gunchillo xhtishllon?, gaklhe tabayllo wsojllon?, gaklhe llenlhello len xhtishllon lo interneten? nhu llwello latj lhelhe daka shlepllon, bi ka proyecto gunllo da gakse ke llo? cha wzelhaullo gunhenllo llin da ka ba nbajse benn'ka, ka nu facebook cha youtube?

Legakseka llunha lhe yexh enchna lliu numbiello benh tzed kada ki le "web" weake latj cho lo interneten dill ka llyoj nha' lhense dillka bi llyoj, enchna ka gak nhello tuse ka sak dillka nha' ndeteaken kuent lo lbaki ba ntill nchay lliu.
