---
author1: Paska Darmawan
author2:
title: "Cahaya Redup: Hambatan dalam Membuat Konten LGBTQIA+ Daring Berbahasa
Indonesia"
date: 2020-01-01
weight: 10
slug: "flickering-hope"
illustration: Flickering_Hope.jpg
illustrationAlt: Two people, both with long, dark hair, face away from the viewer and into a large crowd. The person on the right wears a cochlear implant over their right ear, and has a batik with an intricate pattern in the colors of the lesbian flag. The person on the left wears a batik with diagonal stripes and circles in the colors of the nonbinary flag. In the center distance, a person raises the rainbow pride flag in the air. They are at a pride parade in Indonesia, and in the distance we see a mosque and its speaker tower, celebrating pride along with the parade attendees.
illustrationauthor: Illustration by Maggie Haughey
translationKey: paska
hasaudio: false
imageauthor1: Paska_Darmawan.webp
imageauthor2:
summary: "Tumbuh besar di kota kecil dengan akses terbatas ke dunia luar, saya harus mengandalkan internet untuk menemukan berbagai informasi tentang identitas gender dan orientasi seksual. Ketika saya masih remaja, saya kurang bisa mencerna informasi berbahasa Inggris karena latar belakang sosial, ekonomi, dan pendidikan saya. Oleh karena itu, keberadaan konten LGBTQIA+ berbahasa Indonesia sangat saya butuhkan untuk menjawab berbagai pertanyaan tentang identitas diri saya sebagai individu *queer*."
---

{{< single-quote text="The few voices defending the rights of LGBTs are being drowned out, or rather bullied, by the growing chorus that has turned the LGBT community into Indonesia's public enemy No.1" cite="#" author="Endy Bayuni, Editor-in-Chief of the Jakarta Post (*)">}}

Tumbuh besar di kota kecil dengan akses terbatas ke dunia luar, saya harus mengandalkan internet untuk menemukan berbagai informasi tentang identitas gender dan orientasi seksual. Ketika saya masih remaja, saya kurang bisa mencerna informasi berbahasa Inggris karena latar belakang sosial, ekonomi, dan pendidikan saya. Oleh karena itu, keberadaan konten LGBTQIA+ berbahasa Indonesia sangat saya butuhkan untuk menjawab berbagai pertanyaan tentang identitas diri saya sebagai individu *queer*. Sayangnya, sangat sulit menemukan konten LGBTQIA+ daring berbahasa Indonesia yang bersifat positif dan edukatif. Apabila Anda memasukkan kata pencarian terkait topik LGBTQIA+, sebagian besar hasil pencarian akan didominasi oleh konten-konten yang berasal dari media arus utama dan media keagamaan yang cenderung menggambarkan komunitas LGBTQIA+ secara negatif.

Individu LGBTQIA+ di Indonesia seringkali menghadapi penolakan dan diskriminasi dari masyarakat. Laporan menunjukkan bahwa sekitar 87,6% masyarakat Indonesia merasa terancam oleh keberadaan komunitas LGBTQIA+ [^2]. Sementara itu, jajak pendapat dari Arus Pelangi menunjukkan bahwa 89,3% individu LGBTQIA+ di Indonesia pernah menjadi korban kekerasan [^3]. Pada bulan Juli 2018, misalnya, dua pria di Aceh dicambuk di depan umum setelah tertangkap berhubungan seks [^4]. Beberapa waktu kemudian, di bulan November 2018, oknum polisi di Lampung menangkap dan melecehkan beberapa transpuan [^5]. Selain itu, terdapat juga beberapa peraturan daerah yang melarang keberadaan dan segala aktivitas dari kelompok LGBTQIA+. Contohnya adalah hukum Kota Pariaman yang melarang "tindakan tidak bermoral" antara pasangan sesama jenis dan "perilaku waria". Di sisi lain, tidak ada peraturan di tingkat nasional maupun regional yang melindungi individu LGBTQIA+ dari diskriminasi dan penganiayaan.

Kurangnya perlindungan dari pemerintah dan tekanan yang terus meningkat terhadap komunitas LGBTQIA+ mengakibatkan kurangnya konten positif di internet. Hal ini dikarenakan adanya ketakutan masyarakat untuk menyatakan dukungan mereka secara terbuka terhadap komunitas LGBTQIA+. Berdasarkan laporan dari Kemitraan, ada lebih dari 180 laporan berita negatif terkait LGBTQIA+ di media nasional pada bulan Februari 2016 dibandingkan dengan hanya sekitar 50 berita yang menunjukkan dukungan. Laporan ini juga menunjukkan berita negatif tentang komunitas LGBTQIA+ sebagian besar dirilis oleh media lokal dan media yang berafiliasi dengan agama [^6]. Laporan yang tidak seimbang tentang isu-isu LGBTQIA+ berakibat pada mobilisasi kebencian terhadap individu LGBTQIA+, yang dapat dilihat dari meningkatnya jumlah protes dan serangan dari kaum konservatif sejak tahun 2016 [^7].

Untuk alasan ini, saya akan membagikan pengalaman saya sebagai individu *queer* dalam mencari konten LGBTQIA+ positif dalam Bahasa Indonesia. Selain itu, saya juga telah mewawancarai beberapa orang dari komunitas LGBTQIA+ dan *allies* untuk mengidentifikasi tantangan dalam membuat konten positif tentang LGBTQIA+ di internet. Saya harap tulisan ini dapat memberikan gambaran tentang *status quo* konten LGBTQIA+ berbahasa Indonesia dan berbagai kendala dalam menambahkan lebih banyak konten dari komunitas LGBTQIA+ Indonesia ke internet.

## Tantangan dalam Membuat Artikel Daring

Mesin pencari sering menjadi titik masuk bagi para individu LGBTQIA+ muda yang ingin mengetahui lebih banyak tentang isu-isu LGBTQIA+. Ketika saya masih mengenyam pendidikan di bangku sekolah menengah, saya mulai memiliki begitu banyak pertanyaan tentang identitas dan preferensi seksual saya. Untuk menjawab pertanyaan-pertanyaan tersebut, saya sering mencari artikel melalui mesin pencari. Beberapa topik yang sering saya cari adalah posisi agama terkait isu-isu LGBTQIA+, penjelasan ilmiah tentang homoseksualitas dan *gender dysphoria*, serta kisah-kisah inspiratif dari individu LGBTQIA+ lainnya. Sayangnya, saya seringkali kesulitan menemukan konten-konten tersebut dalam Bahasa Indonesia.

{{< side-fig
    src="/media/stories-content/Flickering_Hope_image1.jpg"
    alt=""
    caption="Contoh hasil pencarian Google untuk kueri 'homoseksualitas'"
    title="Gambar 1" >}}

Beberapa tahun setelahnya, ketersediaan konten-konten LGBTQIA+ berbahasa Indonesia di internet tidak banyak berubah. Jika kita mencari kata "LGBT" atau "homoseksualitas" di Google⁠—mesin pencari terbesar dan terpopuler saat ini⁠—kita akan menemukan begitu banyak hasil pencarian yang mengandung kata "penyimpangan", "dosa", dan "penyakit". Selain itu, terdapat juga banyak artikel berbahasa Inggris yang masuk ke dalam hasil pencarian, meski pencarian tersebut telah diatur untuk hanya menampilkan artikel-artikel dalam Bahasa Indonesia. Observasi tersebut menunjukkan setidaknya dua hal mengenai keberadaan konten LGBTQIA+ berbahasa Indonesia di internet. Pertama, jumlah konten LGBTQIA+ positif berbahasa Indonesia masih sangat terbatas dan seringkali tidak berada di bagian teratas dari hasil pencarian. Kedua, mesin pencari masih memiliki keterbatasan dalam melakukan kurasi konten dalam Bahasa Indonesia, yang menyebabkan artikel non-Indonesia masih dimasukkan ke dalam hasil pencarian.

Beberapa media daring seperti [Magdalene](https://magdalene.co/), [Tirto](https://tirto.id/), dan [Indo Progress](https://indoprogress.com/) telah merilis beberapa artikel edukatif yang mendukung keberadaan komunitas LGBTQIA+ di Indonesia. Namun, sebagian besar dari media-media daring ini cukup tersegmentasi dan kurang memiliki jangkauan pembaca yang luas apabila dibandingkan dengan media-media arus utama. Hal ini berujung pada visibilitas artikel yang relatif rendah pada mesin pencari. Selain itu, organisasi LGBTQIA+ di Indonesia juga memiliki eksistensi yang sangat terbatas di internet. Banyak situs web organisasi yang tidak diperbarui ([PLUSH Yogyakarta](http://www.plush.or.id/), [GAYa Nusantara](https://gayanusantara.or.id/), atau bahkan tidak dapat diakses sama sekali ([Arus Pelangi](http://www.aruspelangi.org/). Halaman Wikipedia bahasa Indonesia tentang LGBTQIA+ juga memiliki jumlah artikel yang terbatas. Pada Desember 2019, hanya ada [17 sub-kategori LGBT di Wikipedia berbahasa Indonesia](https://id.wikipedia.org/wiki/Kategori:LGBT) dibandingkan dengan [34 sub-kategori di Wikipedia berbahasa Inggris](https://en.wikipedia.org/wiki/Category:LGBT). Hal ini menunjukkan kesenjangan yang cukup besar antara ketersediaan informasi dalam Bahasa Inggris dan Bahasa Indonesia.

{{< side-fig
    src="/media/stories-content/Flickering_Hope_image2.jpg"
    alt=""
    caption="BRANI-Belajar, bagian dari situs web BRANI yang menyediakan artikel pendidikan tentang orientasi seksual dan identitas gender."
    title="Gambar 2" >}}

Menanggapi kondisi tersebut, terdapat beberapa organisasi yang mencoba membangun ruang aman virtual dan menyediakan lebih banyak informasi edukatif tentang LGBTQIA+ dalam Bahasa Indonesia. [BRANI](https://www.brani.center/), misalnya, baru-baru ini didirikan pada Oktober 2019 untuk memfasilitasi individu LGBTQIA+ muda dan anggota keluarga mereka untuk berbicara tentang gender dan seksualitas secara daring. Situs web BRANI memuat konten pendidikan dan juga fitur bagi pengunjung untuk berkonsultasi secara anonim dengan psikolog dan relawan lainnya. Komunitas LGBTQIA+ lain, seperti [Kolektif Tanpa Nama](https://www.instagram.com/kolektiftanpanama/), mengandalkan media sosial untuk menyebarkan konten positif tentang orientasi seksual, identitas dan ekspresi gender, serta karakteristik seksual (*Sexual Orientation, Gender Identity and Expression, and Sex Characteristics—SOGIESC*).

{{< side-fig
    src="/media/stories-content/Flickering_Hope_image3.jpg"
    alt=""
    caption="Halaman Instagram dari Kolektif Tanpa Nama."
    title="Gambar 3" >}}

Namun, kelompok-kelompok ini menghadapi beberapa tantangan untuk mengembangkan konten mereka. Menurut Abie, salah satu pendiri dari BRANI, salah satu tantangan terbesar bagi organisasinya adalah kurangnya sumber daya manusia (SDM) untuk menghasilkan lebih banyak konten. Ai, anggota dari Kolektif Tanpa Nama, juga menyatakan bahwa komunitasnya bergantung pada relawan untuk membuat konten.

{{< inline-audio
    title="Audio 1. Hambatan BRANI dalam Meningkatkan Visibilitas Situs Web"
    audio="https://archive.org/download/abiebrani2/Abie%20BRANI%20-%20SEO%20Booster.mp3" >}}

Keterbatasan lain dari organisasi-organisasi ini adalah kurangnya sumber daya finansial dan keterampilan teknis yang diperlukan untuk memelihara situs web. BRANI, misalnya, mengalami kesulitan dalam meningkatkan jumlah pengunjung ke situs mereka karena fitur *search engine optimization* (SEO) *booster* yang mereka miliki hanya dapat digunakan untuk tiga kata kunci. Di sisi lain, Kolektif Tanpa Nama bahkan tidak memiliki situs web karena keterbatasan sumber daya. Individu LGBTQIA+ di Indonesia juga seringkali dihadapkan dengan berbagai penolakan dan perundungan dari masyarakat sekitar. Hal tersebut semakin semakin menyulitkan kami untuk mengumpulkan sumber daya demi memproduksi konten-konten yang dapat berfungsi sebagai kontra-narasi terhadap publikasi negatif tentang LGBTQIA+ di Indonesia.

## Instagram sebagai Media Pilihan

{{< inline-audio
    title="Audio 2. Keterbatasan Sumber Daya dalam Pembuatan Konten Daring"
    audio="https://archive.org/download/danafahadi1/Dana%20Fahadi%20-%20Resources%20Website%20v.%20Social%20Media.mp3" >}}

Karena masalah teknis dan keterbatasan sumber daya dalam membuat situs web, banyak komunitas LGBTQIA+ yang mengalihkan fokus mereka pada media sosial untuk berbagi konten. Instagram, sebagai salah satu media sosial terpopuler bagi generasi muda di Indonesia, menjadi media yang banyak digunakan oleh organisasi dan individu LGBTQIA+. Menurut Dana Fahadi, seorang aktivis feminis dan asisten pengajar di Departemen Ilmu Komunikasi Universitas Gadjah Mada, media sosial menawarkan banyak ruang untuk mengekspresikan diri tanpa perlu keterampilan teknis maupun modal finansial yang besar. Para pembuat konten juga dapat berjejaring dan menyebarkan konten lebih mudah karena desain media sosial yang bersifat komunal. 

{{< inline-audio
    title="Audio 3. Preferensi Masyarakat Indonesia dalam Mengakses Konten Visual"
    audio="https://archive.org/download/danafahadi2/Dana%20Fahadi%20-%20Minat%20Baca%20Indonesia.mp3" >}}

Informasi di Instagram juga lebih mudah diakses oleh pengguna internet di Indonesia. Berdasarkan penelitian dari Central Connecticut State University pada tahun 2019, Indonesia berada di peringkat 60 dari 61 negara dalam hal literasi [^8]. Ini berarti bahwa orang Indonesia lebih cocok untuk memproses tulisan pendek dibandingkan artikel panjang. Selain itu, manusia pada umumnya memiliki kemampuan untuk memproses gambar dengan cepat dan menyimpan informasi visual tersebut dengan akurasi tinggi [^9]. Faktor-faktor ini berkontribusi pada tingginya popularitas Instagram di Indonesia.

{{< inline-audio
    title="Audio 4. Alasan BRANI dalam Menggunakan Instagram"
    audio="https://archive.org/download/abiebrani1/Abie%20BRANI%20-%20Instagram.mp3" >}}

Terdapat beberapa alasan mengapa BRANI memilih untuk fokus di Instagram daripada media sosial lainnya. Menurut Abie, Instagram lebih populer dibandingkan dengan media sosial lainnya seperti Twitter dan Facebook di kalangan generasi muda. Selain itu, Abie dan anggota BRANI lainnya sudah memiliki jejaring yang luas di Instagram sehingga lebih mudah bagi mereka untuk mempromosikan konten mereka. Instagram juga menawarkan berbagai macam opsi bentuk konten untuk pemilik akun, seperti *post*, *story*, dan IGTV. Pesan japri, kolom komentar, dan fitur tambahan pada Instagram *story* seperti tanya jawab dan jajak pendapat juga dapat meningkatkan interaksi dengan pengguna lainnya. Ketersediaan berbagai fitur dan tingkat interaksi yang tinggi menjadikan Instagram sebagai salah satu media utama bagi BRANI untuk berbagi informasi.

{{< inline-audio
    title="Audio 5. Strategi Kolektif Tanpa Nama untuk Memperluas Jangkauan Konten"
    audio="https://archive.org/download/kolektiftanpanama1/Ai%20Kolektif%20Tanpa%20Nama%20-%20Instagram.mp3" >}}

Instagram menjadi satu-satunya media yang digunakan oleh Kolektif Tanpa Nama untuk mengedukasi masyarakat dan berinteraksi dengan komunitas. Menurut Ai, Kolektif Tanpa Nama mencoba memperluas jangkauan dari konten mereka dengan *tagging* akun instagram lain yang memiliki jumlah pengikut lebih besar, dengan harapan akun-akun tersebut akan membantu menyebarkan konten mereka. Selain itu, mereka juga membuat konten yang lebih bersifat interaktif seperti *30-Day Movie Challenge*, di mana pengikut akun Instagram mereka dapat merekomendasikan film-film positif tentang LGBTQIA+. Sebagai "akun bisnis," mereka juga dapat melihat jumlah kunjungan akun, jumlah interaksi, dan jangkauan untuk setiap konten sehingga dapat digunakan untuk mengevaluasi konten dan menentukan strategi dalam meningkatkan visibilitas mereka.

{{< inline-audio
    title="Audio 6. Keterbatasan SDM dalam Memperbanyak Konten dari Kolektif Tanpa Nama"
    audio="https://archive.org/download/kolektiftanpanama2/Ai%20Kolektif%20Tanpa%20Nama%20-%20SDM.mp3" >}}

Namun, seperti halnya situs web, memposting konten di media sosial juga membutuhkan sumber daya. Salah satu tantangan yang disebutkan oleh Ai adalah kurangnya jumlah relawan yang bersedia untuk memposting konten baru dan berinteraksi dengan pengguna Instagram lainnya secara aktif. Masalah yang sama juga terjadi dengan BRANI. Semua pengurus inti dari BRANI memiliki pekerjaan penuh waktu. Hal ini menyulitkan BRANI untuk meningkatkan jumlah konten dan interaksi di akun mereka.

Masalah lainnya berkaitan dengan visibilitas konten sosial media di ruang daring. Konten-konten yang diunggah ke Instagram pada umumnya tidak terindeks di mesin pencari. Hal ini menyulitkan pengguna internet untuk menemukan konten LGBTQIA+ yang bersifat positif karena sebagian besar dari mereka masih mengandalkan mesin pencari untuk menemukan informasi. Selain itu, fitur pencarian di Instagram juga masih belum memadai. Apabila Anda ingin mencari konten tentang Islam dan LGBTQIA+, misalnya, Anda harus memilah berbagai gambar dengan tagar #Islam, #LGBTQIA, atau #LGBTMuslims. Hal ini dikarenakan sistem pencarian Instagram sangat terbatas, dimana pengguna hanya bisa mencari konten dari satu tagar saja di setiap sesi pencarian. Oleh karena itu, penggunaan Instagram sebagai platform untuk mencari informasi menjadi kurang ideal.

## Harapan untuk Masa Depan

Sebagai bagian dari komunitas LGBTQIA+, saya memimpikan masa depan dimana konten-konten positif tentang LGBTQIA+ dalam Bahasa Indonesia dapat dengan mudah diakses oleh masyarakat. Saya ingin mendengar lebih banyak cerita mengenai pengalaman hidup dan pencapaian dari individu LGBTQIA+ di Indonesia. Saya juga ingin mengetahui bagaimana individu LGBTQIA+ di negeri ini dapat mempersatukan nilai-nilai keagamaan dan identitas gender maupun orientasi seksual, mengingat Indonesia merupakan sebuah negara yang berasaskan ketuhanan. Saya percaya bahwa konten positif yang dibuat oleh komunitas LGBTQIA+ lokal, berdasarkan pengalaman dan perspektif kita sendiri, akan memberikan inspirasi dan harapan yang lebih besar bagi generasi LGBTQIA+ muda di Indonesia.

Namun, seperti yang telah disebutkan sebelumnya, komunitas LGBTQIA+ di Indonesia menghadapi tekanan yang cukup besar dari pemerintah dan masyarakat. Akan sangat sulit untuk mengubah sikap mereka tentang isu LGBTQIA+, tetapi ada beberapa langkah yang bisa kita ambil untuk meningkatkan jumlah konten LGBTQIA+ berbahasa Indonesia di internet. Pertama, kita perlu mendorong dan memfasilitasi lebih banyak individu LGBTQIA+ di Indonesia untuk menghasilkan konten, terutama dalam bahasa lokal. Individu LGBTQIA+ di Indonesia seringkali merasa tidak aman untuk bercerita mengenai pengalaman mereka secara terbuka karena kondisi di Indonesia yang masih cukup rawan. Selain itu, mereka juga sering merasa kurang mampu dalam menulis maupun memproduksi konten daring. Oleh karena itu, diperlukan lokakarya pembuatan konten dan juga penyuluhan mengenai protokol keselamatan dalam penggunaan internet untuk advokasi.

Kedua, bantuan teknis juga diperlukan untuk memperbanyak konten LGBTQIA+ berbahasa Indonesia di internet. Banyak individu LGBTQIA+ di Indonesia yang masih belum terbiasa dengan aspek teknis dari pengelolaan situs web. Selain itu, mereka juga masih memiliki pengetahuan yang kurang mengenai cara kerja mesin pencari dan *search engine optimization* (SEO). Terdapat juga kesenjangan antara individu-individu LGBTQIA+ di Indonesia. Individu LGBTQIA+ yang berasal dari daerah pedesaan dan berlatar belakang sosial ekonomi rendah pada umumnya memiliki akses informasi yang lebih terbatas dibandingkan dengan individu LGBTQIA+ yang tinggal di daerah perkotaan. Hal tersebut menyebabkan kurangnya konten-konten yang merepresentasikan realita hidup dari komunitas LGTBQIA+ di daerah rural yang cenderung lebih tradisional. Akan sangat menarik apabila kita bisa belajar tentang bagaimana budaya tradisional Indonesia bersinggungan dengan keragaman gender dan orientasi seksual. Dengan demikian, menjadi sangat penting untuk membantu menyuarakan pengalaman dan mengadvokasi hak-hak dari berbagai sub-kelompok di dalam komunitas LGBTQIA+ yang seringkali terpinggirkan. Cerita-cerita dari individu dengan latar belakang berbeda akan memberi gambaran yang lebih kaya tentang komunitas LGBTQIA+ di Indonesia.

Akhir kata, ada begitu banyak tantangan dalam menambahkan konten LGBTQIA+ Indonesia di ranah daring. Namun, saya percaya bahwa bukanlah suatu yang mustahil untuk membuat berbagai konten LGBTQIA+ yang lebih positif, yang mampu mengilhami generasi muda untuk mencintai diri mereka sendiri dan bertoleransi terhadap sesama. Saya berharap komunitas LGBTQIA+ di Indonesia mampu memiliki kehidupan yang lebih baik di masa yang akan datang, seiring dengan meningkatnya jumlah konten mengenai identitas gender dan orientasi seksual yang dapat mengedukasi masyarakat Indonesia.

(*) E. M. Bayuni, ["Gay bashing signals Indonesia's growing intolerance of minorities"](https://www.straitstimes.com/opinion/gay-bashing-signals-indonesias-growing-intolerance-of-minorities). *The Straits Times*. March 5, 2016.

[^1]: E. M. Bayuni, "Gay bashing signals Indonesia's growing intolerance of minorities". *The Straits Times*, March 5, 2016. Available: [https://www.straitstimes.com/opinion/gay-bashing-signals-indonesias-growing-intolerance-of-minorities](https://www.straitstimes.com/opinion/gay-bashing-signals-indonesias-growing-intolerance-of-minorities). Acessed: December 5, 2019

[^2]: Saiful Mujani Research & Consulting, *Kontroversi Publik Tentang LGBT di Indonesia: Hasil Survei Nasional SMRC 2016-2017*, January 25, 2018. Available: [https://saifulmujani.com/mayoritas-publik-menilai-lgbt-punya-hak-hidup-di-indonesia/](https://saifulmujani.com/mayoritas-publik-menilai-lgbt-punya-hak-hidup-di-indonesia/). Accessed: December 7, 2019

[^3]: "89,3 Persen LGBT di Indonesia Pernah Alami Kekerasan". Tempo, January 27, 2016. Available: [https://nasional.tempo.co/read/739961/893-persen-lgbt-di-indonesia-pernah-alami-kekerasan/full&view=ok](https://nasional.tempo.co/read/739961/893-persen-lgbt-di-indonesia-pernah-alami-kekerasan/full&view=ok). Accessed: December 7, 2019

[^4]: "Indonesia's Aceh whips gay couple for syariah-banned sex". *The Straits Times*, July 13, 2018. Available: [https://www.straitstimes.com/asia/se-asia/indonesias-aceh-whips-gay-couple-for-shariah-banned-sex](https://www.straitstimes.com/asia/se-asia/indonesias-aceh-whips-gay-couple-for-shariah-banned-sex). Accessed: December 7, 2019

[^5]: A. Harsono, "Indonesian Police Harass Transgender Women". *Human Rights Watch*, November 8, 2018. Available: [https://www.hrw.org/news/2018/11/08/indonesian-police-harass-transgender-women](https://www.hrw.org/news/2018/11/08/indonesian-police-harass-transgender-women). Accessed: December 7, 2019

[^6]: Kemitraan, *LGBTI Media Coverage and Community Media Mapping*, Available: [https://www.aidsdatahub.org/sites/default/files/publication/LGBTI_Media_Mapping_Report_on_Indonesia_2017.pdf](https://www.aidsdatahub.org/sites/default/files/publication/LGBTI_Media_Mapping_Report_on_Indonesia_2017.pdf) p. 12

[^7]: Human Rights Watch, *These Political Games Ruin Our Lives: Indonesia's LGBT Community Under Threat*, August 2016. Available: [https://www.hrw.org/sites/default/files/report_pdf/indonesia0816_web_2.pdf](https://www.hrw.org/sites/default/files/report_pdf/indonesia0816_web_2.pdf). Accessed: December 7, 2019

[^8]: J.W. Miller, "World's Most Literate Nations". *Central Connecticut State University*, 2016. Available: [https://www.ccsu.edu/wmln/rank.html](https://www.ccsu.edu/wmln/rank.html). Accessed: December 14, 2019

[^9]: T. Romih, "Humans Are Visual Creatures". *Seyens*, October 12, 2016. Available: [https://www.seyens.com/humans-are-visual-creatures/](https://www.seyens.com/humans-are-visual-creatures/). Accessed: December 14, 2019
