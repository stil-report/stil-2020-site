---
author1: Jeffrey Ansloos
author2: Ashley Caranto Morford
title: Learning and Reclaiming Indigenous Languages of Turtle Island within the Twitter Ecosystem
date: 2020-01-01
weight: 7
slug: "learning-and-reclaiming-indigenous-languages"
skipmeta: true
illustration: Learning_and_Reclaiming_Indigenous_Languages.jpg
illustrationAlt: "Two people stand with their backs to the viewer, their dominant hands raised towards the night sky. They’re each wearing a glowing watch with an aboriginal syllabic character inscribed on the face. In the night sky above a mountain range, we see stars and characters: the characters are glowing to match the symbols on the peoples’ watches. On the right side of the illustration there are geese flying in and preparing to land. On the left side of the illustration we see trees and a lobstick, indicating that this place is special."
illustrationauthor: Illustration by Maggie Haughey
translationKey: jeffrey
hasaudio: true
imageauthor1: Jeffrey_Ansloos.webp
imageauthor2: Ashley_Caranto_Morford.webp
summary: "Through funding by the Canadian Social Sciences and Humanities Research Council, we have been developing a community-engaged research program called the #DecolonizingDigital Project, which tries to better understand Indigenous peoples' utilization of social media platforms. In particular, we have been considering how Indigenous Peoples from across and beyond Turtle Island (colonially called Canada, the United States, and Mexico) have been promoting the survival and learning of Indigenous languages through hashtag and other keyword networks."
---

 ## Introduction

Through funding by the [Canadian Social Sciences and Humanities Research Council](https://www.sshrc-crsh.gc.ca/society-societe/stories-histoires/story-histoire-eng.aspx?story_id=325), we have been developing a community-engaged research program called the #DecolonizingDigital Project, which tries to better understand Indigenous peoples' utilization of social media platforms. In particular, we have been considering how Indigenous Peoples from across and beyond Turtle Island (colonially called Canada, the United States, and Mexico) have been promoting the survival and learning of Indigenous languages through hashtag and other keyword networks. Focusing on the Canadian context, this report documents the challenges and opportunities in promoting Indigenous languages through Twitter. The work makes clear the technical and social factors that must be considered in order to make environments like Twitter truly multilingual and promoting of Indigenous languages. This research has been generated through analyzing tweets from Indigenous language hashtag and keyword networks, which were generated through a systematic identification of over 35 hashtags, 57 keyword terms, and 9 user account types across 60 federally recognized Indigenous language groups in Canada. This amounted to a total of 3812 unique tweets.

 ## Benefits

In a social context where Indigenous languages across Canada have been targeted for erasure under [colonial policies of assimilation](https://indigenousfoundations.arts.ubc.ca/the_residential_school_system/), Twitter hashtag networks have convened a unique and meaningful context for Indigenous people to share knowledge about Indigenous languages. Across the various networks included in our study, there are examples of language reclamation and reconnection for intergenerational survivors of colonial assimilationist policies. The Twitter ecosystem is supporting complex connections across various social and political borders, such as rural and urban, reserve and off-reserve, domestic and international, as well as intergenerational, multiracial, and transnational. Most importantly, the technology of the hashtag encourages active connection, participation, and collaboration by people from across these borders who are committed to the flourishing of Indigenous languages.

There are positive social processes enabled through hashtag networks and sharing functions on Twitter. These technological functions and their purposing by Indigenous Twitter users have inspired and enabled solidarity and a threading together of unique and disparate language survivance and learning movements across Canada (See Figure 1).

{{< side-fig
    src="/media/stories-content/Learning_and_Reclaiming_Indigenous_Languages_image1.webp"
    alt=""
    caption="Dene Melissa Daniels in providing consent to cite this tweet wished to recognize the language teacher who provided this teaching, Dene elder and educator Eileen Beaver."
    title="Figure 1">}}

For example, a hashtag network of [Gwichin](https://en.wikipedia.org/wiki/Gwich%27in) language learners inspired learners of [Anishnaabemowin (Ojibway)](https://en.wikipedia.org/wiki/Anishinaabe) to create their own hashtag network. And similarly, a [Neheyawewin (Cree)](https://en.wikipedia.org/wiki/Cree_language) language network on Twitter inspired learners of [Hul\'qumi\'num](https://en.wikipedia.org/wiki/Halkomelem) to start their own Twitter-based Word of the Day.

Indigenous Twitter users have raised concerns about the accessibility---economically, geographically, psychologically, and culturally---of Indigenous language learning camps and of Indigenous language programming offered through academic and other formal institutions. Further, Indigenous Twitter users have expressed concern about how Indigenous language learning environments are increasingly housed in Western institutions and accessed by non-Indigenous people. Concern regarding the geographic inaccessibility of many Indigenous language programs is heightened given that, as various Indigenous language learners have expressed, being on the land where a language emerged is integral to learning the language in an active, embodied way.

Addressing these issues of in/accessibility is critical to the ability of Indigenous communities to learn their languages in life-nourishing, community-vitalizing, and culturally sustaining ways. Twitter and perhaps the internet more broadly, present a paradox, whereby they are not materially land-based while, at the same time, they are utilized to help to address these issues of land-based in/accessibility.

{{< side-fig
    src="/media/stories-content/Learning_and_Reclaiming_Indigenous_Languages_image2.webp"
    alt=""
    caption="Sḵwx̱wú7mesh Language School"
    title="Figure 2" >}}

For example, we have witnessed Twitter users encouraging non-Indigenous users to monetarily support fundraising campaigns for grassroots-led Indigenous language education initiatives, like the [Sḵwx̱wú7mesh (Squamish)](https://en.wikipedia.org/wiki/Squamish_language) language school. Further, Indigenous people are using Twitter to make their languages more accessible to community members who are geographically disconnected from their homelands. It is important to remember that this displacement is both a product of forced colonial interventions, as well as elective migration. Through tweeting land-specific teachings and embedding multimedia of the land into their tweets, Indigenous language teachers are bringing online language learners to their homelands, in a virtual sense.

While Twitter usage is connecting people across geographical space, Indigenous language learning networks still appear to be primarily urban, and, while rural and remote users are active and significantly engaged online, the lack of use in certain regions of the country reflects the significant lack of internet infrastructure in the same regions.

 ## Challenges

One of the key challenges identified through this project is the technical limitations of current translation technologies for Indigenous languages in the Canadian context. Indigenous languages such as Hul\'qumi\'num, Sḵwx̱wú7mesh (Squamish), Lewkungen, and Neheyawewin (Cree) are being technically misrecognized as German, Estonian, Finnish, Vietnamese, and French by Twitter's translation technology. Another specific and related challenge we have identified is the absence of Twitter's translation capacity for syllabic based text in Indigenous languages, such as [Inuktitut](https://en.wikipedia.org/wiki/Inuktitut).

These translation challenges are well-known within Indigenous language networks, and Indigenous and non-Indigenous Twitter users alike tag Twitter Inc. accounts to draw attention to these concerns. It is worth noting, too, that Twitter users often also tag organizations like Microsoft and Bing to draw attention to these translation issues, which suggests that the translation issues that exist for Indigenous languages are not unique challenges to Twitter.

Across most Indigenous language survivance and learning networks, racism has been identified as a major social challenge within the Twitter ecology. Specifically, there are various ways in which these networks are actively targeted with incendiary text and multimedia content, sometimes constituting hate speech, by a variety of user types, including actual people, and automated bots.

In terms of bot users, these accounts seem to follow similar patterns for dissemination of misinformation, and often distribute nonsensical text reflecting aggregate analytics and content generation. In terms of users who are actual people, these accounts are sometimes anonymized, and therefore difficult to identify and hold accountable. Further, there are frequent examples of what Indigenous Twitter users refer to as "pretend-ians", which are non-Indigenous people who pose as indigenous people online. These users often introduce, defend, and perpetuate racist content, posturing as speaking on behalf of Indigenous communities. [The hashtag network \#settlercollector, in part formed in response to these types of accounts, encourages Indigenous users and their allies to hold racist Twitter users to account](https://www.cbc.ca/radio/unreserved/after-colten-boushie-where-do-we-go-from-here-1.4535052/settlercollector-hashtag-helps-redirect-racist-attacks-on-social-media-1.4537594). This challenge makes clear how Indigenous language learning within digital environments is politically complex, and fraught with challenges to self-determinism through various forms of racism, identity surveillance, appropriation, and multimodal expressions of hate speech.

 ## Conclusion

Our study makes clear that the promotion of Indigenous languages on the internet must be rooted within a critical analysis of the internet's technology itself, as well as the social processes that surround its utilization. We have also learned that some of the most effective approaches to the promotion of Indigenous languages on the internet are not coming from top-down and highly resourced institutional structures, but, rather, from ground-up, community-based, and user-driven networks, empowered through enhanced information and communications technologies. Our work also emphasizes that the goal of a multilingual internet that is inclusive and representative of Indigenous peoples must consider and reckon with the social legacies and ongoing experience of colonial oppression. A multilingual internet cannot merely aspire to be representative, but, considering colonial history, must also seek to promote environments that enhance the survival and learning of Indigenous languages for and by Indigenous people.
