---
author1: Emna Mizouni
author2:
title: "لماذا علينا الكتابة بلغة أخرى؟"
date: 2020-01-01
weight: 4
slug: "write-in-another-language"
illustration: Why_write_in_another_language.jpg
illustrationAlt: Four people work together in a garden. Narrow, angular waterways frame sections of greenery. In the background stands an old building with big, open, arched doorways that reveal the warm yellow sky peeking over green foliage. In the foreground, two people with long hair dig a hole to plant a USB tree. Behind them, we see two USB trees growing happily. Two people work in the middle distance to harvest fruit and care for the plants in the garden.
illustrationauthor: Illustration by Maggie Haughey
translationKey: emna
hasaudio: true
imageauthor1: Emna_Mizouni.webp
imageauthor2:
summary: "في مواطنة رقمية نحن مهتمات بمساعدة Whose Knowledge في هذه الدراسة للوصول لأصحاب المحتوى الرقمي ومستخدمي الإنترنت الأفارقة والعرب ليشاركونا تجربتهم في خلق المحتوى باللغة الأم وتسويق ثقافاتهم. استطعنا محاورة مستخدمين ومستخدمات من تونس ، أوغندا ، السودان وحين بدأنا في اللقاءات ، اهتم أصحاب مهن في المجال الرقمي من هولندا سنغافورة والهند بالموضوع وشاركونا آراءهم أيضا. تطلعنا لفهم سبب الاستخدام المفرط للغات المستعمر على الانترنت والعراقيل التي يواجهونها."
---

{{< rtl >}}

## تقديم

بالنسبة للدراسة التي تمت على لغات المستعملة في الانترنت أو أكثر اللغات المستعملة أو الشائعة على الانترنت، لقيتها تجربة تحفونة ياسر باهية بش نشوف الأشخاص ،  نظرتهم لللغات الموجودة على الانترنت ، نظرتهم للتكنولوجيا بش يتعلموا أو يستعملوا حاجات. 
شفت الغياب التام متاع عدة لغات افريقية على مستوى القارة الافريقية بالطبيعة. 
شفت زادة اللي الثقافة كي تتغير اللغة الثقافة تتغير معاها. ثقافتنا تتغير بتغير اللغة فكيف نكتبو مثلا على أي الثقافة الافريقية بش نلقاوها بأي لغة استعمارية بش تكون مخلتفة أو فيها (بعض التهذيب) ما يسمى بالتهذيب.

بالنسبة لي أنا الدراسة هذي كانت مهمة مهمة جدا والرحلة هذي متاع الدراسة الاجتماعات مع الأشخاص اللي سألتهم وحاورتهم من ضمن هذه الدراسة كانت حاجة مهمة ياسر زادة بش نوري جانب آخر من افريقيا ، جانب آخر من تونس ، ومن الثقافات الموجودة في افريقيا موش اللي يعرفوه الناس الكل لكن زادة الجانب أهمية اللغة والثقافة والغياب الكبير بش ما نقولوش الغياب التام لتوثيق الثقافة الأصلية.

بالنسبة لي زادة كتونسية ومتحدثة باللغة العربية مهم جدا أني نفهم علاش الناس مثلا يستعملوا لغة أجنبية أكثر من اللغة العربية ، وإلا علاش زادة الناس في تونس يستعملوا العربية الفصحى موش التونسي اللي هي أول لغة نحكيوها ، أول لغة نستعملوها ونتعلموها. زيد خلاتني الحكاية هذي نشوف أكثر اللي مثلا فما غياب تام للغة الأمازيغية، الأمازيغ التونسيين لغتهم قاعدة تموت قاعدة تختفي. بالنسبة لتونس اللغة قليل التعاطي بيها أو تثمينها والتعامل معاها. في نفس الوقت ما فماش استعمال تام ليه على وسائل التواصل الاجتماعي أو على الانترنت عموما، نحكيو هوني على اللغة الأمازيغية التونسية. 
التجربة هذي كانت باهية وراتني الناس كيفاش تفكر كيفاش تحاول تخرج أي معلومة على الانترنت، تحاول توثق أو تستعمل يا إما اللغة الدارجة التونسية أو تستعمل لغات أخرى وخاصة التجربة هذي خلاتني نشوف التأثير الكبير متاع الاستعمار واللغات المستعمر، وفي نفس الوقت الوصول للمعلومة وللعلم أصعب وأصعب، موش على قد ما لازمنا نوصلوا على قد ما لازمنا نعرفوا. فإذا كان فما معلومات في الدارجة التونسية أو الأمازيغية تنجم تكون غير متاحة للناس لأنه اللغة هذي غير متاحة على الانترنت وانتشارها غير موجود. 

بالنسبة للحوارات اللي تمت مع ناس مختلفين من جهات مختلفة من ثقافات مختللفة. ناس تعرفت من قبل ، ناس أول مرة نتعرف عليهم أو معرفة سطحية ، ناس قابلتهم بذمة الدراسة هذي. الحوارات هذي كانت مثرية على جميع الأصعدة. شخصيا وراتني الحوارات هذي اللي كان سببها دراسة اللغات على الانترنت وراتني اللي فما انعدام وجود أو نقولوا وجود قليل للغات اللي تعنيلنا أو تعني لثقافاتنا واللي احنا مازلنا في البدايات. واللي يلزمنا نقدموا أكثر ونحسنوا أكثر ونحاولوا نتجاوزوا الصعوبات التقنية والاجتماعية وغيرها بش نوثقوا أكثر ويكون عنا حضور بلغاتنا أحنا. 

في نفس الوقت نقول زادة اللي الدراسة هذي شاركت فيها لعدة أسباب، أسباب شخصية أو مهنية كيف مثلا شفتها فرصة كبيرة ياسر بش تعاون مواطنة رقمية اللي هي مبادرة قمت بها لمستخدمي الانترنت وخاصة الفئات المنسية والأكثر عرضة للمشاكل على الانترنت كيف المراهقات والنساء ، الناشطات في المجتمع المدني، الصحفيات ، أو النساء في المجال السياسي كل ماهو غير موجود في المجال الرقمي أو معرض أكثر للانتهاكات اللفظية أو الانتهاكات الخصوصية أو غيره. 
المهم للدراسة هذي بش نكون جزء منها نحاول نفهم الأشخاص اللي نتعامل معاهم عبر مبادرة مواطنة رقمية ، شنية اللغات اللي يستعملوهم ، بالطبيعة يستعملوهم على الانترنت وشنوة المشاكل والعراقيل اللي يواجهوها كيف يستعملوا اللغة هذي. وهذي خلات تجربة الدراسة شيقة جدا جدا. وخلاتني نفهم برشة حاجات تنجم تحسن لي العمل اللي نقوموا بيه في المبادرة هذي، مبادرة مواطنة رقمية. تنجم تخليني نفكر في طرق جديدة بش نوصلوا المعلومة أو بش نحافظوا على المعلومة باللغات متاعنا. وهوني نحكي أكثر على الدارجة التونسية.

بالنسبة لي زادة نجم نقول اللي فما فراغ كبير كشخص يدافع على الحق في الوصول إلى المعلومة نشوف اللي فما فجوة كبيرة ياسر في الطريقة للوصول للمعلومة نظرا للانعدام الكلي لبعض المعلومات أو الدراسات باللغات الغير أجنبية. نقول زادة أنه المشكلة اللي واجهناها واجهتها أنا شخصيا كإحدى مستخدمات ويكيبيديا أنه ما نلقاوش مصادر للمعلومة في لغاتنا إذا كان نحبوا نكتبوا على شخصيات وطنية أو معالم أثرية تونسية مثلا، صعيب ياسر نلقاو المعلومات باللغة العربية أو بالدارجة التونسية. أغلب المعلومات اللي ارتكزنا عليهم في المقالات اللي كتبناهم كانوا باللغة الفرنسية وفي بعض الأحيان بالعربية والانجليزية. فما غياب للمصادر أو المراجع اللي ممكن نستعملوهم وكيف كيف بالنسبة للقارة الافريقية فما فجوة كبيرة ياسر في المعطيات ، في المراجع والمصادر وفما فجوة كبيرة في الاستعمال للوسائل اللي تخلينا موجودين في المجال الرقمي. هذا يخلي موش كان الفجوة في المعلومات أو انعدام المعلومة ، لكن الفجوة تصير في الوصول ليها موش كان في استهلاكها كمعلومة لكن في الوصول ليها سواء عبر المعلومات المطبوعة أو المعلومات الرقمية.  وهوني نحس اللي هذا موضوع ممكن نتطرقوا له أكثر أو نخدموا عليه أكثر. وممكن هذا أحد الأسباب اللي خلاتني نشارك في الدراسة هذي وخاصة كي نشارك في الدراسة  هذي نحاول نغطي من منظور عربي وافريقي ونشوفوا زادة من المصاعب اللي شفتها عبر الحوارات اللي عملتها في الدراسة هذي كانت      الوصول للأدوات الرقمية ، الحواسيب والهواتف الرقمية الذكية أو غيرها . وهوني نستغربوا في نفس الوقت كيفما نستغربوا من الغياب كيفما نستغربوا من قلة الموارد الموجودة والمتاحة للأفارقة (توانسة وغيرهم) وهذا اللي يخلي المعلومات اللي تكتب عنا سواء بالانجليزية أو بالفرنسية من فئة معينة جدا.

من المشاكل اللي شفناها في الدراسة هذي ، أغلب المعلومات التاريخية مثلا تنجم تكون  عند أشخاص غير متعلمين ، ماعندهمش أبجاديات القراءة والكتابة ما يعرفوش يستعملوا وسائل التكنولوجيا فالمعلومة تضيع معاهم. ماهوش ساهل بالنسبة ليهم أنه يتم تدوين المعلومة أو حفظها رقميا. خسارة كبيرة بالنسبة لينا أحنا ، تاريخيا وبالنسبة للأجيال القادمة كيفاش نجموا نوصلولهم المعلومة إذا كان أجدادنا ماهمش يحكيو باللغة الانجليزية مثلا أو بالفرنسية  كيفاش تنجم توصللهم المعلومة ويتم حفظها وحفظ الذاكرة.

بالنسبة للمشوار هذا ، الرحلة هذي ، نقول من أحسن الحاجات اللي صارت لي في العام اللي تعدى وخاصة بعد ما بديت نتعامل مع الفتيات المراهقات ونشوف زادة فيهم شنوة اللي يحبوه… نتوقع الدراسة هذي في الورشات الجاية سواء ورشات ويكيبيديا أو أي نوع من الورشات اللي يتم تخصيصها للفتيات المراهقات أو الصحفيات أو غيرهم بش يكون عندها تأثير كبير في تغيير محتوى الورشة وزيادة مثلا عدد المدربين خاصة للأشخاص اللي أول مرة يتعاملوا مع المجال الرقمي  أو الانترنت أو غيره.

## حول الحوار الذي تم خلال هذه الدراسة

في مواطنة رقمية نحن مهتمات بمساعدة [Whose Knowledge](https://whoseknowledge.org/) في هذه الدراسة للوصول لأصحاب المحتوى الرقمي ومستخدمي الإنترنت الأفارقة والعرب ليشاركونا تجربتهم في خلق المحتوى باللغة الأم وتسويق ثقافاتهم. استطعنا محاورة مستخدمين ومستخدمات من تونس ، أوغندا ، السودان وحين بدأنا في اللقاءات ، اهتم أصحاب مهن في المجال الرقمي من هولندا سنغافورة والهند بالموضوع وشاركونا آراءهم أيضا. تطلعنا لفهم سبب الاستخدام المفرط للغات المستعمر على الانترنت والعراقيل التي يواجهونها.

في الفترة ما بين خريف وشتاء 2019 - 2020 ، قامت آمنة الميزوني بسلسلة الحوارات في إطار دراسة عن "تحرير لغات الانترنت". وتم طرح الأسئلة على بعض المؤثرين وأصحاب المهن في المجال الرقمي ومستخدمي الإنترنت عن: 

اللغة الأم 
تاريخها إن كانوا على اطلاع بها ،
اللغة التي يتم استخدامها عند نشر المحتوى على الإنترنت وإن كانت مختلفة عن لغتهم الأم لماذا يتم استخدامها ، 
كيف ينظر إلى اللغة الأم (عربية أو أي لغة افريقية) في تلك المجتمعات
ما هي العراقيل والصعوبات التي تتم مواجهتها عند استعمال لغة أخرى غير الانجليزية والفرنسية (لغات المستعمر في قارة افريقيا) ،
هل محركات البحث ناجعة عند استخدام اللغة الأم.

استمعن- استمعوا للحوارات التالية:

{{< interview-block
    title="Interview 1 - Amal Bedhyefi"
    author="Amal Bedhyefi (Amal.Books)" 
    text1="is an English Language, Literature and Civilisation graduate, who is now pursuing a masters degree in International Relations and Diplomacy in the High Institute of Human Sciences of Tunis. Fond of reading, Amal is a part-time Editor and the first Tunisian book blogger and Bookstagrammer. She shares her love for books through social media, where she motivates people to read."
    text2="In this interview Amal talks about the massive use of English to reach out to a wider audience for online content creators."
    text3=""
    audio="https://archive.org/download/set-of-interviews/File1_%20Amal.mp3" >}}

{{< interview-block
    title="Interview 2 - Brahim Boughalmi"
    author="Brahim Boughalmi" 
    text1="is an Educator, Fulbright alumni and active member of Carthagina (Heritage and History oriented organization in Tunisia)."
    text2="This interview is in Tunisian Arabic, the following is the English translation:"
    text3="The languages that I use online most of the time are Tunsy (Tunisian dialect) and English. Tunsy for everyday topics, it’s easier for me because obviously, it’s my mother language, and it’s easier for me to communicate that way especially when in private messaging. If it’s my facebook account, most of it is in English, because I have diverse friends from other countries, so I can deliver the message. Some other times, when I speak about very personal topics I use English, because I think Tunsy could be a bit limited in emotions. We are a nation that doesn’t express its emotions. The second reason is that English gives me the feeling of detachment from myself so I became expressive without being emotionally involved especially when it’s very personal."
    audio="https://archive.org/download/set-of-interviews/File2_%20Brahim.mp3" >}}

{{< interview-block
    title="Interview 3 - Elisabeth van der Steenhoven"
    author="Elisabeth van der Steenhoven" 
    text1="is an enthusiastic member of the El-Karama community since 10 years. She is also a member of the Dutch National Advisory Committee on Foreign Affairs and currently a member of the Dutch Ministry of Health committee on digital tools against Covid-19."
    text2="In this audio interview, Elisabeth talks about the use of Dutch online and the lack of use of Arabic and African languages. The importance of the use of native languages and the preservation of that heritage."
    text3=""
    audio="https://archive.org/download/set-of-interviews/File3_%20Elisabeth.mp3" >}}

{{< interview-block
    title="Interview 4"
    author="" 
    text1="The interviewee is a digital trainer from India."
    text2="In this interview she talks about Hindi and the use of English in trainings."
    text3=""
    audio="https://archive.org/download/set-of-interviews/File4_%20Hindi_language.mp3" >}}

{{< interview-block
    title="Interview 5"
    author="" 
    text1="The interviewee is a digital trainer from India."
    text2="In this interview she talks about the struggle of finding synonyms of technical terms in the local languages while teaching IT and digital."
    text3=""
    audio="https://archive.org/download/set-of-interviews/File5_%20Language_first_learners.mp3" >}}

{{< interview-block
    title="Interview 6 - Mohannad"
    author="Mohannad"
    text1="is a university student from the South of Tunisia."
    text2="In this interview he sustains that the use of English gives more results when seraching online."
    text3=""
    audio="https://archive.org/download/set-of-interviews/File6_%20Mohanad_Bennasr.mp3" >}}

{{< interview-block
    title="Interview 7 - Normalland"
    author="Normalland"
    text1="is a prominent Tunisian blogger."
    text2="This interview is in Tunisian Arabic."
    text3=""
    audio="https://archive.org/download/set-of-interviews/File7_%20Normalland.mp3" >}}

{{< interview-block
    title="Interview 8 - Shem"
    author="Shem"
    text1="is from Uganda."
    text2="In this interview he talks about the importance of the school you go to and the impact of colonialism in the language that his country uses."
    text3=""
    audio="https://archive.org/download/set-of-interviews/File8_%20Shem.mp3" >}}

{{< interview-block
    title="Interview 9 - Ter Yang"
    author="Ter Yang"
    text1="is from Singapore."
    text2="In this interview he sustains that the English and Chinese content online exist, there is no issue with material in either language."
    text3=""
    audio="https://archive.org/download/set-of-interviews/File9_%20Ter_Yang.mp3" >}}

{{< interview-block
    title="Interview 10 - Mohamed Gamil"
    author="Mohamed Gamil"
    text1="is a Brown person who loves peace, from Sudan."
    text2="This interview is in Sudanese Arabic"
    text3=""
    audio="https://archive.org/download/set-of-interviews/File10_%20Gamil.mp3" >}}

{{< interview-block
    title="Interview 11 - Mohamed Gamil"
    author="Mohamed Gamil"
    text1="talks about the Nubian Language."
    text2="This interview is in Sudanese Arabic."
    text3=""
    audio="https://archive.org/download/set-of-interviews/File11_%20Nubian_language.mp3" >}}
