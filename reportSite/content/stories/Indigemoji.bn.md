---
FakeTranslation: true
author1: Joel Liddle
author2: Caddie Brain
title: "Signs across generations: how the Arrernte emojis are bringing an Indigenous language online"
date: 2020-01-01
weight: 8
slug: "signs-across-generations"
skipmeta: true
illustration: Indigemoji.jpg
IllustrationAlt: Indigenous person in the forest holds a phone displaying the Indigemoji application on it.
illustrationauthor: Image by Indigemoji
translationKey: caddie-joel
hasaudio: false
imageauthor1: Joel_Liddle.webp
imageauthor2: Caddie_Brain.webp
summary: "Joel Liddle and Caddie Brain of the Indigemoji Project, Australia, are in conversation with Sneha P.P. and Anasuya Sengupta in this video interview. They describe how emojis representing the Arrernte indigenous language were created in an exciting multi-generational effort, and analyse the broader challenges and opportunities for bringing indigenous languages online."
skipSummary: true
---

{{< video
>}}

Check out some more about the Indigemoji Project:

{{< single-gallery
    src_img="/media/stories-content/Indigemoji_0.webp"
    alt=""
    source="indigemoji.com.au"
    link="https://www.indigemoji.com.au/"
    caption="This is how the Indigemojis look like! Image by Indigemoji"
>}}

{{< single-gallery
    src_img="/media/stories-content/Indigemoji_1.webp"
    alt=""
    source="indigemoji.com.au"
    link="https://www.indigemoji.com.au/"
    caption="Joel Liddle with friends during a working session. Image by Indigemoji"
>}}

{{< single-gallery
    src_img="/media/stories-content/Indigemoji_2.webp"
    alt=""
    source="indigemoji.com.au"
    link="https://www.indigemoji.com.au/"
    caption="Workshop with children. Image by Indigemoji"
>}}

{{< single-gallery
    src_img="/media/stories-content/Indigemoji_3.webp"
    alt=""
    source="indigemoji.com.au"
    link="https://www.indigemoji.com.au/"
    caption="Youth group drawing and painting. Image by Indigemoji"
>}}

{{< single-gallery
    src_img="/media/stories-content/Indigemoji_4.webp"
    alt=""
    source="indigemoji.com.au"
    link="https://www.indigemoji.com.au/"
    caption="Digital drawing of an indigenous person. Image by Indigemoji"
>}}
