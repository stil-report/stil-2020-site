---
FakeTranslation: true
author1: Ishan Chakraborty
author2:
title: প্রান্তিকতার মধ্যে প্রান্তিকতা
date: 2020-01-01
weight: 5
slug: "marginality-within-marginality"
illustration: Marginality_Within_Marginality.jpg
illustrationAlt: An amputee uses a soldering iron to repair the wiring inside their forearm prosthetic. They’re working on top of a craft mat with a pride flag design. On the left side of their work space, their phone plays music, and on the right side, their tablet displays some DIY instructions on how to repair a prosthetic.
illustrationauthor: Illustration by Maggie Haughey
translationKey: ishan-essay
hasaudio: false
imageauthor1: Ishan_Chakraborty.webp
imageauthor2:
summary: "কোলকাতা শহরে বড় হয়ে ওঠা একজন ক্যুইয়ার এবং দৃষ্টি প্রতিবন্ধী মানুষ হিসাবে
আমার মনে হয় হন্গারনেটে প্রান্তকতার হন্গারসেকশন নিয়ে বাংলা ভাষায় প্রকাশিত
আলোচনা (ভিডিও, প্রতিবেদন, ব্লগ ইত্যাদি) অত্যন্ত সীমিত ও অপ্রতুল। এ-
সম্পর্কে আরও বিশ্লেষণ ও গবেষণা প্রয়োজন। পশ্চিমবঙ্গ বা বাংলাদেশ (যেখানে
বাংলাভাষী মানুষদের সংখ্যা সবচেয়ে বেশি) -এর মত জটিল আর্থসামাজিক
ব্যবস্থার মধ্যে দাঁড়িয়ে প্রান্তিকতা সম্পর্কে যে-কোনো আলোচনার ক্ষেত্রে করি।"
---

কোলকাতা শহরে বড় হয়ে ওঠা একজন ক্যুইয়ার এবং দৃষ্টি প্রতিবন্ধী মানুষ হিসাবে
আমার মনে হয় হন্গারনেটে প্রান্তকতার হন্গারসেকশন নিয়ে বাংলা ভাষায় প্রকাশিত
আলোচনা (ভিডিও, প্রতিবেদন, ব্লগ ইত্যাদি) অত্যন্ত সীমিত ও অপ্রতুল। এ-
সম্পর্কে আরও বিশ্লেষণ ও গবেষণা প্রয়োজন। পশ্চিমবঙ্গ বা বাংলাদেশ (যেখানে
বাংলাভাষী মানুষদের সংখ্যা সবচেয়ে বেশি) -এর মত জটিল আর্থসামাজিক
ব্যবস্থার মধ্যে দাঁড়িয়ে প্রান্তিকতা সম্পর্কে যে-কোনো আলোচনার ক্ষেত্রে করি।

একটু ভেঙে বলি, শ্রেণি, ধর্ম, বর্ণ, লিঙ্গ, যৌনতা, সক্ষমতা, ভৌগোলিক অবস্থান
ইত্যাদি সামাজিক-সাংস্কৃতিক পরিচয় নির্ণায়ক ফ্যাক্টরগুলির উপর ভিত্তি করে
বিভিন্ন ধরণের পরস্পর সম্পর্কিত প্রান্তিকতা নির্মিত হয়। যেহেতু প্রান্তিকীকরণের
প্রক্রিয়াটি অত্যন্ত জটিল, সুম্বম এবং বহুস্তরীয়, তাই এর মূলে আঘাত করতে গেলে
ইন্টারসেকশন ( বা বিভিন্ন প্রান্তিকতার মধ্যে পারস্পরিক যোগ) সম্পর্কে ব্যপক
সচেতনতা তৈরী করতেই হবে। তবে এ-কথা বলা-ই বাহুল্য যে বর্তমান
আলোচনার পরিসর যেহেতু সীমিত, তাই আমি এখানে কেবলমাত্র কৃ্যুইয়ার এবং
প্রতিবন্ধকতার ইন্টারসেকশন সম্পর্কে দু-চার কথা উত্থাপন করবার চেষ্টা করব।

গোড়াতেই একটি বিষয়ের দিকে পাঠকদের দৃষ্টি আকর্ষণ করবার প্রয়োজন আছে
বলে মনে করি: ক্যুইয়ার বা প্রতিবন্ধকতা, কোনোটিই কিন্ত একরৈখিক ধারণা
নয়। একদিকে যেমন ক্যুইয়ার বলতে বিভিন্ন ধরণের লিঙ্গ বা যৌন পরিচয়কে
বোঝানো হয়, তেমনই আবার প্রতিবন্ধকতাও নানা ধরণের হতে পারে। প্রসঙ্গত
বলে রাখি, ভারতবর্ষের রাইটস অফ পার্সনস উইথ ডিস্যাবিলিটিস আ্যাক্ট- এ একুশ
ধরণের প্রতিবন্ধকতাকে তালিকাভুক্ত করা হয়েছে। বলা-ই বাহুল্য যে এই দুটি
ব্যপক ধারণা সম্পর্কে একযোগে আলোচনা করা এই ক্ষুদ্র পরিসরে সম্ভব নয়, এবং
আমি সেই চেষ্টাও করছি না। এই লেখাটিতে আমি কেবলমাত্র এমন একটি বিষয়
সম্পর্কে মন্তব্য করতে চাইছি যা ইন্টারনেটে বাংলা ভাষায় সচরাচর পাওয়া যায়
না।

এই প্রবন্ধটি লিখতে বসে আমি যে মৌলিক ভাষাগত সমস্যার সম্মুখীন হয়েছি, সে
সম্পর্কে প্রথমেই কয়েকটি মন্তব্য করতে চাই। ক্যুইয়ার বা প্রতিবন্ধকতা সংক্রান্ত
তাত্বিক বা যে-কোনো সংবেদনশীল আলোচনার জন্য যে-ধরনের শব্দবন্ধ ব্যবহার
করতে হয় তার অধিকাংশই বাংলা ভাষায় প্রচলিত নয়। ধরা যাক 'ক্যুইয়ার'
শব্দটি - বাংলা ভাষায় এর প্রতিশব্দ কোথায়? হ্যাঁ, সমকামী, উভকামী,
রূপান্তরকামী ইত্যাদি শব্দগুলি ব্যবহার করা হয় বটে, কিক্ত বলা-ই বাহুল্য যে
এগুলির কোনোটিই ক্যুইয়ারের প্রতিশব্দ নয়।

আবার যেমন 'এবালজম' শব্দঢ। 'প্রতিবন্ধকতার ভিত্তিতে বেষম্য এবং এবলিজম
শন্দদুটি তাৎপর্যের নিরিখে কাছাকাছি হলেও নিশ্চয়ই সমার্থক নয়। অতএব
ইন্টারনেটে ইন্টারসেকশন সম্পর্কে বাংলায় আলোচনার প্রসঙ্গে প্রথমেই বলতে হয় যে
প্রাথমিকভাবে ভাষাপ্রয়োগের ক্ষেত্রেই বেশকিছু সীমাবদ্ধতা রয়েছে।

এরপর রয়েছে ক্যুইয়ার এবং প্রতিবন্ধকতাকে ঘিরে থাকা প্রচুর ত্রান্ত ও
কুসংস্কারাচ্ছন্ন ধারণার সমস্যাটি। যেমন বাংলায় "সমকামিতা ও প্রতিবন্ধকতী'
শন্দবন্ধাটি ব্যবহার করে যদি একটি সাধারণ গুগল সার্চ করা হয় তবে 'পুরুষ
সমকামী কিনা চেনার উপায়!' বা 'সমকামিতা থেকে মুক্তির উপায়' এইধরনের
রেজাল্ট পাওয়া যায়। কিন্ত সমকামী প্রতিবন্ধী মানুষদের অভিজ্ঞতা সম্পর্কে
আলোচনা বা তাদের ভাষ্য বাংলা ভাষায় প্রায় পাওয়া যায় না বললেই চলে।
বলা-ই বাহুল্য, ইংরেজি ভাষায় 'ক্যুইয়ার ত্যান্ড ডিজ্যাবিলিটি' সংক্রান্ত প্রচুর
আলোচনা, গবেষণাপত্র, ভিডিও ইত্যাদি ইন্টারনেটে সহজেই উপলন্ধ। আবার একটু
অন্য দিক থেকে দেখবার চেষ্টা করলেও বুঝতে পারব যে, কৃযুইয়ার ও
প্রতিবন্ধকতা সম্পর্কিত এই ধরণের বহুল প্রচলিত ও বহুল প্রচারিত, ভ্রান্ত,
কুসংস্কারাচ্ছন্ন এবং সর্বোপরি বিভ্রান্তিকর তথ্য ও ধারণাগুলি সহজেই ইন্টারনেট
ব্যবহারকারীদের মতামত ও দৃষ্টিভঙ্গীর উপরে ছাপ ফেলে। অতএব বাংলা ভাষায়
ইন্টারনেটে ক্যুইয়ার ও প্রতিবন্ধকতা সম্পর্কিত যে সমস্ত তথ্য পাওয়া যায়, তা
একদিকে যেমন অনেকাংশেই হোমোফোবিয়া ( সমকামিতার প্রতি বিদ্বেষ ও ঘৃণা)
ও এবলিজমের ফল, আবার অন্যদিকে এর কারণও বটে।
 
আসলে ভারত বা বাংলাদেশের মত আর্থসামাজিক-সাংস্কৃতিক অবস্থার মধ্যে থেকে
ক্যুইয়ার ও প্রতিবন্ধকতা -একসঙ্গে এই দুটি প্রান্তিক পরিচয় সম্পর্কে খোলামেলা
আলোচনা করা অত্যন্ত কঠিন, এমনকি কিছু কিছু ক্ষেত্রে বিপজ্জনকও বটে। যে
অর্থনীতিতে দাঁড়িয়ে অধিকাংশ প্রতিবন্ধী মানুষদের আজও দারিদ্র, অশিক্ষা, ধর্মীয়
কুসংস্কার ও সামাজিক শ্লাণর সঙ্গে প্রাতানয়ত লড়াই করতে হয় সেখানে তাদের
কাছ থেকে বিকল্প যৌনতার মত 'স্টিগমাটাইজড' বিষয় সম্পর্কে প্রকাশ্য আলোচনা
আশা করাটাও বোধহয় কিছুটা অবাস্তব। আবার অন্যদিক থেকেও বিষয়টাকে দেখা
যায়। অনেক রপান্তরকামী বা সমকামী মানুষরাও তাদের প্রতিবন্ধকতা সম্পর্কে
খোলাখুলি আলোচনা করতে স্বাচ্ছন্দ্য বোধ করেন না, এমনকি অনেকক্ষেত্রে তারা
তাদের প্রতিবন্ধকতাকে লুকিয়েও রাখতে চান। এখানেই চলে আসে অন্তরীণ বা
অন্তর্নিহিত 'হোমোফোবিয়া' এবং 'এবলিজম'-এর প্রসঙ্গটি। একদিকে সমাজের
হোমোফোবিক ও এবলিস্ট আচরণ, এবং অন্যদিকে ব্যক্তির কক্যেইয়ার এবং/অথবা
প্রতিবন্ধী) অন্তর্নিহিত হোমোফোবিয়া ও এবলিজম- এই দুটিই একে অপরের
পরিপূরক এবং একত্রেই তারা প্রান্তিকীকরণের প্রক্রিয়াটিকে ত্বরান্বিত করে। সমাজে
একজন কৃযুয়ার প্রতিবন্ধী মানুষের অবস্থানাডকে 'প্রান্তিকতার মধ্যে প্রান্তিকতা
শন্দবন্ধটির দ্বারা বর্ণনা করা যেতে পারে। ভারত-বাংলাদেশের মত উন্নয়নশীল
আর্থসামাজিক পরিস্থিতিতে দাঁড়িয়ে একজন মধ্যবিত্ত বা নিন্নসধ্যবিত্ত শ্রেণীর মানুষ
যদি তার একাধিক প্রান্তিক পরিচয় প্রকাশ্যে নিয়ে আসতে চান, তবে তাকে
সমাজের বিভিন্ন দিক থেকে অসম্মান, এমনকি হিংসার সম্মুখীন হতে হয়। আমি
লক্ষ্য করেছি যে বহু মানুষ তাদের নিজেদের প্রান্তিক পরিচয়কে সোচ্চারে প্রকাশ
করতে পারেন না, কারণ "সমাজ থেকে বিচ্ছিন্ন হয়ে যাওয়ার ভয়” একটি প্রধান
বাধা হিসেবে তাদের সামনে উঠে আসে।
 
একজন সমকামী 'সেয়েলি বাচনভঙ্গিসম্পন্ন পুরুষ' হিসেবে আমি অনেক সময়েই
আমার একাধিক প্রতিবন্ধী বন্ধুদের কাছ থেকে বিভিন্ন রকম অসম্মানজনক আচরণ
এবং বিরূপ মন্তব্য পেয়েছি। আবার আমার দৃষ্টি প্রতিবন্ধকতার কারণেও আমি
ক্যুইয়ার কমিউনিটির মধ্যে অর্ন্তুক্তকিরণরে ক্ষেত্রে বৈষম্যের শিকার হয়েছি।
অর্ন্তুক্তকিরণরে প্রসঙ্গে বলা যেতে পারে যে, গত কয়েক বছর ধরে 'কোলকাতা
রেইনবো প্রাইড মার্চ' -কে প্রতিবন্ধীবান্ধব করবার একটি চেষ্টা লক্ষ্য করছি।
আপাতভাবে এটি প্রগতিশীল মনোভাবের পরিচায়ক হলেও আমার মনে হয়
বিষয়টিকে আরও একটু তলিয়ে দেখবার প্রয়োজন আছে। শুধুমাত্র সাইন ল্যাঙ্গুয়েজ
ইন্টারপ্রেটার, এবং কয়েকটি হুইলচেয়ারের ব্যবস্থা করলেই কি প্রাইড মার্চ
প্রতিবন্ধীবান্ধব হয়ে উঠবে? কয়েকটি দৃশ্যমান, প্রতীকী বন্দোবস্তের মধ্যেই যদি
অন্তর্ুক্তিকরণ সীমাবদ্ধ হয়ে যায়, তবে তা কখনোই ইন্টারসেকশনের মূল ভাবকে
ধারণ করতে পারে না। একজন দৃষ্টি প্রতিবন্ধী মানুষ হিসেবে আমি কোলকাতা
শহরে আয়োজিত বিভিন্ন কৃযুইয়ার ইভেন্টে অংশগ্রহণ করে লক্ষ্য করেছি যে সেখানে
প্রতিবন্ধকতাকে যথার্থভাবে অন্তর্ভুক্ত করবার কোনো সুব্যবস্থা নেই। আর শুধুমাত্র
প্রাইড মার্চ বা ক্যুইয়ার ইভেন্টই কেন? অনলাইন ক্যুইয়ার ডেটিং আযাপগুলিও €
গ্রাইন্ডার, প্ল্যানেটরোমিও ইত্যাদি) প্রতিবন্ধী মানুষদের কাছে, বিশেষতঃ দৃষ্টি
প্রতিবন্ধী মানুষদের কাছে বেশ ইনতআ্যাকসেসিবল্‌। তাছাড়া বাংলাভাষী মানুষদের
কাছে যোরা ইংরেজি বোঝেন না) এগুলি তো অবশ্যই অনথিগম্য।

ভারতবর্ষের মত একটি আর্থ-সামাজিক ব্যবস্থার মধ্যে থেকে রোজকার জীবনে
বিভিন্ন প্রতিকূলতার সঙ্গে লড়াই করবার পর একজন প্রতিবন্ধী মানুষের পক্ষে কি
তার বিকল্প যৌনতা সম্পর্কে প্রকাশ্যে সোচ্চার হওয়া আদৌ সম্ভব? কিছু প্রতিবন্ধী
মানুষ তাদের বিকল্প লিঙ্গ পরিচয় বা যৌনতার অধিকার সম্পর্কে নিশ্চয়ই কণ্ঠ
তুলেছেন, বা তুলছেন। কিক্ত তাদের সুবিধাজনক অবস্থান (আর্থিক ও সামাজিক)
অনেকের তুলনায় তাদের বেশ খানিকটা এগিয়ে রাখে।

আগেই বলেছি যে, 'একাধিক প্রান্তিকতা' নিয়ে সমাজে ব্যক্তির অধিকার এবং
মর্যাদাকে প্রতিষ্ঠা দেওয়া যে কোনো উন্নয়নশীল অর্থনীতিতে অত্যন্ত কঠিন। তবে
আজ আমি যে এই বিষয় সম্পর্কে এমন একটি আলোচনা প্রকাশ্যে করতে পারছি
তার পিছনে কয়েকটি কারণ আছে। এখানে সে-সম্পর্কে আভাস দেওয়া অপ্রাসঙ্গিক
হবে না বলেই আমি মনে করি। একদিকে যেমন একজন সমকামী দৃষ্টিপ্রতিবন্ধী
মানুষ হিসাবে আমি বিভিন্ন ধরণের প্রান্তিকীকরণ ও বৈষম্যের শিকার হয়েছি,
তেমনই আবার অন্যদিকে আমার 'উচ্চবর্ণ”" শহরাঞ্চলে বড় হয়ে ওঠা, শিক্ষিত,
আর্থিকভাবে স্বাধীন সত্বাটি আমাকে আমার প্রান্তিকতা সম্পর্কে প্রকাশ্য আলোচনা
আমি আমার এবং আমার মত আরো অনেক মানুষের) প্রান্তিকতার অভিজ্ঞতা
সম্পর্কে লিখতে সচেষ্ট হয়েছি। অতএব ইন্টারসেকশন সম্পর্কে আলোচনা করতে
গেলে ব্যক্তির ক্ষমতা এবং ক্ষমতাহানতা -ঙভয়ের ৬পরেহ আলোকপাত করতে
হবে।

আমরা এমন এক সময়ে বেচে আছি, যখন ভিন্নতা, বৈচিত্র্য এবং বহত্ব প্রতি
মুহুর্তে বিপন্ন হচ্ছে। এই সময়ে প্রান্তিকতা এবং প্রান্তিকতার ইন্টারসেকশন সম্পর্কে
সচেতনতা তৈরি করা বিশেষভাবে প্রয়োজনীয়। সোশ্যাল মিডিয়ার যুগে
ইন্টারনেট-কে ব্যবহার করে নিজের ভাষ্য নিজের ভাষায় প্রকাশ করবার বিকল্প
আর কি আছে! -বিশেষতঃ এই কোভিড পরবর্তী সময়ে ইন্টারনেটের উপর
নির্ভরশীলতা এবং অনলাইন দুনিয়াটাই যখন প্রাত্যহিক ও বাস্তবিক সত্য হয়ে
দাঁড়িয়েছে। এখানে আমি 'নিজের ভাষ্য' এবং 'নিজের ভাষা' দুটি শব্দবন্ধের উপরেই
বিশেষ জোর দিচ্ছি। নিজের আর্থ-সামাজিক-সাংস্কৃতিক সম্পদগুলিকে ব্যবহার করে
নিজের অভিজ্ঞতা, আকাঙ্বা ও দাবী নিজেকেই প্রকাশ করতে হবে। ইন্টারনেটকে
প্রকৃত অর্থে ইনক্লুসিভ করে তোলবার দায়িত্ব আমাদের, অর্থাৎ ইন্টারনেট
ব্যবহারকারীদেরই নিতে হবে। এই প্রবন্ধটিতে আমি বাংলা ভাষায় ক্যুইয়ার এবং
প্রতিবন্ধকতার ইন্টারসেকশন বিষয়ে আলোচনার অপ্রতুলতার কারণ সম্পর্কে কিছু
আভাস দেওয়ার চেষ্টা করেছি। আশা করব ভবিষ্যতে এই নিয়ে ইন্টারনেটে বিভিন্ন
স্তরে আরও সংবেদনশীল বক্তব্য উঠে আসবে।
