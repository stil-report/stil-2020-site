---
author1: Donald Flywell Malanga
author2:
title: "The Challenges of Expressing Chindali Language Online: Perspectives from the Native Speakers of Chisitu Village in Misuku, Malawi"
date: 2020-01-01
weight: 3
slug: "challenges-of-expressing-chindali"
illustration: The_Challenges_of_Expressing_Chindali.jpg
illustrationAlt: A group of people relaxes in an open space, surrounded by trees and tall grass. A parent stands and holds the hand of a child while looking to the horizon and speaking to an elder. The elder sits at the base of a tree, holding a mobile phone in their hands. The tree has a screen in its trunk, and the elder’s mobile phone is connected to the tree by a leafy cable. The same cable-vines wrap around the tree’s trunk and branches. In the middle-distance we see another person leaning against a similar tree and reading. Behind them, gentle hills roll in the background and the sun sits low in the sky.
illustrationauthor: Illustration by Maggie Haughey
translationKey: donald
hasaudio: false
imageauthor1: Donald_Flywell_Malanga.webp
imageauthor2:
summary: "Chindali or Ndali is a Bantu language spoken by people of Misuku hills in Northern Malawi. Previously, it was estimated that there awerere approximately 70,000 speakers of the Ndali language in Malawi. However, the number of speakers has declined drastically in recent years due to political, social, cultural and other contextual factors."
---

## Introduction

Chindali or Ndali is a Bantu language spoken by people of Misuku hills in Northern Malawi. Previously, it was estimated that there were approximately 70,000 speakers of the Ndali language in Malawi. However, the number of speakers has declined drastically in recent years due to political, social, cultural and other contextual factors. The Ndali language is now endangered and only spoken by elderly people. It is rare to see the Ndali language appearing on online platforms such as social media and the internet in general. Drawing on an ecological theoretical perspective, this essay explores how the language is expressed online and the challenges the native speakers encounter in using this indigenous language on the internet. The findings show that Chindali natives express themselves online in the form of voice, video, text and other multimedia formats through social media, mobile phones, and computers. However, optimal expression is highly curtailed by a number of factors: mainly technological, economic, linguistic, social-cultural, political, and educational issues. In conclusion, the essay offers recommendations that could inform policy and practices in decolonising the internet's languages.

## Context

Language plays a very important role in the development of a country or society. It preserves cultural heritage, strengthens cooperation, builds inclusive knowledge societies, provides education for all, and ensures cultural diversity and intercultural dialogue. Over 7000 languages are spoken across the world today but not many are represented online. Recent statistics by World Bank shows that 80% of the content online is represented by one of the top ten languages: English (25.2%), Chinese (19.3%), Spanish (7.9%), Arabic (5.2%), Portuguese (3.9), Indonesian/Malaysian (3.9), Japanese (3.2%), Russian (2.5%), and German (2.1%) respectively. So where does this leave speakers of other languages?

In Malawi, as in most African countries, daily communications in rural areas are still in vernacular languages such as Chichewa, Chitumbuka, Kyangonde, Chilambya, Chitonga, Chindali, just to mention a few. The situation remains the same, despite the fact that internet and ICTs (Information and Communication Technologies) in general provide a platform to communicate, learn, manage, and disseminate knowledge specifically in areas of health, education, agriculture, arts and culture to promote socio-economic development of the country. By contrast, vernacular languages like Chindali are experiencing a significant decline in both rural and urban areas. The majority of local languages are increasingly superseded by English. Although internet penetration in Malawian rural areas is estimated at 3.4%, mobile phone use is pegged at 42% compared to 2.9% access to computers, and the use of mobile phones is growing significantly.

## Methodology

The study used PLETES ecological theory postulated by Chaudenson (2003). This theory suggests that politics, linguistics, economic, technology, education, and socio-cultural factors can influence decolonization of internet languages efforts. This theory was used to illuminate the factors that could affect the decolonization of the internet languages with respect to the Chindali language in the Malawian context. The study utilized an interpretive approach employing a qualitative method. Face-to-face interviews were conducted with ten youth and ten adult speakers of Chindali in Chisitu village, Traditional Authority Mwenemisuku, in Chitipa district. The participants were selected based on their gender, age, level of education and occupation. Chisitu Village was chosen because it is situated within the majority of Chindali speakers. Consent to conduct the interviews was sought from the participants.

Some of the questions that were posed to participants included: What type of ICT devices/gadgets do you own? For what purpose do you use them? How do you use these ICT devices to express yourself in Chindali language? What challenges do you meet or have when it comes to expressing in Chindali on the internet? Textual analysis was used to analyze their views, opinions and insights.

## Findings

### ICT device ownership and extent of use in Chindali

Out of the 20 participants interviewed, the majority of them owned the following ICT devices: basic phones (9), smartphones (2) and feature phones (3), while seven did not have any. Only four students owned laptops and the rest did not have any. The elderly participants such as professional teachers said that they had bought these gadgets from their own savings, while farmers indicated that they were given these by their relatives or children who are employed. The college students claimed that their parents bought those laptops and smartphones to use for academic activities. When it comes to internet access, only college students expressed that they had access to the internet through computer labs in their respective colleges or via personal subscription to data or internet bundles. The elderly participants acknowledged the awareness of the internet but had never accessed it. This was not surprising considering the fact that internet penetration in Malawian rural populations is still low despite various government efforts.

Participants stated they used mobile phones, social media and internet to communicate with families, as well as with customers, in the case of those participants who were involved in microenterprises. The youth, especially students, felt that they use the internet for entertainment and academic activities. They could express in Chindali by writing, texting messages, sending audio or video and other multimedia information, to their friends, guardians, classmates and teachers.

## Challenges

Participants expressed a number of challenges that affected them in their quest to express online. Due to divergent views expressed by participants, the researcher grouped the challenges according to the following thematic areas:

### Technological challenges

The youth participants felt that although mobile and internet network coverage is available when they are in college, sometimes the quality of mobile networks and slow internet affect their full participation online using Chindali. They also stated that the absence of translation software in computers and mobile phones curtails their presence in Chindali online. These are what some of these youths had to say: *"Most of these computers come with embedded languages like English and French. As a result, these gadgets do not recognize Chindali. For instance, if you are texting or writing a message in Chindali, it takes a lot of time. But when you are writing in English, as you type, the phone or computer gives suggestions about the word you are to type. This becomes easier. When you are making a call and speaking in Chindali, you need a lot of talk time compared to when you are speaking in English."*

The elders felt that they did not have the skills to utilize the internet. For instance, some respondents had to say this: *"You see sir; I have a very simple phone. I just know where to press the button to receive or cut-off the call. I don't have the skills to use smartphones which I hear have access to the internet or Facebook or WhatsApp".* Another respondent said: *"I also don't know what they call email. I just hear that there is Internet, there is WhatsApp, but I don't know how to use them. Even if I know, do I have money to buy it?"*

This was an indication that ICT skills, slow internet, poor quality of mobile network, digital divide, and absence of Chindali in mobile phones, computers and other gadgets limited the speakers to express themselves to their full potential.

### Economic challenges

Participants felt that since the money they get from whatever activities they work on is used for their livelihoods, buying a phone, data or internet bundles was not a priority. At the same time the cost of buying data bundles in Malawi is very high. Some of the elderly who did not have a phone stated that their level of monthly income is too low for them to buy a phone or a computer. They also stated that their houses were not electrified and feared that buying a phone would require the cost for charging batteries, which they could not manage. This is what some respondents had to say: *"I receive very little money from my work as a primary school teacher. The money I get, I use for feeding my children, buying clothes. Why should I spend money for buying internet bundles when even the money I get is not enough to care for my family?"* Others say that: *"If I don't have food and relish for my family, I have to prioritize between buying talk time or food. Therefore, food always becomes first".* Yet another person says, *"My house is thatched with grasses, and there is no electricity, so where do you think I am going to charge the phone when the phone battery is finished?"*

### Social-cultural challenges

The Chindali language, like other African languages, has not been documented. Culturally, it is transferred from one generation to the other through oral communications. Due to this challenge, participants felt that lack of documentation is a big issue, therefore making Chindali absent from the Internet. With the advancement of ICTs, the younger generations have developed a negative attitude towards Chindali. This is because Chindali is felt by the youth as being an old-fashioned or primitive language, which creates an inferiority complex for them in the society. Further, the poor perceptions that other members have towards the Chindali language has spilled over even in the digital space, making the expression of Chindali difficult. This is what some of the participants had to say: *"I only speak Chindali when I am calling my dad or emailing him."*, *"At our school, Chindali speakers are very few. For example, in the whole school, you may find that there are only 4 or five of us. Therefore, it is through phone, email or social media, that we communicate to each other in Chindali, but in a very secret way. When our fellow students hear us talking, they always perceive us as primitive people. As a result, sometimes we feel isolated and are forced to speak Chichewa which is regarded as superior and is a national language in Malawi*, *"Our language is always transferred to our kids orally. I have never seen a book written in Chindali."*

### Linguistic challenges

Participants felt that writing Chindali on phones or computers or on the internet requires a lot of time compared to English language. They said that expressing in English is easy because most of the software tools embedded in most ICT devices are coded in English, Arabic or French. *"When you write the word **good morning,** before you finish typing, the phone or computer will have already suggested the word. When I am writing the same word in Chindali (**mwalamusha**), I have to type the whole word and this takes a lot of time; and it will be underlined because the computer or phone cannot recognize the word"*, say some respondents.

### Educational and political challenges

Malawi does not have a language policy that protects minority languages like Chindali. As a result, its disappearance is evident even on the internet. Secondly, the participants thought that the absence of Chindali in our education curriculum especially in primary and secondary school adds to difficulties in its maintenance and revitalization. Some participants also indicated that they were illiterate so they could not write and read English fluently. As a result, they did not see any point in having an email account or buying a sophisticated phone with access to the internet, when the language of the phone is completely in English which they could not understand. This means that for some participants it is difficult to even read, write and create an email. "*Our Chindali language is not even taught in our primary and secondary schools. Therefore how do you expect it to appear on the internet! Our government only recognizes Chichewa as a national language, while Chindali and other minority languages like Chitonga, Chilambya, and Kyangonde are nowhere to be seen." "Why should I buy an expensive phone or waste my time to go and access the internet when the language is English which I cannot understand? Unless it can be translated to Chindali or one of our local languages..."*

## Conclusion

Though not comprehensive, this essay suggests how Chindali native speakers express themselves online through various ICT devices such as mobile phones, computers, internet, and social media. However, their full internet participation is hindered by a number of factors. These include linguistic, social, cultural, technological, economical issues, among others. It was also surprising to note that the challenges that native Chindali speakers experienced in expressing Chindali in physical spaces were replicated in the digital sphere too.

In addition, a review of Malawi's ICT for Development (ICT4D) Policy (2006) and ICT Policy (2013) shows that ICT localization and local content creation were priorities for the government to ensure effective participation of citizens in the digital space. However, there are no specific targets in place to monitor how such priorities could be measured and achieved. Therefore, the findings from this essay have implications for the role of academia, technology companies, civil society, government, and the global community for understanding the decolonization of internet's languages:

* Academia and the education sector should invest more in research to develop models and frameworks that are applicable to specific language contexts to aid the decolonization of the internet's languages.
* The government needs to revise the language policy to ensure that minority languages are protected and recognized in digital spaces.
* The government should bring ICT training to rural populations in collaboration with adult literacy programmes.
* The government should make broadband accessible to rural populations that communicate in vernacular languages.
* The global community should partner with national governments to provide more funding for ICT initiatives that target preservation and documentation of vernacular languages that are in a state of endangerment.
* Civil society organizations should lobby with the government and advocate for public awareness for creation of local content in digital platforms.
* Technology companies should develop open source language translation software tools that can be customized to meet specific contexts of local languages.

*This article has been written in Chindali. It has been translated into English by the author.*

## References

* Osborn, D (2010). African languages in the digital age: Challenges and opportunities for indigenous language computing. Cairo: Human Research Council.
* Swilla, I.N (2005). Dynamics of language maintenance among speakers of Chindali in Mbodzi District, Tanzania. Journal of  Asian and African Studies, Vol.(70), 1-10.
* Malawi National ICT for Development Policy. (2006). [https://npc.mw/wp-content/uploads/2020/07/national_ict.pdf](https://npc.mw/wp-content/uploads/2020/07/national_ict.pdf)
* National ICT Policy. (2013). [https://www.macra.org.mw/malawi-ict-policy-2013](https://www.macra.org.mw/?wpdmpro=malawi-ict-policy-2013)
* World Bank (2014). [https://www.worldbank.org/en/news/feature/2014/07/03/internet-access-yes-but-in-my-mother-language](https://www.worldbank.org/en/news/feature/2014/07/03/internet-access-yes-but-in-my-mother-language)
* Malawi Communication and Regulatory Authority [MACRA] (2015). Survey on access and
usage of ICT services in Malawi-Report. [https://www.macra.org.survey_on-_Access_and_Usage_of_ICT-_Services_2014_Report.pdf](https://www.macra.org.mw/wp-content/uploads/2020/08/Survey_on-_Access_and_Usage_of_ICT_Services_2014_Report.pdf)
