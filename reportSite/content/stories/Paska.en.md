---
author1: Josia Paska Darmawan
author2:
title: "Flickering Hope: Challenges in Creating Online LGBTQIA+ Content in Bahasa Indonesia"
date: 2020-01-01
weight: 10
slug: "flickering-hope"
illustration: Flickering_Hope.jpg
illustrationAlt: Two people, both with long, dark hair, face away from the viewer and into a large crowd. The person on the right wears a cochlear implant over their right ear, and has a batik with an intricate pattern in the colors of the lesbian flag. The person on the left wears a batik with diagonal stripes and circles in the colors of the nonbinary flag. In the center distance, a person raises the rainbow pride flag in the air. They are at a pride parade in Indonesia, and in the distance we see a mosque and its speaker tower, celebrating pride along with the parade attendees.
illustrationauthor: Illustration by Maggie Haughey
translationKey: paska
hasaudio: false
imageauthor1: Paska_Darmawan.webp
imageauthor2:
summary: "Growing up in a small town with limited access to the outside world, I had to rely on the internet to find information about gender and sexual orientation. As a teenager from a family with low socio-economic status (SES), my English comprehension was still very limited, so the existence of LGBTQIA+ content in Bahasa Indonesia was very much needed to answer many questions about my self-identity as a queer individual. Unfortunately, it was —and still is— so difficult to find educational and positive queer content in Bahasa Indonesia on the internet."
---


{{< single-quote text="The few voices defending the rights of LGBTs are being drowned out, or rather bullied, by the growing chorus that has turned the LGBT community into Indonesia's public enemy No.1" cite="#" author="Endy Bayuni, Editor-in-Chief of the Jakarta Post (*)">}}

Growing up in a small town with limited access to the outside world, I had to rely on the internet to find information about gender and sexual orientation. As a teenager from a family with low socio-economic status (SES), my English comprehension was still very limited, so the existence of LGBTQIA+ content in Bahasa Indonesia was very much needed to answer many questions about my self-identity as a queer individual. Unfortunately, it was —and still is— so difficult to find educational and positive queer content in Bahasa Indonesia on the internet. If you search for any queries related to LGBTQIA+ issues, the results will be dominated by articles from mainstream and religious media who tend to write negatively about the LGBTQIA+ community.

LGBTQIA+ individuals in Indonesia often face rejection and discrimination from society. Reports suggest that approximately 87.6% of Indonesians feel threatened by the existence of the LGBTQIA+ community [^1]. Meanwhile, polling from Arus Pelangi shows that 89.3% of Indonesian LGBTQIA+ individuals have faced violence in their life [^2]. In July 2018, for instance, two men in Aceh were flogged publicly after being caught having sex [^3]. A few months later, in November 2018, police officers in Lampung arrested and harrassed transgender individuals [^4]. There are also several state/municipal regulations that prohibit LGBTQIA+ activities. An example would be Pariaman's municipal law that prohibits "immoral acts" between same-sex couples and residents from "acting as a transvestite". On another hand, there is no national or local regulation that protects LGBTQIA+ individuals from being discriminated against and persecuted.

This lack of protection from the government and the heightened pressure against the LGBTQIA+ community resulted in the lack of positive content on the internet as people are too afraid to express their support openly. Based on the report from Kemitraan, there were more than 180 reports of negative news about LGBTQIA+ people on national media compared to only around 50 reports that showed support for the community in February 2016. The report also shows an overwhelmingly large volume of negative news about the LGBTQIA+ community from local and religiously-affiliated media [^5]. Unbalanced reporting about LGBTQIA+ issues leads to the mobilization of hatred against the community, which can be seen from the increasing number of protests and attacks from conservatives from 2016 onwards [^6]. 

For these reasons, I will be sharing my experience as a queer Indonesian in search of positive, educational LGBTQIA+ content in Bahasa Indonesia. In addition, I have also interviewed several individuals from the LGBTQIA+ communities and allies to identify the challenges in creating queer content on the internet. I hope this essay will be able to paint a picture of the status quo of Indonesian LGBTQIA+ content and the obstacles in adding more native content to the internet.

## The Challenges with Creating Online Articles

Search engines often become the entry point for young queers and other individuals who want to find out more about the LGBTQIA+ community. I remember when I was in high school, when I started having so many questions about my identity and preferences, I often typed in many queries into the search engine to find some answers that might help me in understanding myself. Several things that I often search for are the position of religion on LGBTQIA+, the scientific explanations about homosexuality and gender dysphoria, and inspiring stories from other LGBTQIA+ individuals. Unfortunately, there was a lack of positive content on these questions on the Indonesian internet at that time. This made my life as a closeted queer individual much harder, since it was difficult for me to find inspiring content and support in my native language on the internet.

Fast forward to now, the condition does not get much better. If we search for "LGBT" or "*homoseksualitas*" (homosexuality) on Google — the largest and most popular search engine — we will find so many results containing the word "*penyimpangan*" (deviation), "*dosa*" (sin), and "*penyakit*" (disease). Besides, there are also many English articles included in the search results, even after we have specified the setting to only include articles in Bahasa Indonesia. This shows at least two things regarding the existence of Indonesian queer content on the internet. First, positive LGBTQIA+ content is either very limited or not very prominent on search engines. Second, search engines still have limitations when it comes to curating content in Bahasa Indonesia, which causes non-Indonesian articles to still be included in the search results.

{{< side-fig
    src="/media/stories-content/Flickering_Hope_image1.webp"
    alt=""
    caption="An example of Google search result for 'homoseksualitas'."
    title="Picture 1" >}}

Some online media have published several educational and affirming articles on LGBTQIA+, such as the [Magdalene](https://magdalene.co/), [Tirto](https://tirto.id/), and [Indo Progress](https://indoprogress.com/). However, these media, with the possible exception of Tirto, are very segmented and therefore do not have the wide reach of mainstream media, which leads to their relatively low visibility on the search engine. On top of that, LGBTQIA+ organizations in Indonesia also have a very limited presence on the internet. Many of these organizations' websites are either not updated, like [GAYa Nusantara](https://gayanusantara.or.id/), or no longer accessible ([Arus Pelangi](https://www.aruspelangi.or.id/), ([PLUSH Yogyakarta](http://www.plush.or.id/). Indonesian Wikipedia pages about LGBTQIA+ related topics are also limited. As of December 2019, there were only [17 sub-categories of LGBT in Indonesian Wikipedia](https://id.wikipedia.org/wiki/Kategori:LGBT) compared to [34 in English Wikipedia](https://en.wikipedia.org/wiki/Category:LGBT). This shows that there is a huge gap between the availability of information in English and in Bahasa Indonesia.

{{< side-fig
    src="/media/stories-content/Flickering_Hope_image2.webp"
    alt=""
    caption="BRANI-Belajar, a section of BRANI's website that provides educational articles on sexual orientations and gender identities."
    title="Picture 2" >}}

In response to this condition, there have been several organizations that try to provide virtual safe spaces and more educational information for queer Indonesians. [BRANI](https://www.brani.center/), for instance, was recently established in October 2019 to facilitate young queers and their family members to talk about gender and sexuality with online counselors. They created a website with educational content about gender and sexuality and a feature for visitors to anonymously talk with psychologists and other volunteers from the organization. Other LGBTQIA+ groups, such as [Kolektif Tanpa Nama](https://www.instagram.com/kolektiftanpanama/), solely use social media to disseminate their contents on Sexual Orientation, Gender Identity and Expression, and Sex Characteristics (SOGIESC).

{{< side-fig
    src="/media/stories-content/Flickering_Hope_image3.webp"
    alt=""
    caption="Kolektif Tanpa Nama's Instagram page."
    title="Picture 3" >}}

However, these groups have been facing several challenges to further develop their content. According to Abie, the co-founder of BRANI, one of the biggest challenges for his organization is the lack of human resources to produce more content. Ai, one of the members of Kolektif Tanpa Nama, also stated that the group relies on volunteers to produce more content. Another limitation for these organizations is the lack of financial resources and technical skills needed to maintain a website. BRANI, for instance, has difficulties in increasing their presence on the internet as their free search engine optimization (SEO) booster can only be used for three keywords. On the other hand, Kolektif Tanpa Nama does not even have a website due to resource constraints. With the rejection that LGBTQIA+ individuals often have to face from our surroundings, it becomes more difficult for us to gather the resources we need to build a website that could serve as a counter-narrative against the current negative publications about the community.

## Instagram as a Chosen Platform

Due to these technical difficulties and resource limitations in creating a website, many LGBTQIA+ groups decide to focus on social media to share their content. [Instagram](https://www.instagram.com/), as the most popular social media for young adults in Indonesia, becomes the chosen platform for many organizations and queer individuals. According to Dana Fahadi, a feminist researcher and adjunct lecturer in Communication Sciences at Universitas Gadjah Mada, social media offers plenty of room to express ourselves without the need of spending money or having some technical skill. We can also grow our network and spread our content more easily on social media due to its communal design and interconnectedness.

From the perspective of Indonesian consumers, information on Instagram is also more accessible. Based on the research from Central Connecticut State University in 2019, Indonesia is ranked 60th out of the top 61 most literate countries [^7]. This means that Indonesians, unfortunately, are more suited to process bite-sized information than lengthy articles. In addition, humans are visual learners as our brain has the ability to process images quickly and retain information from it with remarkable accuracy [^8]. These factors contribute to the strengths of Instagram as a media to share information over conventional websites or other social media.

BRANI also have their own reasons on why they choose to focus on Instagram rather than other social media. According to Abie from BRANI, Instagram is more popular among the younger generation compared to other social media such as Twitter and Facebook. In addition, Abie and other members of BRANI already have an established network on Instagram so it is easier for them to promote their new account. When it comes to its features, Instagram also offers more varied options for content creators such as regular post, story, or IGTV. Direct message, comment section and additional features on Instagram story such as Q&A and poll allow interaction with users. The variety of features and the high level of engagement on Instagram makes it one of their main platforms, along with their website, to share information.

For Kolektif Tanpa Nama, Instagram becomes their only platform to educate and interact with the community. According to Ai, a member of the group, they try to broaden their reach by tagging other more established feminist or LGBT-friendly accounts to facilitate sharing content with their audience. In addition, they also try to create more interactive content such as the 30-Day Movie Challenge, where they ask their followers to recommend movies that offer a positive better portrayal of the LGBTQIA+ community. They also have a business profile so they can see the number of profile visits, level of engagement, and the reach for each post. This helps them to determine the kind of post that is favored by the public and that works for their visibility.

However, just like a website, posting content on social media also needs resources. One of the challenges that Ai highlighted is the lack of contributions to keep the Instagram profile active. She wants the group to consistently post new content every day to keep the level of engagement high. However, there is often a shortage of human resources due to the group's volunteering system. The same problem also happens with BRANI. All of its core members have full-time occupation, which makes it difficult for the organization to frequently post new contents online.

There is also an issue of discoverability of social media content. Posts on Instagram are hardly being indexed on search engines. This makes it difficult for people to find this educational and positive content as many people still rely on search engines to find information. In addition, the search feature on Instagram is still clunky and unreliable. The search feature focuses on hashtags and, while hashtag is useful for self-categorizing, it makes finding specific content nearly impossible. For example, if you want to find some content that explains the relations between Islam and LGBTQIA+, you cannot just type that into the search bar on Instagram. You have to go through thousands of pictures related to #Islam, #LGBTQIA, or #LGBTMuslims to find one specific post that might answer your question. This makes Instagram less ideal as a platform to find information online.

## Hopes for the Future

As a part of the LGBTQIA+ community, I hope for a future where there is various kinds of positive content about LGBTQIA+ persons in Bahasa Indonesia online. Currently, LGBTQIA+ content in Bahasa Indonesia mostly talks about the basics of SOGIESC — which is important to know and understand. However, I would also love to see more personal content such as coming out stories, experience of living and thriving as an LGBTQIA+ individual in Indonesia, or one's journey of reconciling faith and gender identity/sexual orientation. I was looking for inspirational content like this, and I still am looking for one coming from fellow Indonesians. I believe that positive content created by our own LGBTQIA+ community, based on our own experience and perspective, would offer bigger inspiration and hope for the young queer generation in Indonesia as it will be easier for them to relate and understand.

However, as previously mentioned, the community is facing pressure and scrutiny from the government. It might be difficult to change the government's stance on LGBTQIA+ issues, but there are many other steps that we can take to increase the presence of Indonesian queer content on the internet. First, we need to encourage and facilitate more LGBTQIA+ individuals in Indonesia to produce more content, especially in local languages. Many LGBTQIA+ individuals often feel unsafe or even inadequate to write or produce other types of content due to our living conditions in Indonesia. Therefore, it would help if we can hold workshops or training on content writing and on safety protocols for using the internet for advocacy.

Second, technical assistance is also needed to bring in more content from queer Indonesians to the internet. Many LGBTQIA+ individuals in Indonesia are still not familiar with the technical aspects of a website, nor have sufficient knowledge about how a search engine works. Those coming from rural areas, or those with low-SES backgrounds often have different experiences compared to the urban, more educated LGBTQIA+ individuals. Bringing up stories from these marginalized LGBTQIA+ individuals will give us a richer portrayal about the community, as many people in Indonesia still live in rural areas with limited access to economic opportunity. It would also be nice to see more content about how Indonesia's rich traditional culture intersects with gender and sexual diversity. Thus, providing assistance for these marginalized groups within the LGBTQIA+ community will also give them a voice to share their experience and advocate for their rights.

There are so many challenges and much room for improvement when it comes to adding more Indonesian queer content on the internet. However, I believe that it is not impossible to gather our voices and create more positive LGBTQIA+ content that inspires Indonesian queers to live their honest life. It is my dream to provide more support for the next generation of Indonesian queers so they have a better outlook on life, so they will not have to walk alone in the darkness like I did a few
years ago.

*This article has been written in Indonesian. It has been translated into English by the author.*

(*) E. M. Bayuni, ["Gay bashing signals Indonesia's growing intolerance of minorities"](https://www.straitstimes.com/opinion/gay-bashing-signals-indonesias-growing-intolerance-of-minorities). *The Straits Times*. March 5, 2016.

[^1]: Saiful Mujani Research & Consulting, *Kontroversi Publik Tentang LGBT di Indonesia: Hasil Survei Nasional SMRC 2016-2017*, January 25, 2018. Available: [https://saifulmujani.com/mayoritas-publik-menilai-lgbt-punya-hak-hidup-di-indonesia/](https://saifulmujani.com/mayoritas-publik-menilai-lgbt-punya-hak-hidup-di-indonesia/). Accessed: December 7, 2019

[^2]: "89,3 Persen LGBT di Indonesia Pernah Alami Kekerasan". Tempo, January 27, 2016. Available: [https://nasional.tempo.co/read/739961/893-persen-lgbt-di-indonesia-pernah-alami-kekerasan/full&view=ok](https://nasional.tempo.co/read/739961/893-persen-lgbt-di-indonesia-pernah-alami-kekerasan/full&view=ok). Accessed: December 7, 2019

[^3]: "Indonesia's Aceh whips gay couple for syariah-banned sex". *The Straits Times*, July 13, 2018. Available: [https://www.straitstimes.com/asia/se-asia/indonesias-aceh-whips-gay-couple-for-shariah-banned-sex](https://www.straitstimes.com/asia/se-asia/indonesias-aceh-whips-gay-couple-for-shariah-banned-sex). Accessed: December 7, 2019

[^4]: A. Harsono, "Indonesian Police Harass Transgender Women". *Human Rights Watch*, November 8, 2018. Available: [https://www.hrw.org/news/2018/11/08/indonesian-police-harass-transgender-women](https://www.hrw.org/news/2018/11/08/indonesian-police-harass-transgender-women). Accessed: December 7, 2019

[^5]: Kemitraan, *LGBTI Media Coverage and Community Media Mapping*, Available: [https://outrightinternational.org/sites/default/files/Spreads-LGBTI-MediaMediaMapping%20English.pdf](https://outrightinternational.org/sites/default/files/Spreads-LGBTI-MediaMediaMapping%20English.pdf) p. 12

[^6]: Human Rights Watch, *These Political Games Ruin Our Lives: Indonesia's LGBT Community Under Threat*, August 2016. Available: [https://www.hrw.org/sites/default/files/report_pdf/indonesia0816_web_2.pdf](https://www.hrw.org/sites/default/files/report_pdf/indonesia0816_web_2.pdf). Accessed: December 7, 2019

[^7]: J.W. Miller, "World's Most Literate Nations". *Central Connecticut State University*, 2016. Available: [https://www.ccsu.edu/wmln/rank.html](https://www.ccsu.edu/wmln/rank.html). Accessed: December 14, 2019

[^8]: T. Romih, "Humans Are Visual Creatures". *Seyens*, October 12, 2016. Available: [https://www.seyens.com/humans-are-visual-creatures/](https://www.seyens.com/humans-are-visual-creatures/). Accessed: December 14, 2019
