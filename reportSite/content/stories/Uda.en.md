---
author1: Uda Deshapriya
author2:
title: "Amidst *Virtual* Impunity: The experience of using local languages online in Sri Lanka"
date: 2020-01-01
weight: 11
slug: "amidst-virtual-impunity"
illustration: Amidst_Virtual_Impunity.jpg
illustrationAlt: We see a person’s left hand reaching towards a purple keyboard with black keys. On the keys are bright Sinhala characters. Vines and sprouting plants wind through the spaces between the keys and reach over the edge of the keyboard and out of frame.
illustrationauthor: Illustration by Maggie Haughey
translationKey: uda
hasaudio: false
imageauthor1: Uda_Deshapriya.webp
imageauthor2:
summary: "Sri Lanka is home to people belonging to several ethnic groups who speak a number of languages. Sinhalese and Tamil are the official languages in the country. Studies show that there are 7.3 million internet users in Sri Lanka, while 6.2 million people actively use social media. In fact for some, social media *is* the internet. If you ask someone in a village whether they are familiar with the internet, they may say no. But they may tell you that they are on 'FB'. The majority of social media users in Sri Lanka access the internet through mobile phones, at the same time, the number of mobile subscriptions exceeds its population."
---

Sri Lanka is home to people belonging to several ethnic groups who speak a number of languages[^1]. Sinhalese and Tamil are the official languages in the country[^2]. Studies show that there are 7.3 million internet users in Sri Lanka, while 6.2 million people actively use social media. In fact for some, social media *is* the internet. If you ask someone in a village whether they are familiar with the internet, they may say no. But they may tell you that they are on 'FB'. The majority of social media users in Sri Lanka access the internet through mobile phones, at the same time, the number of mobile subscriptions exceeds its population.[^3].

{{< side-fig
    src="/media/stories-content/Amidst_Virtual_Impunity_image1.webp"
    alt="Population in Sri Lanka (20.98 million) and the number of mobiles users (28.71 million), and facebook (6 million), instagran (1.1 million), and twitter (182.5 thousand) users."
    caption=""
    title="Figure 1" >}}

Despite the increasing number of internet users, access to information in local languages is limited. Sri Lanka's government has a trilingual administrative information service. But the websites of many state institutions do not have information in local languages. The official website of the Department of Census and Statistics can only be accessed in English[^4]. Circulars on language policy are only available in English[^5]. Broken links on the official website of Human Rights Commission of Sri Lanka give you hope, for a brief moment, that you will be able to read core human rights treaties in Sinhala[^6].

Sri Lanka's Ministry of Justice has developed an excellent online portal to access legislation and published cases. But none of these are in local languages. The Appellate Courts, whose decisions are binding on lower courts, function in English. Lower courts function in Sinhala or Tamil. But the decisions from the Appellate Courts are not translated into local languages-- not even the binding part. While almost all statutes state that when there is inconsistency between the different versions, the Sinhala text shall prevail, the Sinhala text is rarely available online.

Sri Lankan conversations on human rights and democracy often take place in English. Twitter is the preferred social media platform of academics, researchers, critics, and media personnel who hold liberal political views and who are generally pro human rights. Though several alternative online media attempt to carry out these conversations in local languages, their reach is limited[^7]. 

Feminist content is particularly inaccessible in local languages. Women's Development Foundation is a rural women's group who have been working on women's rights issues since 1983. But it was only in 2019 that we started sharing Sinhala language feminist content on socio-political and economic issues online[^8]. Though several other civil society organisations (CSOs) also create feminist content in local languages on the internet[^9], the online space, especially social media, remains hostile to women.

The reason for this hostility fundamentally lies in the lack of moderation of online content when published in local languages, particularly on Facebook. Accountability is absent. 

A few years ago, a Facebook page in Sri Lanka openly called for raping and killing school girls causing a public outcry. In 2018, a group of CSOs, in a letter to Mark Zuckerberg demanded that Facebook's community standards are properly implemented. They noted several instances where Facebook did not consider content in local languages which promoted sexual and gender based violence to be in violation of its community standards[^10]. Despite the subsequent assurance of Facebook that community standards will be implemented[^11], a quick look through Facebook shows us that there are numerous pages dedicated to share photos of women and girls--without their consent. Photos are coupled with demeaning and vulgar comments.

{{< side-fig
    src="/media/stories-content/Amidst_Virtual_Impunity_image2.webp"
    alt=""
    caption="Sexual and gender based violence on Facebook: Facebook groups sharing photos of women and girls acquired without consent. Women and girls are often referred to as 'badu' — a derogative term used for women in Sinhala, that connotes loose morals. 'Badu' directly translates to 'things'. Page shown on the far left invites people to comment the sexual acts they would perform on the women/girls in the photos."
    title="Figure 2" >}}

Similarly, a study published in 2014, showed the severity of racial hate speech on Facebook. It was noted that hate speech is often undetected by Facebook because content is written in Sinhala[^12]. In March 2018, violent mobs attacked hundreds of Muslim families in Kandy and Facebook was accused of inaction over incitement of communal violence on its platform [^13]. A few days prior to this, a video of a shopkeeper being accused of adding 'infertility pills' to food had gone viral online[^14]. The allegations were later found to be false[^15]. Despite the confirmations by experts that such a substance does not exist, some still believe that there are 'infertility pills' that can be mixed with food to cause permanent sterility. Thus, they also believe that these 'pills' can cause the extinction of a 'race'. Following the violence in Kandy, the government blocked Facebook for a week[^16]. Despite Facebook's promise to take improved measures[^17], it was used to incite and organize mob violence against Muslim communities again in May 2019. Socio-political discussions on Facebook are laced with misinformation and hate speech. One can find plenty of content on Facebook which further communal tensions.

{{< side-fig
    src="/media/stories-content/Amidst_Virtual_Impunity_image3.webp"
    alt=""
    caption="Racial hate speech: 1) 'In India there are no Muslim MPs in the ruling party. Are you ready to make Sri Lanka like this?' 2) Describes the punishments in hell for insulting a Buddhist monk. Depicts a Muslim man in the image. 3) 'Sinhalese race is the most supreme. Don't forget to birth Sinhalese legitimate children of Sakya dynasty' 4) Carries the fake news that Muslims have caused Sinhalese mothers to 'become infertile'."
    title="Figure 3" >}}

While social media is infested with misinformation and hate, mainstream news media remain polarized along identity lines. In Sri Lanka, news you may find online (and offline) differs based on whether you read in Sinhala or Tamil. For instance, when a new Commander of the Army was appointed in August 2019, Sinhala media referred to him as a 'war hero', and the Tamil media referred to him as 'an accused war criminal'[^18]. Divide.lk monitors news and continuously points out Sinhala and Tamil media reports which are divided. Polarization of communities based on language can directly be attributed to political decisions. In 1956, Parliament of Ceylon brought the oppressive 'Sinhala Only Act', imposing the language of the majority on the minorities. Sinhala was made the only official language[^19], and Tamil was not recognized as an official language until 1987[^20].

Creation of content in local languages is as challenging as accessing content online. Keyboards with Sinhala and Tamil letters are rare. Our parents printed tiny Sinhala letters, cut them out, and taped them onto the keys beside the original English characters. Though numerous Sinhala fonts have been developed, none work as well as Unicode fonts. 

{{< side-fig
    src="/media/stories-content/Amidst_Virtual_Impunity_image4.webp"
    alt="Correctly and incorrectly Sinhala characters displayed using different fonts."
    caption=""
    title="Figure 4" >}}

The key issue with Unicode Sinhala relates to the order in which different characters need to be inserted to create a letter. This order requires you to follow consonants, with diacritics. This thinking follows rules in European Languages based on Latin script. However, in Sinhalese, sometimes diacritics precede the consonant. 

{{< side-fig
    src="/media/stories-content/Amidst_Virtual_Impunity_image5.webp"
    alt="Unicode letters fails to recognize the order for writting some Sinhalese words."
    caption=""
    title="Figure 5" >}}

At the same time, Unicode Sinhala does not allow you to insert some interesting combined letters such as these: 

{{< side-fig
    src="/media/stories-content/Amidst_Virtual_Impunity_image6.webp"
    alt="Sinhalese characters."
    caption=""
    title="Figure 6" >}}

A Sinhalese letter that is becoming non-existent as few or no keyboards recognize it is;

{{< side-fig
    src="/media/stories-content/Amidst_Virtual_Impunity_image7.webp"
    alt="Sinhalese character."
    caption="dā"
    title="Figure 7" >}}

Neither do these fonts work well in Adobe-- diacritics do not stay in the correct place. This makes the creation of artwork tedious. What you can do, is to type your text using Google Cloud Input Tools, copy and paste on Pages, export it as a PDF and then as an image, open the image in Photoshop, and select the letters using the magic wand tool.

{{< side-fig
    src="/media/stories-content/Amidst_Virtual_Impunity_image8.webp"
    alt=""
    caption="Sinhala Unicode in Adobe"
    title="Figure 8" >}}

In conclusion, creating digital content in local languages remains a challenge due to the unavailability of tools, and the complexity of tools that are available. Creating content in a local language requires special tools and skills. This challenge contributes to the lack of progressive content in local languages.

The lack of digital content that is feminist, human rights friendly, and respectful, makes online spaces where communication primarily takes place in local languages, hostile to women, queer people, and minorities. There is an obvious lack of digital media that counters the mainstream narrative which remains contaminated with negative stereotypes. This exacerbates hate speech and sexual and gender based violence online.

In addition to hate speech, the prevalence of misinformation online, particularly on social media, is a cause for concern. In Sri Lanka, misinformation spreads like wildfire online. As our recent experience shows, violence and hate shared online easily transforms into real and physical violence.

The veil of anonymity enjoyed online creates a sense of impunity. In fact, when it comes to sexual and gender based violence and incitement of communal tensions online, there is virtual impunity in Sri Lanka. The failure to take action in the face of systemic and relentless sexual and gender based violence and racist hate speech online violates the fundamental right of people to equality and equal protection of the law.

*This article has been written in Sinhala. It has been translated into English by the author.*

[^1]: Census of Population and Housing, Department of Census & Statistics, 2012. [http://www.statistics.gov.lk/PopHouSat/CPH2011/Pages/Activities/Reports/FinalReport/FinalReportE.pdf](http://www.statistics.gov.lk/PopHouSat/CPH2011/Pages/Activities/Reports/FinalReport/FinalReportE.pdf). Accessed on 01/12/2019

[^2]: Article 19 of the Constitution of Sri Lanka. Available at [https://www.parliament.lk/files/pdf/constitution.pdf](https://www.parliament.lk/files/pdf/constitution.pdf)

[^3]: Datareportal. [https://datareportal.com/reports/digital-2017-sri-lanka](https://datareportal.com/reports/digital-2017-sri-lanka?rq=sri%20lanka). Accessed on 01/12/2019

[^4]: Department of Census & Statistics. [http://www.statistics.gov.lk](http://www.statistics.gov.lk). Accessed on 01/12/2019

[^5]: Department of Official Languages. [http://www.languagesdept.gov.lk/web/exam/index.php](http://www.languagesdept.gov.lk/web/exam/index.php?option=com_content&view=article&id=5&Itemid=134&lang=en#circulars). Accessed on 01/12/2019

[^6]: Human Rights Commission of Sri Lanka. Example of broken link: [http://www.hrcsl.lk/sinhala](http://www.hrcsl.lk/sinhala/?page_id=245). Accessed on 01/12/2019

[^7]: See [https://www.vikalpa.org](https://www.vikalpa.org), [https://www.bbc.com/sinhala/topics](https://www.bbc.com/sinhala/topics/cg7267dz901t), [https://ravaya.lk](https://ravaya.lk), [https://www.anidda.lk/](https://www.anidda.lk/), and [https://sinhala.srilankabrief.org](https://sinhala.srilankabrief.org)

[^8]: See our content at [අත්වැල — Women's Development Foundation facebook page](https://www.facebook.com/pg/අත්වැල-Womens-Development-Foundation-314664939214231/about/?ref=page_internal).

[^9]: See [http://www.bakamoono.lk](http://www.bakamoono.lk), [https://www.facebook.com/womenandmediacollective/](https://www.facebook.com/womenandmediacollective/), [https://www.facebook.com/dedunu.sanwada/](https://www.facebook.com/dedunu.sanwada/), [https://www.facebook.com/EQUALGROUND/](https://www.facebook.com/EQUALGROUND/), and [https://www.bbc.com/sinhala](https://www.bbc.com/sinhala)

[^10]: Centre for Policy Alternatives, "Open letter to Facebook: Implement Your Own Community Standards" 10/04/2018 [https://www.cpalanka.org/open-letter-to-facebook-implement-your-own-community-standards/](https://www.cpalanka.org/open-letter-to-facebook-implement-your-own-community-standards/). Accessed on 01/12/2019

[^11]: Centre for Policy Alternatives, "Facebook responds to open letter from Sri Lankan civil society" 12/04/2018. [https://www.cpalanka.org/facebook-responds-to-open-letter-from-sri-lankan-civil-society/](https://www.cpalanka.org/facebook-responds-to-open-letter-from-sri-lankan-civil-society/). Accessed on 01/12/2019 

[^12]: Shilpa Samaratunge and Sanjana Hattotuwa, Liking violence: A study of hate speech on Facebook in Sri Lanka, Centre for Policy Alternatives, 2014. [https://www.cpalanka.org/wp-content/uploads/2014/09/Hate-Speech-Final.pdf](https://www.cpalanka.org/wp-content/uploads/2014/09/Hate-Speech-Final.pdf). Accessed on 01/12/2019

[^13]: "Sri Lanka accuses Facebook over hate speech after deadly riots", The Guardian, 14/03/2018. [https://www.theguardian.com/world/2018/mar/14/facebook-accused-by-sri-lanka-of-failing-to-control-hate-speech](https://www.theguardian.com/world/2018/mar/14/facebook-accused-by-sri-lanka-of-failing-to-control-hate-speech). Accessed on 01/12/2019 

[^14]: "Tensions in Ampara after fake 'sterilization pills' controversy", Sunday Observer. [http://www.sundayobserver.lk/2018/03/04/news/tension-ampara-after-fake-'sterilization-pills'-controversy](http://www.sundayobserver.lk/2018/03/04/news/tension-ampara-after-fake-%E2%80%98sterilization-pills%E2%80%99-controversy). Accessed on 6/02/2020 

[^15]: "Ampara: Carbohydrate clumps; not sterilizing chemical- Govt. Analyst", Daily Mirror. [http://www.dailymirror.lk/article/Ampara-Carbohydrate-clumps-not-sterilizing-chemical-Govt-Analyst-146954.html](http://www.dailymirror.lk/article/Ampara-Carbohydrate-clumps-not-sterilizing-chemical-Govt-Analyst-146954.html). Accessed 06/02/2020

[^16]: "Sri Lanka lifts ban on Facebook imposed after spasm of communal violence", Reuters, 16/03/2018. [https://www.reuters.com/article/us-sri-lanka-clashes-socialmedia](https://www.reuters.com/article/us-sri-lanka-clashes-socialmedia/sri-lanka-lifts-ban-on-facebook-imposed-after-spasm-of-communal-violence-idUSKCN1GR31R). Accessed on 01/12/2019 

[^17]: "The deadly Facebook cocktail: Hate Speech & Misinformation in Sri Lanka", Readme.lk, 14/05/2019. [https://www.readme.lk/facebook-hate-speech-misinformation-sri-lanka](https://www.readme.lk/facebook-hate-speech-misinformation-sri-lanka/). Accessed on 01/12/2019

[^18]: Divide.lk [http://divide.lk/shavendra-silva-a-war-hero-for-the-sinhala-press-an-accused-war-criminal-for-the-tamil-press](http://divide.lk/shavendra-silva-a-war-hero-for-the-sinhala-press-an-accused-war-criminal-for-the-tamil-press/). Accessed on 01/12/2019

[^19]: Official Language Act No. 33 of 1956. Available at [https://www.lawnet.gov.lk/1947/12/31/official-language-2](https://www.lawnet.gov.lk/1947/12/31/official-language-2/)

[^20]: Thirteenth Amendment to the Constitution 1987. Available at [https://www.lawnet.gov.lk/1947/12/31/thirteenth-amendment-to-the-constitution-2](https://www.lawnet.gov.lk/1947/12/31/thirteenth-amendment-to-the-constitution-2/)
