---
author1: Kimeltuwe Project
author2:
title: El uso de nuestro idioma ancestral como herramienta para preservar nuestra identidad en la era digital
date: 2020-01-01
weight: 9
slug: "use-of-our-ancestral-language"
illustration: Kimeltuwe.jpg
illustrationAlt: Caricatura de tres personas Mapuche paradas detrás de un escritorio con dos computadoras y un vaso de mate.
illustrationauthor: Illustración de Kimeltuwe, via kmm.cl
translationKey: kimeltuwe
hasaudio: true
imageauthor1: Kimeltuwe.webp
imageauthor2:
summary: "Kimeltuwe es una propuesta que surgió en 2014 con el sueño de convertirse en un proyecto educativo visual que contribuya a la enseñanza y aprendizaje del idioma Mapuche. La finalidad de Kimeltuwe es compartir material gráfico y audiovisual en Mapuzugun sobre distintos temas de interés a través de distintas plataformas en internet material gráfico y audiovisual. En general, el material está orientado para docentes, pero también a la difusión y revitalización del Mapuzugun en el contexto de las nuevas tecnologías y redes sociales."
---

*Esta entrevista se realizó en el mes de Julio de 2020, como un aporte para el reporte sobre el Estado de los Idiomas de internet. El proyecto Kimeltuwe se enfoca en la enseñanza online del idioma Mapuzugun hablado por el pueblo Mapuche en los territorios que hoy en día se conocen como Chile y Argentina. En su trabajo, el proyecto Kimeltuwe también rescata temas históricos e identitarios del pueblo Mapuche, que plasman en material visual y audiovisual.*

**1. Cuéntennos sobre ustedes: ¿quienes son, qué es el proyecto Kimeltuwe, cómo surgió y qué necesidad intenta cubrir?**

[Kimeltuwe](http://kmm.cl/inchin/) es una [propuesta educativa](https://revistas.ucm.es/index.php/ARIS/article/view/59167) que surgió en 2014, con el sueño de convertirse en un proyecto educativo visual, que contribuya a la enseñanza y aprendizaje del idioma Mapuche. La finalidad de Kimeltuwe es compartir material gráfico y audiovisual en Mapuzugun, sobre distintos temas de interés, a través de distintas plataformas en internet. En general, el material está orientado a docentes, pero también a la difusión y revitalización del Mapuzugun en el contexto de las nuevas tecnologías y redes sociales.

Publicamos periódicamente láminas y videos, que siguen un enfoque de desarrollo progresivo de habilidades comunicacionales y formación de vocabulario del Mapuzugun. Pero también abordamos temáticas de índole política y actualidad, en pos de la recuperación y revitalización de una identidad Mapuche, en el territorio ocupado en la actualidad por los estados Chileno y Argentino.

Para más información ver los artículos: [Estudio de caso por Rising Voices](https://rising.globalvoices.org/lenguas/investigacion/activismo-digital-de-lenguas-indigenas/estudios-de-caso/kimeltuwe/), [Kimeltuwe: un proyecto que busca mantener vivo el mapudungun](https://interferencia.cl/articulos/kimeltuwe-un-proyecto-que-busca-mantener-vivo-el-mapudungun), y [Kimeltuwe: mapudungún a través de las redes sociales](https://www.endemico.org/actualidad/kimeltuwe-revitalizando-la-lengua-Mapuche-a-traves-de-las-redes-sociales/).

**2. Hablennos del Mapuzugun, para quienes no conocemos el contexto: en qué regiones se habla, quiénes son el pueblo Mapuche, y otros aspectos históricos, sociales y culturales que les parezca importante compartirnos.**

La nación Mapuche habita un territorio denominado actualmente y en Mapuzugun *Wallmapu* (literalmente "territorio alrededor", en castellano). En términos muy generales -- puesto que es tema de mucho debate circunscribir el territorio en un mapa actual, debido a las implicancias políticas y de identidad --, se podría decir que el territorio corresponde a lo que ahora es zona centro sur de Chile (regiones del Biobio, Araucanía, Los Lagos y Los Ríos), y en Argentina correspondería a las provincias de Newken, la Pampa, Río Negro y Chubut.

No obstante lo anterior, para algunos el territorio también corresponde a las áreas donde históricamente se ha hablado Mapuzugun, lo que refuerza la idea de una profunda conexión entre el idioma y la identidad.

Independiente de lo anterior, las áreas donde todavía ha sido circunscrito (o que es lo mismo sobrevivido) el Mapuzugun, corresponde a distintos territorios menores dentro de ese gran territorio *Wallmapu*. La transmisión intergeneracional del idioma está en evidente disminución, y los espacios de habla se han reducido a lo formal (expresión de la religiosidad, espacios políticos Mapuche, e identidad nacional Mapuche), y en algunos lados, los menos, aún persiste en el espacio familiar.

En las últimas décadas, los estados chilenos y argentinos han tratado de revertir las políticas que en otrora posibilitaron la disminución de hablantes, por medio de programas donde personas Mapuche hablantes -- o no hablantes -- enseñan lengua y cultura Mapuche en las escuelas. Lamentablemente, esto no se ha traducido en mayores hablantes, o en detener la velocidad de la disminución de hablantes. Las razones son muchas, pero principalmente porque este esfuerzo no está acompañado de una mayor autonomía política de las personas Mapuche, quienes son tratadas como cualquier ciudadano o ciudadana chilena o argentina hispanohablante, persistiendo la visión de que el Mapuzugun es un idioma inferior. También se debe a la poca seriedad por parte del estado en la formación de las entidades educativas, que están enseñando Mapuzugun en el aula.

**3. ¿Cómo está su comunidad utilizando el Mapuzugun en internet?**

En la actualidad, el Mapuzugun se utiliza en redes sociales por hablantes, neo-hablantes y estudiantes del idioma. Dependiendo con qué otro idioma se compara su uso, nos damos cuenta que todavía se usa muy poco respecto a idiomas dominantes como el inglés o el castellano. Incluso en comparación con otros idiomas de sudamérica como el quechua o el aymara. Sin embargo, si se compara con otros idiomas que están a punto de desaparecer, vemos que hay una mayor presencia en la actualidad. El Mapuzugun entonces que se ve en redes sociales (facebook, twitter, instagram), se utiliza con muchos propósitos comunicativos, y para tratar diferentes temas. Hay quienes lo ocupan para enseñar y practicar, otros para bromear y tener esparcimiento -- incluso hay algunas páginas de memes en Mapuzugun.

{{< single-gallery
    src_img="/media/stories-content/Kimeltuwe_0.webp"
    alt=""
    source="Twitter"
    link="https://twitter.com/kimeltuwe/status/823986460407844864"
    caption="Tomas café o tomas mate? Meme en Mapuzungun. Imagen de @Kimeltuwe"
>}}

{{< single-gallery
    src_img="/media/stories-content/Kimeltuwe_1.webp"
    alt=""
    source="Twitter"
    link="https://twitter.com/kimeltuwe/status/1342056698022866947"
    caption="El 25 de Diciembre, celebras Navidad o la victoria de los Mapuches en la batalla de Tucapel en 1553? Meme en Mapuzungun. Imagen de @Kimeltuwe"
>}}

**4. ¿Qué desearían poder crear o compartir en internet en Mapuzugun que hoy no es posible?**

Sería genial poder colocar Mapuzugun en la plataforma de Youtube o Facebook. Ni siquiera en cuanto a la traducción de la interfaz, sino en cuanto a cosas tan simples como poder colocar que un contenido está en Mapuzugun. Por ejemplo, cuando uno sube un video a Youtube o Facebook y desea colocarle la transcripción en Mapuzugun, no es posible hacerlo ya que no aparece en el listado de idiomas predeterminados. Entonces, si quieres subir una transcripción en Mapuzugun, debes decir que está en castellano o inglés.

Nos parece que estas plataformas, en general, perpetúan la idea colonialista de que hay idiomas con mayor valor y capacidad para podernos comunicar, lo que va en desmedro de los idiomas minorizados.

Creemos que una solución a este problema es crear conciencia en los y las usuarias de usar su propia lengua en las plataformas, y así, evidenciar su importancia ante las grandes empresas que controlan la información en redes sociales, para que comiencen a respetar el uso de nuestras lenguas.

En las aplicaciones que usamos para las impartir clases, es posible elegir el idioma en el cual están escritas las fichas educativas, pero para muchas lenguas minorizadas solamente nos queda la opción "otros/desconocido".

**5. ¿Cómo se ve el contenido en Mapuzugun en internet? ¿Qué está presente y que no?**

En la actualidad, si bien existe mucho contenido, creo que la mayor parte no pertenece a un proyecto que sea perdurable en el tiempo. Por ejemplo, uno puede encontrar noticias en Mapuzugun, pero corresponden a algunas instancias separadas solamente. Sería genial contar con proyectos que se mantengan en el tiempo. Sería genial que existiera un noticiero en Mapuzugun que de cuenta de los hechos que suceden en la actualidad.

También sería genial que se realizaran telenovelas o series en Mapuzugun, u otras obras de entretenimiento. También deberían existir más páginas de enseñanza, con mayor cantidad de audio y videos.

También sería bueno ver más traducciones de obras clásicas. He leído algunas obras que se escribieron en castellano, y que fueron muy bien traducidas al Mapuzugun. La exposición a buenas traducciones es de beneficio para quienes están aprendiendo Mapuzugun, al tener la posibilidad de poder leer en este idioma. En la misma línea, contar con herramientas de idiomas, como Google Translator en Mapuzugun o que las "traducciones" que propone Facebook o Netflix, se pudieran cambiar al Mapuzugun. Sería de mucha utilidad para aumentar la exposición al idioma por parte de personas que lo están aprendiendo.

**6. ¿Cómo, dónde y con qué tecnologías comparten o crean contenidos en su idioma?**

Utilizamos diversas tecnologías y plataformas. Subimos nuestros contenidos en redes sociales en formato de imagen, video y audios. Asimismo, creamos materiales para que puedan ser impresos o fotocopiados por educadores del Mapuzugun.

Un ejemplo de lo anterior es la página [azumchefe.cl](http://azumchefe.cl/) que cuenta con un diccionario en línea con pronunciaciones. La idea de esta página es poder usarla en procesos de revitalización, en paralelo con clases que realiza la organización [Mapuzuguletuaiñ](http://mapuzuguletuain.cl/). En clases, muchas veces se utilizan diccionarios que no son óptimos, ya sea porque utilizan otro sistema de escritura, o porque su publicación es muy antigua y no cuenta con neologismos que se utilizan en clases. Por eso dejamos a disposición esta herramienta en línea.

Además colocamos la pronunciación de algunas palabras para que el o la estudiante se vuelva también un ser más autónomo en su aprendizaje, y no tenga que depender para todo de un educador o educadora.

Finalmente, la plataforma también hace uso de una aplicación llamada [Wordtheme](https://play.google.com/store/apps/details?id=fr.jmmoriceau.wordtheme&hl=en) que permite cargarle listados o vocabularios de palabra. Entonces, creamos una base de datos que cuente con marca gramatical, ejemplos y definiciones. Si la persona descarga esta app y baja nuestra hoja de datos, puede acarrear consigo el diccionario en cualquier parte que ande. Lo cual es muy práctico, considerando que muchos cursos se realizan en lugares apartados con nula o muy precaria conexión a internet.

Asimismo, la página cuenta con gramática básica para ir entendiendo los conceptos que se tratan en clase. Algo que queremos realizar a futuro, es generar un plan de estudio con objetivos comunicativos y secuencia de actividades, que puedan realizar de manera autónoma los y las estudiantes, y así contar con un curso online masivo de Mapuzugun.

Trabajando en la modalidad online, enseñando Mapuzugun se tiene que utilizar mucho aplicaciones ya existentes como [Memrise](https://www.memrise.com/), [Quizlet](https://quizlet.com/), [Kahoot](https://kahoot.com/), o [Learning Apps](https://learningapps.org/), ya que sin estas herramientas es más difícil lograr una clase dinámica. Con estas plataformas se logra crear sesiones de autoestudio o seguimiento, entremedio de las clases sincrónicas online. Todas estas plataformas y los ejercicios para las clases de autoestudio, se suben a [Google Classroom](https://en.wikipedia.org/wiki/Google_Classroom) para tenerlos en un lugar de manera ordenada. Lo bueno es que estas plataformas tienen la opción para que la profesora pueda crear cuentas para los alumnos, crear aulas con secuencias de actividades, y pueda revisar la actividad del alumno en la plataforma, su avance y los ejercicios que todavía le faltan. Para explicaciones de ejercicios o plataformas se usa [Loom](https://www.loom.com/) para grabar la pantalla del computador y el video de uno, explicando el tema. Una manera entretenida para revisar el conocimiento del vocabulario durante la clase sincrónica es Kahoot o Quizlet Live, ya que los alumnos trabajan en equipos o "compiten" de manera individual.

**7. ¿Cuál es un reto para conseguir crear o compartir en internet en su idioma?**

En la actualidad hay una dificultad en cuanto a la normalización o regularización de la escritura. Existen 3 alfabetos importantes, por lo cual escoger un alfabeto implica un poco dejar de lado lo producido en los otros alfabetos, o tener que modificarlo de antemano para poder usarlo.

También existe una percepción respecto de los idiomas minorizados como estrictamente orales, como si eso fuese una particularidad de los mismos. Cuando, en rigor, el castellano y cualquier idioma del mundo, tienen una forma oral -- que es a la primera que accedemos cuando somos infantes. Vale decir, todos los idiomas se transmiten de forma oral, es al momento de escolarizarnos (y aprender las escrituras) cuando nuestros idiomas ya no están presentes. Entonces, en nuestra visión, debe enseñarse la escritura para poder contar con esa herramienta.

Debido a los procesos históricos de colonización europea, donde se impuso una lengua dominante y no se pudo desarrollar una escritura propia, en la actualidad debemos usar las letras del alfabeto latino. No obstante lo anterior, contamos con una rica tradición escrita que se remonta al siglo XVI, cuando se escribió la primera gramática del Mapuzugun. En el siglo XIX también aparecieron los primeros autores y autoras Mapuche, y gracias a sus trabajos escritos contamos con un acervo escrito que de otro modo quizás no tendríamos.

De todas maneras, la naturaleza multimediática de las plataformas sociales permite transmitir el Mapuzugun por muchos medios, y es bueno que haya muchas modalidades de Mapuzugun (escrito, oral, etc.)

Les dejamos este enlace en donde hablamos de la [Historia de la escritura Mapuche](https://kmm.cl/uncategorized/historia-de-la-escritura-Mapuche/).

{{< single-gallery
    src_img="/media/stories-content/Kimeltuwe_2.webp"
    alt="Dibujo de mujer Mapuche saludando"
    source="kmm.cl"
    link="https://kmm.cl/"
    caption="Imagen de Kimeltuwe"
>}}

**8. ¿Porqué es importante traer a internet nuestros idiomas originarios?**

La juventud y los niños Mapuche crecen con la tecnología y con el internet. El internet es una instancia para que este público se pueda acercar al Mapuzugun. Con las plataformas antes mencionadas como Quizlet, Learning Apps o Kahoot, se puede retroalimentar a los niños, pero a través del dispositivo que usan todo el día, ya que son aplicaciones que se pueden usar en el computador, pero también en la tablet o celular. Y en vez de hacer un test de vocabulario escrito, se puede hacer una competición en Kahoot o Quizlet live.

Aunque hay que mencionar que el acceso al internet no está garantizado en las zonas rurales y muchos alumnos no tienen conexión al internet, o tampoco tienen acceso a un computador.

Las historias de nuestro pueblo tienen que ser escritas, y necesitan ser escritas o habladas en Mapuzugun también. Existen algunos colectivos y personas que investigan y escriben la historia de nuestro pueblo desde su perspectiva, pero todavía no podemos encontrar nuestra historia a un nivel macro, como son los libros de estudio para la escuela. Todavía rige el pensamiento y la pluma de la sociedad mayoritaria. Nuestras historias no necesariamente son las de grandes héroes como lo hacen celebrar los estados coloniales y postcoloniales. Más bien, nuestra historia es la historia de cada Mapuche que sobrevivió a las adversidades y violencias: la mujer que tuvo que irse a la ciudad para tener un sueldo, las mujeres y hombres que después volvieron pero ya no había más lugar en sus [*lof*](https://es.wikipedia.org/wiki/Lof), y tuvieron que volver a las urbes con las raíces cortadas y sin un lugar a cual volver. Las experiencias y recuerdos de cada uno constituyen nuestra memoria como pueblo.
