---
Title: Rejoignez-nous!
date: 2020-01-01
tags: engage
summary: ""
---

Ce rapport est pour nous un travail en cours et une initiative continue alliant recherche et action.

Nous voulons que de multiples générations et communautés de personnes nous rejoignent dans notre travail et nous recevrons avec plaisir les commentaires et suggestions d’amélioration et de remise à jour des informations proposées ici.

Voici comment vous pouvez participer à cette initiative :

### Vous êtes traducteur·ice

Si vous êtes traducteur·ice et que vous souhaitez rendre ce rapport disponible dans votre langue, n’hésitez pas à nous contacter. [Écrivez-nous un e-mail](mailto:languages@whoseknowledges.org) mentionnant **NEW TRANSLATOR** en objet pour recevoir plus d’informations.

### Vous travaillez dans les médias

Si vous êtes journaliste ou chargé·e de communication et que votre demande concerne les médias, n’hésitez pas à [nous contacter via e-mail](mailto:languages@whoseknowledges.org) en mentionnant le nom de votre publication ou média en objet. N’oubliez pas de nous communiquer vos délais et la langue de publication.

Nous avons créé une fiche de référence contenant les informations essentielles de ce rapport, accessible [ici](http://LINK).

### Vous faites partie d’une communauté marginalisée

Si vous faites partie d’une communauté marginalisée, prenez conscience que vous n’êtes pas seul·e et que votre communauté a le droit de décider quels savoirs partager avec le monde et comment. Vous aimeriez entrer en contact avec d’autres personnes effectuant un travail de justice du savoir ? [Contactez-nous par e-mail](mailto:languages@whoseknowledges.org).

### Vous travaillez dans la recherche académique ou les technologies

Si vous travaillez dans la recherche académique ou les technologies, entrez en contact  avec les communautés linguistiques avec éthique : lisez, écoutez, citez les œuvres, élargissez vos manières d’enseigner et de travailler pour inclure leurs connaissances. Travaillez avec les communautés linguistiques marginalisées pour créer les technologies et les savoirs dont elles ont besoin, plutôt que ceux dont vous pensez qu’elles ont besoin. Si vous souhaitez contribuer à ce travail, [écrivez-nous un e-mail](mailto:languages@whoseknowledges.org) avec l’objet **STIL ACADEMIA** ou **STIL TECH**.

### Vous travaillez dans la philanthropie

Si vous êtes une fondation, prenez conscience de l’importance d’utiliser et de soutenir la production, la préservation et la numérisation des supports et la production du savoir dans les langues des communautés que vous servez. Si vous souhaitez contribuer à notre initiative, [envoyez-nous un e-mail](mailto:languages@whoseknowledges.org) avec l’objet **STIL PHILANTHROPY**.

### Restons en contact !

Nous sommes un collectif de trois organisations : Whose Knowledge?, l’Oxford Internet Institute et le Centre for Internet and Society (Inde).

Si vous souhaitez recevoir des informations sur cette initiative et d’autres projets défendus par Whose Knowledge?, [inscrivez-vous à notre newsletter « The Internet We Want »](https://whoseknowledge.org/join/).

Si vous souhaitez rester en contact avec les trois organisations qui ont mené à bien ce rapport, vous trouverez les liens vers nos réseaux sociaux ci-dessous.

#### Twitter

[@WhoseKnowledge](https://twitter.com/whoseknowledge)

[@oiioxford](https://twitter.com/oiioxford)

[@cis_india](https://twitter.com/cis_india)

#### Facebook

[Whose Knowledge?](https://www.facebook.com/WhoseKnowledge/)

[Oxford Internet Institute, University of Oxford](https://www.facebook.com/oiioxford)

[Centre for Internet and Society - Access To Knowledge](https://www.facebook.com/cisa2k)
 
#### Instagram

[Whose Knowledge?](https://www.instagram.com/whoseknowledge/)

[oiioxford](https://www.instagram.com/oiioxford/)

[#centreforinternetandsociety](https://www.instagram.com/explore/tags/centreforinternetandsociety/)

#### Youtube

[Whose Knowledge?](https://www.youtube.com/channel/UCR97k4sWCKHxKVeI64xACSg)

[Oxford Internet Institute, University of Oxford](https://www.youtube.com/user/OIIOxford)

[Centre for Internet and Society](https://www.youtube.com/channel/UC0SLNXQo9XQGUE7Enujr9Ng)
