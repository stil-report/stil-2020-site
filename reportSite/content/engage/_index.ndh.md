---
Title: Join us!
---
This report is a work-in-progress and a work-in-process, and an ongoing research-action initiative for us.

We want multiple generations and communities of people to join us in our work, and welcome comments and suggestions for improving and updating the information we’ve offered here at any time!

Here’s how you can engage with this initiative:

### If you’re a translator

If you’re a translator who would like to make this report available in your language, do not hesitate to reach out. [Email us](mailto:languages@whoseknowledges.org) with the subject line **NEW TRANSLATOR**, and we’ll be happy to get in touch with you!

### If you’re in media

If you’re a journalist or communicator who has a media request, do not hesitate to [contact us via email](mailto:languages@whoseknowledges.org) with the subject line **MEDIA/NAME OF PUBLICATION**. Make sure to let us know about your deadline and the language of publication. 

We have created a short fact sheet for quick reference that captures the key information from this report, and you can access it [here](/media/pdf/FACTSHEET_State_of_the_Internets_Languages_Report.pdf).

### If you’re from a marginalized community

If you’re from a marginalized community, recognize that you’re not alone and that it is your community’s right to decide what knowledge you would like to share with the world and how. Would you like to be connected to others doing knowledge justice work? [Reach out to us via email](mailto:languages@whoseknowledges.org)! 

### If you’re in academia or technology

If you’re working in academia or technology, engage with marginalized language communities with intention — read, listen, cite their work, expand your ways of teaching and working to include their knowledges. Work with marginalized language communities to create the technologies and knowledges they need, rather than those you think they need. If you’d like to contribute to this work, [email us](mailto:languages@whoseknowledges.org) with the subject line **STIL ACADEMIA or STIL TECH**. 

### If you’re in philanthropy

If you’re a funder, recognize the importance of resourcing and supporting the production, preservation and digitization of materials and production of knowledge in the languages of the communities you serve. If you’d like to contribute to our initiative, [send us an email](mailto:languages@whoseknowledges.org) with the subject line **STIL PHILANTHROPY**.

### Stay in touch with us!

We are a collaborative of three organizations: Whose Knowledge?, the Oxford Internet Institute, and the Centre for Internet and Society (India). 

If you’d like to receive updates from this initiative and other projects championed by Whose Knowledge?, [sign up for our newsletter "The Internet We Want"](https://whoseknowledge.org/join/).

If you want to stay in touch with the three organizations leading this report, find the links to our social media channels below.

#### Twitter

[@WhoseKnowledge](https://twitter.com/whoseknowledge)

[@oiioxford](https://twitter.com/oiioxford)

[@cis_india](https://twitter.com/cis_india)

#### Facebook

[Whose Knowledge?](https://www.facebook.com/WhoseKnowledge/)

[Oxford Internet Institute, University of Oxford](https://www.facebook.com/oiioxford)

[Centre for Internet and Society - Access To Knowledge](https://www.facebook.com/cisa2k)
 
#### Instagram

[Whose Knowledge?](https://www.instagram.com/whoseknowledge/)

[oiioxford](https://www.instagram.com/oiioxford/)

[#centreforinternetandsociety](https://www.instagram.com/explore/tags/centreforinternetandsociety/)

#### Youtube

[Whose Knowledge?](https://www.youtube.com/channel/UCR97k4sWCKHxKVeI64xACSg)

[Oxford Internet Institute, University of Oxford](https://www.youtube.com/user/OIIOxford)

[Centre for Internet and Society](https://www.youtube.com/channel/UC0SLNXQo9XQGUE7Enujr9Ng)