---
Title: ¡Ben Nheto Txen!
---
Llín nhi nha llalhal llaken, nha llalhal dzogton, kakse llunhen lue yell enchnha guzedo, gaksedlho nha zuyibo. 

¡Llaklhallto ben lhao yell nha bene za nhao gunk´ ake nheto txen, leskakse lhe waweto guzow dill,  gaklhe shlhelho llín nhí,  enchanha bán, bateze llenlho! 
 
Ganhí nheto gaklhe wak gunhenho nheto txen:

### Cha lhue llazogo dill 

Cha lhue llazogo dill nha llenlho wayog llín nhí xtillo bit gakanlho [wayilgo nheto](mailto:languages@whoseknowledges.org). Gan nhe bi ken dzogo de gullíao  **NEW TRANSLATOR**, le waweto guchal nhento lhue. 

### Cha lhue yixwelho bi llak yell líu 

Cha dzogo nha yixwelho bi llak yell líu nha llenlho kwago llín ko niog, bit gakanlho guzogo nheto. Gan nhe bi ken dzogo de gullíao **MEDIA/NAME OF PUBLICATION**. Bi galallo gullíao bat llenlo xog xchínho nha bí dill yogen.

Bayunto tu chop ze dill llín nhí, ga batup to yoyte da blhao dzan, [ganhí wak lelhon](/media/pdf/FACTSHEET_State_of_the_Internets_Languages_Report.pdf).

### Cha zúu yell ga llun´ake kaze dill ka.

Cha zúu yell ga llun´ake kaze dill ka, de nezlho nllayllo nha yell kan de né bi llen´aklhe nha a´klhe gunhe´ake llín nha guzed´aklhe benhe yublhe yoy da nez´aklhe nhaa da zu kueznhen´ake. 

¿Llenlo gumbielho bene yublhe zuyib enchanha gat latg ke nhuteze lhao yel wazeden? [¡Bzog nheto!](mailto:languages@whoseknowledges.org)

### Cha lhue nhun xchínho yel waseden o cha dan nhe dill xtilhe tecnología.

Cha lhue nhun xchínho yel waseden o cha dan nhe dill xtilhe tecnología btil chay nhen yell ka ga llun´ake kase xtill´ aken —

bzog, bzenay, bayumbié xchín´aken, nha blay yoy da nezlho nha gaklhe xlhuelho enchnha bi gunho kase gaklhe zu kuez´ake. 

De gunhenho llín yell kí enchanha zak shel yoy da yashj´aklhe, le´aken de nhé bin yashj, kelhe lhue. Cha saklhallo gudao nía nhao llín nhi, [bzog nheto](mailto:languages@whoseknowledges.org), gan  nhe bi ken dzogo de gullíao  **STIL ACADEMIA** o cha **STIL TECH**.

### Cha lhue llaklhelo benhe o yell´ake.

Cha lhue llaklhelho, de wayumbielho da blhaon shel mesh nha yelá xchín laze da gaklhelhe wayog, wayak chí, wayo ltipe nha gaz laz dill ka nhe´ake yell ka dzeo niá nha.  

Cha llenlo gaklhelho xchínto, [bzog nheto](mailto:languages@whoseknowledges.org) nha gullíao **STIL PHILANTHROPY**.

### ¡Bchalg nhen nheto!

Chone ton nhauto llín nhi: ¿Nhu ke nhak yel nezen?, Xlatge Internet gan nhe Oxford nha xlatge Internet nha yoy yell Internet (India).

Cha llenlho chíshjueto lhue gaklhe zaá llín nhí nha yelan da ngo nía nha ¿Nhu ke nhak yel nezen?, [ganhí bllía lhao enchanha waselto dan dzogto zelishg wag “Ká Interneten Llenlesho”](https://whoseknowledge.org/join/). 

Cha llenlho nhao nha nezlho yelhat ke bene ka nhao llín nhi bda llín nhi, ganhí gukwanto da shgua lhue ga nhak xlatg´aken.

#### Twitter

[@WhoseKnowledge](https://twitter.com/whoseknowledge)

[@oiioxford](https://twitter.com/oiioxford)

[@cis_india](https://twitter.com/cis_india)

#### Facebook

[Whose Knowledge?](https://www.facebook.com/WhoseKnowledge/)

[Oxford Internet Institute, University of Oxford](https://www.facebook.com/oiioxford)

[Centre for Internet and Society - Access To Knowledge](https://www.facebook.com/cisa2k)
 
#### Instagram

[Whose Knowledge?](https://www.instagram.com/whoseknowledge/)

[oiioxford](https://www.instagram.com/oiioxford/)

[#centreforinternetandsociety](https://www.instagram.com/explore/tags/centreforinternetandsociety/)

#### Youtube

[Whose Knowledge?](https://www.youtube.com/channel/UCR97k4sWCKHxKVeI64xACSg)

[Oxford Internet Institute, University of Oxford](https://www.youtube.com/user/OIIOxford)

[Centre for Internet and Society](https://www.youtube.com/channel/UC0SLNXQo9XQGUE7Enujr9Ng)