---
Title: Jiunge nasi!
date: 2020-01-01
tags: engage
summary: ""
---

Ripoti hii ni kazi inayoendelea na mchakato-kazi, na ni mpango unaoendelea wa hatua za utafiti kwetu.

Tunataka vizazi na jumuiya nyingi za watu zijiunge nasi katika kazi yetu, na kukaribisha maoni na mapendekezo ya kuboresha na kusasisha maelezo ambayo tumetoa hapa wakati wowote!

Hivi ndivyo unavyoweza kujihusisha na mpango huu:

### Ikiwa wewe ni mfasiri

Ikiwa wewe ni mfasiri ambaye ungependa kutoa ripoti hii katika lugha yako, usisite kuwasiliana nasi. [Tutumie barua pepe](mailto:languages@whoseknowledges.org) yenye mada **NEW TRANSLATOR**, na tutafurahi kuwasiliana nawe!

### Ikiwa uko kwenye vyombo vya habari

Iwapo wewe ni mwandishi wa habari au mwasiliani ambaye ana ombi la maudhui, usisite [kuwasiliana nasi kupitia barua pepe](mailto:languages@whoseknowledges.org) yenye mada **MEDIA/NAME OF PUBLICATION**. Hakikisha kuwa umetufahamisha kuhusu tarehe yako ya mwisho na lugha ya uchapishaji.

Tumeunda karatasi fupi ya ukweli kwa marejeleo ya haraka ambayo yananasa taarifa muhimu kutoka kwa ripoti hii, na unaweza kuipata [hapa](/media/pdf/FACTSHEET_State_of_the_Internets_Languages_Report.pdf).

### Ikiwa unatoka kwenye jamii iliyotengwa

Ikiwa unatoka katika jumuiya iliyotengwa, tambua kuwa hauko peke yako na kwamba ni haki ya jumuiya yako kuamua ni maarifa gani ungependa kushiriki na ulimwengu na jinsi gani. Je, ungependa kuunganishwa na wengine wanaofanya kazi ya haki ya maarifa? [Wasiliana nasi kupitia barua pepe](mailto:languages@whoseknowledges.org)!

### Ikiwa uko katika taaluma au teknolojia

Ikiwa unafanya kazi katika taaluma au teknolojia, jishughulishe na jumuiya za lugha zilizotengwa kwa nia — soma, sikiliza, taja kazi zao, panua njia zako za kufundisha na kufanya kazi ili kujumuisha maarifa yao. Fanya kazi na jamii za lugha zilizotengwa ili kuunda teknolojia na maarifa wanayohitaji, badala ya yale unayofikiria kuwa wanahitaji. Ikiwa ungependa kuchangia kazi hii,  [tutumie barua pepe](mailto:languages@whoseknowledges.org) na mada **STIL ACADEMIA or STIL TECH.**

### Ikiwa uko katika uhisani

Iwapo wewe ni mfadhili, tambua umuhimu wa kupata rasilimali na kusaidia uzalishaji, uhifadhi na uwekaji nyenzo kidijitali na utayarishaji wa maarifa katika lugha za jumuiya unazohudumia.  Ikiwa ungependa kuchangia kazi hii, tutumie  [barua pepe](mailto:languages@whoseknowledges.org) na mada **PHILANTHROPY**.

### Endelea kuwasiliana nasi!

Sisi ni ushirikiano wa mashirika matatu: Ujuzi wa Nani? {Whose Knowledge?}, Taasisi ya Mtandao ya Oxford {Oxford Internet Institute}, na Kituo cha Mtandao na Jamii {The Centre for Internet and Society} (India).

Ikiwa ungependa kupokea masasisho kutoka kwa mpango huu na miradi mingine inayoendeshwa na Ujuzi wa Nani? {Whose Knowledge?}, [jiandikishe kwa jarida letu "Mtandao Tunaotaka".](https://whoseknowledge.org/join/) 

Ikiwa ungependa kuwasiliana na mashirika matatu yanayoongoza ripoti hii, pata viungo vya chaneli zetu za mitandao ya kijamii hapa chini.

#### Twitter

[@WhoseKnowledge](https://twitter.com/whoseknowledge)

[@oiioxford](https://twitter.com/oiioxford)

[@cis_india](https://twitter.com/cis_india)

#### Facebook

[Whose Knowledge?](https://www.facebook.com/WhoseKnowledge/)

[Oxford Internet Institute, University of Oxford](https://www.facebook.com/oiioxford)

[Centre for Internet and Society - Access To Knowledge](https://www.facebook.com/cisa2k)
 
#### Instagram

[Whose Knowledge?](https://www.instagram.com/whoseknowledge/)

[oiioxford](https://www.instagram.com/oiioxford/)

[#centreforinternetandsociety](https://www.instagram.com/explore/tags/centreforinternetandsociety/)

#### Youtube

[Whose Knowledge?](https://www.youtube.com/channel/UCR97k4sWCKHxKVeI64xACSg)

[Oxford Internet Institute, University of Oxford](https://www.youtube.com/user/OIIOxford)

[Centre for Internet and Society](https://www.youtube.com/channel/UC0SLNXQo9XQGUE7Enujr9Ng)
