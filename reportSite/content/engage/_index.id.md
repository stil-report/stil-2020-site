---
Title: Bergabunglah dengan kami!
date: 2020-01-01
tags: engage
summary: ""
---

Laporan ini adalah pekerjaan yang sedang berlangsung dan pekerjaan dalam proses dan  sebuah riset-aksi yang berkelanjutan.

Kami ingin melipatgandakan generasi dan komunitas orang-orang untuk bergabung dalam pekerjaan kami, dan kami menyambut baik komentar dan usul untuk memperbaiki dan memperbarui informasi yang kami tawarkan disini kapan saja!

Inilah cara anda dapat terlibat dalam inisiatif ini:

### Jika anda penerjemah

Jika anda penerjemah yang menginginkan laporan ini tersedia dalam bahasa anda, jangan ragu untuk menghubungi. [Email us](mailto:languages@whoseknowledges.org) dengan judul **NEW TRANSLATOR** dan dengan senang hanyi kami akan menghubungi anda!

### Jika anda bekerja di media

Jika anda bekerja sebagai jurnalis atau penghubung yang memiliki permintaan untuk media, [jangan ragu untuk menghubungi](mailto:languages@whoseknowledges.org) dengan judul **MEDIA/NAME OF PUBLICATION**, untuk memastikan kami mengetahui tenggat dan bahasa publikasi anda.

Kami telah menciptakan lembar fakta pendek untuk referensi cepat yang memuat informasi kunci dari laporan ini dan dapat diakses di [sini](/media/pdf/FACTSHEET_State_of_the_Internets_Languages_Report.pdf).

### Jika anda berasal dari komunitas termarjinalkan

JIka anda dari komunitas yang termarjinalkan, ketahuilah bahwa anda tidak sendiri, dan bahwa  komunitas anda berhak untuk memutuskan pengetahuan apa yang ingin dibagikan dan bagaimana caranya, pada dunia. Apakah anda ingin terhubung pada yang lain untuk melakukan kerja keadilan pengetahuan? [Hubungi kami](mailto:languages@whoseknowledges.org)! 


### Jika anda akademisi atau bekerja di bidang teknologi

JIka anda akademisi atau bekerja di bidang teknologi, bermaksud terlibat dengan komunitas dengan bahasa yang termarjinalkan - membaca, mendengarkan, mengutip karya mereka, memperluas cara anda untuk mengajar dan bekerja untuk memasukkan pengetahuan mereka. Bekerja dengan komunitas dengan bahasa yang termarjinalkan untuk menciptakan teknologi dan pengetahuan yang mereka butuhkan, daripada untuk mereka yang anda pikir mereka butuhkan. Jika anda ingin berkontribusi dalam pekerjaan ini, [hubungi kami](mailto:languages@whoseknowledges.org) dengan judul **STIL ACADEMIA** atau **STIL TECH**.

### JIka anda dermawan

Jika anda dermawan, mengakui pentingnya sumberdaya dan mendukung produksi, pelestarian dan digitalisasi materi dan produksi pengetahuan dalam bahasa komunitas yang anda layani. Jika anda ingin berkontribusi untuk inisiatif kami, [anda dapat mengirim email](mailto:languages@whoseknowledges.org) dengan judul **STIL PHILANTHROPY**.


### Tetap terhubung dengan kami!

Kami bekerja bersama tiga organisasi : Whose Knowledge?, the Oxford Internet Institute, dan the Centre for Internet and Society (India).

Jika anda ingin menerima informasi terbaru dari inisitiaf ini dan proyek utama _Whose Knowledge [daftarkan diri untuk menerima buletin kami "The Internet We Want"](https://whoseknowledge.org/join/).

Jika anda ingin tetap terhubung dengan tiga organisasi utama yang memimpin laporan ini, anda dapat menemukan tautan sosial media kami.

#### Twitter

[@WhoseKnowledge](https://twitter.com/whoseknowledge)

[@oiioxford](https://twitter.com/oiioxford)

[@cis_india](https://twitter.com/cis_india)

#### Facebook

[Whose Knowledge?](https://www.facebook.com/WhoseKnowledge/)

[Oxford Internet Institute, University of Oxford](https://www.facebook.com/oiioxford)

[Centre for Internet and Society - Access To Knowledge](https://www.facebook.com/cisa2k)
 
#### Instagram

[Whose Knowledge?](https://www.instagram.com/whoseknowledge/)

[oiioxford](https://www.instagram.com/oiioxford/)

[#centreforinternetandsociety](https://www.instagram.com/explore/tags/centreforinternetandsociety/)

#### Youtube

[Whose Knowledge?](https://www.youtube.com/channel/UCR97k4sWCKHxKVeI64xACSg)

[Oxford Internet Institute, University of Oxford](https://www.youtube.com/user/OIIOxford)

[Centre for Internet and Society](https://www.youtube.com/channel/UC0SLNXQo9XQGUE7Enujr9Ng)