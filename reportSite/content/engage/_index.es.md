---
Title: ¡Súmate!
---
Para nosotras, este informe es un trabajo en construcción, y una iniciativa de investigación-acción en proceso.

Queremos que diversas generaciones y comunidades de personas se unan a nuestro trabajo. Son bienvenidos los comentarios y sugerencias para mejorar y actualizar la información que ofrecemos aquí en cualquier momento!

Así es como puedes participar en esta iniciativa:

### Si eres traductor/a

Si eres traductora o traductor y quieres que este informe esté disponible en tu idioma, no dudes en ponerte en contacto con nosotras. [Escríbenos]((mailto:languages@whoseknowledges.org)) con el asunto **NEW TRANSLATOR**, y estaremos felices de ponernos en contacto contigo.

### Si trabajas en medios de comunicación

Si eres periodista o comunicador/a y tienes una solicitud de información, no dudes en ponerte en contacto con nosotras [por correo electrónico](mailto:languages@whoseknowledges.org) con el asunto **MEDIA/NAME OF PUBLICATION**. No olvides indicarnos el plazo de entrega y el idioma de la publicación.

Hemos creado una breve hoja informativa de consulta rápida que recoge la información clave de este informe (por ahora en inglés). [Puedes acceder a ella aquí](/media/pdf/FACTSHEET_State_of_the_Internets_Languages_Report.pdf).

### Si perteneces a una comunidad marginalizada

Si perteneces a una comunidad marginalizada, no te olvides de que no estás en soledad y de que tu comunidad tiene derecho a decidir qué conocimientos quiere compartir con el mundo y cómo. ¿Te gustaría estar en contacto con otras personas que trabajan por la justicia del conocimiento? Ponte en contacto con nosotras [por correo electrónico]((mailto:languages@whoseknowledges.org)).

### Si trabajas en el ámbito académico o tecnológico

Si trabajas en la academia o en la industria de la tecnología, colabora  con las comunidades lingüísticas marginalizadas a conciencia: lee, escucha, cita su trabajo, amplía tu forma de enseñar y trabajar para incluir sus conocimientos. Trabaja con las comunidades lingüísticas marginadalizadas para crear las tecnologías y los conocimientos que necesitan, en lugar de los que tú crees que necesitan. Si quieres contribuir a este trabajo, [escríbenos](mailto:languages@whoseknowledges.org)con el asunto **STIL ACADEMIA or STIL TECH**.

### Si eres donante

Si eres donante, reconoce la importancia de dotar de recursos y apoyar la producción, preservación y digitalización de materiales y la producción de conocimiento en las lenguas de las comunidades a las que sirves. Si quieres contribuir a nuestra iniciativa, [escríbenos](mailto:languages@whoseknowledges.org) con el asunto **STIL PHILANTHROPY**.

### ¡Sigue en contacto!

Somos una asociación de tres organizaciones: Whose Knowledge?, Oxford Internet Institute y Centre for Internet and Society (India).

Si quieres recibir información actualizada sobre esta iniciativa y otros proyectos impulsados por Whose Knowledge?, [suscríbete a nuestro boletín "La Internet que queremos"](https://whoseknowledge.org/join/).

Si quieres estar en contacto con las tres organizaciones que lideran este informe, encuentra los enlaces a nuestros canales de redes sociales a continuación.

#### Twitter

[@WhoseKnowledge](https://twitter.com/whoseknowledge)

[@oiioxford](https://twitter.com/oiioxford)

[@cis_india](https://twitter.com/cis_india)

#### Facebook

[Whose Knowledge?](https://www.facebook.com/WhoseKnowledge/)

[Oxford Internet Institute, University of Oxford](https://www.facebook.com/oiioxford)

[Centre for Internet and Society - Access To Knowledge](https://www.facebook.com/cisa2k)
 
#### Instagram

[Whose Knowledge?](https://www.instagram.com/whoseknowledge/)

[oiioxford](https://www.instagram.com/oiioxford/)

[#centreforinternetandsociety](https://www.instagram.com/explore/tags/centreforinternetandsociety/)

#### Youtube

[Whose Knowledge?](https://www.youtube.com/channel/UCR97k4sWCKHxKVeI64xACSg)

[Oxford Internet Institute, University of Oxford](https://www.youtube.com/user/OIIOxford)

[Centre for Internet and Society](https://www.youtube.com/channel/UC0SLNXQo9XQGUE7Enujr9Ng)