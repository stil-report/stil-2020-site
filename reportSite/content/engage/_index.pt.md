---
Title: Join us!
date: 2020-01-01
tags: engage
summary: ""
---

Este relatório é um trabalho em curso ou um trabalho em processo, e uma iniciativa de pesquisa e ação em andamento para nós. 

Queremos que múltiplas gerações e comunidades se juntem ao nosso trabalho, e aceitamos comentários e sugestões para melhorar e atualizar as informações que disponibilizamos aqui a qualquer momento!

Veja como você pode se engajar com esta iniciativa:

### Se você é tradutor/a/e

Se você é tradutor/a/e e gostaria de tornar esse relatório disponível em seu idioma, não hesite em entrar em contato. [Nos envie um email](mailto:languages@whoseknowledges.org) com o assunto **NEW TRANSLATOR**, e ficaremos felizes em manter contato com você!

### Se você trabalha com mídia

Se você é jornalista ou comunicador/a/e e tem uma solicitação relacionada a mídia, não hesite em [nos contatar por email](mailto:languages@whoseknowledges.org), com o assunto **MEDIA/NAME OF PUBLICATION**. Assegure-se de nos informar sobre o seu prazo e o idioma da públicação.

Nós criamos uma factsheet para referência rápida com as principais informações deste relatório, e você [pode acessá-la aqui](/media/pdf/FACTSHEET_State_of_the_Internets_Languages_Report.pdf).

### Se você for de uma comunidade marginalizada

Se você for de uma comunidade marginalizada, reconheça que você não está sozinho e saiba que é um direito da sua comunidade decidir quais conhecimentos vocês gostariam de compartilhar com o mundo e de que forma. Você gostaria de se conectar com outras pessoas que atuam com justiça epistêmica? Entre em contato conosco [por email](mailto:languages@whoseknowledges.org)!

### Se você estiver na área acadêmica ou de tecnologia

Se você trabalha na área acadêmica ou de tecnologia, engaje-se com comunidades linguísticas marginalizadas com intenção — leia, escute, cite seus trabalhos, expanda suas formas de ensinar e trabalhar para incluir seus conhecimentos. Trabalhe junto a comunidades linguísticas marginalizadas para criar as tecnologias e conhecimentos dos quais necessitam, em vez dos que você pensa que eles precisam. Se você gostaria de contribuir para este trabalho, [envie-nos um email](mailto:languages@whoseknowledges.org) com o assunto **STIL ACADEMIA ou STIL TECH**.

### Se você estiver na área de filantropia

Se você é um financiador, reconheça a importância de apoiar a produção, preservação e digitalização de materiais e da produção de conhecimento nos idiomas das comunidades que você apoia. Se você gostaria de contribuir para a nossa iniciativa, [envie um email](mailto:languages@whoseknowledges.org) com o assunto **STIL PHILANTHROPY**.

### Entre em contato conosco!

Nós somos um coletivo de três organizações: Whose Knowledge?, Oxford Internet Institute, e Centre for Internet and Society (Índia).

Se você gostaria de receber atualizações desta iniciativa e outros projetos liderados pela Whose Knowledge?, assine nossa newsletter [“The Internet We Want”](https://whoseknowledge.org/join/).


Se você gostaria de manter contato com as três organizações que realizaram este relatório, confira os links para nossos canais de redes sociais abaixo.

#### Twitter

[@WhoseKnowledge](https://twitter.com/whoseknowledge)

[@oiioxford](https://twitter.com/oiioxford)

[@cis_india](https://twitter.com/cis_india)

#### Facebook

[Whose Knowledge?](https://www.facebook.com/WhoseKnowledge/)

[Oxford Internet Institute, University of Oxford](https://www.facebook.com/oiioxford)

[Centre for Internet and Society - Access To Knowledge](https://www.facebook.com/cisa2k)
 
#### Instagram

[Whose Knowledge?](https://www.instagram.com/whoseknowledge/)

[oiioxford](https://www.instagram.com/oiioxford/)

[#centreforinternetandsociety](https://www.instagram.com/explore/tags/centreforinternetandsociety/)

#### Youtube

[Whose Knowledge?](https://www.youtube.com/channel/UCR97k4sWCKHxKVeI64xACSg)

[Oxford Internet Institute, University of Oxford](https://www.youtube.com/user/OIIOxford)

[Centre for Internet and Society](https://www.youtube.com/channel/UC0SLNXQo9XQGUE7Enujr9Ng)
