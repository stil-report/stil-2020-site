---
Title: আমাদের সাথে যোগ দাও!
date: 2020-01-01
tags: engage
summary: ""
---

এই প্রতিবেদনটি নিয়ে কাজ চলছে, এবং এটি ধারাবাহিক বিবর্তনের মধ্য দিয়ে যাচ্ছে। সেইসঙ্গে এটি আমাদের  চলতে থাকা গবেষণারও একটি উদ্যোগ।
 
আমরা চাই বিভিন্ন প্রজন্ম ও সম্প্রদায়ের মানুষ এই কাজে আমাদের সঙ্গী হোন, এবং আমরা আমাদের তথ্যগুলিকে আরো উন্নত ও সময়োপযোগী করে তুলতে তাদের অভিমত ও মন্তব্যকে সবসময়েই স্বাগত জানাই।
 
আপনারা কীভাবে এই উদ্যোগের সঙ্গে যুক্ত হতে পারেন তা দেখে নেওয়া যাক:

### আপনি যদি একজন অনুবাদক হন

আপনি যদি একজন অনুবাদক হন এবং যদি এই প্রতিবেদনটি আপনার ভাষায় মানুষের কাছে পৌঁছে দিতে চান, তাহলে নির্দ্বিধায় আমাদের সঙ্গে যোগাযোগ করুন। আমাদের [ই-মেইল পাঠান](mailto:languages@whoseknowledges.org) **NEW TRANSLATOR** -এই শিরোনামে, আমরা আপনার সঙ্গে যোগাযোগ করতে পারলে খুশি হব।

## যদি আপনি গণমাধ্যমের কর্মী হন

আপনি যদি সাংবাদিক বা গণসংযোগের সঙ্গে যুক্ত থাকেন, এবং আপনার যদি গণমাধ্যম সংক্রান্ত কোনো অনুরোধ থাকে, তবে **MEDIA/NAME OF PUBLICATION** -শিরোনামে উল্লেখ করে নির্দ্বিধায় [আমাদের সঙ্গে ই-মেইলে যোগাযোগ করুন](mailto:languages@whoseknowledges.org), সেইসঙ্গে আপনার প্রকাশনার ভাষা এবং সময়সীমা জানাতে ভুলবেন না। এই প্রতিবেদনের মূল তথ্যগুলি চটজলদি জানার জন্য আমরা একটি সংক্ষিপ্ত তথ্যতালিকা তৈরি করেছি, যা আপনি [এখানে](/media/pdf/FACTSHEET_State_of_the_Internets_Languages_Report.pdf) পাবেন।

## আপনি যদি কোনো প্রান্তিক গোষ্ঠীর মানুষ হন

আপনি যদি কোনো প্রান্তিক গোষ্ঠীর মানুষ হন, মনে রাখবেন আপনি একা নন, এবং আপনাদের জ্ঞান আপনাদের নিজেদের মতন করে নিজেদের ভাষায় সকলের সঙ্গে ভাগ করে নেওয়া - এটি আপনাদের গোষ্ঠীর অধিকার। আপনি কি চান এমন অন্যান্য মানুষদের সঙ্গে যুক্ত হতে, যারা জ্ঞান ও সুবিচার নিয়ে কাজ করছেন? তবে [আমাদের সঙ্গে ইমেইলে যোগাযোগ করুন](mailto:languages@whoseknowledges.org)। 

## আপনি যদি প্রযুক্তি বা শিক্ষাসংক্রান্ত কাজের সঙ্গে যুক্ত থাকেন

আপনি যদি প্রযুক্তি বা শিক্ষাসংক্রান্ত কাজের সঙ্গে যুক্ত থাকেন, তাহলে সচেতনভাবে প্রান্তিক ভাষাগোষ্ঠীগুলির সঙ্গে সংযোগ স্থাপন করুন- তাদের কাজ পড়ুন, শুনুন এবং যথাযথভাবে উদ্ধৃত করুন -এবং এইভাবে তাদের জ্ঞানকে অন্তর্ভুক্ত করার মধ্য দিয়ে আপনার পঠনপাঠন ও কাজের পরিধি বাড়িয়ে তুলুন। আপনার মতে প্রান্তিক ভাষাগোষ্ঠীগুলির কী কী দরকার তা-না করে, তারা নিজেরা যা যা প্রয়োজনীয় বলে মনে করেন, সেইসব প্রযুক্তি ও জ্ঞান সৃষ্টি করতে তাদের সঙ্গে সম্মিলিতভাবে কাজ করুন। যদি আপনি এই কাজে অংশগ্রহণ করতে চান, তাহলে  **STIL ACADEMIA** অথবা **STIL TECH** এই শিরোনামে [আমাদের ইমেইল করুন](mailto:languages@whoseknowledges.org)।


## যদি আপনি মানবকল্যাণকর কাজের সঙ্গে জড়িত থাকেন

আপনি যদি অনুদানকারী হন, তাহলে যে জনগোষ্ঠীগুলির সেবা করেন, তাদের ভাষায় জ্ঞান সৃষ্টি, তথ্য উৎপাদন, সংরক্ষণ ও ডিজিটাইজেশনের গুরুত্ব অনুভব করুন, সেইসঙ্গে এই প্রকল্পের পাশে দাঁড়ান। আপনি যদি আমাদেরকে অনুদান দিয়ে সাহায্য করতে চান, তাহলে [আমাদেরকে ই-মেইল করুন](mailto:languages@whoseknowledges.org) **STIL PHILANTHROPY** শিরোনাম সহ।

## আমাদের সঙ্গে থাকুন!

আমরা তিনটি সংস্থার একটি সম্মিলিত উদ্যোগ: হুজ নলেজ?, দ্য অক্সফোর্ড ইন্টারনেট ইন্সটিটিউট, এবং দ্য সেন্টার ফর ইন্টারনেট অ্যান্ড সোসাইটি (ভারত)।

আপনি যদি আমাদের এই উদ্যোগ এবং হুজ নলেজ?-এর অন্যান্য প্রকল্প সম্পর্কে নিত্যনতুন তথ্য পেতে চান তাহলে আমাদের [“দ্য ইন্টারনেট উই ওয়ান্ট” -শীর্ষক নিউজলেটারের জন্য আপনার নাম নথিভুক্ত করুন](https://whoseknowledge.org/join/)।

যদি আপনি এই প্রতিবেদন প্রস্তুতকারী তিনটি সংস্থার সঙ্গে সংযোগ রাখতে চান, তাহলে নিম্নে প্রদত্ত সমাজমাধ্যমের লিংকগুলি ব্যবহার করুন। 

#### Twitter

[@WhoseKnowledge](https://twitter.com/whoseknowledge)

[@oiioxford](https://twitter.com/oiioxford)

[@cis_india](https://twitter.com/cis_india)

#### Facebook

[Whose Knowledge?](https://www.facebook.com/WhoseKnowledge/)

[Oxford Internet Institute, University of Oxford](https://www.facebook.com/oiioxford)

[Centre for Internet and Society - Access To Knowledge](https://www.facebook.com/cisa2k)
 
#### Instagram

[Whose Knowledge?](https://www.instagram.com/whoseknowledge/)

[oiioxford](https://www.instagram.com/oiioxford/)

[#centreforinternetandsociety](https://www.instagram.com/explore/tags/centreforinternetandsociety/)

#### Youtube

[Whose Knowledge?](https://www.youtube.com/channel/UCR97k4sWCKHxKVeI64xACSg)

[Oxford Internet Institute, University of Oxford](https://www.youtube.com/user/OIIOxford)

[Centre for Internet and Society](https://www.youtube.com/channel/UC0SLNXQo9XQGUE7Enujr9Ng)
