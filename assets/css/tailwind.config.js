module.exports = {
  theme: {  
    fontWeight: {
      hairline: 100,
      'extra-light': 100,
      thin: 200,
      light: 300,
      normal: 400,
      medium: 500,
      semibold: 600,
      bold: 700,
      extrabold: 800,
      'extra-bold': 800,
      black: 900,
    }, /*maybe remove this*/

    flex: {
        '1': '1 1 0%',
        auto: '1 1 auto',
        initial: '0 1 auto',
        inherit: 'inherit',
        none: 'none',
        '100': '1 1 100%',
    },
    fontFamily: {
        'sans': ['Jost', 'sans-serif'],
        'mono': [ 'Anonymous Pro', 'monospace'],
        'serif': ['Spectral', 'serif'],
    },
    extend: {
      screens: {
        '3xl': '1920px',
      },

      typography: {
        DEFAULT: {
          css: {
            /*img: {
              'marginTop': 100,
              'marginBottom': 100,
            }, I DON'T KNOW WHAT IS THIS, SO I'M COMMENTING IT*/
            a: {
              color: '#0F172A',
              'text-decoration-thickness': '1px',
              'text-underline-offset': 8,
              'text-decoration-color': '#029caf',
              'font-weight': 400,
              '&:hover': {
                color: '#029caf',
              },
            },
            '.footnote-ref': {
              'margin-right':'1px',
              'margin-left':'1px',
              'font-weight': 700,
              'text-decoration': 'none',
              '&:hover': {
                color: '#029caf',
              },
            },
            h2: {
              color: '#111827',
              'font-family': 'Jost',
              'font-size': '1.9rem',
              'font-weight': 600,
            },
            h3: {
              color: '#111827',
              'font-family': 'Jost',
              'font-size': '1.7rem',
              'font-weight': 500,
              'line-height': '2.2rem',              
            },
            h4: {
              color: '#111827',
              'font-family': 'Jost',
              'font-size': '1.5rem',
              'font-weight': 500,
              'line-height': '2.2rem',              
            },
            h5: {
              color: '#111827',
              'font-family': 'Jost',
              'font-size': '1.2rem',
              'font-weight': 500,
              'line-height': '2.2rem',              
            },
            p: {
              'font-family': 'Spectral',
              'font-size': '1.35rem',
              '&::selection': {
                background: '#CBD5E1',  
              },
            },
            blockquote: {
              color: '#029caf',
              'font-family': 'Spectral',
              'marginBottom': 5,
              'border-left-color': '#029caf',
              'padding-left': 30,
              '@media (min-width: 1024px)': {
                'padding-left': 70,
              },
            },
            figure: {
              'font-family': 'Jost',
              figcaption: {
                color: '#718096',
                'font-family': 'Spectral',
                'font-style': 'italic',
              },
            },
            ul: {
              'font-family': 'Jost',
              'letter-spacing': '0.005em',
              li:{
                'font-size': '1.2rem',
                'z-index': 0,
                'letter-spacing': '0.005em',
              },
            },
            'p.interview': {
              'font-family': 'Jost',
              'margin-top': 8,
              'margin-bottom': 8,
            },
          },
        }, /* default */
      }, /* typography */
      zIndex: {
        '-10': '-10',
        '-20': '-20',
        '-30': '-30',
        '-40': '-40',
        '-50': '-50',
      },
      colors: {
        orange: {
          '100': '#fef9f8',
          '200': '#fcefec',
          '300': '#fae5e0',
          '400': '#f8ddd6',
          '500': '#f7d4cb',
          '600': '#f5c9bf',
          '700': '#f3c0b5',
          '800': '#f2b8ab',
          '900': '#f0b0a1',
        },
        green: {
          '100': '#e6f9f5',
          '200': '#cbf2ea',
          '300': '#aeebde',
          '400': '#91e3d3',
          '500': '#73dcc6',
          '600': '#53d4ba',
          '700': '#38cdaf',
          '800': '#18c5a2',
          '900': '#00b2a4',
        },
        blue: {
          '100': '#e8f6f8',
          '200': '#ccebef',
          '300': '#b3e1e7',
          '400': '#90d4dc',
          '500': '#74c9d3',
          '600': '#54bcc9',
          '700': '#38b1c0',
          '800': '#1aa5b7',
          '900': '#029caf',
        },
        purple: {
          '100': '#f1ecfe',
          '200': '#ddd2ed',
          '300': '#c8b7de',
          '400': '#b29acb',
          '500': '#9f81b7',
          '600': '#8a66a2',
          '700': '#764b8e',
          '800': '#792a8f',
          '900': '#521c68',
        },
        pink: {
          '100': '#f8edf3',
          '200': '#ecd0e2',
          '300': '#e2b6d1',
          '400': '#d596bd',
          '500': '#cb7bad',
          '600': '#c1619c',
          '700': '#b6468b',
          '800': '#ab2b7a',
          '900': '#a2146c',
        }
      },
      spacing: {
        '1/2': '50%',
        '1/3': '33.333333%',
        '2/3': '66.666667%',
        '1/4': '25%',
        '2/4': '50%',
        '3/4': '75%',
        '1/5': '20%',
        '2/5': '40%',
        '3/5': '60%',
        '4/5': '80%',
        '1/6': '16.666667%',
        '2/6': '33.333333%',
        '3/6': '50%',
        '4/6': '66.666667%',
        '5/6': '83.333333%',
        '1/12': '8.333333%',
        '2/12': '16.666667%',
        '3/12': '25%',
        '4/12': '33.333333%',
        '5/12': '41.666667%',
        '6/12': '50%',
        '7/12': '58.333333%',
        '8/12': '66.666667%',
        '9/12': '75%',
        '10/12': '83.333333%',
        '11/12': '91.666667%',
        full: '100%',
        screen: '100vw',
      } /* spacing */
    }, /* extend */
  }, /* close theme */
  variants: {
    backgroundColor: ['responsive', 'hover', 'focus', 'active'],
    textDecoration: ['hover', 'active'],
  },
  plugins: [
    require('@tailwindcss/typography'),
  ]
}